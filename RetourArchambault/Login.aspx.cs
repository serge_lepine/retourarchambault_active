﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;


namespace RetourArchambault
{
    public partial class Login : BasePage
    {
        public string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            Language lang = new Language(System.Threading.Thread.CurrentThread.CurrentCulture.Name);
            Lang = lang.getLanguage();
           //Vide la variable session
            Session.RemoveAll();
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

           
            Usager usager = new Usager(Tool.formatText(txtUser.Text),Tool.formatText(txtPassword.Text));
            Usr u = new Usr();

            VerificationUsager vu = new VerificationUsager(usager);

            u = vu.verificationUsager();

                if (!u.erreurMessage.Erreur)
                {

                    Session["USR"] = u;

                    if (u.is_amem)
                    {
                        if(u.is_admin)
                        {
                            Server.Transfer("AchatAdmin/dashboard_v2.aspx?Temp=0&usr=" + u.id + "&cie=" + u.idCie);
                        }
                        else
                        {
                            Server.Transfer("Achat/Amem.aspx?Temp=0&usr=" + u.id + "&cie=" + u.idCie);
                        }
                      
                        
                    }
                    else if (u.is_public)
                    {
                        Server.Transfer("Fournisseur/Fournisseurs.aspx?usr=" + u.id + "&cie=" + u.idCie);
                        
                    }
                    else if (u.is_store)
                    {
                        Server.Transfer("Succursales/Magasin.aspx?usr=" + u.id + "&cie=" + u.idCie);
                    }
                  
                }
              
                else
                {
                    
                    //Retourner popup
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "scripts", "<script>alert('" + Tool.getMessage("LoginErreur_" + Lang.ToUpper() + ".txt") + "')</script>", false);
                }

          
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
           
        }

        protected void lnkLang_Click(object sender, EventArgs e)
        {
            //Si premier log alors francais vers anglais
            if (Session["lang"] == null)
            {
                Session["lang"] = "en-CA";
                Response.Redirect("Login.aspx");

            }
            else
            {
                string lang = Convert.ToString(Session["lang"]).Substring(0, 2);

                switch (lang)
                {
                    case "en":
                        //Mettre les valeurs de anglais vers francais
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    case "fr":
                        Session["lang"] = "en-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    default:
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;

                }


            }
        }
    }
}
