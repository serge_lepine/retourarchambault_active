﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="RetourArchambault.Login" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LOGIN Archambault</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="css/default.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body class="t02 tLogin">
    <form id="form1" method="post" runat="server" class="frmAlt shortForm"> 
    <div id="wrapper">
        <div id="header">
            <img src="images/lg_archambault01White.png" />
                <div class="languageBtn">
                    <asp:LinkButton ID="lnkLang" OnClick="lnkLang_Click" meta:resourcekey="lnkLang" runat="server">English</asp:LinkButton>
                </div>
        </div>
        <div class="wrapMain">
            <div class="subWrap main">
                <div id="content">
                    <asp:Label ID="lblWelcome" runat="server" Text="&lt;h1&gt;Connexion&lt;/h1&gt;" meta:resourcekey="lblWelcomeResource1"></asp:Label> 				
                    <div class="refLine">
                        <div class="intro">
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Literal1Resource1" Text="&lt;p&gt;Entrez votre identifiant et le mot de passe liés à votre compte.&lt;/p&gt;"></asp:Literal> 
                        </div> 
                        <div class="frmInnerWrap">
                            <div class="frmElmnt fTxt">
                                <div class="fLbl">
                                    <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1">Identifiant</asp:Label>
                                </div>
                                <div class="fWdgt">
                                    <asp:TextBox ID="txtUser" CssClass="placeholderJs"  runat="server" meta:resourcekey="txtUserResource1"></asp:TextBox>
                                    <asp:RequiredFieldValidator CssClass="errorMsg"  ID="RequiredFieldValidator1" ControlToValidate="txtUser"  runat="server" ErrorMessage="Entrer votre nom d'usager" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                                <div class="clearer"></div>
                            </div>
                            <div class="frmElmnt fTxt">
                                <div class="fLbl"> 
                                    <asp:Label ID="Label2" runat="server" Text="Mot de passe" meta:resourcekey="Label2Resource1"></asp:Label>
                                </div> <%--<label for="element03">Mot de passe</label></div>--%>
                                <div class="fWdgt">
                                    <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="placeholderJs" runat="server" meta:resourcekey="txtPasswordResource1"></asp:TextBox>
                                     <asp:RequiredFieldValidator CssClass="errorMsg" ID="RequiredFieldValidator2" ControlToValidate="txtPassword" runat="server" ErrorMessage="Entrer votre mot de passe" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                </div>
                                <div class="clearer"></div>
                            </div>
                            <div class="frmElmnt fBtn">
                                <div class="fWdgt">
                                    <asp:Button ID="btnLogin" CssClass="btn primaryAction" runat="server" Text="Continuer" OnClick="btnLogin_Click" meta:resourcekey="btnLoginResource1" />
                                <%--  <asp:HyperLink ID="LinkMotPasseOublie" CssClass="secondaryAction" NavigateUrl="Recover.aspx" runat="server" meta:resourcekey="LinkMotPasseOublieResource1">Mot de passe oublié ?</asp:HyperLink>--%>
                                 
                                </div>
                                <div class="clearer"></div>
                            </div>
                        </div>                    
                    </div>
                </div>
            </div>
            <div class="extra01"><!-- --></div>
            <div class="extra02"></div>
            </div>
        <div id="footer">
            <div id="brand">
                &copy; Tous droits réservés. Groupe Archambault inc.1999-2014.
            </div>
        </div>
        
    </div>
    </form>
</body>
</html>
