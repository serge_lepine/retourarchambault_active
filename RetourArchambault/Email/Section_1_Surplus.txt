﻿Bonjour,

Un nouveau fichier des retours d'overstock est maintenant disponible pour traitement. 

•	Le fichier est disponible au https://retour.archambault.ca 
•	S’il y a des refus, veuillez les cocher dans la colonne « refus » prévu à cet effet
•	Cliquez sur envoyer afin d’inscrire vos numéros d’autorisation pour chaque magasin
•	Comme il s’agit de stock pas encore boxé, les # de retours sont « temporaires » 
•	Les magasins inscriront ce # de retour temporaire dans le champ « Commentaire » de leur documentation de retour lorsqu’ils vont l’expédier ainsi que sur chacune des boîtes.

Merci de votre collaboration.
