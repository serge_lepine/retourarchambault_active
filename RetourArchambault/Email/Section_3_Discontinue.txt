﻿Bonjour,

Voici le https://retour.archambault.ca afin de télécharger le PDF de cueillette de produits Discontinués {1} ({0}). (Dept 400 et/ou 420).

Veuillez entrer votre numéro de retour réel dans la case prévu à cet effet, en cliquant sur l’icône de la colonne « confimer ».

Nul besoin de nous communiquer votre # de retour temporaire ainsi que votre vrai # retour, le numéro d’RA sera affiché automatiquement lors de l’entrée du numéro de retour réel.

Un seul # de retour par magasin est permis.

Comme d’habitude, inclure les documents de retour dans les boîtes et inscrire sur chaque boîte :

• # de retour
• # de retour temporaire
• # d’autorisation
• Lot des boîtes (ex. 1de3, 2de3, 3de3)

Merci d'effectuer ce retour le plus rapidement possible en faisant attention à ne pas dépasser la date de limite de retour du moratoire. Sinon ces produits ne seront plus retournable après cette date.

Merci de votre collaboration.