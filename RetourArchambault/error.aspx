﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="RetourArchambault.error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Archambault</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="css/default.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="header">
        <img src="images/lg_archambault01White.png" />
    </div>
    <form id="form1" runat="server">

        <div id="wrapper">
            <div id="content">
                <div class="welcomeMessage">
                    <asp:Label ID="lblBonjour" runat="server" Text="Merci"></asp:Label>
                </div>
                <div class="container">
                    <h2 class="title">Désolé</h2>
                    <p>L'erreur suivante c'est produite pendant traitement.
                        <br />
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                        Merci d'utiliser Retour Archambault</p>
                    <div class="button_cont">
                        <asp:Button CssClass="btn primaryAction" ID="btnRetour" Visible="false" runat="server" Text="Faire une autre transaction" OnClick="btnRetour_Click" />
                        <asp:Button CssClass="btn secondaryAction" ID="btnQuitter" runat="server" Text="Quitter" OnClick="btnQuitter_Click" />
                    </div>
                </div><!--END /.container-->
            </div><!--END /.content-->
            <div id="footer">
                <div id="brand">&copy; Tous droits réservés. Groupe Archambault inc.</div>
            </div>
        </div><!--END /.wrapper-->
    </form>
</body>
</html>
