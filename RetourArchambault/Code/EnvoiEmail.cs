﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class EnvoiEmail
    {
        private string txtFrom = ConfigurationManager.AppSettings["mailFrom"];
        private string txtSubject = "";
        private string message = "";
        private string pathSubject = "";
        private string pathConfirm = "";
        private string retour = "";
        private Courriel courriel;
        private MailMessage mailobj;
        SmtpClient SMTPServer;
        MailAddress expediteur;
        private int id_destinataire;
        private int Section;
        private int id_provider;
        private string noRetour;
        private int noMag;

        public EnvoiEmail()
        {

        }
        public EnvoiEmail(string NoRetour, int id_Provider, int NoMag,string errorMessagbe)
        {
            //initialise variable
            this.noRetour = NoRetour;
            this.id_provider = id_Provider;
            this.noMag = NoMag;
            courriel = new Courriel();
            courriel = Connexion.getEmail(this.noMag, this.id_provider);
            this.message = String.Format(File.ReadAllText(HttpContext.Current.Server.MapPath("/Email/" + "TraitementRetour.txt")), this.noRetour, this.id_provider.ToString(), this.courriel.cieName, this.noMag.ToString());
            this.txtSubject = String.Format(File.ReadAllText(HttpContext.Current.Server.MapPath("/Email/" + "Subject_TraitementRetour.txt")), this.noRetour);
            //Envoi courriel
            sendMail();
        }
        public EnvoiEmail(int Id_provider, int Id_destinataire, int section, bool succes, string errorMessage,int TransactionType)
        {
           
            //Initialise le type de retour par nom
            setRetour(TransactionType);
            
            this.id_provider = Id_provider;
            this.Section = section;
            this.id_destinataire = Id_destinataire;

            //Si cie 978 modifier si section 3
            if(this.id_provider == 978 && this.Section == 3)
            {
                this.retour += "_978";
            }
            //Obtenir la liste des courriel
            this.courriel = Connexion.getEmail(Id_destinataire, Id_provider);
            
            setEmail();
       

            //Joindre le message si erreur
            if (!succes)
            {
                this.txtSubject = String.Format(txtSubject, this.id_provider.ToString(), this.courriel.cieName);
                this.message = String.Format(message, errorMessage);
            }

            sendMail();

        }
          
        public EnvoiEmail(string Lien, int id_Provider,int TransactionType)
        {
            
            setRetour(TransactionType);
            this.id_provider = id_Provider;
            
            Courriel adress = new Courriel();
            string query = Tool.getQuery("mailStore.sql");
            query = String.Format(query, Lien, this.id_provider);

            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd = new SqlCommand(query, con);
            SqlDataReader reader;

            try
            {
                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    adress.emailAdress += reader[0] + ";";

                }

                reader.NextResult();

                reader.Read();

                if (reader.HasRows)
                {
                    adress.cieName = reader[0].ToString();
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                log.logError("EnvoiEmail 105 :" + ex.Message);
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(con);
            }
            this.courriel = adress;
            this.Section = 3;
            if (this.id_provider == 978 && this.Section == 3) { this.retour += "_978"; }

            setEmail();
            sendMail();
        }
        void setEmail()
        {
            //Instancie variable 
           
            this.pathConfirm = String.Format("Section_{0}.txt", this.Section.ToString() + this.retour);
            this.pathSubject = String.Format("Subject_{0}.txt", this.Section.ToString() + this.retour);
            this.txtSubject = File.ReadAllText(HttpContext.Current.Server.MapPath("/Email/" + this.pathSubject));
            this.txtSubject = String.Format(txtSubject, this.id_provider.ToString(), this.courriel.cieName);
            this.message = String.Format(File.ReadAllText(HttpContext.Current.Server.MapPath("/Email/" + this.pathConfirm)),this.id_provider.ToString(),this.courriel.cieName);
        }
        protected void sendMail()
        {
            this.mailobj = new MailMessage();
            try
            {
                foreach (var adress in this.courriel.emailAdress.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    this.mailobj.To.Add(adress);
                }
                this.expediteur = new MailAddress(this.txtFrom);
                this.mailobj.Body = this.message;
                this.mailobj.Subject = this.txtSubject;
                this.mailobj.From = this.expediteur;
                this.SMTPServer = new SmtpClient();
                this.SMTPServer.UseDefaultCredentials = false;
                this.SMTPServer.Host = "courriel.archambault.ca";
                this.SMTPServer.Port = 25;
                this.SMTPServer.EnableSsl = false;


                this.SMTPServer.Send(mailobj);
                //Conserver une trace des email dans BD
                log.Log("Courriel succes à : " + this.id_destinataire.ToString());

            }
            catch (Exception ex)
            {
                log.logError("Problème envoi courriel : " + ex.ToString());
            }
        }
        void setRetour(int TransactionType)
        {
           switch (TransactionType)
           {
               case 0:
                   this.retour = "";
                   break;
               case 1:
                   this.retour = "_Surplus";
                   break;
               case 2:
                   this.retour = "_Discontinue";
                   break;
               case 3:
                   this.retour = "_Defectueux";
                   break;
               default:
                   break;
           }
        }
     
    }

}