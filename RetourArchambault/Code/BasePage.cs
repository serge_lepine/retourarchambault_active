﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Threading;

namespace RetourArchambault
{
    public class BasePage : System.Web.UI.Page
    {
        protected override void InitializeCulture()
        {

            if (Session["lang"] == null)
            {
              
                Session["lang"] = "fr-CA"; 
            }
            string lang = Convert.ToString(Session["lang"]).Substring(0, 2);
            string culture = string.Empty;
            if (lang.ToLower().CompareTo("en") == 0)
            {
                culture = "en-CA";
            }
            else
            {
                
                    culture = "fr-CA";
                
            }
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(culture);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(culture);

            base.InitializeCulture();

        }
    }
}