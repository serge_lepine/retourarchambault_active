﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;

namespace RetourArchambault
{
    public class XLS
    {
        private RetourXLS m_response;
        private string m_FilePath;
        private string m_getExcelSheetName;
        private string m_FileExt;
        private int count;
        private string m_lien;
        private OleDbConnection con;

        public XLS()
        {

        }
        public XLS(string FilePath,string Extension)
        {
            m_FilePath = FilePath;
            m_FileExt = Extension;
        }

        private OleDbConnection connect()
        {
            try
            {
                if (this.m_FileExt == ".xls")
                {
                    con = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + this.m_FilePath + ";Extended Properties=Excel 8.0;");

                }
                else if (this.m_FileExt == ".xlsx")
                {
                    con = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.m_FilePath + ";Extended Properties=Excel 12.0;");
                }
            }
            catch (Exception e)
            {
                log.logError("XLS OleDbConnection : " + e.Message);
            }
            
            return con;
        }
        public RetourXLS getUPC()
        {
            //Connexion à la feuille Excel
            connect();
            con.Open();
            if (con.State == ConnectionState.Open)
            {
                try
                {

                    //Obtien la liste des feuilles disponibles
                    DataTable dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    //Obtient la première feuille
                    this.m_getExcelSheetName = dt.Rows[0]["Table_Name"].ToString();
                    //Select rows from first sheet in excel sheet and fill into dataset
                    OleDbCommand ExcelCommand = new OleDbCommand(@"SELECT * FROM [" + this.m_getExcelSheetName + @"]", con);

                    OleDbDataReader reader;
                    this.m_response = new RetourXLS();
                    reader = ExcelCommand.ExecuteReader();
                    if (reader.HasRows)
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    if (!String.IsNullOrEmpty(reader[1].ToString()) || !String.IsNullOrEmpty(reader[2].ToString()))
                                    {

                                        this.m_response.UPC += reader[0].ToString() + ",";
                                        //Prendre uniquement le premier enregistrement du fichier
                                        if (count == 0)
                                        {
                                            this.m_response.NoCie = int.Parse(reader[1].ToString());
                                            this.m_response.Date_Limite = Tool.ConvertDateTime(reader[2].ToString());
                                            this.m_response.NoAvis = reader[3].ToString();
                                        }

                                        //Incrémente le compteur
                                        count++;
                                    }
                                }
                            }
                            //Supprimer la dernière virgule
                            this.m_response.UPC = Tool.Left(this.m_response.UPC, 1);
                        }
                        catch (Exception ex)
                        {
                            log.logError("XLS Impossible" + ex.Message + " StackTrace : " + ex.StackTrace);
                        }
                    }
                    reader.Close();
                    ExcelCommand.Dispose();
                }
                catch (Exception ex)
                {
                    log.logError("Impossible d'ouvrir la feuille Excel :" + ex.Message + " StackTrace : " + ex.StackTrace);
                }
                finally
                {

                    Connexion.close(con);

                }
            }

            return this.m_response;
        }

        /// <summary>
        /// Extrait les données de la feuille Excel pour traitement en lot
        /// </summary>
        /// <returns></returns>
        public RetourXLS getSku()
        {
            //Instancie reponse
            this.m_response = new RetourXLS();
            //Connexion à la feuille Excel
            connect();
            con.Open();
            if (con.State == ConnectionState.Open)
            {
                try
                {

                    //Obtien la liste des feuilles disponibles
                    DataTable dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    //Obtient la première feuille
                    this.m_getExcelSheetName = dt.Rows[0]["Table_Name"].ToString();
                    //Select rows from first sheet in excel sheet and fill into dataset
                    OleDbCommand ExcelCommand = new OleDbCommand(@"SELECT * FROM [" + this.m_getExcelSheetName + @"]", con);

                    OleDbDataReader reader;
                    this.m_response = new RetourXLS();
                    reader = ExcelCommand.ExecuteReader();
                    if (reader.HasRows)
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                if (reader.HasRows)
                                {
                                    if (!String.IsNullOrEmpty(reader[1].ToString()) || !String.IsNullOrEmpty(reader[2].ToString()))
                                    {
                                        
                                        this.m_response.UPC += reader[0].ToString() + ",";
                                        //Prendre uniquement le premier enregistrement du fichier
                                        if (count == 0)
                                        {
                                            this.m_response.NoCie = int.Parse(reader[1].ToString());
                                           
                                        }

                                        //Incrémente le compteur
                                        count++;
                                    }
                                }
                            }
                            //Supprimer la dernière virgule
                            this.m_response.UPC = Tool.Left(this.m_response.UPC, 1);
                            //Ajouter 14 jours à la date du jour
                            this.m_response.Date_Limite = DateTime.Now;
                            this.m_response.Date_Limite = this.m_response.Date_Limite.AddDays(14);
                            this.m_response.Lien = Tool.creerLien(this.m_response.NoCie, 1, 999);
                            this.m_response.NoAvis = "";
                        }
                        catch (Exception ex)
                        {
                            log.logError("XLS Impossible" + ex.Message + " StackTrace : " + ex.StackTrace);
                        }
                    }
                    reader.Close();
                    ExcelCommand.Dispose();
                }
                catch (Exception ex)
                {
                    log.logError("Impossible d'ouvrir la feuille Excel :" + ex.Message + " StackTrace : " + ex.StackTrace);
                }
                finally
                {

                    Connexion.close(con);

                }
            }

          
            return this.m_response;
        }

        public string setQuery()
        {
            string query = Tool.getQuery("des_Upload.sql");
            setLien();
            query = String.Format(query, this.m_response.UPC, this.m_response.NoCie, this.m_lien, this.m_response.Date_Limite,this.m_response.NoAvis);
            log.Log(query);

            return query;
        }

        private void setLien()
        {
            this.m_lien = Tool.creerLien(this.m_response.NoCie, 2, 500);
        }

        public string getLien()
        {
            return this.m_lien;
        }
        public DateTime getDate_Limite()
        {
            return this.m_response.Date_Limite;
        }
    }
   
}