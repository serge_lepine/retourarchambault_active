﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class InsertDefectNumber
    {
        private string querySelect;
        private string queryUpdate;
        private string TableName;
        private string NoRetour;
        private string noRetour;
        private string SKU;
        private int Qty;
        private int NoMag;
        private DateTime newDate;

        private OleDbConnection conn400;
        private OleDbCommand cmd400;

        private SqlConnection connSQL;
        private SqlCommand cmdSQL;
        private SqlDataReader reader;

        public InsertDefectNumber()
        {

        }
        public InsertDefectNumber(string tableName)
        {
            this.TableName = tableName;
            setquerySelect();
            setqueryUpdate();
        }
        public bool setDefectNumber()
        {
            int i = 0;
            this.conn400 = Connexion.Connect();
            this.connSQL = Connexion.ConnectSQL();

            this.cmdSQL = new SqlCommand(querySelect, connSQL);

            this.reader = cmdSQL.ExecuteReader();
            try
            {
                while (this.reader.Read())
                {
                    if (this.reader.HasRows)
                    {
                        this.NoRetour = reader[0].ToString();
                        if(i == 0)
                        {
                            this.noRetour = this.NoRetour;
                            this.NoMag = int.Parse(reader[4].ToString());
                        }
                        this.SKU = reader[1].ToString();
                        this.Qty = int.Parse(reader[2].ToString());
                        this.newDate = reader.GetDateTime(3);

                        //Insert dans AS400
                        this.queryUpdate = String.Format(this.queryUpdate, this.NoRetour, this.SKU, this.Qty, this.newDate);
                        log.LogQuery("InsertDefectNumber AS400 : " + this.queryUpdate);
                        this.cmd400 = new OleDbCommand(this.queryUpdate, conn400);
                        this.cmd400.ExecuteNonQuery();
                        this.cmd400.Dispose();
                        initialise();
                    }
                }
            }
            catch (Exception e)
            {
                log.logError("InsertDefectNumber : " + e.Message);
                return false;
            }
            finally
            {
                this.reader.Close();
                this.cmdSQL.Dispose();
                this.cmd400.Dispose();
                Connexion.closeSQL(this.connSQL);
                Connexion.close(this.conn400);
            }
            return true;
        }
        private void setquerySelect()
        {
            this.querySelect = Tool.getQuery("SelectDefectNumber.sql");
            this.querySelect = String.Format(this.querySelect,this.TableName);
            log.LogQuery("InsertDefectNumber SQL : " + this.querySelect);
        }
        private void setqueryUpdate()
        {
            this.queryUpdate = Tool.getQuery("CreateDefectTransaction.sql");
        }
        private void initialise()
        {
            this.NoRetour = "";
            this.SKU = "";
            this.Qty = 0;
            this.newDate = new DateTime();
            this.setqueryUpdate();
        }
        public string getNoRetour()
        {
            return this.noRetour;
        }
        public int getNoMag()
        {
            return this.NoMag;
        }
       
    }
}