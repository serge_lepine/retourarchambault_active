﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class Usager
    {
        public string NomUsager { get; set; }
        public string MotDePasse { get; set; }

        public Usager()
        {

        }
        public Usager(string usager,string motDePasse)
        {
            NomUsager = usager;
            MotDePasse = motDePasse;
        }
    }
}