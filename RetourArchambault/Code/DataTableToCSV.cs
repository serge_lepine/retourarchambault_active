﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Text;
using System.Data;
using System.Data.Common;

/// <summary>
/// Description résumée de DataTableToCSV
/// </summary>
public  class DataTableToCSV
{
    private string m_newPathCSV;
    private string m_fileOut;
    private string m_pathName;
    private string m_separateur;
    private DataTableReader m_dtr;
    private DataTable dtSave;

    public DataTableToCSV()
	{
        initialise();
	}
    private void initialise()
    {
        m_separateur = ",";
    }
    /// <summary>
    /// Exporte les données du DataTable au CSV de Amem vers Provider
    /// </summary>
    /// <param name="fileName">Nom de création du fichier sans extension</param>
    /// <param name="reader">DataTable qui contient les données</param>
    /// <returns>Nom et chemin complet du fichier</returns>
    //public string dataTableToCSV(string fileName, DataTable reader,int step)
    //{
    //     //Crée le nouveau fichier du fournisseur 
    //    this.m_fileOut = Tool.fileNameCreateNewProvider(fileName,step);
       
    //    //Initialise le dataReader
    //    dtSave = reader;
    //    //Instancie le Reader
    //    m_dtr = new DataTableReader(dtSave);
    //    //Crée le csv
    //    exportToCSVfile();
    //    //Retourne le nom complet du fichier
    //    return m_newPathCSV;
    //}
    /// <summary>
    /// Exporte les données du DataTable au CSV de Amem vers Provider
    /// </summary>
    /// <param name="fileName">Nom de création du fichier sans extension</param>
    /// <param name="reader">DataTable qui contient les données</param>
    /// <returns>Nom et chemin complet du fichier</returns>
    //public string dataTableToCSVProvider(string fileName, DataTable reader, int step)
    //{
    //    //Crée le nouveau fichier du fournisseur 
    //    this.m_fileOut = Tool.fileNameCreateRetourAmem(fileName, step);

    //    //Initialise le dataReader
    //    dtSave = reader;
    //    //Instancie le Reader
    //    m_dtr = new DataTableReader(dtSave);
    //    //Crée le csv
    //    exportToCSVfile();
    //    //Retourne le nom complet du fichier
    //    return m_newPathCSV;
    //}
    private void exportToCSVfile()
    {

        // Recherche le schema de la table.
        DataTable dtSchema = new System.Data.DataTable();
        this.m_dtr.GetSchemaTable();

        // Crée le fichier CSV file dans un stream en forcant l'encodage en UTF8.
        StreamWriter sw = new StreamWriter(this.m_fileOut, false, Encoding.UTF8);

        string strRow = "";
        int x = 0;
        string[] columnNames = GetColumnNames().ToArray();
        foreach (string s in columnNames)
        {
            strRow += s;

            if (x < columnNames.Length - 1)
            {
                strRow += this.m_separateur;
            }
            x++;
        }
        x = 0;
        sw.WriteLine(strRow);
        try
        {
            while (this.m_dtr.Read())
            {
                strRow = "";
                for (int i = 0; i < this.m_dtr.FieldCount; i++)
                {
                    switch (Convert.ToString(this.m_dtr.GetFieldType(i)))
                    {
                        case "System.Int16":
                            strRow += Convert.ToString(this.m_dtr.GetInt16(i)).Replace(",", "''");
                            break;

                        case "System.Int32":
                            strRow += Convert.ToString(this.m_dtr.GetInt32(i)).Replace(",", "''");
                            break;

                        case "System.Int64":
                            strRow += Convert.ToString(this.m_dtr.GetInt64(i)).Replace(",", "''");
                            break;

                        case "System.Decimal":
                            strRow += Convert.ToString(this.m_dtr.GetDecimal(i)).Replace(",", "''");
                            break;

                        case "System.Double":
                            strRow += Convert.ToString(this.m_dtr.GetDouble(i)).Replace(",", "''");
                            break;

                        case "System.Float":
                            strRow += Convert.ToString(this.m_dtr.GetFloat(i)).Replace(",", "''");
                            break;

                        case "System.Guid":
                            strRow += Convert.ToString(this.m_dtr.GetGuid(i)).Replace(",", "''");
                            break;

                        case "System.String":
                            strRow += this.m_dtr.GetString(i).Replace(",", "''");
                            break;

                        case "System.Boolean":
                            strRow += Convert.ToString(this.m_dtr.GetBoolean(i)).Replace(",", "''");
                            break;

                        case "System.DateTime":
                            strRow += Convert.ToString(this.m_dtr.GetDateTime(i)).Replace(",", "''");
                            break;
                        default:
                            strRow += Convert.ToString(this.m_dtr.GetValue(i)).Replace(",", "''");
                            break;
                    }

                    if (i < m_dtr.FieldCount - 1)
                    {
                        strRow += this.m_separateur;
                    }
                }
                sw.WriteLine(strRow);
            }
        }
        catch (Exception csv)
        {
            log.logError(csv.Message);
        }
        finally
        {
            // Closes the text stream and the database connenction.
            sw.Close();
            this.m_dtr.Close();
        }


    }
    private IEnumerable<string> GetColumnNames()
    {
        foreach (DataRow row in m_dtr.GetSchemaTable().Rows)
        {

            yield return (string)row["ColumnName"];

        }
    }

}