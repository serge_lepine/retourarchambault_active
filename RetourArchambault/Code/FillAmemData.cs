﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using ConvertToCSV;

namespace RetourArchambault
{
    public static class FillAmemData
    {
        public static bool copyDefctProduct(string query, string TableName)
        {
            //Ouvre Connexion 400
            OleDbConnection con400 = Connexion.Connect();
            //Ouvre Connexion SQL
            SqlConnection conSQL = Connexion.ConnectSQL();
            log.LogQuery(query);
            var adapter400 = new OleDbDataAdapter(query, con400);

            var table = new DataTable();
            adapter400.AcceptChangesDuringFill = false;

            adapter400.Fill(table);


            SqlBulkCopy bulkCopy = new SqlBulkCopy(conSQL, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);
            bulkCopy.DestinationTableName = TableName;
            try
            {
                SqlBulkCopyColumnMapping mapNoRetour = new SqlBulkCopyColumnMapping("NoRetour", "NoRetour");
                bulkCopy.ColumnMappings.Add(mapNoRetour);

                SqlBulkCopyColumnMapping mapNoMag = new SqlBulkCopyColumnMapping("NoMag", "NoMag");
                bulkCopy.ColumnMappings.Add(mapNoMag);

                SqlBulkCopyColumnMapping mapSku = new SqlBulkCopyColumnMapping("Sku", "Sku");
                bulkCopy.ColumnMappings.Add(mapSku);

                SqlBulkCopyColumnMapping mapUPC = new SqlBulkCopyColumnMapping("UPC", "UPC");
                bulkCopy.ColumnMappings.Add(mapUPC);

                SqlBulkCopyColumnMapping mapNoCie = new SqlBulkCopyColumnMapping("NoCie", "NoCie");
                bulkCopy.ColumnMappings.Add(mapNoCie);

                SqlBulkCopyColumnMapping mapISBN = new SqlBulkCopyColumnMapping("ISBN", "ISBN");
                bulkCopy.ColumnMappings.Add(mapISBN);

                SqlBulkCopyColumnMapping mapAuteur = new SqlBulkCopyColumnMapping("Auteur", "Auteur");
                bulkCopy.ColumnMappings.Add(mapAuteur);

                SqlBulkCopyColumnMapping mapTitre = new SqlBulkCopyColumnMapping("Titre", "Titre");
                bulkCopy.ColumnMappings.Add(mapTitre);

                SqlBulkCopyColumnMapping mapCompos = new SqlBulkCopyColumnMapping("Compos", "Compos");
                bulkCopy.ColumnMappings.Add(mapCompos);

                SqlBulkCopyColumnMapping mapFormat = new SqlBulkCopyColumnMapping("Format", "Format");
                bulkCopy.ColumnMappings.Add(mapFormat);

                SqlBulkCopyColumnMapping mapEtiquette = new SqlBulkCopyColumnMapping("Etiquette", "Etiquette");
                bulkCopy.ColumnMappings.Add(mapEtiquette);

                SqlBulkCopyColumnMapping mapModel_Calcule = new SqlBulkCopyColumnMapping("Model_Calcule", "Model_Calcule");
                bulkCopy.ColumnMappings.Add(mapModel_Calcule);

                SqlBulkCopyColumnMapping mapCOST = new SqlBulkCopyColumnMapping("COST", "COST");
                bulkCopy.ColumnMappings.Add(mapCOST);

                SqlBulkCopyColumnMapping mapQty_a_Retourner = new SqlBulkCopyColumnMapping("Qty_a_Retourner", "Qty_a_Retourner");
                bulkCopy.ColumnMappings.Add(mapQty_a_Retourner);

                SqlBulkCopyColumnMapping mapMontant_Retour = new SqlBulkCopyColumnMapping("Montant_Retour", "Montant_Retour");
                bulkCopy.ColumnMappings.Add(mapMontant_Retour);

                SqlBulkCopyColumnMapping mapCategory = new SqlBulkCopyColumnMapping("Category", "Category");
                bulkCopy.ColumnMappings.Add(mapCategory);

                SqlBulkCopyColumnMapping mapDept = new SqlBulkCopyColumnMapping("Dept", "Dept");
                bulkCopy.ColumnMappings.Add(mapDept);

                SqlBulkCopyColumnMapping mapDate_Creation = new SqlBulkCopyColumnMapping("Date_Creation", "Date_Creation");
                bulkCopy.ColumnMappings.Add(mapDate_Creation);

                SqlBulkCopyColumnMapping mapDate_Modification = new SqlBulkCopyColumnMapping("Date_Modification", "Date_Modification");
                bulkCopy.ColumnMappings.Add(mapDate_Modification);


                bulkCopy.WriteToServer(table);
            }
            catch (Exception ex)
            {
                log.logError("copyDefectueux " + ex.Message);
                return false;
            }
            finally
            {
                bulkCopy.Close();
                Connexion.closeSQL(conSQL);
                Connexion.close(con400);

            }

            return true;
        }
        public static bool copyAddProduct(string query, string TableName)
        {
            //Ouvre Connexion 400
            OleDbConnection con400 = Connexion.Connect();
            //Ouvre Connexion SQL
            SqlConnection conSQL = Connexion.ConnectSQL();

            //  Tool.setThread_EN();
            OleDbDataAdapter adapter400 = new OleDbDataAdapter(query, con400);

            var table = new DataTable();
            adapter400.AcceptChangesDuringFill = false;

            adapter400.Fill(table);


            SqlBulkCopy bulkCopy = new SqlBulkCopy(conSQL, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);
            bulkCopy.DestinationTableName = TableName;
            try
            {
                SqlBulkCopyColumnMapping mapNoMag = new SqlBulkCopyColumnMapping("NoMag", "NoMag");
                bulkCopy.ColumnMappings.Add(mapNoMag);

                SqlBulkCopyColumnMapping mapSku = new SqlBulkCopyColumnMapping("SKU", "Sku");
                bulkCopy.ColumnMappings.Add(mapSku);

                SqlBulkCopyColumnMapping mapNoCie = new SqlBulkCopyColumnMapping("NoCie", "NoCie");
                bulkCopy.ColumnMappings.Add(mapNoCie);

                SqlBulkCopyColumnMapping mapISBN = new SqlBulkCopyColumnMapping("ISBN", "ISBN");
                bulkCopy.ColumnMappings.Add(mapISBN);

                SqlBulkCopyColumnMapping mapUPC = new SqlBulkCopyColumnMapping("UPC", "UPC");
                bulkCopy.ColumnMappings.Add(mapUPC);

                SqlBulkCopyColumnMapping mapAUTEUR = new SqlBulkCopyColumnMapping("AUTEUR", "Auteur");
                bulkCopy.ColumnMappings.Add(mapAUTEUR);

                SqlBulkCopyColumnMapping mapTITRE = new SqlBulkCopyColumnMapping("TITRE", "Titre");
                bulkCopy.ColumnMappings.Add(mapTITRE);

                SqlBulkCopyColumnMapping mapCOMPOS = new SqlBulkCopyColumnMapping("COMPOS", "Compos");
                bulkCopy.ColumnMappings.Add(mapCOMPOS);

                SqlBulkCopyColumnMapping mapFORMAT = new SqlBulkCopyColumnMapping("FORMAT", "Format");
                bulkCopy.ColumnMappings.Add(mapFORMAT);

                SqlBulkCopyColumnMapping mapEtiquette = new SqlBulkCopyColumnMapping("Etiquette", "Etiquette");
                bulkCopy.ColumnMappings.Add(mapEtiquette);

                SqlBulkCopyColumnMapping mapModel_Calcule = new SqlBulkCopyColumnMapping("Model_Calcule", "Model_Calcule");
                bulkCopy.ColumnMappings.Add(mapModel_Calcule);

                SqlBulkCopyColumnMapping mapCOST = new SqlBulkCopyColumnMapping("COST", "COST");
                bulkCopy.ColumnMappings.Add(mapCOST);

                SqlBulkCopyColumnMapping mapQty_a_Retourner = new SqlBulkCopyColumnMapping("Qty_a_Retourner", "Qty_a_Retourner");
                bulkCopy.ColumnMappings.Add(mapQty_a_Retourner);

                SqlBulkCopyColumnMapping mapCategory = new SqlBulkCopyColumnMapping("Category", "Category");
                bulkCopy.ColumnMappings.Add(mapCategory);

                SqlBulkCopyColumnMapping mapENSTOCK = new SqlBulkCopyColumnMapping("EnStock", "EnStock");
                bulkCopy.ColumnMappings.Add(mapENSTOCK);

                SqlBulkCopyColumnMapping mapidTransaction = new SqlBulkCopyColumnMapping("idTransactionType", "idTransactionType");
                bulkCopy.ColumnMappings.Add(mapidTransaction);

                SqlBulkCopyColumnMapping mapDate = new SqlBulkCopyColumnMapping("Date_Limite", "Date_Limite");
                bulkCopy.ColumnMappings.Add(mapDate);

                SqlBulkCopyColumnMapping mapAdd_Sku = new SqlBulkCopyColumnMapping("Add_Sku", "Add_Sku");
                bulkCopy.ColumnMappings.Add(mapAdd_Sku);



                bulkCopy.WriteToServer(table);

            }
            catch (Exception ex)
            {
                log.logError("copyDesuetude " + ex.Message);
                return false;
            }
            finally
            {
                bulkCopy.Close();
                Connexion.closeSQL(conSQL);
                Connexion.close(con400);
                Tool.setThread_FR();
            }

            query = "";
            //Supprime les doublons
            query = Tool.getQuery("AddSkuDelDouble.sql");
            query = String.Format(query, TableName);
            log.Log(query);
            Connexion.execCommand(query);

            return true;


        }

        public static bool copyDesuetude(string query, string TableName)
        {
            //Ouvre Connexion 400
            OleDbConnection con400 = Connexion.Connect();
            //Ouvre Connexion SQL
            SqlConnection conSQL = Connexion.ConnectSQL();

            var adapter400 = new OleDbDataAdapter(query, con400);

            var table = new DataTable();
            adapter400.AcceptChangesDuringFill = false;

            adapter400.Fill(table);


            SqlBulkCopy bulkCopy = new SqlBulkCopy(conSQL, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);
            bulkCopy.DestinationTableName = TableName;
            try
            {
                SqlBulkCopyColumnMapping mapNoMag = new SqlBulkCopyColumnMapping("NoMag", "NoMag");
                bulkCopy.ColumnMappings.Add(mapNoMag);

                SqlBulkCopyColumnMapping mapSku = new SqlBulkCopyColumnMapping("SKU", "SKU");
                bulkCopy.ColumnMappings.Add(mapSku);

                SqlBulkCopyColumnMapping mapNoCie = new SqlBulkCopyColumnMapping("NoCie", "NoCie");
                bulkCopy.ColumnMappings.Add(mapNoCie);

                SqlBulkCopyColumnMapping mapISBN = new SqlBulkCopyColumnMapping("ISBN", "ISBN");
                bulkCopy.ColumnMappings.Add(mapISBN);

                SqlBulkCopyColumnMapping mapUPC = new SqlBulkCopyColumnMapping("UPC", "UPC");
                bulkCopy.ColumnMappings.Add(mapUPC);

                SqlBulkCopyColumnMapping mapAUTEUR = new SqlBulkCopyColumnMapping("AUTEUR", "AUTEUR");
                bulkCopy.ColumnMappings.Add(mapAUTEUR);

                SqlBulkCopyColumnMapping mapTITRE = new SqlBulkCopyColumnMapping("TITRE", "TITRE");
                bulkCopy.ColumnMappings.Add(mapTITRE);

                SqlBulkCopyColumnMapping mapCOMPOS = new SqlBulkCopyColumnMapping("COMPOS", "COMPOS");
                bulkCopy.ColumnMappings.Add(mapCOMPOS);

                SqlBulkCopyColumnMapping mapFORMAT = new SqlBulkCopyColumnMapping("FORMAT", "FORMAT");
                bulkCopy.ColumnMappings.Add(mapFORMAT);

                SqlBulkCopyColumnMapping mapEtiquette = new SqlBulkCopyColumnMapping("Etiquette", "Etiquette");
                bulkCopy.ColumnMappings.Add(mapEtiquette);

                SqlBulkCopyColumnMapping mapModel_Calcule = new SqlBulkCopyColumnMapping("Model_Calcule", "Model_Calcule");
                bulkCopy.ColumnMappings.Add(mapModel_Calcule);

                SqlBulkCopyColumnMapping mapCOST = new SqlBulkCopyColumnMapping("COST", "COST");
                bulkCopy.ColumnMappings.Add(mapCOST);

                SqlBulkCopyColumnMapping mapQty_a_Retourner = new SqlBulkCopyColumnMapping("Qty_a_Retourner", "Qty_a_Retourner");
                bulkCopy.ColumnMappings.Add(mapQty_a_Retourner);

                SqlBulkCopyColumnMapping mapCategory = new SqlBulkCopyColumnMapping("Category", "Category");
                bulkCopy.ColumnMappings.Add(mapCategory);

                SqlBulkCopyColumnMapping mapENSTOCK = new SqlBulkCopyColumnMapping("EnStock", "EnStock");
                bulkCopy.ColumnMappings.Add(mapENSTOCK);

                SqlBulkCopyColumnMapping mapDEPT = new SqlBulkCopyColumnMapping("DEPT", "IDEPT");
                bulkCopy.ColumnMappings.Add(mapDEPT);

                SqlBulkCopyColumnMapping mapIBALD2 = new SqlBulkCopyColumnMapping("IBALD2", "IBALD2");
                bulkCopy.ColumnMappings.Add(mapIBALD2);

                SqlBulkCopyColumnMapping mapidTransaction = new SqlBulkCopyColumnMapping("idTransactionType", "idTransactionType");
                bulkCopy.ColumnMappings.Add(mapidTransaction);

                SqlBulkCopyColumnMapping mapLien = new SqlBulkCopyColumnMapping("Lien", "Lien");
                bulkCopy.ColumnMappings.Add(mapLien);

                SqlBulkCopyColumnMapping mapDate = new SqlBulkCopyColumnMapping("Date_Limite", "Date_Limite");
                bulkCopy.ColumnMappings.Add(mapDate);

                SqlBulkCopyColumnMapping mapNoAvis = new SqlBulkCopyColumnMapping("NoAvis", "NoAvis");
                bulkCopy.ColumnMappings.Add(mapNoAvis);

                SqlBulkCopyColumnMapping mapAddSku = new SqlBulkCopyColumnMapping("Add_Sku", "Add_Sku");
                bulkCopy.ColumnMappings.Add(mapAddSku);


                bulkCopy.WriteToServer(table);
            }
            catch (Exception ex)
            {
                log.logError("copyDesuetude " + ex.Message);
                return false;
            }
            finally
            {
                bulkCopy.Close();
                Connexion.closeSQL(conSQL);
                Connexion.close(con400);

            }



            return true;


        }
        /// <summary>
        /// Transfert les données et renomme la table Temporaire
        /// </summary>
        /// <param name="query"></param>
        /// <param name="TableName"></param>
        /// <param name="newName"></param>
        /// <param name="OldName"></param>
        /// <returns></returns>
        public static bool copyTable(string query, string prefixTableName, string suffixName)
        {
            //Ouvre connexion SQL
            SqlConnection conSQL = Connexion.ConnectSQL();
            var adapterSQL = new SqlDataAdapter(query, conSQL);

            var table = new DataTable();
            adapterSQL.AcceptChangesDuringFill = false;

            adapterSQL.Fill(table);

            SqlBulkCopy bulkCopy = new SqlBulkCopy(conSQL, SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.UseInternalTransaction, null);
            bulkCopy.DestinationTableName = prefixTableName + suffixName;
            try
            {
                SqlBulkCopyColumnMapping mapNoMag = new SqlBulkCopyColumnMapping("NoMag", "NoMag");
                bulkCopy.ColumnMappings.Add(mapNoMag);

                SqlBulkCopyColumnMapping mapSku = new SqlBulkCopyColumnMapping("SKU", "SKU");
                bulkCopy.ColumnMappings.Add(mapSku);

                SqlBulkCopyColumnMapping mapNoCie = new SqlBulkCopyColumnMapping("NoCie", "NoCie");
                bulkCopy.ColumnMappings.Add(mapNoCie);

                SqlBulkCopyColumnMapping mapISBN = new SqlBulkCopyColumnMapping("ISBN", "ISBN");
                bulkCopy.ColumnMappings.Add(mapISBN);

                SqlBulkCopyColumnMapping mapUPC = new SqlBulkCopyColumnMapping("UPC", "UPC");
                bulkCopy.ColumnMappings.Add(mapUPC);

                SqlBulkCopyColumnMapping mapAUTEUR = new SqlBulkCopyColumnMapping("AUTEUR", "AUTEUR");
                bulkCopy.ColumnMappings.Add(mapAUTEUR);

                SqlBulkCopyColumnMapping mapTITRE = new SqlBulkCopyColumnMapping("TITRE", "TITRE");
                bulkCopy.ColumnMappings.Add(mapTITRE);

                SqlBulkCopyColumnMapping mapCOMPOS = new SqlBulkCopyColumnMapping("COMPOS", "COMPOS");
                bulkCopy.ColumnMappings.Add(mapCOMPOS);

                SqlBulkCopyColumnMapping mapFORMAT = new SqlBulkCopyColumnMapping("FORMAT", "FORMAT");
                bulkCopy.ColumnMappings.Add(mapFORMAT);

                SqlBulkCopyColumnMapping mapEtiquette = new SqlBulkCopyColumnMapping("Etiquette", "Etiquette");
                bulkCopy.ColumnMappings.Add(mapEtiquette);

                SqlBulkCopyColumnMapping mapModel_Calcule = new SqlBulkCopyColumnMapping("Model_Calcule", "Model_Calcule");
                bulkCopy.ColumnMappings.Add(mapModel_Calcule);

                SqlBulkCopyColumnMapping mapCOST = new SqlBulkCopyColumnMapping("COST", "COST");
                bulkCopy.ColumnMappings.Add(mapCOST);

                SqlBulkCopyColumnMapping mapQty_a_Retourner = new SqlBulkCopyColumnMapping("Qty_a_Retourner", "Qty_a_Retourner");
                bulkCopy.ColumnMappings.Add(mapQty_a_Retourner);

                SqlBulkCopyColumnMapping mapCategory = new SqlBulkCopyColumnMapping("Category", "Category");
                bulkCopy.ColumnMappings.Add(mapCategory);

                SqlBulkCopyColumnMapping mapENSTOCK = new SqlBulkCopyColumnMapping("EnStock", "EnStock");
                bulkCopy.ColumnMappings.Add(mapENSTOCK);

                SqlBulkCopyColumnMapping mapIdTransactionType = new SqlBulkCopyColumnMapping("idTransactionType", "idTransactionType");
                bulkCopy.ColumnMappings.Add(mapIdTransactionType);

                SqlBulkCopyColumnMapping mapDate_Limite = new SqlBulkCopyColumnMapping("Date_Limite", "Date_Limite");
                bulkCopy.ColumnMappings.Add(mapDate_Limite);



                bulkCopy.WriteToServer(table);
                return true;
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
                return false;
            }
            finally
            {
                bulkCopy.Close();
                Connexion.closeSQL(conSQL);
                //   Connexion.close(con400);
            }





        }
        /// <summary>
        /// Renomme la table temporaire
        /// </summary>
        /// <param name="newName"></param>
        /// <param name="OldName"></param>
        /// <returns></returns>
        public static bool rename(string newName, string OldName)
        {
            //Renomme la table
            RenameTable rt = new RenameTable(newName, OldName);

            return rt.renameTable();
        }
        /// <summary>
        /// Vide la table
        /// </summary>
        /// <param name="TableName">Nom de la table a vider</param>
        /// <param name="conSQL">Connexion</param>
        /// <returns>Vrai si OK</returns>
        public static bool deleteTableSQL(string TableName)
        {

            string query = "TRUNCATE TABLE " + TableName;
            log.Log(query);
            return Connexion.execCommand(query);
        }

    }
}