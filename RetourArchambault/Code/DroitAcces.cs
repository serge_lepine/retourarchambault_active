﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class DroitAcces
    {
        //private int cie;
        //private int usr;
        private Usr usager;

        public DroitAcces()
        {

        }
        public DroitAcces(Usr Usager)
        {
            usager = Usager;
        }

        public bool VerifAcces()
        {

            return getResponse();
        }

        private bool getResponse()
        {
            //Premierement vérifie les paramètres entrée
            if (this.usager.id != this.usager.idParam || this.usager.idCie != this.usager.idCieParam)
            {
                log.logError("Il y a une erreur entre l'usagé du login et l'url... Est ce que l'url a été modifié ?");
                return false;
            }
            //Deuxièmement vérifie si l'usager a les droits selon la provenance
            return getVerifDept();

        }
        private bool getVerifDept()
        {
            bool rep = false;

            switch (usager.index)
            {
                case 1:
                    if (usager.is_amem == true) rep = true;
                    break;
                case 2:
                    if (usager.is_public == true) rep = true;
                    break;
                case 3:
                    if (usager.is_store == true) rep = true;
                    break;
                default:
                    rep = false;
                    break;
            }
            return rep;
        }

    }
}