﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class LoadDefectueux
    {
        private string query;
        private string tableName;
        private string clearList;
        public LoadDefectueux()
        {
            setQuery();
            setTableName();
        }
        public bool TransfertData()
        {    
            //Vide la table DefectList
            clearDefectList();
            //Remplit nouvelle table
            return FillAmemData.copyDefctProduct(this.query,this.tableName);
        }
        private void setQuery()
        {
            this.query = Query.GetDefectueuxFrom400(); // Tool.getQuery("getDefectueuxFrom400.sql");
        }
        private void setTableName()
        {
            this.tableName = "DefectList";
        }
        private void clearDefectList()
        {
            this.clearList = Query.ClearDefectList(); // Tool.getQuery("ClearDefectList.sql");
            log.LogQuery("CreateNewProvider : " + this.clearList);
            Connexion.execCommand(clearList);
        }
    }
}