﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Web;
using ConvertToCSV;

/// <summary>
/// Description résumée de ExportToCSV
/// </summary>
public class ExportToCSV 
{
    private OleDbDataReader m_dr;
    private string m_fileOut;
    private string m_separateur;
    private string m_newPathCSV;
   
    public ExportToCSV()
    {
        initialise();
    }
    //public string exportToCSV(string fileName, OleDbDataReader reader)
    //{
    //    //Crée le nom du chemin
    //    this.m_fileOut = Tool.fileNameCreateNewAmem(fileName,1);
    //    //Initialise le dataReader
    //    m_dr = reader;
    //    //Crée le csv
    //    exportToCSVfile();
    //    //
    //    return m_newPathCSV;

    //}
   

    private void initialise()
    {
        this.m_dr = null;
        this.m_fileOut = "";
        this.m_separateur = ",";
        this.m_newPathCSV = "";
    }
    
    private void exportToCSVfile()
    {

        // Recherche le schema de la table.
        DataTable dtSchema = new System.Data.DataTable();
        this.m_dr.GetSchemaTable();

        // Crée le fichier CSV file dans un stream en forcant l'encodage en UTF8.
        StreamWriter sw = new StreamWriter(this.m_fileOut, false, Encoding.UTF8);

        string strRow = ""; 
        int x = 0;
        string[] columnNames = GetColumnNames().ToArray();
        foreach (string s in columnNames)
        {
            strRow += s;
           
                if (x < columnNames.Length - 1)
                {
                    strRow += this.m_separateur;
                }
                x++;
        }
        x = 0;
        sw.WriteLine(strRow);
        try
        {
            while (this.m_dr.Read())
            {
                strRow = "";
                for (int i = 0; i < this.m_dr.FieldCount; i++)
                {
                    switch (Convert.ToString(this.m_dr.GetFieldType(i)))
                    {
                        case "System.Int16":
                            strRow += Convert.ToString(this.m_dr.GetInt16(i)).Replace(",","''");
                            break;

                        case "System.Int32":
                            strRow += Convert.ToString(this.m_dr.GetInt32(i)).Replace(",", "''");
                            break;

                        case "System.Int64":
                            strRow += Convert.ToString(this.m_dr.GetInt64(i)).Replace(",", "''");
                            break;

                        case "System.Decimal":
                            strRow += Convert.ToString(this.m_dr.GetDecimal(i)).Replace(",", "''");
                            break;

                        case "System.Double":
                            strRow += Convert.ToString(this.m_dr.GetDouble(i)).Replace(",", "''");
                            break;

                        case "System.Float":
                            strRow += Convert.ToString(this.m_dr.GetFloat(i)).Replace(",", "''");
                            break;

                        case "System.Guid":
                            strRow += Convert.ToString(this.m_dr.GetGuid(i)).Replace(",", "''");
                            break;

                        case "System.String":
                            strRow += this.m_dr.GetString(i).Replace(",", "''");
                            break;

                        case "System.Boolean":
                            strRow += Convert.ToString(this.m_dr.GetBoolean(i)).Replace(",", "''");
                            break;

                        case "System.DateTime":
                            strRow += Convert.ToString(this.m_dr.GetDateTime(i)).Replace(",", "''");
                            break;
                         default:
                            strRow += Convert.ToString(this.m_dr.GetValue(i)).Replace(",","''");
                            break;
                    }

                    if (i < m_dr.FieldCount - 1)
                    {
                        strRow += this.m_separateur;
                    }
                }
                sw.WriteLine(strRow);
            }
        }
        catch (Exception csv)
        {
            log.logError(csv.Message);
        }
        finally
        {
            // Ferme le builder
            sw.Close();
            this.m_dr.Close();
        }


    }
    private IEnumerable<string> GetColumnNames()
    {
        foreach (DataRow row in m_dr.GetSchemaTable().Rows)
        {
            
            yield return (string)row["ColumnName"];

        }
    }



}