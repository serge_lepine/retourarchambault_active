﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Description résumée de Grid1
/// </summary>
public class Grid1
{
    public int txt_01_dept { get; set; }
    public int txt_02_mac { get; set; }
    public string txt_V2_modOuVente { get; set; }
    public string txt_V3 { get; set; }
    public string txt_O5_ExcSku { get; set; }
    public int txt_O6_EclProduit { get; set; }
    public string txt_S6_ActParam { get; set; }
    public int txt_O8_JourDernRec { get; set; }
    public int txt_O10_JourDernVente{ get; set; }
    public int txt_O12_JourEclPremRec { get; set; }
    public int txt_014_ExclQtyRetourPlus { get; set; }
    public int txt_015_ExclQtyRetourMoins{ get; set; }
   
	public Grid1()
	{
        initialise();
	}
    private void initialise()
    {
        txt_01_dept = 400;

    }
}