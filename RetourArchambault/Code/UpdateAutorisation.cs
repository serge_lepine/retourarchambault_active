﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;

namespace RetourArchambault
{
    public class UpdateAutorisation
    {
        private int qty;
        private double amount;
        private int NoMag;
        private int NoAuth400;
        private int NoProvider;
        private string Lien;
        SqlConnection conSQL;
        SqlCommand cmdSQL;
        OleDbConnection con400;
        OleDbCommand cmd400;

        public UpdateAutorisation()
        {

        }
        public UpdateAutorisation(string lien)
        {
            this.Lien = lien;
            try
            {
                //Ouvre connection SQL et 400
                this.conSQL = Connexion.ConnectSQL();
                this.con400 = Connexion.Connect();

                //Rechercher les transaction relié au numero de Lien
                //qui on un numero as400 et qty = null ou 0
                string query = Tool.getQuery("verifAutorisationSQL.sql");
                query = String.Format(query, this.Lien);
                cmdSQL = new SqlCommand(query, conSQL);

                if(Connexion.execScalar(query))
                {
                    //Boucle dans le resultat
                    SqlDataReader readerSQL = cmdSQL.ExecuteReader();
                    //Lire  chaque ligne du resultat et verifier si status = s
                    while (readerSQL.Read())
                    {
                        this.NoAuth400 = int.Parse(readerSQL[0].ToString());
                        this.NoMag = int.Parse(readerSQL[1].ToString());
                        this.NoProvider = int.Parse(readerSQL[2].ToString());

                        string query400 = Tool.getQuery("verifAutorisation400.sql");
                        query400 = String.Format(query400, this.NoAuth400, this.NoMag, this.NoProvider);
                        //Vérifie si retourne valeur
                        cmd400 = new OleDbCommand(query400, con400);
                        int rep = Convert.ToInt32(cmd400.ExecuteScalar());
                        if (rep > 0)
                        {
                            //Si oui incrire la qty et montant dans transaction_store
                            OleDbDataReader reader400 = cmd400.ExecuteReader();

                            reader400.Read();
                            this.qty = int.Parse(reader400[1].ToString());
                            this.amount = double.Parse(reader400[2].ToString());

                            //UPDATE Transaction_Store
                            string querySQL = Tool.getQuery("UpdateAutorisationSQL.sql");
                            querySQL = String.Format(querySQL, this.qty, this.amount, this.Lien, this.NoMag, this.NoProvider);
                            Connexion.execCommand(querySQL);
                            reader400.Close();
                        }
                        cmd400.Dispose();
                        

                    }
                   
                }
                else
                {
                    //Aucune données à verifier alors on quitte
                }
               

            }
            catch (SqlException se)
            {
                log.logError("UpdateAutorisation SQL erreur : " + se.Message + " StackTrace : " + se.StackTrace);
            }
            catch (OleDbException oe)
            {
                log.logError("UpdateAutorisation OLDB erreur : " + oe.Message + " StackTrace : " + oe.StackTrace);
            }
            catch (Exception e)
            {
                log.logError("UpdateAutorisation  erreur : " + e.Message + " StackTrace : " + e.StackTrace);
            }
            finally
            {
                Connexion.close(con400);
                Connexion.closeSQL(conSQL);
            }
         
        }

     
    }
}