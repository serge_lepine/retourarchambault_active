﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.IO;
/// <summary>
/// Description résumée de Maj_Temp_Table
/// </summary>
public class Maj_Temp_Table
{
    private string itrdat_str;
    private string itrdat_end;
    private string ittref;
    private int asnum;
    private int idept;
    private string idsccd;
    private string iatrib;
    private bool maj;
    private string requete;
    private OleDbConnection con;
    private bool del;
    /// <summary>
    /// Constructeur par défault
    /// </summary>
	public Maj_Temp_Table()
	{
        initialise();
	}
    /// <summary>
    /// Mise à jour de la bd temporaire
    /// </summary>
    /// <param name="itrdat_debut">Début de la période aammjj</param>
    /// <param name="itrdat_fin">Fin de la période aammjj</param>
    /// <param name="itrref">Provenance</param>
    /// <param name="asnum">No fournisseur</param>
    /// <param name="idept">No département</param>
    /// <param name="idsccd">Type de sku</param>
    /// <param name="iatrb1">Format</param>
    /// <returns></returns>
    public bool maj_Temp_Table(string Ittref,int Asnum,int Idept,string Idsccd,string Iatrib )
    {
        ittref = Ittref;
        asnum = Asnum;
        idept = Idept;
        idsccd = Idsccd;
        iatrib = Iatrib;
        // Connexion au A/S400
        con = new OleDbConnection();
        Tool.setThread_EN();
        try
        {
            con = Connexion.Connect();
        }
        catch (OleDbException odb)
        {
            log.logError(odb.Message);
        }
        finally
        {
            if (con.State == ConnectionState.Open)
            {
                del = delete_table();
                requete = "";
                if (del == true)
                {
                    requete = File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/Requete.sql")) ;

                    log.Log("Connexion établie au A/S400");

                    OleDbCommand sqlCMD = new OleDbCommand();
                    sqlCMD.Connection = con;
                    sqlCMD.CommandType = System.Data.CommandType.Text;
                    sqlCMD.CommandText = String.Format(requete,itrdat_str,itrdat_end,ittref,asnum,idept,idsccd,iatrib);
                    try
                    {
                        sqlCMD.ExecuteNonQuery();
                        maj = true;
                    }
                    catch (OleDbException oe)
                    {
                        log.logError(oe.Message);
                    }
                    finally
                    {
                        con.Close();
                    }

                }
                else
                {
                    log.logError("Erreur lors de la connexion au A/S400");
                    maj = false;
                    con.Close();
                }


            }
        }

        Tool.setThread_FR();

        return maj;
    }
    /// <summary>
    /// Initialise les variables
    /// </summary>
    private void initialise()
    {
        itrdat_str = "120412"; //lastperiod(); 
        itrdat_end = "120711"; //DateTime.Now.ToString("yyMMdd");
        ittref = "";
        asnum = 0;
        idept = 0;
        idsccd = "";
        iatrib = "";
        maj = false;
        requete = "";
        con = null;
        del = false;
    }
    private bool delete_table()
    {
        requete = @"DELETE FROM T_LEPINESE.T_invaud";
        OleDbCommand sqlCMDDel = new OleDbCommand();
        sqlCMDDel.Connection = con;
        sqlCMDDel.CommandType = System.Data.CommandType.Text;
        sqlCMDDel.CommandText = requete;
        try
        {
            sqlCMDDel.ExecuteNonQuery();
            del = true;
        }
        catch (OleDbException de)
        {
            log.logError(de.Message);
        }

        return del;
    }
    private string lastperiod()
    {
        DateTime d = DateTime.Now;
        
        d = d.AddDays(-90);
        return d.ToString("yyMMdd");
    }

}