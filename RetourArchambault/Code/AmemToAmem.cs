﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class AmemToAmem
    {

        private SqlConnection con;
        private SqlCommand cmd;
    //    private DataSet ds;
        private ParamAmem pa;

        public bool amemToAmem(ParamAmem p)
        {
            con = Connexion.ConnectSQL();
            // ds = new DataSet();
            //    pa = new ParamAmem();
            //   ds = pa.ds;
            pa = p;
       //     int y = 0;

            int cie = pa.cie;
            int usrDestination = pa.idUsrDestination;
            string TransactName = pa.lien;

            string req = "";
           


            if (con.State == ConnectionState.Open)
            {
               
                Tool.setThread_EN();
             //   bool OK = true;
                //3 - Boucle foeach du dataset
                try
                {
                    // 1 - Insert into transaction provider
                   

                    foreach (DataRow row in p.dt.Rows)
                    {

                        //4 - Créer insert pour chacune des lignes
                        req = "INSERT INTO Temp_Transaction_Provider (NoMag,Sku,NoCie,ISBN,UPC,Auteur,Titre,Compo,Format,Etiquette,Cost,Qty_a_Retourner,ValeurRetour,Dept,Lien,IdUser,Date_Creation,Model_Calcule) VALUES " +
                                                                 "(@NoMag,@Sku,@NoCie,@ISBN,@UPC,@AUTEUR,@TITRE,@COMPO,@FORMAT,@Etiquette,@COST,@Qty_a_Retourner,@ValeurRetour,@Dept,@Lien,@IdUser,@Date_Creation,@Model_Calcule)";

                        //Declaration des parametres
                        cmd = new SqlCommand(req, con);
                        cmd.Parameters.Add(new SqlParameter("@NoMag", SqlDbType.Int, 4));
                        cmd.Parameters.Add(new SqlParameter("@Sku", SqlDbType.VarChar, 50));
                        cmd.Parameters.Add(new SqlParameter("@NoCie", SqlDbType.Int, 4));
                        cmd.Parameters.Add(new SqlParameter("@ISBN", SqlDbType.VarChar, 50));
                        cmd.Parameters.Add(new SqlParameter("@UPC", SqlDbType.VarChar, 50));
                        cmd.Parameters.Add(new SqlParameter("@AUTEUR", SqlDbType.VarChar, 150));
                        cmd.Parameters.Add(new SqlParameter("@TITRE", SqlDbType.VarChar, 150));
                        cmd.Parameters.Add(new SqlParameter("@COMPO", SqlDbType.VarChar, 150));
                        cmd.Parameters.Add(new SqlParameter("@FORMAT", SqlDbType.VarChar, 50));
                        cmd.Parameters.Add(new SqlParameter("@Etiquette", SqlDbType.VarChar, 150));
                        cmd.Parameters.Add(new SqlParameter("@Date_Creation", SqlDbType.DateTime));
                        cmd.Parameters.Add(new SqlParameter("@Model_Calcule", SqlDbType.Int, 4));
                        cmd.Parameters.Add(new SqlParameter("@COST", SqlDbType.Money));
                        cmd.Parameters.Add(new SqlParameter("@Qty_a_Retourner", SqlDbType.Int, 4));
                        cmd.Parameters.Add(new SqlParameter("@ValeurRetour", SqlDbType.Money));
                        cmd.Parameters.Add(new SqlParameter("@Dept", SqlDbType.Int, 4));
                        cmd.Parameters.Add(new SqlParameter("@Lien", SqlDbType.VarChar, 50));
                        cmd.Parameters.Add(new SqlParameter("@IdUser", SqlDbType.Int, 4));

                        //Attribution des valeurs
                        cmd.Parameters["@NoMag"].Value = row[1].ToString();
                        cmd.Parameters["@Sku"].Value = row[2].ToString();
                        cmd.Parameters["@NoCie"].Value = row[3].ToString();
                        cmd.Parameters["@ISBN"].Value = row[4].ToString();
                        cmd.Parameters["@UPC"].Value = row[5].ToString();
                        cmd.Parameters["@AUTEUR"].Value = row[6].ToString();
                        cmd.Parameters["@TITRE"].Value = row[7].ToString();
                        cmd.Parameters["@COMPO"].Value = row[8].ToString();
                        cmd.Parameters["@FORMAT"].Value = row[9].ToString();
                        cmd.Parameters["@Etiquette"].Value = row[10].ToString();
                        cmd.Parameters["@Date_Creation"].Value = DateTime.Now.ToString();
                        cmd.Parameters["@Model_Calcule"].Value = row[11].ToString();
                        cmd.Parameters["@COST"].Value = row[12].ToString();
                        cmd.Parameters["@Qty_a_Retourner"].Value = row[13].ToString();
                        cmd.Parameters["@ValeurRetour"].Value = row[14].ToString();
                        cmd.Parameters["@Dept"].Value = pa.dept;
                        cmd.Parameters["@Lien"].Value = pa.lien;
                        cmd.Parameters["@IdUser"].Value = pa.idUsrEnvoi;



                        ////Insère les données
                        cmd.ExecuteNonQuery();
                    }

                    req = "";

                    // 3 - Insert into transaction amem avec section 3
                    req = "INSERT INTO Transaction_Link_Amem (NoCie,Dept,Section,Lien,Id_UsrCreate,Date_Creation,Id_UsrDestination) VALUES " +
                                                 "(@NoCie,@Dept,@Section,@Lien,@Id_UsrCreate,@Date_Creation,@Id_UsrDestination)";

                    InsertLinkData(req);


                }
                catch (Exception er)
                {
                    //Inscrire log erreur
                    log.logError(er.Message);
                    //Mettre flag à false
                  //  OK = false;
                }
                finally
                {
                    //Fermer connexion
                    Connexion.closeSQL(con);

                }

            }
            Tool.setThread_FR();
            return true;
        }
        private void InsertLinkData(string query)
        {
            cmd = new SqlCommand(query, con);
            log.Log(query);
            cmd.Parameters.Add(new SqlParameter("@NoCie", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Dept", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Section", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Lien", SqlDbType.VarChar, 200));
            cmd.Parameters.Add(new SqlParameter("@Id_UsrCreate", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Date_Creation", SqlDbType.DateTime));
            cmd.Parameters.Add(new SqlParameter("@Id_UsrDestination", SqlDbType.Int, 4));

            cmd.Parameters["@NoCie"].Value = pa.cie;
            cmd.Parameters["@Dept"].Value = pa.dept;
            cmd.Parameters["@Section"].Value = pa.section;
            cmd.Parameters["@Lien"].Value = pa.lien;
            cmd.Parameters["@Id_UsrCreate"].Value = pa.idUsrEnvoi;
            cmd.Parameters["@Date_Creation"].Value = DateTime.Now.ToString();
            cmd.Parameters["@Id_UsrDestination"].Value = pa.idUsrDestination;
            cmd.ExecuteNonQuery();

        }

    }


    public class ParamAmem
    {
        public DataTable dt { get; set; }
        public int cie { get; set; }
        public int dept { get; set; }
        public int idUsrEnvoi { get; set; }
        public int idUsrDestination { get; set; }
        public string lien { get; set; }
        public int section { get; set; }
        public string condition { get; set; }
        public string lang { get; set; }
        public int idTransactionType { get; set; }
        public string noAvis { get; set; }
        public DateTime DateLimite { get; set; }
        public ParamDemande amount = new ParamDemande();
    }
    public class ParamDemande
    {
        public int qty_demande { get; set; }
        public double montant_demande { get; set; }
    }
}