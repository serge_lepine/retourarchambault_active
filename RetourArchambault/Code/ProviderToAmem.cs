﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ConvertToCSV;

namespace RetourArchambault
{
    public class ProviderToAmem
    {
        private string query = "";

        public bool providerToAmem(string Lien,int qtyAutorise,double montantAutorise)
        {
           
            //Prepare la requete
            this.query = Tool.getQuery("CreateAmem_2.sql");
            Tool.setThread_EN();
            this.query = String.Format(this.query,Lien,qtyAutorise,montantAutorise);

            log.Log(this.query);

            //bool rep =  Connexion.execCommand(this.query);
            Tool.setThread_FR();
         
            return Connexion.execCommand(this.query);
          
        }
     
    }
  
}