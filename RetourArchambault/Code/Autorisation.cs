﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class Autorisation
    {
       private string Lien { get; set; }
       private string TempNumber { get; set; }
       private int Id_user { get; set; }

       public  Autorisation()
       {

       }
       public Autorisation(string lien)
       {
           this.Lien = lien;
           
       }
       public Autorisation(string lien,int id_user)
       {
           this.Lien = lien;
           this.Id_user = id_user;
       }
        public Autorisation(string lien,string tempNum)
       {
           this.Lien = lien;
           this.TempNumber = tempNum;
       }
        /// <summary>
        /// Enregistre la commande dans table Autorisation
        /// </summary>
        public void setAutorisation()
        {
            string query = Tool.getQuery("setAutorisation.sql");
            query = String.Format(query, this.Lien);
            Connexion.execCommand(query);
        }
        /// <summary>
        /// Met à jour la table Autorisation par magasin
        /// </summary>
        public void setAutorisationStore()
        {
            string query = Tool.getQuery("setAutorisationStore.sql");
            query = String.Format(query, this.Lien);
            Connexion.execCommand(query);
        }
        /// <summary>
        /// Recherche si existant dans la table Autorisation
        /// </summary>
        /// <returns>Vrai si existant</returns>
        public bool getAutorisationList()
        {
            string query = Tool.getQuery("getAutorisationList.sql");
            query = String.Format(query, this.Lien);
            return Connexion.execScalar(query);
        }
        public string urlReturn()
        {
            return "AutorizeProvider.aspx?lien=" + this.Lien + "&Usr=" + this.Id_user;
        }
    }
}