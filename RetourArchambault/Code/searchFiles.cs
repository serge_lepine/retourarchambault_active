﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace ConvertToCSV
{
    class searchFiles
    {
        /// <summary>
        /// Retourne tout les fichiers du répertoire.
        /// </summary>
        /// <param name="FilePath">Chemin du répertoire</param>
        /// <returns>Liste des fichiers trouvé</returns>
        public static string[] GetFiles(string FilePath)
        {
            //Obtenir nom des fichiers
            string[] rep = null;
            string[] response = null;

            try
            {
                rep = Directory.GetFiles(FilePath);
            }
            catch
            {
                response = null;
                return response;
            }
            if (rep.Length != 0)
            {
                response = Tools.CreateArray(rep);
            }
            else
            {
                response = null;
            }
            return response;

        }
        
        /// <summary>
        /// Retourne le nom du fichier selon l'index
        /// </summary>
        /// <param name="sourcePath">Chemin du répertoire</param>
        /// <param name="startIndex">Numero de l'index du fichier à trouvé</param>
        /// <returns>Nom du fichier selon index</returns>
        public static string Get_Name(string sourcePath,string startIndex)
        {
            string response = null;
        
             if (System.IO.Directory.Exists(sourcePath))
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);

                try
                {
                    foreach (string s in files)
                    {
                   
                     //Trouve le nom du fichier avec index
                        if (Tools.get_index(s) == startIndex)
                        {
                            response = Tools.get_NameFiles(s);
                            return response;
                        }
                    }
                }
                catch
                {
                    return response;
                }
              
            }
             return response;
        
        }

       
       
    }
}
