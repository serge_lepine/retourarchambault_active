﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Common;
using ConvertToCSV;

namespace RetourArchambault
{
    public class ZoneRepeater
    {
        private DataTable m_dt;
    //    private string m_folderName;
        private int m_usr;
        private string lang;
        public DataTable zoneRepeater(int etape,string provenance,int id_usr,string Lang)
        {
            initialise();
            m_dt = new DataTable();
            m_usr = id_usr;
            lang = Lang;
            m_dt = SetInitialRowSQL(etape, provenance,id_usr,this.lang);
            return m_dt;
        
        }
        private void initialise()
        {
            m_dt = null;
          //  m_folderName = "";
            m_usr = 0;
        }
        /// <summary>
        /// Créer une Table selon les fichiers qui se trouve dans les répertoires
        /// </summary>
        /// <param name="etape">1-Nouveau 2-A traiter 3-Historique</param>
        /// <returns></returns>
        private DataTable SetInitialRow(int etape,string provenance)
        {
            //Instancie une nouvelle DataTable
            DataTable dt = new DataTable();
            //Créer l'en-tete des colonnes
            dt = get_Column();
            //Instancie nouvelle lignes
            DataRow dr = null;
            //Récupère les fichiers non traité
            string[] myTable = searchFiles.GetFiles(HttpContext.Current.Server.MapPath(String.Format("/{0}/", FolderName(etape, provenance))));
            //Si le tableau est vide entrer ligne par defaut
            if (myTable == null)
            {
                //Créer la ligne par defaut 
                dr = dt.NewRow();
                dr["ID"] = 1;
                dr["Text"] = "Aucune nouvelle activité";
                dr["Lien"] = "";
                dr["TransactDate"] = DateTime.Now.ToLongDateString();
                dt.Rows.Add(dr);
            }
            else
            {
                //Lire chaque fichier dans le répertoire
                for (int i = 0; i < myTable.Length; i++)
                {
                    string[] words = Tool.ExtractLien(Tool.shortName(myTable[i]));
                    //Inscrit les lignes pour les colonnes
                    dr = dt.NewRow();
                    dr["ID"] = i + 1;
                    dr["Text"] = words[0];
                    dr["Lien"] = GetLink(provenance,etape,this.m_usr)+HttpContext.Current.Server.MapPath(String.Format("/{0}/{1}", FolderName(etape,provenance), myTable[i]));
                    string sDate = Tool.formatDate(DateTime.Parse(Tool.GetDate(words[3])),this.lang);
                    dr["TransactDate"] = sDate;
                                            // DateTime.Parse(Tool.GetDate(words[3])).ToLongDateString();
                    dt.Rows.Add(dr);
                }
            }

            //Retourne nouvelle table
            return dt;
        }
        /// <summary>
        /// Créer une Table selon les fichiers qui se trouve dans les répertoires
        /// </summary>
        /// <param name="etape">1-Nouveau 2-A traiter 3-Historique</param>
        /// <returns></returns>
        private DataTable SetInitialRowSQL(int etape, string provenance,int id_usr,string Lang)
        {
            this.m_usr = id_usr;
            //Instancie une nouvelle DataTable
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            FillDataTable fdt = new FillDataTable();
            dt2 = fdt.fillDataTableSQL(provenance, etape,id_usr,Lang);
            //Crée l'en-tete des colonnes
            dt = get_Column();
            ////Instancie nouvelle lignes
            DataRow dr = null;

            DataTableReader read = dt2.CreateDataReader();

            if (!read.HasRows)
            {
                //Créer la ligne par defaut 
                dr = dt.NewRow();
                dr["ID"] = 1;
                dr["monText"] = "Aucune nouvelle activité";
                dr["Lien"] = "";
                dr["TransactDate"] = DateTime.Now.ToLongDateString();
                dt.Rows.Add(dr);
            }
            else
            {
                int i = 0;
                if (read.HasRows)
                {
                    while (read.Read())
                    {
                        //Lire chaque ligne du le répertoire

                        i++;
                        //Inscrit les lignes pour les colonnes
                        dr = dt.NewRow();
                        dr["ID"] = read.GetInt32(0);
                        dr["monText"] = read.GetString(1);
                        dr["Lien"] = GetLink(provenance, etape, this.m_usr) + String.Format("{0}", read.GetString(3));
                        string sDate = Tool.formatDate(read.GetDateTime(2), this.lang);
                        dr["TransactDate"] = sDate; //read.GetDateTime(2).ToLongDateString();
                        dr["TranssactionType"] = read.GetString(4);
                        dt.Rows.Add(dr);

                    }
                }
            }

            //Retourne nouvelle table
            return dt;
        }
        /// <summary>
        /// Instancie la table de données
        /// </summary>
        /// <returns>Nouvelle table</returns>
        private DataTable get_Column()
        {
            //m_dt = new DataTable();
            //m_dt.Clear();
            //Crée les colonnes
            m_dt.Columns.Add(new DataColumn("ID", typeof(int)));
            m_dt.Columns.Add(new DataColumn("monText", typeof(string)));
            m_dt.Columns.Add(new DataColumn("Lien", typeof(string)));
            m_dt.Columns.Add(new DataColumn("TransactDate", typeof(string)));
            m_dt.Columns.Add(new DataColumn("TranssactionType", typeof(string)));
            return m_dt;
        }
        /// <summary>
        /// Recherche nom du folder
        /// </summary>
        /// <param name="no">No de l'étape</param>
        /// <returns>Nom du fo</returns>
        private string FolderName(int no)
        {
            string folder = "";

            switch (no)
            {
                case 1:
                    folder = "FileAmem";
                    break;
                case 2:
                    folder = "FileAmemTraiter";
                    break;
                case 3:
                    folder = "FileAmemHistorique";
                    break;
                default:
                    break;
            }
            return folder;

        }
        private string FolderName(int etape, string fournisseur)
        {
            string folder = "";
            folder = fournisseur + "_" + etape;

            return folder;
        
        }
        private string GetLink(string provenance,int etape,int usr)
        {
            string folder = "";
            string path = provenance + "_" + etape.ToString();
            switch (path)
            {
                case "Transaction_Link_Amem_1":
                    folder = "~/Achat/AmemView.aspx?Temp=1&usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Amem_2":
                    folder = "~/Achat/WebForm1.aspx?Temp=0&usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Amem_3":
                    folder = "~/Achat/WebForm1.aspx?Temp=0&usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Provider_1":
                    folder = "~/Fournisseur/ViewDataProvider.aspx?usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Provider_2":
                    folder = "~/Fournisseur/ViewDataProvider.aspx?usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Provider_11":
                    folder = "~/Fournisseur/ViewDataProvider.aspx?usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Magasin_1":
                    folder = "~/Succursales/ViewDataMagasin.aspx?usr=" + usr.ToString() + "&Lien=";
                    break;
                case "Transaction_Link_Magasin_2":
                    folder = "~/Succursales/ViewDataMagasin.aspx?usr=" + usr.ToString() + "&Lien=";
                    break;
                default:
                    break;
            }
            return folder;
        
        }
    }
}