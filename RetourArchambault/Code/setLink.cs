﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RetourArchambault
{
    public class setLink
    {
        /// <summary>
        /// Met la section et le lien à jour
        /// </summary>
        /// <param name="Lien">Nouveau lien </param>
        /// <param name="TO">Nom de la table a mettre à jour</param>
        /// <param name="section">Nouveau numero de section</param>
        /// <param name="IdUsrDestination">No de Cie fournisseur</param>
        public static void setUpdateLink(string TO, int section, int IdUsrDestination, string Lien, string newLien)
        {
            Tool.setThread_EN();
            //Requete
            string query = String.Format("UPDATE Transaction_Link_{0} SET section={1} Lien='{2}' WHERE Lien={3}", TO, section, newLien, Lien);
            Tool.setThread_FR();
            //Ouvree connexion
            SqlConnection con = Connexion.ConnectSQL();
            //Instancie la Commande
            SqlCommand cmd = new SqlCommand(query, con);

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch(SqlException se)
            {
                log.logError(se.Message);
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(con);
            }

        }
    
    }
}