﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class Update_TransLink
    {
        public bool upDate_TransLink(string oldName,int section)
        {
            Tool.setThread_EN();
            //Ouvre connexion 
            SqlConnection con = Connexion.ConnectSQL();
            string query = Tool.getQuery("UpdateAmemLink.sql"); // File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/Req_Update_AmemLink.sql"));
            query = String.Format(query,section, oldName);
            log.Log(query);
            Tool.setThread_FR();
            return Connexion.execCommand(query);

        }
    }
}