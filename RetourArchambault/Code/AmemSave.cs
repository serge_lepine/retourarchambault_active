﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class AmemSave
    {
        private string lien;
        const int is_complete = 0;
        private int cieSearch;
        private string query;

        public AmemSave()
        {

        }
        public AmemSave(NewTransaction nt)
        {
            this.lien = nt.Lien;
            this.cieSearch = nt.NoCie;
            setQuery();
        }
        public bool setAmemSave()
        {
            
            this.query = String.Format(this.query, this.lien, is_complete, this.cieSearch, Connexion.getTransactionType("Temp_Trans_Provider_" + this.lien));

            try
            {
                Connexion.execCommand(query);
                return true;
            }
            catch (Exception e)
            {
                log.logError("AmemView.aspx BtnSave_Click :" + e.Message);
                return false;
            }
        }
        private void setQuery()
        {
            this.query = Tool.getQuery("TempAmemInsert.sql");
        }

    }
}