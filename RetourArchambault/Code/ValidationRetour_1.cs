﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;

namespace RetourArchambault
{
    public class ValidationRetour_1
    {
      
        private OleDbCommand cmd400;
        private IDataReader record;
        private OleDbDataReader reader;
        private ValidationRetour_Reponse response = new ValidationRetour_Reponse();
        private ValidationRetour_Param vrp;

        public ValidationRetour_1()
        {

        }
        public ValidationRetour_1(ValidationRetour_Param Vrp)
        {
            this.vrp = Vrp;
        }
        public ValidationRetour_Reponse validationRetour_1()
        {
            try
            {
                setCommand();
                this.reader = this.cmd400.ExecuteReader();
                this.reader.Read();
                this.record = (IDataReader)this.reader;
                //Validation fournisseur
                if(int.Parse(this.record[1].ToString()) != this.vrp.NoFournisseur)
                {
                    this.response.succes = false;
                    this.response.errorMessage = String.Format(Tool.getMessage("ValidationFournisseur.txt"), this.vrp.NoFournisseur);
                    return this.response;
                }
                //Validation Magasin
                if(int.Parse(this.record[2].ToString()) != this.vrp.NoMag)
                {
                    this.response.succes = false;
                    this.response.errorMessage = String.Format(Tool.getMessage("ValidationNoMag.txt"), this.vrp.NoMag);
                    return response;
                }
                //Validation statut
                if(this.record[3].ToString() != "A")
                {
                    this.response.succes = false;
                    this.response.errorMessage = String.Format(Tool.getMessage("ValidationStatut.txt"), this.record[3].ToString());
                    return response;
                }
                //Validation du type
                switch (this.vrp.TypeTransaction)
                {
                    case 1:
                        //Vérifie si overstock Type F ou Z
                        if(this.record[0].ToString() == "F" || this.record[0].ToString() == "Z")
                        {
                            this.response.succes = true;
                        }
                        else
                        {
                            this.response.errorMessage = String.Format(Tool.getMessage("ValidationOverstock.txt"), this.record[0].ToString());
                            this.response.succes = false;
                        }
                        break;
                    case 2:
                        //Verifie si Discontinuer Type C
                        if (this.record[0].ToString() == "C")
                        {
                            this.response.succes = true;
                        }
                        else
                        {
                            this.response.errorMessage = String.Format(Tool.getMessage("ValidationDiscontinue.txt"), this.record[0].ToString());
                            this.response.succes = false;
                        }
                        break;
                    case 3:
                        //Verifie si  Defectueux Type B
                        if (this.record[0].ToString() == "B")
                        {
                            this.response.succes = true;
                        }
                        else
                        {
                            this.response.errorMessage = String.Format(Tool.getMessage("ValidationDefectueux.txt"), this.record[0].ToString());
                            this.response.succes = false;
                        }
                        break;
                    default:
                        //Si autre Type retourner erreur
                        this.response.errorMessage = String.Format(Tool.getMessage("ValidationTypeGeneral.txt"), this.record[0].ToString());
                            this.response.succes = false;
                        break;
                }


                return response;

            }
            catch (Exception e)
            {
                log.logError("ValidationRetour_1 : " + e.Message);
                this.response.succes = false;
                this.response.errorMessage = "ValidationRetour_1 : " + e.Message;
                return this.response;
            }
            finally
            {
                this.record.Close();
                this.reader.Close();
                this.cmd400.Dispose();
            }


           
        }
        void setCommand()
        {
             cmd400 = new OleDbCommand(String.Format(Query.Validation_1(), this.vrp.NoRetour),this.vrp.Conn400);
        }
        
        
    }
    public class ValidationRetour_Reponse
    {
        public bool succes{get;set;}
        public string errorMessage{get;set;}
    }
    public class ValidationRetour_Param
    {
        public string NoRetour { get; set; }
        public int NoMag { get; set; }
        public int NoFournisseur { get; set; }
        public int TypeTransaction { get; set; }
        public OleDbConnection Conn400 { get; set; }
        public string Lien { get; set; }
    }
}