﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RetourArchambault
{
    public class CreateTransactionStoreList
    {
        private string query;

        public bool createTransactionStoreList(string lien)
        {
            Tool.setThread_EN();
            if(Tool.getTransactionType(lien) == 3)
            {
                setQuery3();
            }
            else
            {
                setQuery();
            }
           
            query = String.Format(query, lien);

            Tool.setThread_FR();
            log.Log(query);

            return Connexion.execCommand(query);
           
        }
         private void setQuery()
        {
            this.query = Query.CreateTransactionStoreList(); //Tool.getQuery("CreateTransactionStoreList.sql");
        }
        private void setQuery3()
         {
             this.query = Query.CreateTransactionStoreList_Type3(); //Tool.getQuery("CreateTransactionStoreList_type3.sql");
         }
       
    }
}