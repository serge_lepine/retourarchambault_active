﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ConvertToCSV;

namespace RetourArchambault
{
    public class FillProviderData_1
    {
        private bool resp = false;
        private string Lien;
        private int Dept;
        private int Cie;
        private string Lang;

        public FillProviderData_1()
        {

        }
        public FillProviderData_1(SetProviderData paramProvider)
        {
            this.Lien = paramProvider.Lien;
            this.Dept = paramProvider.Dept;
            this.Cie = paramProvider.Cie;
            this.Lang = paramProvider.Lang;
        }
        public bool fillProviderData_1()
        {
            Tool.setThread_EN();
                //Creation de la requete
                string query = Tool.getQuery("CreateTransactionProvider.sql"); 
                query = String.Format(query, this.Lien,this.Dept,this.Cie);
                log.Log(query);
                Tool.setThread_FR();
                resp = Connexion.execCommand(query);
                if (resp)
                {
                    query = "";
                    query = Tool.getQuery("MergeTransactionProvider.sql"); 
                    query = String.Format(query, this.Lien);
                    log.Log(query);
                    return Connexion.execCommand(query);
                }
                else
                {
                    return false;
                }
           
        }
        public bool fillProviderData_2()
        {
            Tool.setThread_EN();
                //Creation de la requete
            string query = Tool.getQuery("TransactionProviderViewTemp.sql"); 
                query = String.Format(query, this.Lien,this.Cie,this.Lang);
                log.Log(query);
                Tool.setThread_FR();
               return Connexion.execCommand(query);
        }
       
    }
    public class SetProviderData
    {
        public string Lien { get; set; }
        public int Dept { get; set; }
        public int Cie { get; set; }
        public string Lang { get; set; }

        public SetProviderData()
        {

        }
    }
 
}