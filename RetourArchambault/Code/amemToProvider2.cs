﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class amemToProvider2
    {
       //Faut ajouter condition pour item 6

        public static bool transfertByCopy(ParamAmem p)
        {
            Tool.setThread_EN();
            //Transfert vers Transsaction_Provider
            string query = Tool.getQuery("AmemToProvider.sql");
            
            query = String.Format(query, p.dept, p.lien, p.idUsrEnvoi,p.cie,p.condition,p.idTransactionType,p.DateLimite,p.noAvis);
            Tool.setThread_FR();
            log.Log(query);
            if (Connexion.execCommand(query))
            {
              
                setAmemLink(p);
                //Supprime la table temporaire
                return Connexion.DropTableTemp(p.lien);
            }
            else
            {
                return Connexion.execCommand(query);
            }
          
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pa"></param>
        /// <returns></returns>
        private static bool setAmemLink(ParamAmem pa)
        {
            Tool.setThread_EN();
            // 2 - Insert into transaction-link-provider avec section 1
           string req = "INSERT INTO Transaction_Link_Provider (NoCie,Dept,Section,Lien,Id_UsrCreate,Date_Creation,Qty_demande,Montant_demande,Date_Limite,is_complete,idTransactionType) VALUES " +
                                                          "(@NoCie,@Dept,@Section,@Lien,@Id_UsrCreate,@Date_Creation,@Qty_demande,@Montant_demande,@Date_Limite,@is_complete,@idTransactionType)";

           log.Log("amemToProvider2 setAmemLink : " + req);
            pa.section = 1;

            InsertLinkData(req, pa);

            // 3 - Insert into transaction amem avec section 3
            req = "INSERT INTO Transaction_Link_Amem (NoCie,Dept,Section,Lien,Id_UsrCreate,Date_Creation,idTransactionType) VALUES " +
                                         "(@NoCie,@Dept,@Section,@Lien,@Id_UsrCreate,@Date_Creation,@idTransactionType)";

            log.Log("amemToProvider2 setAmemLink 2 : " + req);
            pa.section = 0;
            Tool.setThread_FR();
           return InsertLinkData(req,pa);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        private static bool InsertLinkData(string query,ParamAmem pa)
        {
            Tool.setThread_EN();
            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd;
            cmd = new SqlCommand(query, con);
            log.Log(query);
            cmd.Parameters.Add(new SqlParameter("@NoCie", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Dept", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Section", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Lien", SqlDbType.VarChar, 200));
            cmd.Parameters.Add(new SqlParameter("@Id_UsrCreate", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Date_Creation", SqlDbType.DateTime));
            cmd.Parameters.Add(new SqlParameter("@Qty_demande", SqlDbType.Int, 4));
            cmd.Parameters.Add(new SqlParameter("@Montant_demande", SqlDbType.Money));
            cmd.Parameters.Add(new SqlParameter("@Date_Limite", SqlDbType.DateTime));
            cmd.Parameters.Add(new SqlParameter("@is_complete", SqlDbType.Bit));
            cmd.Parameters.Add(new SqlParameter("@idTransactionType", SqlDbType.Int));

            cmd.Parameters["@NoCie"].Value = pa.cie;
            cmd.Parameters["@Dept"].Value = pa.dept;
            cmd.Parameters["@Section"].Value = pa.section;
            cmd.Parameters["@Lien"].Value = pa.lien;
            cmd.Parameters["@Id_UsrCreate"].Value = pa.idUsrEnvoi;
            cmd.Parameters["@Date_Creation"].Value = DateTime.Now.ToString();
            cmd.Parameters["@Qty_demande"].Value = pa.amount.qty_demande;
            cmd.Parameters["@Montant_demande"].Value = pa.amount.montant_demande;
            cmd.Parameters["@Date_Limite"].Value = pa.DateLimite; //DateTime.Now.AddDays(15);
            cmd.Parameters["@is_complete"].Value = 0;
            cmd.Parameters["@idTransactionType"].Value = pa.idTransactionType;
            Tool.setThread_FR();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                log.logError("AmemToProvider2 : " + e.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
            return true;

        }
    }
}