﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault
{
    public class VerificationUsager
    {
        private Usager unUsager;
        private Usr user;
        private string query;
        private SqlConnection SQLcon;

        public VerificationUsager()
        {
            
        }
        public VerificationUsager(Usager usager)
        {
            this.unUsager = usager;
            if(this.unUsager != null)
            { 
                // Instancie la requete
                setQuery(); 
                // Instancie connection
                setConnection();
            }
        }
        void setQuery()
        {
            this.query = String.Format(Query.setLogin(),this.unUsager.NomUsager,this.unUsager.MotDePasse);
        }
        void setConnection()
        {
            this.SQLcon = Connexion.ConnectSQL();
        }

        public Usr verificationUsager()
        {
            user = new Usr();

            log.Log(this.query);
            SqlCommand cmd = new SqlCommand(this.query, this.SQLcon);

            SqlDataReader reader;
            {
                try
                {
                    reader = cmd.ExecuteReader();
                    reader.Read();
                    {

                        user.id = reader.GetInt32(0);
                        user.idCie = reader.GetInt32(1);
                        user.first_name = reader.GetString(2);
                        user.last_name = reader.GetString(3);
                        user.is_amem = reader.GetBoolean(4);
                        user.is_public = reader.GetBoolean(5);
                        user.is_admin = reader.GetBoolean(6);
                        user.is_store = reader.GetBoolean(7);

                    }
                }
                catch (Exception ex)
                {
                    log.logError(ex.Message);
                    user.erreurMessage.Erreur = true;
                    user.erreurMessage.MessageErreur = "Utilisateur ou mot de passe invalide !";


                }
                finally
                {
                    cmd.Dispose();
                    Connexion.closeSQL(this.SQLcon);
                }
                return user;
            }
        }
    }
}