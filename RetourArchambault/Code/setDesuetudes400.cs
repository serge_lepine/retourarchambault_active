﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    
    public class setDesuetudes400
    {
        private string sku;
        private string lien;
        private string query;
        private string query400;
        private string query400_1;
        private string query400_2;
        private string m_date;
        private string m_hour;

        public setDesuetudes400()
        {

        }
        public setDesuetudes400(string Lien)
        {
            //Instancie variable
            this.lien = Lien;
            this.m_date = Tool.setDate400();
            this.m_hour = Tool.setTime400();
            setQuerySku();
            getSku();
        }
        public bool setStatusDesuetudes()
        {
            setQuery400();
            if(!Connexion.execCommand400(this.query400)){return false;}
            setQuery400_1();
            if (!Connexion.execCommand400(this.query400_1)) { return false; }
            setQuery400_2();
            if (!Connexion.execCommand400(this.query400_2)) { return false; }
            //Exécute la requête
            return true;
        }
        private void setQuerySku()
        {
            //Lit la requete
            this.query = Tool.getQuery("getSkuDesuetudes.sql");
            //Entre le parametre dans la requete
            this.query = String.Format(this.query, this.lien);
            log.Log(query);
        }
        private void getSku()
        {
            //Obtient la liste des SKU a modifier
            this.sku = Connexion.getSKU(this.query); 
        }
        private void setQuery400()
        {
            //Lit la requete
            this.query400 = Tool.getQuery("UpdateDes400.sql");
            //Entre le parametre
            this.query400 = String.Format(this.query400, this.sku);
            log.Log(query400);
         
        }
        private void setQuery400_1()
        {
            //Lit la requete
            this.query400_1 = Tool.getQuery("UpdateDes400_1.sql");
            //Entre le parametre
            this.query400_1 = String.Format(this.query400_1, this.sku, this.m_date, this.m_hour);
            log.Log(query400_1);
          
        }
        private void setQuery400_2()
        {
            //Lit la requete
            this.query400_2 = Tool.getQuery("UpdateDes400_2.sql");
            //Entre le parametre
            this.query400_2 = String.Format(this.query400_2, this.sku);
            log.Log(query400_2);
        }

    }
}