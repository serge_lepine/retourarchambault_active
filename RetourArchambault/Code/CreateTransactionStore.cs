﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

namespace RetourArchambault
{
    
    public class CreateTransactionStore
    {
        private DateTime dateCueillete;

        public bool createTransactionStore(string Lien)
        {
            Tool.setThread_EN();
            getDateCueillette();
            string query = Tool.getQuery("TransactionStore.sql"); 
            query = String.Format(query, Lien, dateCueillete);
            log.Log(query);
            Tool.setThread_FR();
            return Connexion.execCommand(query);
           
        }
        void getDateCueillette()
        {
            DateTime dt = DateTime.Now;
            int jl = int.Parse(ConfigurationManager.AppSettings["JourLimite"]);
            dateCueillete = dt.AddDays(jl);

        }
        
    }
}