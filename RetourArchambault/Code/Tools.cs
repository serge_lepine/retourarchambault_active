﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RetourArchambault;

namespace ConvertToCSV
{
   
    class Tools : format
    {
         
        /// <summary>
        /// Obtient le nombre d'occurence d'une chaine
        /// </summary>
        /// <param name="text">Chaine à parcourir</param>
        /// <param name="pattern">Caractère de recherche</param>
        /// <returns>Nombre d'occurence</returns>
        public static int CountStringOccurrences(string text, string pattern)
        {
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }
            return count;
        }
        
        /// <summary>
        /// Obtient l'index du fichier situé avant le .XLS
        /// </summary>
        /// <param name="fileName">Chaine complète du fichier</param>
        /// <returns>L'index du fichier</returns>
        public static string get_index(string fileName)
        {

            string response = null;
            try
            {
                response = fileName.Substring(fileName.IndexOf("CSV") - 2, 1);
            }
            catch
            {
                response = fileName.Substring(fileName.IndexOf("csv") - 2, 1);
            }

            return response;

        }
        /// <summary>
        /// Retourne le nom du fichier
        /// </summary>
        /// <param name="sourceFiles">Chaine complète du fichier</param>
        /// <returns>Nom complet du fichier XLS</returns>

        public static string get_NameFiles(string sourceFiles)
        {
            string response = null;

            response = sourceFiles.Substring(sourceFiles.LastIndexOf("\\") + 1);
            return response;
        }
        /// <summary>
        /// Extraire chaine selon l'index
        /// </summary>
        /// <param name="chaineInitial">Chaine complète</param>
        /// <param name="index">Numero de section recherché</param>
        /// <param name="valeurRechercher">Symbole de séparation</param>
        /// <returns>Chaine se trouvant dans la section</returns>
        public static string ExtractString(string chaineInitial, int index, string valeurRechercher)
        {
            string rep = null;
            string[] record = null;
            int i = 0;

            try
            {
                record = chaineInitial.Split(Convert.ToChar(valeurRechercher));
            }
            catch
            {
                return null;
            }
            foreach (string r in record)
            {
                if (i == index)
                {
                    rep = r;
                    return rep;
                }
                i++;
            }

            return rep;
        }
        /// <summary>
        /// Création d'un tableau de string
        /// </summary>
        /// <param name="newArray">Tableau à trié</param>
        /// <returns>Nouveau tableau</returns>
        public static string[] CreateArray(string[] newArray)
        {
            string[] response = new string[newArray.Length];

            for (int i = 0; i < newArray.Length; i++)
                response[i] = get_NameFiles(newArray[i]);

            //Trie
            Array.Sort(response);

            return response;
        }
      
        public static string searchStartName(string FileName)
        {

            string rep_extension = FileName.Substring(0, (FileName.Length - 4));


            return rep_extension;


        }
        public static string searchStartName(string FileName,string extension)
        {
            string response = null;
            string rep_extension = FileName.Substring(FileName.Length - 4);
            if (rep_extension == extension)
            {
                response = rep_extension;
            }
            else
            {
                response = null;
            }

            return response;

        }
        /// <summary>
        /// Obtient le nom de la page selon l'index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static string get_name_sheet(string index)
        {
            switch (index)
            {
                case "1":
                    return "Récapitulatif";
                case "2":
                    return "Page travail";
                case "3":
                    return "Données Descriptives";
                case "4":
                    return "Ventes";
                case "5":
                    return "Catégorie";
                default:
                    return null;

            }

        }
        public static int nbrFiles(string sourcePath)
        {
            int response = 0;
            try
            {
                string[] files = System.IO.Directory.GetFiles(sourcePath);
                response = files.Length;
            }
            catch
            {
                response = 0;
            }

            return response;
        }
        /// <summary>
        /// Nombre d'item
        /// </summary>
        /// <param name="chaineInitial">String complete</param>
        /// <param name="valeurRechercher">Symbole de séparation</param>
        /// <returns>Retourne le nombre d'item séparé par le symbole</returns>
        public static int nbrString(string chaineInitial, string valeurRechercher)
        {
            string[] record = null;
            int response = 0;
            record = chaineInitial.Split(Convert.ToChar(valeurRechercher));
            response = record.Length;
            return response;
        }
        public static string formater_date(DateTime dt)
        {
            return dt.ToString("yyMMdd");
        }
        public static string format_string(string words)
        {
            string rep = words.Replace(",", " ");
            rep = rep.Replace(@"\" , " - ");
            rep = rep.Replace("'", " ");
            rep = "'" + rep + "'";
            return rep;
        }
        public static string setTransactName(int NoCie, int Dept, int index)
        {
             string TransactName = "";
            TransactName = NoCie.ToString().PadLeft(3, '0') + "_" + Dept.ToString().PadLeft(3, '0') + "_" + DateTime.Now.ToString("yyMMddHmmss") + "_" + index.ToString().PadLeft(2, '0');
            return TransactName;
        }
      
        public static string newLink(string lien,int section)
        {
            string newLink = "";
            
            newLink = lien.Substring(0,lien.LastIndexOf("_") + 2);
            newLink = newLink + section.ToString();
            return newLink;
        }
     
        public static string formatLink(string text)
        {

            string rep = text.Replace(",", " ");
            rep = rep.Replace(@"\", " - ");
            rep = rep.Replace("/", " - ");
            rep = rep.Replace("'", " ");
            rep = rep.Replace("¤", " ");
            rep = rep.Replace("*", " ");
            rep = rep.Replace("\"", "");

            return rep;
        }
       

          
        
    }

    class format 
    {

        public string format_Time(string stringDate)
        {
            string newString;
            newString = stringDate.Replace("-", "_");
            newString = newString.Replace(":", "");
            newString = newString.Replace(" ", "_");

            return newString;
        }
       
    }
}
