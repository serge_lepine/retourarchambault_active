﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;

/// <summary>
/// Description résumée de FillData
/// </summary>
public class FillData
{
    OleDbDataAdapter adapter;
  
	public FillData()
	{
        initialise();
	}
    public FillData(OleDbCommand cmd)
    {
        adapter = new OleDbDataAdapter(cmd);
        DataTable tl = new DataTable();
        adapter.Fill(tl);
        
       
    }
    private void initialise()
    {
        adapter = null;
        //productAdapter = null;
        //ds = null;
    }
}
public class FillDataTable
{
    private string m_where = "";
    public DataTable fillDataTableSQL(string nameTable,int section, int id_usr,string Lang)
    {
        Tool.setThread_EN();
        string query = "";
  
        switch (nameTable)
        {
            case "Transaction_Link_Amem":
                query = Tool.getQuery("LinkPageAmem.sql"); 
                query = String.Format(query, section,Lang);
                break;
            case "Transaction_Link_Provider":
                query = Tool.getQuery("LinkPageProvider.sql"); 
                query = String.Format(query,id_usr,section,Lang);
                break;
            case "Transaction_Link_Store":
               query = Tool.getQuery("LinkPageAmem.sql"); 
                query = String.Format(query,id_usr, section,Lang);
                break;
            default:
                break;
        }
       
        SqlConnection con = Connexion.ConnectSQL();
       
        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        Connexion.closeSQL(con);
        Tool.setThread_FR();
        return dt;

    }
    void getWhere(int section, int usr, string nameTable)
    {
        string nameColumn = "";
        switch (nameTable)
        {
            case "Transaction_Link_Amem":
                nameColumn = "Id_UsrAmem";
                break;
            case "Transaction_Link_Provider":
                nameColumn = "Id_UsrProvider";
                break;
            case "Transaction_Link_Store":
                nameColumn = "Id_UsrStore";
                break;
            default:
                break;
        }

        m_where = " WHERE Section=" + section + " AND " + nameColumn + " = " + usr;
    }


}