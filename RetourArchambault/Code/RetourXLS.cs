﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class RetourXLS
    {
        public string UPC { get; set; }
        public int NoCie { get; set; }
        public DateTime Date_Limite { get; set; }
        public string NoAvis { get; set; }
        public string Lien { get; set; }
    }
}