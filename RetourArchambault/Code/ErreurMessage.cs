﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class ErreurMessage
    {
        public bool Erreur { get; set; }
        public string MessageErreur { get; set; }
    }
}