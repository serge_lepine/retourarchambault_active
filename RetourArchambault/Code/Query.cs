﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public static class Query
    {
        public static string Validation_1()
        {
            return "SELECT RTVPTY,RTVVND,RTVFLC,RTVSTS FROM mmarclib.RTVHDR WHERE RTVNUN='{0}'"; ;
        }
        public static string Validation_sku()
        {
            return "SELECT sku FROM Transaction_Store_List WHERE Lien='{0}' AND Sku='{1}'";
        }
        public static string SelectSku400()
        {
            return "SELECT inumbr FROM mmarclib.rtvdtl WHERE rtvnum='{0}' AND rtvalc > 0";
        }
        public static string Return400()
        {
            return "SELECT RTVNUM,RTTOTQ , RTTOTB FROM mmarclib.RTVHDR WHERE RTVNUN='{0}' AND RTVSTS='A' AND RTVFLC='{1}' AND RTVVND='{2}' AND RTVPTY in ('F','C', 'Z', 'B')";
        }
        public static string UpdateTransactionStore()
        {
            return "UPDATE Transaction_Store SET Auth400='{0}' ,is_complete={1},Date_Cueillette='{3}',Qty_Retourner={4},Montant_retourne={5} WHERE id={2}";
        }
        public static string UpdateRTVSaut()
        {
            return "UPDATE mmarclib.rtvsaut SET autrtvnum='{0}',autmoddate='{1}',autmodusr='ARCDOTNET', autmodpgm='RETOUR' WHERE autautcde='{2}'";
        }
        public static string InsertValidation()
        {
            return "INSERT INTO mmarclib.rtvsaut (autasnum,autstrnum,autautcde,autcredate,autmoddate,autasgflg,autcreusr,autcrepgm) VALUES ('{0}','{1}','{2}','{3}','{3}',0,'ARCDOTNET','RETOUR')";
        }
        public static string SelectValidation()
        {
            return "SELECT NoMag,Autorisation,CONVERT(time,CURRENT_TIMESTAMP) FROM Autorisation WHERE Lien='{0}'";
        }
        public static string GetDefectueuxFrom400()
        {
           return   @"SELECT rt.RTVNUM as NoRetour,rt.RTVFLC as NoMag,rdtl.INUMBR as Sku,u.IUPC as UPC,rdtl.RTVVND as NoCie,rdtl.IVNDP# as ISBN,iv.ARTIST as Auteur,COALESCE((SELECT IDESCR160 FROM mmarclib.INVDSC2  WHERE inumbr=rdtl.INUMBR),iv.IDESCR) as Titre,
                      iv.COMPSR as Compos,iv.IATRB1 as Format,iv.LABEL as Etiquette,0 as Model_Calcule,iv.ICUCST as COST,rdtl.RTVQTY as Qty_a_Retourner,(rdtl.RTVQTY * iv.ICUCST) as Montant_Retour,
                      (SELECt dpt.DPTNAM FROM mmarclib.invdpt dpt WHERE dpt.IDEPT=rdtl.IDEPT AND dpt.ISDEPT=rdtl.ISDEPT AND dpt.ICLAS=rdtl.ICLAS AND dpt.ISCLAS=rdtl.ISCLAS ) as Category,
                        rdtl.IDEPT as Dept,
                        Date(Timestamp_Format(DIGITS(rt.RTVIDT),'YYMMDD')) as Date_Creation,
                        Date(Timestamp_Format(DIGITS(rt.RTVBDT),'YYMMDD')) as Date_Modification
                          FROM mmarclib.RTVHDR  rt
                        JOIN mmarclib.RTVDTL rdtl ON rdtl.RTVNUM=rt.RTVNUM
                        JOIN mmarclib.invmst iv ON iv.INUMBR=rdtl.INUMBR
                        JOIN mmarclib.invupc u ON u.INUMBR=rdtl.INUMBR AND u.IUPPRM=1
                        WHERE  rt.RTVPTY in ('B')
                        AND rt.RTVSTS = 'W'
                        AND rt.RTVVND IN(6,7,9,10,15,36,38,40,46,50,55,56,62,67,68,94,102,103,109,117,155,166,197,227,234,341,400,469,905,1084,1159,1203,1206,1207,2250,2951,140044,11207)
                        AND rt.RTVNUM NOT IN(SELECT RTVNUM FROM mmarclib.RTVDFT)
                        ORDER BY rt.RTVVND,rt.RTVIDT,rt.RTVBDT";
        }
        public static string ClearDefectList()
        {
            return "TRUNCATE TABLE DefectList";
        }
        public static string GetDefectueux()
        {
            return @"SELECT  d.NoRetour,d.NoMag, d.NoCie,p.CieName,tt.Description_fr as TransactionName 
                    ,SUM(d.Qty_a_Retourner) as Qty
                    ,SUM(d.Montant_Retour) as Montant
                    ,CONVERT(varchar,Date_Creation,105) as Date_Creation
                    ,CONVERT(varchar,Date_Modification,105) as Date_Modification
                    FROM DefectList d
                    JOIN Provider p ON p.NoCie=d.NoCie
                    JOIN TransactionType tt ON tt.Id = 3
                    GROUP BY d.NoRetour,d.NoMag, d.NoCie,p.CieName,tt.Description_fr,Date_Creation,Date_Modification
                    ORDER BY Montant DESC";

        }
        public static string VerifQuantite()
        {
            return @"SELECT w.wiqdis,COALESCE(rpl.rfmax,'0') as Model FROM mmarclib.wintrf w
                        LEFT JOIN mmarclib.rplprf rpl 
                        ON w.wisku#=rpl.INUMBR AND w.wistor=rpl.istore
                        WHERE w.wisku#='{0}'
                        AND w.wistor={1}";
        }
        public static string VerifQuantite2()
        {
            return @"SELECT bal.ibhand,COALESCE(rpl.rfmax,'0') as Model 
                        FROM mmarclib.invbal bal
                        LEFT JOIN mmarclib.rplprf rpl  
                        ON bal.INUMBR=rpl.INUMBR AND bal.ISTORE=rpl.ISTORE
                        WHERE bal.INUMBR = '{0}'
                        AND bal.ISTORE = {1}";
        }
        public static string Update_VerifQty()
        {
            return @"UPDATE Transaction_Provider 
                        SET Qty_Dispo={0},EnStock={5},Model_Calcule={6},Amount_Dispo={7} 
                        WHERE id={1} AND Lien='{2}' AND NoCie={3} AND NoMag={4};";
        }
        public static string WebForm1_1()
        {
            return @"SELECT tp.Id,tp.NoMag,tp.Sku,tp.NoCie,tp.ISBN,tp.UPC,tp.Auteur,tp.Titre,tp.Compo,tp.Format,tp.Etiquette
                      ,tp.Cost,tp.Qty_a_Retourner,tp.ValeurRetour,tp.Dept,tp.Lien,tp.IdUser,tp.Date_Creation,tp.Model_Calcule,tp.Refuse
                      ,tp.Qty_autorise,tp.EnStock,tp.Commentaires,tp.Qty_Dispo,tp.Amount_Dispo,tt.Description_{1} as 'Desc'  FROM [Transaction_Provider] tp
                       JOIN TransactionType tt ON tt.Id = tp.idTransactionType  WHERE Lien = '{0}' ";
        }
        public static string Create_TempListeDefectueux()
        {
            return @"SELECT Id,NoMag,Sku,NoCie,ISBN,UPC,Auteur,Titre,
	                   Compos,Format,Etiquette,Model_Calcule,
	                   COST,Qty_a_Retourner,Category,Qty_a_Retourner as EnStock,
	                   3 as idTransactionType,(GETDATE() + 15) as Date_Limite,0 as Add_Sku,NoRetour  
	                   INTO [Temp_Trans_Provider_{0}]
	                   FROM DefectList
	                   WHERE  NoCie={1} AND NoRetour={2}";
        }
        public static string DeleteFromDefectList()
        {
            return @"DELETE FROM DefectList WHERE  NoCie={0}";
        }
        public static string Des_DeleteLoad()
        {
            return @"  DELETE FROM TransferLoad WHERE SKU IN(
                          SELECT DISTINCT SKU FROM DesuetList
                          WHERE Lien = '{0}' AND NoCie={1})";
        }
        public static string Create_TempDesuet()
        {
            return @" SELECT *
                          INTO [Temp_Trans_Provider_{0}]
                          FROM DesuetList
                          WHERE Lien='{0}' AND NoCie={1}
                          AND Qty_a_Retourner > 0";
        }
        public static string CreateTransactionStoreList_Type3()
        {
            return @"  INSERT INTO Transaction_Store_List
                        SELECT
                               NoMag,Sku,NoCie,ISBN,UPC,Auteur,Titre,Compo,Format,Etiquette,Cost
                              ,Qty_a_Retourner,ValeurRetour,Dept,Lien,IdUser,Date_Creation,Model_Calcule
                              ,Refuse,Qty_autorise,EnStock,Category,Qty_Dispo,idTransactionType,NoRetour
                              FROM [Transaction_Provider] 
                              WHERE Lien = '{0}'
                              ORDER BY NoMag

                              UPDATE Transaction_Link_Amem SET Section=3 WHERE Lien='{0}';
                              UPDATE Transaction_Link_Provider SET Section=3 WHERE Lien='{0}';";
        }
        public static string CreateTransactionStoreList()
        {
            return @" INSERT INTO Transaction_Store_List
                        SELECT NoMag,Sku,NoCie,ISBN,UPC,Auteur,Titre,Compo,Format,Etiquette,Cost,Qty_a_Retourner
                              ,ValeurRetour,Dept,Lien,IdUser,Date_Creation,Model_Calcule,Refuse,Qty_autorise
                              ,EnStock,Category,Qty_Dispo,idTransactionType,NoRetour
                               FROM [Transaction_Provider] 
                               WHERE Lien = '{0}'
                               AND Qty_Dispo > 0
                               ORDER BY NoMag

                           UPDATE Transaction_Link_Amem SET Section=3 WHERE Lien='{0}';
                           UPDATE Transaction_Link_Provider SET Section=3 WHERE Lien='{0}';";
        }
        public static string setUser()
        {
            return @"SELECT u.Id,u.IdCie,
                        COALESCE((SELECT CieName FROM Provider p WHERE p.NoCie=u.idCie AND is_public = 1),
                        (SELECT NomMagasin FROM Magasin m WHERE m.NoMag=u.idCie)) as CieName
                        ,u.email,u.password,u.is_active,u.is_public,u.is_store
                        FROM usr u 
                        ORDER BY idCie";
        }
        public static string setLogin()
        {
            return @"SELECT id,idCie,first_name,last_name,is_amem,is_public,is_admin,is_store
                        FROM [usr] 
                        WHERE is_active=1 
                        AND email='{0}'
                        AND [password] = '{1}'";
        }
    }
}