﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    
    public class AjoutProduitManuel
    {
        private RetourXLS m_retourXLS;
        private string m_query;
        private string m_tableName;
        private string m_date;

        public AjoutProduitManuel()
        {

        }
        public AjoutProduitManuel(RetourXLS DescriptionProduit)
        {
            this.m_retourXLS = new RetourXLS();
            this.m_retourXLS = DescriptionProduit;
            initialise();
        }
        private void initialise()
        {
            setQuery();
            setTableName();
        }
        public void addProduct()
        {
            
            FillAmemData.copyAddProduct(this.m_query,this.m_tableName);
        }
        private void setQuery()
        {
            getDate();
            this.m_query = Tool.getQuery("AddSku.sql");
            this.m_query = String.Format(this.m_query,this.m_retourXLS.UPC,this.m_retourXLS.NoCie, this.m_date);
        }
        private void setTableName()
        {
            this.m_tableName = "Temp_Trans_Provider_" + this.m_retourXLS.Lien;
        }
        private void getDate()
        {
            this.m_date = this.m_retourXLS.Date_Limite.ToString("yy-MM-dd");
            
        }
    }
}