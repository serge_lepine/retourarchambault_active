﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.IO;
using System.Globalization;
using System.Threading;

namespace RetourArchambault
{
    public class VerifQuantite
    {
        private SqlConnection conSQL;
        private OleDbConnection con400;
        private string Lien;
        private string Lang;
        private string query;
        private string verif;
        private int id = 0;
        private int noCie = 0;
        private int noMag = 0;
        private int Qty_autorise = 0;
        private int modelCalcul = 0;
        private int newQty_autorise = 0;
        private int QtyDisponible = 0;
        private string SKU = "";
        private string lien = "";
        private double cost = 0.0;
        private double newValeurRetour = 0.0;
        private string difference = "";

        public VerifQuantite()
        {
 
        }
        private void initialise()
        {
             id = 0;
             noCie = 0;
             noMag = 0;
             Qty_autorise = 0;
             modelCalcul = 0;
             newQty_autorise = 0;
             QtyDisponible = 0;
             SKU = "";
             lien = "";
             cost = 0.0;
             newValeurRetour = 0.0;
             difference = "";
        }
        public VerifQuantite(string lien,int noCie)
        {
            this.noCie = noCie;
            this.Lien = lien;
          
        }
        public VerifQuantite(string lien,string lang)
        {
            this.Lien = lien;
            Lang = lang;
            setQuery();
        }
        private void setQuery()
        {
           // this.query = Tool.getQuery("WebForm1_1.sql");
            this.query = String.Format(Query.WebForm1_1(), this.Lien, this.Lang);
        }
        public bool checkQtyType3()
        {
            Tool.setThread_EN();
            //Ouvre connexion SQL
            conSQL = Connexion.ConnectSQL();
            //Creer la commande SQL
            SqlCommand cmd = new SqlCommand(this.query, conSQL);
            SqlDataReader reader;

            try
            {
                reader = cmd.ExecuteReader();

                if(reader.HasRows)
                {
                    while(reader.Read())
                    {
                        initialise();
                        this.id = reader.GetInt32(0);
                        this.noCie = reader.GetInt32(3);
                        this.noMag = reader.GetInt32(1);
                        this.SKU = reader.GetString(2);
                        this.lien = reader.GetString(15);
                        this.Qty_autorise = reader.GetInt32(20);
                        this.cost = Convert.ToDouble(reader["Cost"].ToString());
                        this.newValeurRetour = (this.Qty_autorise * this.cost);


                        //On met à jour la base de données
                       // string upt = Tool.getQuery("Update_VerifQty.sql"); // 
                        string upt = String.Format(Query.Update_VerifQty(), this.Qty_autorise, id, lien, noCie, noMag, this.Qty_autorise, modelCalcul, newValeurRetour);
                        Tool.setThread_FR();
                        log.Log(upt);
                        Tool.setThread_EN();
                        this.verif += upt;

                    }
                    reader.Close();

                }
            }
            catch
            {
                return false;
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(conSQL);
                //Remettre le langage francais
                Tool.setThread_FR();
            }
            Connexion.execCommand(verif);
            return true;
        }
        public bool checkQty()
        {
            Tool.setThread_EN();
            //Ouvre connexion SQL
            this.conSQL = Connexion.ConnectSQL();
            //Ouvre connexion au 400
            this.con400 = Connexion.Connect();
            OleDbCommand cmd400;
        
            //Creer la commande SQL
            SqlCommand cmd = new SqlCommand(this.query, conSQL);
            SqlDataReader reader;
            int f = 0;
            try
            {
                reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        initialise();

                        f++;
                        this.id = reader.GetInt32(0);
                        this.noCie = reader.GetInt32(3);
                        this.noMag = reader.GetInt32(1);
                        this.SKU = reader.GetString(2);
                        this.lien = reader.GetString(15);
                        this.Qty_autorise = reader.GetInt32(20);
                        this.cost = Convert.ToDouble(reader["Cost"].ToString());

                        if (noCie > 0)
                        {
                            //On verifie la quantite a retourner du 400 
                            //Creation de la requete
                           // string query400 = Tool.getQuery("VerifQuantite.sql");
                            string query400 = String.Format(Query.VerifQuantite(), SKU, noMag);
                            Tool.setThread_FR();
                            log.Log(query400);
                            Tool.setThread_EN();
                            //Créer commande 400
                            cmd400 = new OleDbCommand(query400, con400);

                            //===================================================================================
                            //Valider avec Robin si seulement necessaire lors de la désuétude
                            // Si oui il faut ajout le idtransactionType dans WebForm1_1.sql et
                            //récupérer la valeur du ID dans la requete SQL et mettre en condition = 3

                            //Vérifie si retourne vide
                            if(!Connexion.execScalar400(query400,con400))
                            {
                                
                                
                                //Faire si magasins Web uniquement
                                if (noMag == 600)
                                {
                                    cmd400.Dispose();
                                    //query400 = "";
                                    //query400 = Tool.getQuery("VerifQuantite2.sql");
                                    query400 = String.Format(Query.VerifQuantite2(), SKU, noMag);
                                    cmd400 = new OleDbCommand(query400, con400);
                                }
                            }
                            //===================================================================================
                            OleDbDataReader reader400;
                            //IDataRecord record;
                            reader400 = cmd400.ExecuteReader();

                            if (reader400.HasRows)
                            {
                                reader400.Read();
                                //Nouvelle quantité disponible
                                QtyDisponible = Convert.ToInt32(reader400[0].ToString()); // (Convert.ToInt32(record[0]));
                                //Nouveau modele calculé
                                modelCalcul = Convert.ToInt32(reader400[1].ToString()); // (Convert.ToInt32(record[1]));
                            }
                            else
                            {
                                QtyDisponible = 0;
                                modelCalcul = 0;
                            }

                            //Si nouvelle quantité est supérieur à zéro
                            //Il faut déduire le nouveau model calculé

                            if (QtyDisponible > 0)
                            {
                                //Obtient la quantité net pour retour
                                newQty_autorise = (QtyDisponible - modelCalcul);
                                //Si montant négatif mettre à zéro
                                if (newQty_autorise < 0)
                                {
                                    newQty_autorise = 0;
                                }
                            }
                            else
                            {
                                newQty_autorise = 0;
                            }

                            //Si nouvelle quantité plus petit que la qty d'origine
                            //alors modifier la quantité autorisé
                            if (newQty_autorise < Qty_autorise)
                            {
                                //Message à mettre dans les commentaires
                                difference = "Nouvelle quantité ! " + (newQty_autorise - Qty_autorise).ToString();
                                newValeurRetour = (newQty_autorise * cost);

                            }
                            else
                            {
                                //Sinon on met la quantité autorisé par défault
                                newQty_autorise = Qty_autorise;
                                newValeurRetour = (newQty_autorise * cost);
                            }
                            //On met à jour la base de données
                           // string upt = Tool.getQuery("Update_VerifQty.sql"); // 
                             string upt = String.Format(Query.Update_VerifQty(), newQty_autorise,id, lien, noCie, noMag, QtyDisponible, modelCalcul, newValeurRetour);
                            Tool.setThread_FR();
                            log.Log(upt);
                            Tool.setThread_EN();
                            this.verif += upt;
                        
                            cmd400.Dispose();

                        }
                    }
                }
                Connexion.execCommand(verif);
            }
            catch (Exception e)
            {
                log.logError("VerifQuantité : " + e.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                //Fermer les connexion
                Connexion.close(con400);
                Connexion.closeSQL(conSQL);
                //Remettre le langage francais
                Tool.setThread_FR();
            }

            return true;
        }
        public void InsertAut()
        {
            //Ouvre connexion as400
            Tool.setThread_EN();
            //Ouvre connexion SQL
            this.conSQL = Connexion.ConnectSQL();
            //Ouvre connexion au 400
            this.con400 = Connexion.Connect();
            OleDbCommand cmd400;

           // string myQuery = String.Format(Query.SelectValidation(), this.Lien);
            //Creer la commande SQL
            SqlCommand cmd = new SqlCommand(String.Format(Query.SelectValidation(), this.Lien), conSQL);
            SqlDataReader reader;

            try
            {
                reader = cmd.ExecuteReader();


                while (reader.Read())
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("Autorisation")))
                    {
                        this.noMag = int.Parse(reader.GetString(0));


                        string autorisation = reader.GetString(1);


                        string newDate =  DateTime.Now.ToString("yyyy-MM-dd-HH.mm.ss.ffffff");
                        //DateTime y = new DateTime();
                        //y = Convert.ToDateTime(y);
                      //  this.Query = String.Format(Tool.getQuery("InsertValidation.sql"), this.noCie, this.noMag, autorisation);
                        cmd400 = new OleDbCommand(String.Format(Query.InsertValidation(), this.noCie, this.noMag, autorisation,newDate), this.con400);
                        cmd400.ExecuteNonQuery();
                    }



                }
            }


            catch (Exception q)
            {
                log.logError("InsertAuth ERROR :" + q.Message);
            }
            finally
            {
                Connexion.close(this.con400);
                Connexion.closeSQL(this.conSQL);
            }
        }
    }
}