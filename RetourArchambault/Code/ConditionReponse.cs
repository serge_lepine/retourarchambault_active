﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class ConditionReponse
    {
        public string condition { get; set; }
        public int qty_minus { get; set; }
        public int qty_over { get; set; }
        public int model { get; set; }
    }
}