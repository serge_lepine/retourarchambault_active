﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Text.RegularExpressions;
using RetourArchambault;
using System.Threading;
using System.Globalization;


/// <summary>
/// Description résumée de Tool
/// </summary>
public static class Tool
{
	  

    /// <summary>
    /// Création du nom de fichier
    /// </summary>
    /// <param name="amem">Numero de AMEM(No employé qui crée la demande</param>
    /// <param name="provider">Numéro du fournisseur</param>
    /// <param name="dept">Numero du département</param>
    /// <param name="index">1=nouveau 2=Retour 3=Store 4-Historique</param>
    /// <returns></returns>
    public static string createNameFile(string amem,string provider,string dept,int index)
    {
        string fileName ="";
        fileName = Tool.format_Nombre(provider, 6) + Tool.format_Nombre(amem, 6) + dept + DateTime.Now.ToString("yyMMdd"); //+ "_" + index;
        fileName = fileName.Replace(":", "");
        fileName = fileName.Replace("-", "");
        fileName = fileName.Replace(" ", "");
        return fileName;
    }
    /// <summary>
    /// Formater le numéro selon le nombre de zéro désiré
    /// </summary>
    /// <param name="text">Nombre initial</param>
    /// <param name="nbr_caracteres">Nombre désiré</param>
    /// <returns>Nouveau nombre formaté</returns>
    public static string format_Nombre(string text, int nbr_caracteres)
    {
        string m_response = null;
        text = format_symbol(text);
        int generation = Convert.ToInt32(text);
        m_response = String.Format("{0:D" + nbr_caracteres + "}", generation);
        return m_response;
    }
    /// <summary>
    /// Supprimer les caractère autre que des nombres
    /// </summary>
    /// <param name="nombre"></param>
    /// <returns></returns>
    public static string format_symbol(string nombre)
    {
        // Remplace les caractères
        try
        {
            return Regex.Replace(nombre, @"[^\w\.@-]", "");

        }
        // If we timeout when replacing invalid characters, 
        // we should return Empty.
        catch (Exception)
        {
            return String.Empty;
        }
    }
    /// <summary>
    /// Obtient le nom du fichier sans l'extension
    /// </summary>
    /// <param name="longName">Nom complet du fichier avec path</param>
    /// <returns>Nom du fichier sans path et extension</returns>
    public static string shortName(string longName)
    {
        string rep = "";

        rep = longName.Substring(longName.LastIndexOf("\\") + 1);
        rep = rep.Substring(0, rep.Length - 4);
        return rep;
    }
    /// <summary>
    /// Extrait la date du nom de fichier
    /// </summary>
    /// <param name="LaDate">Date de 6 caractere</param>
    /// <returns>La date séparé par des /</returns>
    public static string GetDate(string maDate)
    {
        maDate = maDate.Insert(2, "/");
        maDate = maDate.Insert(5, "/");
        return maDate;
    
    }
  
    /// <summary>
    /// Recherche 0-NoFournisseur 1-NoAmem 2-Departement 3-Date 4-Etape
    /// </summary>
    /// <param name="lien">Chemin du fichier sans extension</param>
    /// <returns>Tableau descriptif</returns>
    public static string[] ExtractLien(string lien)
    {
        string[] Lien = new string[5];
        Lien[0] = lien.Substring(0, 6);
        Lien[1] = lien.Substring(6, 6);
        Lien[2] = lien.Substring(12, 3);
        Lien[3] = lien.Substring(15, 6);
        Lien[4] =lien.Substring((lien.Length - 1), 1);
        return Lien;
    }
   public static String GetTimeStamp()
    {
        return DateTime.Now.ToString("yyyyMMddHHmmssffff");

    }
   public static string getQuery(string queryName)
   {
       string query = string.Empty;
       query = File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/" + queryName));
       return query;
   }
   public static string getMessage(string messageName)
   {
       string newMessage = string.Empty;
       newMessage = File.ReadAllText(HttpContext.Current.Server.MapPath("/Message/" + messageName));
       return newMessage;
   }
   public static void ErrorMail(string errorMessage)
   {
       EnvoiEmail em = new EnvoiEmail(1, 2, 5, false, errorMessage,0);

   }
    public static string formatCondition(string condition)
    {
        
        condition = condition.Replace("'", "+CHAR(39)+");
        return condition;
    }
    public static void setThread_EN()
    {
        if (Thread.CurrentThread.CurrentCulture.Name != "en-us")
        {
            //Set culture info
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-us");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-us");
            
        }
    }
    public static void setThread_FR()
    {
        if (Thread.CurrentThread.CurrentCulture.Name != "fr-CA")
        {
            //Set culture info
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-CA");
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("fr-CA");

        }
    }
    public static string searchMag(int index)
    {
        string response = "";
        switch (index)
        {
            case 1:
                response = " AND NoMag=14";
                break;
            case 3:
                response = " AND NoMag=17";
                break;
            case 4:
                response = " AND NoMag=27";
                break;
            case 5:
                response = " AND NoMag=37";
                break;
            case 6:
                response = " AND NoMag=47";
                break;
            case 7:
                response = " AND NoMag=57";
                break;
            case 8:
                response = " AND NoMag=67";
                break;
            case 9:
                response = " AND NoMag=77";
                break;
            case 10:
                response = " AND NoMag=87";
                break;
            case 11:
                response = " AND NoMag=137";
                break;
            case 12:
                response = " AND NoMag=147";
                break;
            case 13:
                response = " AND NoMag=157";
                break;
            case 14:
                response = " AND NoMag=167";
                break;
            case 15:
                response = " AND NoMag=545";
                break;
            case 16:
                response = " AND NoMag=600";
                break;
            default:
                response = "";
                break;
        }
        return response;
    }
    public static string searchMagTP(int index)
    {
        string response = "";
        switch (index)
        {
            case 1:
                response = " AND {0}NoMag=14";
                break;
            case 3:
                response = " AND {0}NoMag=17";
                break;
            case 4:
                response = " AND {0}NoMag=27";
                break;
            case 5:
                response = " AND {0}NoMag=37";
                break;
            case 6:
                response = " AND {0}NoMag=47";
                break;
            case 7:
                response = " AND {0}NoMag=57";
                break;
            case 8:
                response = " AND {0}NoMag=67";
                break;
            case 9:
                response = " AND {0}NoMag=77";
                break;
            case 10:
                response = " AND {0}NoMag=87";
                break;
            case 11:
                response = " AND {0}NoMag=137";
                break;
            case 12:
                response = " AND {0}NoMag=147";
                break;
            case 13:
                response = " AND {0}NoMag=157";
                break;
            case 14:
                response = " AND {0}NoMag=167";
                break;
            case 15:
                response = " AND {0}NoMag=545";
                break;
            case 16:
                response = " AND {0}NoMag=600";
                break;
            default:
                response = "";
                break;
        }
        return response;
    }
    public static string Right(this string str,int lenght)
    {
        return str.Substring(str.Length - lenght, lenght);
    }
    public static string Left(this string str, int lenght)
    {
        return str.Substring(0,str.Length - lenght);
    }
    public static string formatDate(DateTime myDate, string Lang)
    {
        if (myDate != null)
        {

            if (Lang.ToUpper() == "EN")
            {
                return myDate.ToString("ddd d MMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            }
            else
            {
                return myDate.ToString("ddd d MMM yyyy", CultureInfo.CreateSpecificCulture("fr-CA"));
            }
        }
        else
        {
            if (Lang.ToUpper() == "EN")
            {
                return DateTime.Now.ToString("ddd d MMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            }
            else
            {
                return DateTime.Now.ToString("ddd d MMM yyyy", CultureInfo.CreateSpecificCulture("fr-CA"));
            }

        }
    }
    public static string formatDate(DateTime myDate)
    {
        if (myDate != null)
        {  
                return myDate.ToString("d MMM yyyy", CultureInfo.CreateSpecificCulture("fr-CA"));  
        }
        else
        {
                return DateTime.Now.ToString("d MMM yyyy", CultureInfo.CreateSpecificCulture("fr-CA"));
        }
    }
    public static string creerLien(int NoCie, int index,int dept)
    {
        string Dept = dept.ToString();
        string TransactName = "";
        TransactName = NoCie.ToString().PadLeft(3, '0') + "_" + Dept + "_" + DateTime.Now.ToString("yyMMddHmmss") + "_" + index.ToString().PadLeft(2, '0');
        return TransactName;
    }
    public static DateTime ConvertDateTime(string Date)
    {
        DateTime date = new DateTime();
       
        try
        {
            DateTime.TryParse(Date, out date);

        }
        catch
        {

        }
        return date;
    }
    public static string setDate400()
    {    
        return DateTime.Now.ToString("yyMMdd");
    }
    public static string setTime400()
    {
        return DateTime.Now.ToString("HHmmss");
    }
    public static int getTransactionType(string lien)
    {
        string rep;
        rep = lien.Substring(lien.LastIndexOf("_") + 2);
        return int.Parse(rep);
    }
    public static string formatText(string texte)
    {
        string newTexte = "";
        newTexte = texte.Replace("<", "");
        newTexte = newTexte.Replace("%", "");
        newTexte = newTexte.Replace(">", "");
        return newTexte;
    }
}