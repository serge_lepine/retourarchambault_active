﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class Usr
    {
        public int id{get;set;}
        public int idCie {get;set;}
        public string first_name {get;set;}
        public string last_name{get;set;}
        public bool is_amem { get; set; }
        public bool is_admin { get; set; }
        public bool is_public { get; set; }
        public bool is_store { get; set; }
        public bool is_active { get; set; }
        public int idParam { get; set; }
        public int idCieParam { get; set; }
        public int index { get; set; } // 1-Amem 2-Provider 3- Store
        public int CieSearch { get; set; }
        public ErreurMessage erreurMessage = new ErreurMessage();
        
    }
}