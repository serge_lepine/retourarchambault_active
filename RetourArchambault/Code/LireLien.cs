﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RetourArchambault
{
    public class LireLien 
    {
        private string lien = string.Empty;
        private myLien ml;

        public LireLien()
        {
            
        }
        public LireLien(string Lien)
        {
            this.lien = Lien;
        }
        public myLien ExtraitLien()
        {
            ml = new myLien();
            var adress = this.lien.Split(new[] { "_" }, StringSplitOptions.RemoveEmptyEntries);

            ml.idCie = int.Parse(adress[0].ToString());
            ml.Dept = int.Parse(adress[1].ToString());
            ml.DateLien = adress[2].ToString();
            ml.Type = int.Parse(adress[3].ToString());

            return ml;
        }

    }
    public class myLien
    {
        public int idCie { get; set; }
        public int Dept { get; set; }
        public string DateLien { get; set; }
        public int Type { get; set; }
    }
}