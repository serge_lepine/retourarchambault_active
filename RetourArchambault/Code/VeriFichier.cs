﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data.OleDb;
using System.Reflection;
using System.Diagnostics;
using Utilitaires;

namespace ConvertToCSV
{
    class VeriFichier
    {  
        /// <summary>
        /// Copier fichier
        /// </summary>
        /// <param name="nameFile">Nom du fichier à copier</param>
        /// <param name="cheminSource">Chemin de départ</param>
        /// <param name="cheminDestination">Chemin de destination</param>
        /// <returns>Vrai ou faux</returns>
        public static string SimpleFileCopy(string nameFile,string cheminSource, string cheminDestination,string newNameFile)
        {
            string sourcePath = cheminSource;
            string targetPath = cheminDestination;

            //Manipuler nom du fichier et du répertoire
            string sourceFile = sourcePath; 
            string destinationFile = System.IO.Path.Combine(targetPath, newNameFile);
            try
            {
                
                //Création du nouveau répertoire cible si necessaire
                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                //Copie le fichier dans l'autre répertoire
                System.IO.File.Copy(sourceFile, destinationFile, true);

            }
            catch (Exception e)
            {
                log.Log(e.Message);
                destinationFile = null;
            }

            return destinationFile;
        }
        /// <summary>
        /// Supprime un seul fichier
        /// </summary>
        /// <param name="fileName">Nom du fichier à supprimer</param>
        /// <returns>True si OK</returns>
        public static Boolean deteteFile(string fileName)
        {
          
            try
            {
                System.IO.File.Delete(fileName);
            }
            catch
            {
                return false;
            }
            return true;
        }
        /// <summary>
        /// Supprime un seul fichier
        /// </summary>
        /// <param name="fileName">Nom du fichier à supprimer</param>
        /// <param name="sourcePath">Chemin du fichier</param>
        /// <returns>True si OK</returns>
        public static Boolean deteteFile(string fileName, string sourcePath)
        {
           string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
           try
           {
               System.IO.File.Delete(sourceFile);
           }
           catch 
           {
               return false;
           }
           return true;
        }
        /// <summary>
        /// Supprimer tous les fichiers
        /// </summary>
        /// <param name="sourcePath">Chemin complet du fichier</param>
        /// <returns>Vrai ou faux</returns>
        public static Boolean deleteFileInFolder(string sourcePath)
        {

            if (System.IO.Directory.Exists(sourcePath))
            {
                                  
               string[] files = System.IO.Directory.GetFiles(sourcePath);
               
                // Supprime tout les fichiers
                try
                {
                    foreach (string s in files)
                    {

                        System.IO.File.Delete(s);
                     
                    }
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }
      
       
    }
}
