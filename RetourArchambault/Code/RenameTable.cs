﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RetourArchambault
{
    public class RenameTable
    {
        private string query;
        private string newName_;
        private string oldName_;

       public RenameTable()
        {

        }
       public RenameTable(string newName,string oldName)
        {
            newName_ = newName;
            oldName_ = oldName;
        }

        public bool renameTable()
        {
            Tool.setThread_EN();
            this.query = Tool.getQuery("Class_RenameTable.sql"); 
            this.query = String.Format(this.query, this.newName_,this.oldName_);
            log.Log(query);
            Tool.setThread_FR();
            return  Connexion.execCommand(query);
           
        }
    }
}