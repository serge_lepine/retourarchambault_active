﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RetourArchambault
{
    public class CreateNewProvider
    {
        private string query;
        private string sessionValue;
        private NewTransaction nt;
    

        public CreateNewProvider()
        {
            
        }
        public CreateNewProvider(string SessionValue)
        {
            sessionValue = SessionValue;
        }
        public CreateNewProvider(NewTransaction NT)
        {
            nt = NT;
        }
        public bool createNewProvider()
        {
            this.query = Tool.getQuery("Class_CreatenewProvider.sql");
            this.query = String.Format(this.query, this.sessionValue);
            log.Log(this.query);

            return Connexion.execCommand(this.query);
        }
        public bool createAddNewProvider()
        {
            this.query = Tool.getQuery("Class_CreateAddNewProvider.sql");
            this.query = String.Format(this.query, this.sessionValue);
            log.Log(this.query);

            return Connexion.execCommand(this.query);
        }
        public bool createDesProvider()
        {
            //Transférer les données dans table temporaire
            string desTable = Query.Create_TempDesuet(); //Tool.getQuery("Create_TempDesuet.sql");
            desTable = String.Format(desTable, nt.Lien,nt.NoCie);
            log.Log(desTable);

            return Connexion.execCommand(desTable);
        }
        public bool delDesTransferLoad()
        {
            this.query = Query.Des_DeleteLoad(); //Tool.getQuery("des_DeleteLoad.sql");
            this.query = String.Format(this.query, nt.Lien, nt.NoCie);
            log.Log(this.query);

            return Connexion.execCommand(this.query);
        }
        public bool createDefectProvider()
        {
            
            //Reload avec nouvelle données.
            string queruDefect = Query.Create_TempListeDefectueux(); //Tool.getQuery("Create_TempListeDefectueux.sql");
            queruDefect = String.Format(queruDefect, nt.Lien, nt.NoCie,nt.NoRetour);
            log.Log(queruDefect);

            return Connexion.execCommand(queruDefect);
        }
       public bool deleteDefectProvider()
        {
            string queryDel = Query.DeleteFromDefectList();  //Tool.getQuery("DeleteFromDefectList.sql");
            queryDel = String.Format(queryDel, nt.NoCie);
            log.LogQuery("CreateNewProvider : " + queryDel);

            return Connexion.execCommand(queryDel);
           
        }
   
    }
    public class NewTransaction
    {
        public string Lien { get; set; }
        public DateTime Date_Limite { get; set; }
        public int NoCie { get;set; }
        public string NoRetour { get; set; }

        public NewTransaction()
        {

        }
       

    }
}