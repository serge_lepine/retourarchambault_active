﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading;
using System.Globalization;

namespace RetourArchambault
{
    public class Language
    {
        private string langue;

        public Language()
        {

        }
        public Language(string LangueSession)
        {
            langue = LangueSession;
        }
        public string getLanguage()
        {
            if (String.IsNullOrEmpty(this.langue))
            {
                return "fr";
            }
            else
            {
                return langue.Substring(0, 2);
            }
        }
    }
}