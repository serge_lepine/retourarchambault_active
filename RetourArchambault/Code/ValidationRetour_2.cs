﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.OleDb;

namespace RetourArchambault
{
    public class ValidationRetour_2
    {
        private ValidationRetour_Param vrp;
        private string inumbr;
        private ValidationRetour_Reponse response;
        private string querySelectSku;

        public ValidationRetour_2()
        {

        }
        public ValidationRetour_2(ValidationRetour_Param Vrp)
        {
            this.vrp = Vrp;
            this.response = new ValidationRetour_Reponse();
        }
        public ValidationRetour_Reponse validationRetour_2()
        {
            OleDbCommand cmd400;
            OleDbDataReader reader400;
            this.querySelectSku = String.Format(Query.SelectSku400(), this.vrp.NoRetour);

            if (!String.IsNullOrEmpty(querySelectSku))
            {
                cmd400 = new OleDbCommand(this.querySelectSku, this.vrp.Conn400);
                try
                {

                    reader400 = cmd400.ExecuteReader();

                    while (reader400.Read())
                    {
                        this.inumbr = string.Empty;
                        this.inumbr = reader400[0].ToString();
                        bool result = Connexion.execScalar(String.Format(Query.Validation_sku(), this.vrp.Lien, this.inumbr));

                        if (!result)
                        {
                            //Message indiquant que le SKU n'appartient pas à la commande
                            response.errorMessage = String.Format(Tool.getMessage("ValidationSku.txt"), inumbr);
                            response.succes = false;
                            return response;

                        }
                    }

                }
                catch (Exception ex)
                {
                    log.logError("req400 : " + ex.Message);
                    response.succes = false;
                    response.errorMessage = "Erreur requete validation 2 : " + ex.Message;
                    return response;
                }
                finally
                {

                    cmd400.Dispose();
                }
            }
            response.succes = true;
            return response;

        }
    }
    
}