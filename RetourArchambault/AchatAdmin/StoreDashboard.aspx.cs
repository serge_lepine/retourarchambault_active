﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault.AchatAdmin
{
    public partial class StoreDashboard : System.Web.UI.Page
    {
        private string StoreLien;
        private Usr usager = null;
        private string Lang;
        protected void Page_Init(object sender,EventArgs e)
        {
            this.gvStoreList.RowDataBound += new GridViewRowEventHandler(gvStoreList_RowDataBound);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            Language lang = new Language((string)Session["lang"]);
            this.Lang = lang.getLanguage();
            //Met a jour les commandes
            setMenu();
            //Obtient le numero de lien
            getParam();
            //Met en mode EN pour symbole .
            Tool.setThread_EN();
            //Met à jour les quantite et montant des retours
            UpdateAutorisation ua = new UpdateAutorisation(StoreLien);
            //Remet la culture en français
            Tool.setThread_FR();

            if (!IsPostBack)
            {
                //Lit la requête depuis le fichier 
                DataSet ds = new DataSet();
                //Ouvre connexion
                SqlConnection c_con = Connexion.ConnectSQL();

                try
                {
                    string query = Tool.getQuery("gvStoreList.sql");
                    query = String.Format(query, StoreLien,this.Lang);
                    SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
                    //Remplit dataset
                    adapter.Fill(ds);

                    //myDataTable2.GlobalDataTable = ds.Tables[0];
                    log.Log(query);
                    gvStoreList.DataSource = ds.Tables[0];
                    gvStoreList.DataBind();
                }
                catch
                {

                }
                finally
                {
                    Connexion.closeSQL(c_con);
                }
            }

        }
        private void getParam()
        {
            StoreLien = Request.Params["StoreLien"];
        }
        private void setMenu()
        {
            usager = (Usr)Session["USR"];
            //Menu 1
            this.Master.HyperLink1Text = "Tableau de bord";
            this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam;
            this.Master.HyperLink1Visible = true;
            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;
            this.Master.Lang = "fr";
            //Bonjour
         //   this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name;

        }

        protected void gvStoreList_RowDataBound(object sender, GridViewRowEventArgs e)
        {

             DataRowView drv = (DataRowView)e.Row.DataItem;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Convert.ToInt16(drv["is_complete"]) == 1)
                    {
                            e.Row.Font.Bold = true;
                            e.Row.ForeColor = System.Drawing.Color.Black;
                            e.Row.BackColor = System.Drawing.Color.FromName("#7cff1c");  
                    }
                   
                }
            }
            catch (Exception ex)
            {
                log.logError("StoreDashBoard.aspx : " + ex.Message);
            }

        }

        protected void btnAnnulation_Click(object sender, EventArgs e)
        {
            
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label Id = (Label)grv.FindControl("Label15");
            Label NoMag = (Label)grv.FindControl("Label16");
            Button btnNew = (Button)grv.FindControl("btnAnnulation");

            string query = Tool.getQuery("delStoreCueillette.sql");
            query = String.Format(query, Id.Text, NoMag.Text, this.StoreLien);
            Connexion.execCommand(query);
            btnNew.Enabled = false;
            Response.Redirect(Request.RawUrl);


        }

       
    }
}
