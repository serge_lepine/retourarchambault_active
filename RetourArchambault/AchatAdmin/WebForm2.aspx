﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="RetourArchambault.AchatAdmin.WebForm2" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Rapports</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
   <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" /> 
</head>
<body>
     <div id="header">
        <asp:Image ID="Image1" ImageUrl="~/images/lg_archambault01White.png" runat="server" meta:resourcekey="Image1Resource1" />
    </div>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
         <div id="wrapper">
            <div id="content">
                 
                <div class="container">
                    <div class="rapport_container">
                        <asp:Button ID="Button1" CssClass="btn backAction" runat="server" Text="Retour au tableau de bord" OnClick="Button1_Click" />
                        <div class="frame">
                            <div class="table_cont">
                                <h2>Proposé</h2>
                                <div class="stock_table_cont">
									<table class="stock_table">
										<tr class="even">
											<td>À retourner</td>
											<td>
												<asp:Label ID="lbl_Distinct" runat="server" Text="0" ></asp:Label></td>
										</tr>
										<tr class="odd last">
											<td>À retourner ($)</td>
											<td>
												<asp:Label ID="lblRetour_money" runat="server" Text="0.0"></asp:Label></td>
										</tr>
									</table>
                                </div>
                            </div>
                            <div class="table_cont">
                                <h2>Accepté</h2>
                                <div class="stock_table_cont lastTbl">
                                    <table class="stock_table">
                                        <tr class="even">
                                            <td>À retourner</td>
                                            <td>
                                                <asp:Label ID="lblDistinctAutorise" runat="server" Text="0"></asp:Label></td>
                                        </tr>
                                        <tr class="odd last">
                                            <td>À retourner ($)</td>
                                            <td>
                                                <asp:Label ID="lblRetourAutorise" runat="server" Text="0.0"></asp:Label></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="button_cont">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <%--<asp:Button ID="BtnSave" runat="server" Text="" CssClass="sauvegarder_btn"  OnClick="BtnSave_Click" />
                                    <asp:Button ID="btnUpdate" CssClass="update_btn" runat="server" Text="" Visible="false" OnClick="btnUpdate_Click" />
                                    <asp:Button ID="BtnSend" runat="server" CssClass="email_btn" Text=""  OnClick="BtnSend_Click" />--%>
                                    <div class="button_cont">
                                         </div>
                                    <div class="frmElmnt fTxt">
                                       <%-- <div class="fLbl">
                                            <asp:Label ID="lblSelection" runat="server" Visible="false" Text="Un avis par courriel sera envoyé pour chaque succursales :"></asp:Label>
                                        </div>--%>
                                        <div class="fWdgt"></div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div class="overlay">
                                        <div class="overlayContent">
                                            <div class="loading_msg">
                                                <img src="/images/loader.gif" alt="Loading" />
                                                <p>Création du rapport en cours....<br />
                                                ce processus peut prendre quelques minutes,<br />
                                                veuillez patienter svp...</p>
                                            </div>                                               
                                        </div>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                        <asp:SqlDataSource ID="MagasinEmail" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT [Id], [Email], [IdMagasin] FROM [Magasin_Usr]"></asp:SqlDataSource>
                        <asp:SqlDataSource ID="ProviderEmail" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT DISTINCT [Id], [Email] FROM [Magasin_Usr] WHERE ([IdMagasin] = @IdMagasin)" OnSelecting="ProviderEmail_Selecting">
                            <SelectParameters>
                                <asp:QueryStringParameter DefaultValue="0" Name="IdCie" QueryStringField="Cie" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </div>
                </div><!--END /.container-->        
     
                <div  class="result_table_cont">
                    <div style="display:inline-block; margin-right: 20px;">
                        <asp:GridView ID="GridView1"
                        runat="server"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        OnSorting="GridView1_Sorting"
                        PageSize="50"
                        AllowPaging="True"
                        AllowSorting="True"
                        OnRowEditing="GridView1_RowEditing"
                        OnRowUpdating="GridView1_RowUpdating"
                        OnRowCancelingEdit="GridView1_RowCancelingEdit" 
                        AutoGenerateColumns="False" 
                        DataKeyNames="id" OnRowDataBound="GridView1_RowDataBound">
                             <FooterStyle HorizontalAlign="Left" />
                         <PagerSettings Mode="NumericFirstLast" FirstPageText="Début" LastPageText="Fin" NextPageText="Suivante" PreviousPageText="Précédente" Position="Bottom" />
                        <RowStyle CssClass="odd" />
                                        <AlternatingRowStyle CssClass="even" />
                        <Columns>
                            <asp:TemplateField HeaderText="NoMag" SortExpression="NoMag">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SKU" SortExpression="SKU">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Sku") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoCie" SortExpression="NoCie">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ISBN" SortExpression="ISBN">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("ISBN") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UPC" SortExpression="UPC">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("UPC") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AUTEUR" SortExpression="Auteur">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Auteur") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TITRE" SortExpression="Titre">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Titre") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FORMAT" SortExpression="Format">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Format") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ÉTIQUETTE" SortExpression="Etiquette">
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Etiquette") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction">
                                <ItemTemplate>
                                    <asp:Label ID="Label17" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COST" SortExpression="Cost">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Cost","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty à Retourner" SortExpression="Qty_a_Retourner">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("Qty_a_Retourner") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valeur des Retours" SortExpression="ValeurRetour">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("ValeurRetour","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Refusé" SortExpression="Refuse">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRefuse" CssClass="inputSizeSml" runat="server" Text='<%# Eval("Refuse") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("Refuse") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty Autorisée" SortExpression="Qty_autorise">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtQtyAutorise" CssClass="inputSizeSml" runat="server" Text='<%# Eval("Qty_autorise") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("Qty_autorise") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="En Stock">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("EnStock") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Commentaires">

                                <EditItemTemplate>
                                    <asp:TextBox ID="txtCommentaires" CssClass="inputSizeLong" runat="server" Text='<%# Eval("Commentaires") %>'></asp:TextBox>
                                </EditItemTemplate>

                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("Commentaires") %>' ></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:CommandField HeaderText="Opération" ButtonType="Image" ShowEditButton="True" EditImageUrl="~/images/icon-edit.png" CancelImageUrl="~/images/icon-Cancel.png" UpdateImageUrl="~/images/icon-update.png" />--%>
                        </Columns>
                    </asp:GridView>
                    </div>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div><!--END /.content-->
            <div id="footer">
                <div id="brand">&copy; Tous droits réservés. Groupe Archambault inc., une société de Québecor Média, 1999-2014.div></div>
            </div>
        </div><!--END /.wrapper-->
    </form>
</body>
</html>
