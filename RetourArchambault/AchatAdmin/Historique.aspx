﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Historique.aspx.cs" Inherits="RetourArchambault.AchatAdmin.Historique" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <asp:Image ID="Image3" ImageUrl="~/images/imageDefault.jpg" runat="server" />

    <asp:GridView ID="gvHistorique" runat="server" BackColor="White" 
        BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" AutoGenerateColumns="False">

        <AlternatingRowStyle BackColor="#DCDCDC" />
        <Columns>
            <asp:TemplateField HeaderText="Id" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date ">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Date_Creation") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CieName">
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dept">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                </ItemTemplate>
                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lien" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty demandé">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Qty_demande") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Montant demandé">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Montant_demande","{0:C2}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty autorisé">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Qty_autorise") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Montant autorisé">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Montant_autorise","{0:C2}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Voir">
                <ItemTemplate>
                    <asp:Button ID="btnAffiche" runat="server" OnClick="btnAffiche_Click" Text="Afficher" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#0000A9" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

</asp:Content>
