﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="StoreDashboard.aspx.cs" Inherits="RetourArchambault.AchatAdmin.StoreDashboard" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
                <div class="container">
                    <div class="result_table_cont storeDashboard">
                        <%--<table>--%>
                           <asp:GridView ID="gvStoreList" runat="server" CellPadding="5" 
                               OnRowDataBound="gvStoreList_RowDataBound" DataKeyNames="Id" AutoGenerateColumns="False">
                        <%--<AlternatingRowStyle BackColor="White" />--%>
                            <RowStyle CssClass="odd" />
                            <AlternatingRowStyle CssClass="even" />
                        <Columns>
                            <asp:TemplateField HeaderText="Id" SortExpression="Id" Visible="False" >
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoMag" SortExpression="NoMag">
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Envoi" SortExpression="Date_Envoi">
                                <ItemTemplate>
                                    <asp:Label ID="Label17" runat="server" Text='<%# Eval("Date_Envoi") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No Cie" SortExpression="IdCie">
                                <ItemTemplate>
                                    <asp:Label ID="Label18" runat="server" Text='<%# Eval("IdCie") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="nom Cie" SortExpression="CieName">
                                <ItemTemplate>
                                    <asp:Label ID="Label19" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                <ItemTemplate>
                                    <asp:Label ID="Label25" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                </ItemTemplate>
                                 <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Montant" SortExpression="Montant">
                                <ItemTemplate>
                                    <asp:Label ID="Label20" runat="server" Text='<%# Eval("Montant","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Completé" SortExpression="is_complete">
                                <ItemTemplate>
                                    <asp:Label ID="Label21" runat="server" Text='<%# Eval("is_complete") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cueillette" SortExpression="Cueillette">
                                <ItemTemplate>
                                    <asp:Label ID="Label22" runat="server" Text='<%# Eval("Cueillette") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Retourné" SortExpression="Qty_Retourner">
                                <ItemTemplate>
                                    <asp:Label ID="Label23" runat="server" Text='<%# Eval("Qty_Retourner") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Montant Retourné" SortExpression="Montant_Retourne">
                                <ItemTemplate>
                                    <asp:Label ID="Label24" runat="server" Text='<%# Eval("Montant_Retourne","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annuler">
                                <ItemTemplate>
                                    <asp:Button ID="btnAnnulation" runat="server" OnClick="btnAnnulation_Click" Text="Annuler" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#e10027" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                        <%--<RowStyle BackColor="#FFFBD6" ForeColor="#333333" />--%>
                        <%--<SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />--%>
                        <SortedAscendingCellStyle BackColor="#FDF5AC" />
                        <SortedAscendingHeaderStyle BackColor="#4D0000" />
                        <SortedDescendingCellStyle BackColor="#FCF6C0" />
                        <SortedDescendingHeaderStyle BackColor="#820000" />
                    </asp:GridView>
                        
                        <%--</table>--%>
                        <div class="button_cont">
                         <%--   <asp:Button ID="Button1" CssClass="btn secondaryAction" runat="server" Text="Retour au tableau de bord" OnClick="Button1_Click" />--%>
                        
                        </div>
                    </div>
                </div>
    

</asp:Content>
