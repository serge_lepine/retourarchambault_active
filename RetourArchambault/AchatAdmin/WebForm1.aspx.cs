﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Threading;
using ConvertToCSV;

namespace RetourArchambault.Achat
{
    public partial class WebForm1 : System.Web.UI.Page
    {
     //   private int m_qty;
    //    private double m_amount;
        private string query;
        private int dept;
        private int cie;
    //    private DataSet ds1;
        private int count;
        private double amount;
     //   private int qty_over;
     //   private int qty_minus;
      //  private string m_condition;
      //  private string TransactName;
        private string Lien;
        private int idUser;
        private Usr usager = null;
      //  private string m_pathFiles;
        private int AutCount = 0;
        private double AutAmount = 0.0;
        private int m_qtyDispo = 0;
        private double m_amountDispo = 0.0;
        public string Lang;
        private int type;

        protected void Page_Load(object sender, EventArgs e)
        {
          
            
            initialise();
            Tool.setThread_FR();
           
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
         
        
            if (!IsPostBack)
            {
                //if (Session["Lien"] == null) { Session["Lien"] = Request.Params["Lien"]; }
                //if (Session["IdUsr"] == null) { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Lien"] = Request.Params["Lien"]; }
                { Session["IdUsr"] = int.Parse(Request.Params["usr"]); }
                //Vérifie si on affiche bouton envoi
                if (Request.Params["Temp"] == "1") { BtnSend.Visible = true; }
                getParam();

                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    BinData();

                }
                else
                {
                    if (usager.is_admin)
                    {
                        Response.Redirect("Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                    }
                    else
                    {
                        Response.Redirect("Achat/Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                    }

                }
            }
        }
        private void BinData()
        {
            //Initialise les paramètres fournit par l'url
            getParam();
          
            //Requete pour vérifier quantité en date du jour
         //   VerifQuantite vf = new VerifQuantite(this.Lien);
            
            //Lit la requete stockée
            query = Tool.getQuery("WebForm1_1.sql"); 
            //Insère les paramètrews dans la requête
            query = String.Format(query, this.Lien,this.Lang);
            log.Log(query);
         
            setData(query);

        }
        private void setData(string query)
        {
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            //Ouvre connexion
            SqlConnection c_con = Connexion.ConnectSQL();
            SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
            try
            {
                //Remplit dataset
                adapter.Fill(ds);
                count = 0;
                amount = 0.0;
                Session["dtwf1"] = ds.Tables[0];
               
                setTopSection();
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }

        }
        void setTopSection()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtwf1"];
            //initialise Qty et montant
            foreach (DataRow row in dt.Rows)
            {
                int tempCount = 0;
                double tempAmount = 0.0;
                int qtyDispo = 0;
                double amountDispo = 0.0;

               
                count += int.Parse(row[12].ToString());
                amount += double.Parse(row[13].ToString());
                if (String.IsNullOrEmpty(row[20].ToString())) { tempCount = 0; } else { tempCount = int.Parse(row[20].ToString()); }
                 if (String.IsNullOrEmpty(row[23].ToString())) { qtyDispo = 0;} else {qtyDispo = int.Parse(row[23].ToString());}
                 if (String.IsNullOrEmpty(row[24].ToString())) { amountDispo = 0.0; } else { amountDispo = double.Parse(row[24].ToString()); }

                if (tempCount != 0)
                {

                    //Faut calculer le montant selon la quantité autorisé
                    tempAmount = (tempCount * double.Parse(row[11].ToString()));
                }
                else
                {
                    //Sinon on ajoute le montant prévu
                    tempAmount = double.Parse(row[13].ToString());
                    //On met à zéro la quantité et le montant de la ligne si annulé
                    if (!String.IsNullOrEmpty(row[19].ToString()) || tempCount == 0)
                    {
                        tempAmount = 0;
                        tempCount = 0;
                    }
                   
                }
                //Compteur pour montant autorisé
                AutCount += tempCount;
                AutAmount += (tempAmount);
                //Compteur pour montant disponible
                this.m_qtyDispo += qtyDispo;
                this.m_amountDispo += amountDispo;
            }
            getParam();
            //Initialise le textbox 
            lbl_Distinct.Text = count.ToString();
            lblRetour_money.Text = String.Format("{0:C2}", amount);
            lblDistinctAutorise.Text = AutCount.ToString();
            lblRetourAutorise.Text = String.Format("{0:C2}", AutAmount);
            lblQtyDispo.Text = this.m_qtyDispo.ToString();
            lblMontantDispo.Text = String.Format("{0:C2}",this.m_amountDispo);


            log.Log("Bonjour");
            //Update Transaction_Link_Provider
            Tool.setThread_EN();
           
            if(this.m_qtyDispo != 0)
            {
                string query = Tool.getQuery("Update_ProviderLinkProvider.sql");
                query = String.Format(query, this.m_qtyDispo, this.m_amountDispo, this.Lien);
                Tool.setThread_FR();
              
                log.Log(query);
                Tool.setThread_EN();
            
                Connexion.execCommand(query);
            }
            Tool.setThread_FR();
        
            if (this.cie == 7)
            {
                GridView1.PageSize = 50;
            }
            else
            {
                GridView1.PageSize = 50;
            }

            if (Session["Sort"] != null)
            {
                DataView dv = dt.DefaultView;
                dv.Sort = Session["Sort"].ToString();
                GridView1.DataSource = dv;
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }


        }
        void initialise()
        {
          //  m_amount = 0.0;
          //  m_qty = 0;
            query = "";
         //   ds1 = null;
         //   qty_over = 0;
         //   qty_minus = 0;
        //    m_condition = "";
        //    TransactName = "";
            idUser = 0;
        }
        void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            //Initilise variable selon parametre url
            if (Session["Lien"] != null) { this.Lien = (string)Session["Lien"]; }

            LireLien gl = new LireLien(this.Lien);
            myLien l = gl.ExtraitLien();
            this.cie = l.idCie; //int.Parse(this.Lien.Substring(0, 3));
            this.dept = l.Dept;
            this.type = l.Type;
            //Identification de l'utilisateur
            if (Session["USR"] != null)
            {
                usager = (Usr)Session["USR"];
                usager.idCieParam = usager.idCie;
                idUser = usager.id;
            }
            if (Session["IdUsr"] != null) { usager.idParam = (int)Session["IdUsr"]; }
            usager.index = 1;

        }
        
        private void setGrid()
        {
            //Met a jour la section du haut
            setTopSection();
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtwf1"];
            //Met a jour la grille
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            setGrid();
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            TextBox txtRefuse = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtRefuse");
            TextBox txtQty = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtQtyAutorise");
            TextBox txtCommentaires = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtCommentaires");

            if (String.IsNullOrEmpty(txtQty.Text)) { txtQty.Text = "0"; }
            if (txtRefuse.Text.Length != 0) { txtQty.Text = "0"; }
            if (String.IsNullOrEmpty(txtCommentaires.Text)) { txtCommentaires.Text = ""; }

            UpdateCommand(id, int.Parse(txtQty.Text), txtRefuse.Text,txtCommentaires.Text);
            GridView1.EditIndex = -1;
            BinData();

        }
        private void UpdateCommand(int id, int Qty, string refuse,string Commentaires)
        {
            getParam();

            //Creation de la requete
            string query = Tool.getQuery("Update_WebForm1.sql");  //UPDATE [Transaction_Amem_" + this.Lien + "] SET Qty_autorise=" + Qty + ", Refuse='" + refuse + "', Commentaires='"+ Commentaires +"'  WHERE id=" + id;
            query = String.Format(query, Qty, refuse, Commentaires, id);
            //Creation de la commande
            Connexion.execCommand(query);
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            setGrid();
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            setGrid();
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = (DataTable)Session["dtwf1"];
            //Crée un DataView
            DataView dv = new DataView(dt);
            //Trie le Dataview
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression.ToString());
                Session["Sort"] = dt.DefaultView.Sort;
                setGrid();
            }
        }
        /// <summary>
        /// Order de trie des colonnes
        /// </summary>
        /// <param name="column">Nom de la colonne à trier</param>
        /// <returns>Ordre de la direction du trie</returns>
        private string GetSortDirection(string column)
        {
            // Ordre croissance par défaut.
            string sortDirection = "ASC";
            // Recherche le dernier ordre de croissance.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                //Vérifie si c"est la meme colonne qui doit être triée
                //Sinon la valeur par defaut peut être retourné
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }

            // Sauvegarde le dernier resultat dans le ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }
        protected void GridView1_DataBound(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Convert.ToInt32(drv["Qty_a_Retourner"]) != Convert.ToInt32(drv["Qty_autorise"]))
                    {
                        if (!String.IsNullOrEmpty(drv["Refuse"].ToString()))
                        {
                            e.Row.Font.Bold = true;
                            e.Row.ForeColor = System.Drawing.Color.White;
                            e.Row.BackColor = System.Drawing.Color.Blue;
                        }
                        else if (!String.IsNullOrEmpty(drv["Commentaires"].ToString()))
                        {
                            e.Row.Font.Bold = true;                            
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                        }
                 
                        else
                        {
                            e.Row.Font.Bold = true;
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                        }

                    }
                    else if (!String.IsNullOrEmpty(drv["Commentaires"].ToString()))
                    {
                        e.Row.Font.Bold = true;
                        e.Row.BackColor = System.Drawing.Color.Yellow;
                    }
                 
                    e.Row.Cells[14].ToolTip = "Modifier";
                    if (e.Row.RowState == DataControlRowState.Edit || e.Row.RowState.ToString() == "Alternate, Edit")
                    {
                        int i = 0;
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            if (e.Row.Cells.GetCellIndex(cell) == 14)
                            {
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[14].Controls[0])).ToolTip = "Modifier ou Refuser";
                                ((System.Web.UI.LiteralControl)(e.Row.Cells[14].Controls[1])).Text = "&nbsp;";
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[14].Controls[2])).ToolTip = "Fermer modification";
                            }
                            i++;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }

        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            //Cacher le bouton Save
            BtnSave.Visible = false;
            //Rendre visible label de sélection
         //   lblSelection.Visible = true;
            // Rendre visible dropdownlist
            //  DropDownEmailStore.Visible = true;
            // Rendre visible le bouton send
            BtnSend.Visible = true;


        }
     
        protected void BtnSend_Click(object sender, EventArgs e)
        {
            getParam();

         
            //Mettre à jour Amem_Link
            Update_TransLink ul = new Update_TransLink();
            ul.upDate_TransLink(this.Lien, 3);
            // Préparer l'envoi courriel

         
            //Créer un lien dans le Transaction Store
            CreateTransactionStore cts = new CreateTransactionStore();
            bool rep = cts.createTransactionStore(this.Lien);
            //Enregistrer dans la table transaction en cours pour la liste de Amem afin qu'il puisse suivre les transactions complété ou non

            if (rep)
            {
                //Enregistrer dans Transaction_Store_List
                CreateTransactionStoreList ctsl = new CreateTransactionStoreList();
                bool cs = ctsl.createTransactionStoreList(this.Lien);

                if (cs)
                {
                    EnvoiEmail em = new EnvoiEmail(this.Lien, this.cie, Tool.getTransactionType(this.Lien));
                    Response.Redirect("dashboard_v2.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                }
            }
            else
            {
                if (usager.is_admin)
                {
                    Response.Redirect("Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                }
                else
                {
                    Response.Redirect("Achat/Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                }
            }


        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void ProviderEmail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //Si type transaction = 3 alors vérification avec statut Defectueux.
            //Obtenir statut

            getParam();
            bool rep = false;
            string ButtonSend = "0";
            int type = Connexion.getTransactionType("Transaction_Provider WHERE Lien='" + this.Lien + "'");
            //Si TransactionType = 3 alors aucune vérification nécessaire
          
                VerifQuantite vq = new VerifQuantite(this.Lien, this.Lang);
                try
                {
                    //Si pas type 3 defectueux
                    if (type != 3)
                    {
                        rep = vq.checkQty();
                        //affiche le bouton envoi
                        if (rep) { ButtonSend = "1"; }
                    }
                    else
                    {
                        //Type defectueux
                        rep = vq.checkQtyType3();
                        if (rep) { ButtonSend = "1"; }
                    }

                    //Si OK enregistrer dans AS400 dans RTVSAUT
                    vq = new VerifQuantite(this.Lien, this.cie);
                    vq.InsertAut();
           
                }
                catch (TimeoutException te)
                {
                    log.logError(" Update TimeOut : " + te.Message + " StacTrace : " + te.StackTrace);
                }
                catch (Exception ex)
                {
                    log.logError(" Update Exception : " + ex.Message + " StacTrace : " + ex.StackTrace);
                }
                finally
                {
                   
                    Response.Redirect("WebForm1.aspx?Temp=" + ButtonSend + "&usr=" + usager.id + "&Lien=" + this.Lien);
                }

           
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            getParam();
            Response.Redirect("dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam);
        }
    }
}