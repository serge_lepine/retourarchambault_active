﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using ConvertToCSV;
using System.IO;
using System.Globalization;
using System.Threading;

namespace RetourArchambault.Achat
{
    public partial class Amem : System.Web.UI.Page
    {
        private DataTable m_dt;
        private ZoneRepeater zr;
        private int id_Usr;
        private Usr usager = new Usr();
        private string m_lien;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Thread.CurrentThread.CurrentCulture.Name != "fr-CA")
            {
                //Set culture info
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-CA");
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("fr-CA");
            }
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
            usager = (Usr)Session["USR"];
            usager.idCieParam = int.Parse(Request.Params["Cie"]);
            usager.idParam = int.Parse(Request.Params["Usr"]);
            usager.index = 1;
            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {

              
                DroitAcces da = new DroitAcces();

                if (da.VerifAcces(usager))
                {
                    //Initialise les variables local
                    initialise();
                    //Créer dropdownlist pour creation 1- nom des provider 2- departement 3- Provider usr selon provider
                    SetInitialRow();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        private void initialise()
        {
            m_dt = null;
            zr = null;
            id_Usr = usager.id;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            bool fdata = false;
            //Création de la nouvelle table temporaire
            CreateNewProvider cnp = new CreateNewProvider();
            fdata = cnp.createNewProvider(Session.SessionID);
            //On extrait les données du as400 et on 
            // les transfert dans une table temporaire
            string param1 = DropDownCie.SelectedItem.Value;
            string param2 = DropDownDept.SelectedItem.Value;
            int excDays;
            if (txtExclu.Text != "") { excDays = int.Parse(txtExclu.Text); } else { excDays = 0; }
            string datePeriode = Dateperiod(excDays);// Selon textbox
            m_lien = Tools.setTransactName(int.Parse(param1), int.Parse(param2), 1);
            System.Threading.Thread.Sleep(3000);
            string query = File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/Over_MV_01.sql"));
            query = String.Format(query, param1, param2, datePeriode, "");
            log.Log(query);

            if (fdata != false)
            {
                try
                {
                    fdata = FillAmemData.copyTable(query, "Temp_Trans_Provider_", m_lien, Session.SessionID);

                }
                catch
                {

                }
            }

            if (fdata)
            {
                //Appel nouvelle page ave paramçtres dans URL
                string pathURL = setURL(); Response.Redirect(pathURL);
            }
            else { Response.Redirect("Amem.aspx"); }

        }
        /// <summary>
        /// Création de l'url de la page AmemView.aspx
        /// </summary>
        /// <returns>L'URL complet pour l'appel de la page AmemView.aspx</returns>
        private string setURL()
        {
           
            usager.CieSearch = int.Parse(DropDownCie.SelectedItem.Value);
            string p = "AmemView.aspx?CieSearch=" + usager.CieSearch + "&Cie=" + usager.idCie + "&Dept=" + DropDownDept.SelectedItem.Value + "&Usr=" + usager.id + "&Lien=" + m_lien;
            return p;
        }
        /// <summary>
        /// Remplit les sections
        /// </summary>
        private void SetInitialRow()
        {
            //Inscrit le nom dans en-tête
            lblBonjour.Text = "Bonjour " + usager.first_name + " " + usager.last_name;


            //Section complete

            //m_dt = new DataTable();
            //m_dt.Clear();
            //zr = new ZoneRepeater();
            //m_dt = zr.zoneRepeater(1, "Transaction_Link_Amem", id_Usr);
            //ViewState["Complete"] = m_dt;

            //Repeat_1.DataSource = m_dt;
            //Repeat_1.DataBind();

            //Section complete

            m_dt = new DataTable();
            m_dt.Clear();
            zr = new ZoneRepeater();
            m_dt = zr.zoneRepeater(2, "Transaction_Link_Amem", id_Usr);
            ViewState["Complete"] = m_dt;

            Repeat_2.DataSource = m_dt;
            Repeat_2.DataBind();

            //Historique

            m_dt = new DataTable();
            m_dt.Clear();
            zr = new ZoneRepeater();
            m_dt = zr.zoneRepeater(3, "Transaction_Link_Amem", id_Usr);

            ViewState["Historique"] = m_dt;

            Repeat_3.DataSource = m_dt;
            Repeat_3.DataBind();
        }

        /// <summary>
        /// Calcul la dernière période
        /// </summary>
        /// <returns>Date du jour moins 3 mois</returns>
        private string Dateperiod(int nbrJour)
        {
            DateTime d = DateTime.Now;
            if (nbrJour == null || nbrJour == 0)
            {
                //Si vide on instancie a 90 jours
                d = d.AddDays(-90);
            }
            else
            {
                d = d.AddDays(-nbrJour);
            }

            return d.ToString("yyMMdd");
        }
    }
}