﻿//------------------------------------------------------------------------------
// <généré automatiquement>
//     Ce code a été généré par un outil.
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </généré automatiquement>
//------------------------------------------------------------------------------

namespace RetourArchambault.AchatAdmin {
    
    
    public partial class HistoriqueStore {
        
        /// <summary>
        /// Contrôle Image3.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image Image3;
        
        /// <summary>
        /// Contrôle gvHistoriqueStore.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gvHistoriqueStore;
        
        /// <summary>
        /// Propriété Master.
        /// </summary>
        /// <remarks>
        /// Propriété générée automatiquement.
        /// </remarks>
        public new RetourArchambault.Maitre Master {
            get {
                return ((RetourArchambault.Maitre)(base.Master));
            }
        }
    }
}
