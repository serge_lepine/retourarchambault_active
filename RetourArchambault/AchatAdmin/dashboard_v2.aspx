﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard_v2.aspx.cs" Inherits="RetourArchambault.AchatAdmin.dashboard_v2" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-ca">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dashboard</title>
    
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />
   
</head>
<body class="dashboard">
    <div id="header">
        <asp:Image ID="entete" ImageUrl="~/images/lg_archambault01White.png" AlternateText="Archambault" runat="server" />
    </div>
    <div id="wrapper">
        <div id="content">
            <form id="form1" runat="server">
                <div class="container">
                    <div class="left_menu_content">
                        <ul>
                            <li class="odd">
                                <asp:HyperLink ID="hlDemande" runat="server">Nouvelle demande</asp:HyperLink></li>
                            <li>
                                <asp:HyperLink ID="hlHistorique" runat="server">Historique</asp:HyperLink></li>
                            <li class="odd">
                                <asp:HyperLink ID="hlDesuetude" runat="server">Discontinué</asp:HyperLink></li>
                            <li>
                                <asp:HyperLink ID="hlDefectueux" runat="server">Défectueux</asp:HyperLink></li>
                            <li class="odd">
                                <asp:HyperLink ID="hlTraitementLot" runat="server">Traitement en lot</asp:HyperLink>
                            </li>
                          <%--  <li>
                                <asp:HyperLink ID="hlGestion" runat="server">Administration</asp:HyperLink>
                            </li>--%>
                            <li>
                                <asp:HyperLink ID="hlQuit"  runat="server">Quitter</asp:HyperLink></li>
                        </ul>
                    </div>
                    <div class="rapport_container">
                          <div class="subContainer">
                            <div class="frame">
                                <h2 class="title">Sauvegardé non complété</h2>
                                <div class="dashboard_table_cont">
                                                <asp:Image ID="Image6" ImageUrl="~/images/imageDefault.jpg" runat="server" />
                                                <%--<table class="dashboard_table auto-style2" border="0" >--%>
                                        <asp:GridView ID="gvNonComplete"
                                        runat="server" 
                                        CellPadding="4" 
                                        GridLines="None" AutoGenerateColumns="False" Width="789px" CssClass="dashboard_table" Visible="False" ForeColor="#333333">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" SortExpression="Id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Transaction" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLien1" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date" SortExpression="datesave">
                                            <ItemTemplate>
                                                <asp:Label ID="Label16" runat="server" Text='<%# Eval("datesave") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("IdCie") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cie Nom" SortExpression="CieName">
                                            <ItemTemplate>
                                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                            <ItemTemplate>
                                                <asp:Label ID="Label17" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Voir">
                                            <ItemTemplate>
                                                <asp:Button ID="btnNonComplete" OnClick="btnNonComplete_Click" runat="server" Text="Afficher" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Supprimer">
                                            <ItemTemplate>
                                                <asp:Button ID="btnDelete" runat="server" Text="Supprimer" OnClick="btnDelete_Click" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EFF3FB" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                </asp:GridView>
                                                <%--</table>--%>
                                </div>
                            </div> <div class="clearer"></div>
                            </div>
                        <div class="subContainer">
                            <div class="frame">
                                <h2 class="title">Fournisseur en attente</h2>
                                <div class="dashboard_table_cont">
                                                <asp:Image ID="Image3" ImageUrl="~/images/imageDefault.jpg" runat="server" />
                                                <%--<table class="dashboard_table auto-style2" border="0" >--%>
                                        <asp:GridView ID="gvFournisseur1"
                                        runat="server" 
                                        BackColor="White" 
                                        BorderColor="#999999" 
                                        BorderStyle="None" 
                                        BorderWidth="1px" 
                                        CellPadding="3" 
                                        GridLines="Vertical" AutoGenerateColumns="False" Width="789px" CssClass="dashboard_table" Visible="False">
                                    <AlternatingRowStyle BackColor="Gainsboro" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" SortExpression="Id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Transaction" SortExpression="Lien" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cie Nom" SortExpression="CieName">
                                            <ItemTemplate>
                                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dept" SortExpression="Dept">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                            <ItemTemplate>
                                                <asp:Label ID="Label18" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Création" SortExpression="DateCreation">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("DateCreation") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty Demandé" SortExpression="Qty_demande">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("Qty_demande") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Montant demandé" SortExpression="Montant_demande">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("Montant_demande","{0:C2}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiration" SortExpression="Date_Limite">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("DateLimite") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Complété" SortExpression="is_complete" Visible="false">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" Enabled="false" runat="server" Checked='<%# Eval("is_complete") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty Autorisé" SortExpression="Qty_autorise">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("Qty_autorise") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Montant autorise" SortExpression="MontantAutorise">
                                            <ItemTemplate>
                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("Montant_Autorise","{0:C2}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Transaction" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Voir">
                                            <ItemTemplate>
                                                <asp:Button ID="btnProvider" OnClick="btnProvider_Click1" runat="server" Text="Afficher" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                </asp:GridView>
                                                <%--</table>--%>
                                </div>
                            </div> <div class="clearer"></div>
                            </div>
                          <div class="subContainer">
                            <div class="frame">
                                <h2 class="title">Fournisseur complété</h2>
                                <div class="dashboard_table_cont">
                                                <asp:Image ID="Image1" ImageUrl="~/images/imageDefault.jpg" runat="server" />
                                                <%--<table class="dashboard_table auto-style2" border="0" >--%>
                                        <asp:GridView ID="gvFournisseur2"
                                        runat="server" 
                                        BackColor="White" 
                                        BorderColor="#999999" 
                                        BorderStyle="None" 
                                        BorderWidth="1px" 
                                        CellPadding="3" 
                                        GridLines="Vertical" AutoGenerateColumns="False" Width="789px" CssClass="dashboard_table" Visible="False">
                                    <AlternatingRowStyle BackColor="Gainsboro" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Id" SortExpression="Id" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Transaction" SortExpression="Lien" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cie Nom" SortExpression="CieName">
                                            <ItemTemplate>
                                                <asp:Label ID="Label15" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dept" SortExpression="Dept">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                            <ItemTemplate>
                                                <asp:Label ID="Label19" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Création" SortExpression="DateCreation">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("DateCreation") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty Demandé" SortExpression="Qty_demande">
                                            <ItemTemplate>
                                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("Qty_demande") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Montant demandé" SortExpression="Montant_demande">
                                            <ItemTemplate>
                                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("Montant_demande","{0:C2}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiration" SortExpression="Date_Limite">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("DateLimite") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Complété" SortExpression="is_complete">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox1" Enabled="false" runat="server" Checked='<%# Eval("is_complete") %>' />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Qty Autorisé" SortExpression="Qty_autorise">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("Qty_autorise") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Montant autorise" SortExpression="MontantAutorise">
                                            <ItemTemplate>
                                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("Montant_Autorise","{0:C2}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No Transaction" Visible="False">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Voir">
                                            <ItemTemplate>
                                                <asp:Button ID="btnProvider" OnClick="btnProvider_Click" runat="server" Text="Afficher" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                    <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                    <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#000065" />
                                </asp:GridView>
                                                <%--</table>--%>
                                </div>
                            </div>
                        <%--    <div class="frame">--%>
                                <%-- <img src="../images/imageDefault.jpg" />--%>
                                <asp:Image ID="Image4" Visible="false" ImageUrl="~/images/imageDefault-2.jpg" runat="server" />
                             <%--   <asp:Chart ID="Chart4" runat="server" Height="150px" Width="389px" Visible="false" DataSourceID="SqlDataSource1" BackColor="Navy" BackGradientStyle="DiagonalLeft">
                                    <Series>
                                        <asp:Series Name="Series1" XValueMember="CieName" YValueMembers="Montant_demande">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SWPPIXWEB02ConnectionString %>" SelectCommand="SELECT p.CieName,tlp.Montant_demande FROM Transaction_Link_Provider tlp, Provider p
      WHERE p.NoCie = tlp.NoCie
      AND tlp.Section=2"></asp:SqlDataSource>--%>
                               
                           
                            <%--</div>--%>
                            <div class="clearer"></div>
                        </div><!-- END .subContainer/Fournisseur-->
                        <div class="subContainer">
                            <div class="frame">
                                <h2 class="title">Magasin</h2>
                                <div class="dashboard_table_cont">                
                                   <asp:Image ID="Image2" ImageUrl="~/images/imageDefault.jpg" runat="server" />
                                    <asp:GridView ID="gvStore1" runat="server" AutoGenerateColumns="False" 
                                    CellPadding="4" ForeColor="#333333" Width="789px" CssClass="dashboard_table" Visible="False" DataKeyNames="Lien">
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No Transaction" Visible="true">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStoreLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Envoi" SortExpression="Date_Envoi">
                                            <ItemTemplate>
                                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("Date_Envoi") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cie Nom" SortExpression="CieName">
                                            <ItemTemplate>
                                                <asp:Label ID="Label12" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                            <ItemTemplate>
                                                <asp:Label ID="Label20" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Montant" SortExpression="Montant">
                                            <ItemTemplate>
                                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("Montant","{0:C2}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiration" SortExpression="Date_Limite">
                                            <ItemTemplate>
                                                <asp:Label ID="Label14" runat="server" Text='<%# Eval("Date_Limite") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Complété" SortExpression="Tot">
                                            <ItemTemplate>
                                                <asp:Label ID="Label21" runat="server" Text='<%# Eval("Tot") %>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Voir">
                                            <ItemTemplate>
                                                <asp:Button ID="btnStore1" runat="server" OnClick="btnStore1_Click" Text="Afficher" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                                    <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                    <SortedDescendingHeaderStyle BackColor="#820000" />
                                </asp:GridView>
                                    <%--</table>--%>
                                </div>
                            </div>
                          <%--  <div class="frame">--%>
                                <%-- <img src="../images/imageDefault-2.jpg" />--%>
                                <asp:Image ID="Image5" Visible="false" ImageUrl="~/images/imageDefault-2.jpg" runat="server" />
                           <%--     <asp:Chart ID="Chart5" runat="server" 
                                    Height="150px" Width="389px" BackColor="Firebrick" 
                                    BackGradientStyle="DiagonalLeft" Visible="false" DataSourceID="SqlDataSource2">
                                    <Series>
                                        <asp:Series Name="Series1" XValueMember="CieName" YValueMembers="Montant">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                        
                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:SWPPIXWEB02ConnectionString %>" SelectCommand="SELECT ts.CieName,(SELECT SUM(tls.ValeurRetour) FROM Transaction_Store_List tls WHERE tls.Lien=ts.Lien) as 'Montant'
    FROM Transaction_Store ts
    GROUP BY ts.CieName,ts.Lien"></asp:SqlDataSource>--%>
                        
                           <%-- </div>--%>
                            <div class="clearer"></div>
                        </div><!-- END .subContainer/Magasin-->
                    </div>               
                </div><!-- END .container-->
            </form>
        </div><!-- END .content-->
    </div><!-- END .wrapper-->
</body>
</html>
