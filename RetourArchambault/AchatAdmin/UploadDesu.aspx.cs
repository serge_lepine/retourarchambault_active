﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace RetourArchambault.AchatAdmin
{
    public partial class UploadDesu : System.Web.UI.Page
    {
        private Usr usager = new Usr();
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Contents.Remove("sort");
            Session.Contents.Remove("sort");
            Session.Contents.Remove("Date_Limite");
            Session.Contents.Remove("RefNumber");
            Session.Contents.Remove("RETOUR");
              //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {
               
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }

                getParam();

                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                   
                    //Créer dropdownlist pour creation 1- nom des provider 2- departement 3- Provider usr selon provider
                    setMenu();
                  
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = (int)Session["Cie"]; }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            if (Session["CieSearch"] != null) { usager.CieSearch = (int)Session["CieSearch"]; }
            
            usager.index = 1;
        }
        private void setMenu()
        {
            getParam();
            //Menu 1
            this.Master.HyperLink1Text = "Tableau de bord";
            this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr="+usager.idParam+"&Cie="+usager.idCieParam;
            this.Master.HyperLink1Visible = true;

            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;

            this.Master.Lang = "fr";
         
        }
      

        protected void BtnUpLoad_Click(object sender, EventArgs e)
        {
            getParam();
            NewTransaction nt = new NewTransaction();
            Session["RETOUR"] = "Desuet";
            //Get path from web.config file to upload
            string FilePath =  "/Temp/"; 
            string filename = string.Empty;
            string upc = string.Empty;
            string p = string.Empty;
          //  int NoCie = 0;
            int Dept = 0;
     //       DateTime date_limite = new DateTime();
            //Vérifie si le Upload n'est pas vide
            if (FileUploadToServer.HasFile)
            {
                try
                {
                    string[] allowdFile = { ".xls", ".xlsx" };
                    //On verifie si Excel File
                    string FileExt = System.IO.Path.GetExtension(FileUploadToServer.PostedFile.FileName);
                    //Verifie extension
                    bool isValidFile = allowdFile.Contains(FileExt);
                    if (!isValidFile)
                    {
                        lblMsg.ForeColor = System.Drawing.Color.Red;
                        lblMsg.Text = "Seul les fichiers Excel sont accepté";
                    }
                    else
                    {
                        // Obtient la grosseur du fichier
                        int FileSize = FileUploadToServer.PostedFile.ContentLength;
                        //Limite le fichier à maximum 3MB
                        //if (FileSize <= 3145728)//1048576 byte = 1MB
                        //{
                            //Obtient le nom du fichier
                            filename = Path.GetFileName(Server.MapPath(FileUploadToServer.FileName));

                            //Sauvegarde le fichier sur le serveur
                            FileUploadToServer.SaveAs(Server.MapPath(FilePath) + filename);

                            //Obtient le nom du chemin
                            string filePath = Server.MapPath(FilePath) + filename;

                            //Instancie Classe pour lecture du fichier Uploader
                            XLS readXLS = new XLS(filePath, FileExt);

                            //Instancie la Classe pour la réponse de la lecture du fichier Uploader
                            RetourXLS response = new RetourXLS();

                            //Extrait les données du fichier Uploader
                            response = readXLS.getUPC();
                          
                            //Si UPC non vide alors Lance requete dans as400
                            if (!String.IsNullOrEmpty(response.UPC) || response.NoCie != 0)
                            {
                                Session["CieSearch"] = response.NoCie;
                                Session["Dept"] = Dept;
                                //string query = Tool.getQuery("des_Upload.sql");
                                //string Lien = Tool.setLienDes(response.NoCie, 2, 500);
                                //query = String.Format(query, response.UPC, response.NoCie, Lien, response.Date_Limite);
                                //log.Log(query);
                               //Transfert les données vers table SQL
                              //  bool transfer = FillAmemData.copyDesuetude(query, "DesuetList");
                               //Afficher message succes ou echec
                                if (FillAmemData.copyDesuetude(readXLS.setQuery(),"DesuetList")) 
                                {
                                  //  int Dept = Connexion.getDept(Lien);
                                   
                                    lblMsg.Text = "Fichier transféré avec succès ! <br> Préparation de votre grille de travail."; 
                                    //Créer pause de 2 secondes
                                    System.Threading.Thread.Sleep(200);
                                    //Créer table temporaire
                                    nt.Lien = readXLS.getLien();
                                   // nt.Dept = Dept;
                                    nt.NoCie = response.NoCie;
                                    Session["Date_Limite"] = response.Date_Limite;
                                    Session["RefNumber"] = response.NoAvis;
                                    CreateNewProvider cp = new CreateNewProvider(nt);
                                   
                                    if(cp.createDesProvider())
                                    {
                                        //if (cp.delDesTransferLoad())
                                        //{
                                            //Créer URL pour redirection
                                            p = "../Achat/AmemView.aspx?CieSearch=" + response.NoCie + "&Cie=" + usager.idCie + "&Dept=" + Dept.ToString() + "&Usr=" + usager.id + "&Lien=" + nt.Lien;

                                      //  }
                                    }
                                   
                                } 
                                else
                                { 
                                    lblMsg.Text = "Erreur lors du transfert du fichier !"; 
                                }
                            }
                      //  }
                        else
                        {
                            lblMsg.Text = "Le fichier ne doit pas excéder 3 MB!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMsg.Text = "Erreur lors de l'ouverture du fichier : " + ex.Message;
                }
            }
            else
            {
                lblMsg.Text = "SVP Sélectionner un fichier à télécharger";
            }
            if (!String.IsNullOrEmpty(p))
            {
                Response.Redirect(p);
            }
       
        }
    }
}