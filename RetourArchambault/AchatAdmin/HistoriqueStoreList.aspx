﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HistoriqueStoreList.aspx.cs" Inherits="RetourArchambault.AchatAdmin.HistoriqueStoreList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       <title>Historique Detail</title>
    
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />
</head>
<body class="dashboard">
    <div id="header">
        <asp:Image ID="entete" ImageUrl="~/images/lg_archambault01White.png" AlternateText="Archambault" runat="server" />
    </div>
    <form id="form1" runat="server">
    <div>
          <div class="left_menu_content">
                        <ul>
                            <li>
                                <asp:HyperLink ID="hlPrecedent" runat="server">Page précédente</asp:HyperLink></li>
                            <li class="odd">
                               <asp:HyperLink ID="hlHistorique" runat="server">Historique</asp:HyperLink></li>
                             <li>
                                 <asp:HyperLink ID="hlDashboard" runat="server">Tableau de bord</asp:HyperLink></li>
                     
                        </ul>
                    </div>
    <div  class="result_table_cont">
          <asp:Image ID="Image3" ImageUrl="~/images/imageDefault.jpg" runat="server" />
                    <div style="display:inline-block; margin-right: 20px;">
                        <asp:GridView ID="GridView1" runat="server" PageSize="50" OnPageIndexChanging="GridView1_PageIndexChanging" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="Id">
                            <Columns>
                                <asp:TemplateField HeaderText="Id" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NoMag">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sku">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("Sku") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="NoCie">
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ISBN">
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("ISBN") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="UPC">
                                    <ItemTemplate>
                                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("UPC") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Auteur">
                                    <ItemTemplate>
                                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("Auteur") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Titre">
                                    <ItemTemplate>
                                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("Titre") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Compo">
                                    <ItemTemplate>
                                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("Compo") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Format">
                                    <ItemTemplate>
                                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("Format") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Etiquette">
                                    <ItemTemplate>
                                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("Etiquette") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cost">
                                    <ItemTemplate>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("Cost") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cty à retourner">
                                    <ItemTemplate>
                                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("Qty_a_Retourner") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Valeur retour">
                                    <ItemTemplate>
                                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("ValeurRetour","{0:C2}") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Dept">
                                    <ItemTemplate>
                                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                    <ItemTemplate>
                                        <asp:Label ID="Label22" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Lien" Visible="False">
                                    <ItemTemplate>
                                        <asp:Label ID="Label16" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date création">
                                    <ItemTemplate>
                                        <asp:Label ID="Label17" runat="server" Text='<%# Eval("Date_Creation") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Refuse">
                                    <ItemTemplate>
                                        <asp:Label ID="Label18" runat="server" Text='<%# Eval("Refuse") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty autorisé">
                                    <ItemTemplate>
                                        <asp:Label ID="Label19" runat="server" Text='<%# Eval("Qty_autorise") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="En stock">
                                    <ItemTemplate>
                                        <asp:Label ID="Label20" runat="server" Text='<%# Eval("EnStock") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Catégorie">
                                    <ItemTemplate>
                                        <asp:Label ID="Label21" runat="server" Text='<%# Eval("Category") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                        </asp:GridView>
                            
                    </div>
                    <div style="clear: both;">&nbsp;</div>
                </div>
    </div>
    </form>
</body>
</html>
