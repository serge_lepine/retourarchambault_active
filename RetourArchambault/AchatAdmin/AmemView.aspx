﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AmemView.aspx.cs" Inherits="RetourArchambault.Achat.AmemView" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Rapports</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div id="header">
        <asp:Image ID="Image1" ImageUrl="~/images/lg_archambault01White.png" runat="server" meta:resourcekey="Image1Resource1" />
    </div>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <div class="rapport_container">
                        <div class="frame">
                            <div class="dept_table_cont">
                                <table class="dept_table">
                                    <tr class="even">
                                        <td>Exclure les retours dont la qté à retourner est de</td>
                                        <td class="centered">
                                            <asp:TextBox ID="txt_O1" runat="server"></asp:TextBox>
                                        </td>
                                        <td>unités ou moins</td>
                                        <td class="last"></td>
                                    </tr>
                                    <tr class="odd">
                                        <td>Exclure les retours dont la qté à retourner est de</td>
                                        <td class="centered">
                                            <asp:TextBox ID="txt_O2" MaxLength="3" runat="server"></asp:TextBox></td>
                                        <td>unités ou plus</td>
                                        <td class="last"></td>
                                    </tr>
                                    <tr class="even last">
                                        <td>Exclure les retours dont le Modèle calculé est de</td>
                                        <td class="centered">
                                            <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>
                                        </td>
                                        <td>unités ou plus</td>
                                        <td class="last"></td>
                                    </tr>

                                </table>


                            </div>
                            <div class="pdf_btn">
                                <asp:Button ID="Button1" CssClass="createPdf_btn" runat="server" Text="" OnClick="Button1_Click" />
                            </div>
                            <div class="stock_table_cont">
                                <table class="stock_table">
                                    <tr class="even">
                                        <td>À retourner</td>
                                        <td>
                                            <asp:Label ID="lbl_Distinct" runat="server" Text="0" meta:resourcekey="lbl_DistinctResource1"></asp:Label></td>
                                    </tr>
                                    <tr class="odd last">
                                        <td>À retourner ($)</td>
                                        <td>
                                            <asp:Label ID="lblRetour_money" runat="server" Text="0.0" meta:resourcekey="lblRetour_moneyResource1"></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="button_cont">
                            <asp:Button ID="btnCalcul" CssClass="refresh_btn" runat="server" Text="" OnClick="btnCalcul_Click" />
                            <asp:Button ID="BtnSave" runat="server" CssClass="sauvegarder_btn" Visible="false" Text="" OnClick="BtnSave_Click" />
                            <asp:Button ID="BtnSend" runat="server" CssClass="email_btn" Text="" OnClick="BtnSend_Click" />
                            <p>Une demande de traitement sera envoyé par courriel à : <br />
                                <asp:Label ID="Label16" runat="server" Text=""></asp:Label> </p>
                          <%--  <asp:ListBox ID="ListBox1" DataSourceID="ProviderEmail" DataTextField="Email" DataValueField="IdCie" runat="server" Rows="2"></asp:ListBox>
                            <div class="frmElmnt fTxt">
                                <div class="fLbl">
                                    <asp:Label ID="lblSelection" runat="server" Visible="false" Text="Sélectionner expéditeur :"></asp:Label></div>
                                <div class="fWdgt">
                                    <asp:DropDownList ID="DropDownEmailProvider" runat="server" DataSourceID="ProviderEmail" DataTextField="Email" DataValueField="IdCie" Visible="false"></asp:DropDownList>
                                </div>
                            </div>--%>

                            <asp:SqlDataSource ID="ProviderEmail" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT DISTINCT [IdCie], [Email] FROM [Provider_Usr] WHERE ([IdCie] = @IdCie)" OnSelecting="ProviderEmail_Selecting">
                                <SelectParameters>
                                    <asp:QueryStringParameter DefaultValue="0" Name="IdCie" QueryStringField="Cie" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--END /.content-->
        <%--</div>--%><!--END /.wrapper-->
        <%--  <div class="result_table_cont">--%>
        <div style="align-content:center;" class="result_table_cont">
            <asp:GridView ID="GridView1"
                runat="server"
                AutoGenerateColumns="False"
                OnPageIndexChanging="GridView1_PageIndexChanging"
                OnSorting="GridView1_Sorting"
                PageSize="50"
                OnRowCancelingEdit="GridView1_RowCancelingEdit"
                OnRowDeleting="GridView1_RowDeleting"
                OnRowUpdating="GridView1_RowUpdating"
                OnRowEditing="GridView1_RowEditing"
                AllowPaging="True" AllowSorting="True" DataKeyNames="id" GridLines="None" OnRowDataBound="GridView1_RowDataBound">
                <RowStyle CssClass="odd" />
                <AlternatingRowStyle CssClass="even" />
                <FooterStyle HorizontalAlign="Left" />
                <PagerSettings Mode="NumericFirstLast" FirstPageText="Début" LastPageText="Fin" NextPageText="Suivante" PreviousPageText="Précédente" Position="Bottom" />
                <Columns>
                    <asp:TemplateField HeaderText="No Magasin" SortExpression="NoMag">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SKU" SortExpression="SKU">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("SKU") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ISBN" SortExpression="ISBN">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("ISBN") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UPC" SortExpression="UPC">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("UPC") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Auteur" SortExpression="AUTEUR">
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("AUTEUR") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Titre" SortExpression="TITRE">
                        <ItemTemplate>
                            <asp:Label ID="Label7" runat="server" Text='<%# Eval("TITRE") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Compos." SortExpression="COMPOS">
                        <ItemTemplate>
                            <asp:Label ID="Label8" runat="server" Text='<%# Eval("COMPOS") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Format" SortExpression="FORMAT">
                        <ItemTemplate>
                            <asp:Label ID="Label9" runat="server" Text='<%# Eval("FORMAT") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ètiquette" SortExpression="Etiquette">
                        <ItemTemplate>
                            <asp:Label ID="Label10" runat="server" Text='<%# Eval("Etiquette") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Modèle Calculé" SortExpression="Model_Calcule">
                        <ItemTemplate>
                            <asp:Label ID="Label11" runat="server" Text='<%# Eval("Model_Calcule") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Cost" SortExpression="COST">
                        <ItemTemplate>
                            <asp:Label ID="Label12" runat="server" Text='<%# Eval("COST","{0:C2}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="En Stock">
                        <ItemTemplate>
                            <asp:Label ID="Label15" runat="server" Text='<%# Eval("EnStock") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Qté à Retourner" SortExpression="Qty_a_Retourner">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQtyReturn" runat="server" Text='<%# Eval("Qty_a_Retourner") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label13" runat="server" Text='<%# Eval("Qty_a_Retourner") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Valeur de retour" SortExpression="Valeur_Retour">
                        <ItemTemplate>
                            <asp:Label ID="Label14" runat="server" Text='<%# Eval("Valeur_Retour","{0:C2}") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:CommandField HeaderText="Opération" ButtonType="Image" ControlStyle-Width="25" ShowDeleteButton="True" ShowEditButton="True" ShowHeader="True" CancelImageUrl="~/images/icon-Cancel.png" DeleteImageUrl="~/images/Delete.png" EditImageUrl="~/images/icon-edit.png" UpdateImageUrl="~/images/icon-update.png">
                        <ControlStyle Width="25px"></ControlStyle>
                    </asp:CommandField>
                </Columns>
            </asp:GridView>

        </div>
        <asp:GridView ID="GridView2" Visible="false" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
        </asp:GridView>
    </form>
    <%-- <div id="dvLoading">
	<div class="loading_msg_cont"><div class="loading_msg">Création du rapport en cours....<br />ce processus peut prendre quelques minutes,<br />veuillez patienter svp...</div></div>
</div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(window).load(function () {
        $('#dvLoading').fadeOut(2000);
    });
</script>--%>
</body>
</html>
