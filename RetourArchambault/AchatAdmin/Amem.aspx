﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Amem.aspx.cs" Inherits="RetourArchambault.Achat.Amem" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <ul>
        <li><a href="../Admin/Default.aspx" accesskey="1">Administration</a></li>
        <li class="active"><a href="../Achat/Amem.aspx" accesskey="2">Achat & Mise en marché</a></li>
        <li><a href="../Fournisseur/Fournisseurs.aspx" accesskey="3">Fournisseur</a></li>
        <li><a href="../Succursales/Magasin.aspx" accesskey="4">Succursale</a></li>
    </ul>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="wrapper">

        <div id="content">
            <div class="welcomeMessage">
                <asp:Label ID="lblBonjour" runat="server" Font-Bold="true" Text="Bonjour"></asp:Label>
            </div>
            <div class="container">
                <h2 class="title">Nouveau</h2>
                <p>Sélectionner le fournisseur ainsi que le département</p>
                <div class="frame">
                    <div class="frmElmnt fSlct">
                        <div class="fLbl">
                            <label>No cie</label>
                        </div>
                        <div class="fWdgt">
                            <asp:DropDownList ID="DropDownCie" runat="server" DataSourceID="ProviderList" DataTextField="CieName" DataValueField="NoCie"></asp:DropDownList>
                        </div>
                        <asp:SqlDataSource ID="ProviderList" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT [NoCie], [CieName] FROM [Provider] ORDER BY CieName"></asp:SqlDataSource>
                        <div class="clearer"></div>
                    </div>
                    <div class="frmElmnt fSlct">
                        <div class="fLbl">
                            <label>Dept</label>
                        </div>
                        <div class="fWdgt">
                            <asp:DropDownList ID="DropDownDept" runat="server" DataSourceID="ProviderDept" DataTextField="Descriptions" DataValueField="Dept"></asp:DropDownList>
                        </div>
                        <asp:SqlDataSource ID="ProviderDept" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT * FROM [Provider_Dept]"></asp:SqlDataSource>
                        <div class="clearer"></div>
                    </div>
                    <div class="frmElmnt fTxt">
                        <div class="fLbl">
                            <label>Nombre de jour  exclu</label>
                        </div>
                        <div class="fWdgt inputSize1">
                            <asp:TextBox ID="txtExclu" ToolTip="Entrer le nombre de jours à exclure" runat="server" Text="90"></asp:TextBox>
                        </div>
	                    <div class="clearer"><!-- --></div>
                    </div>
                    <div class="frmElmnt fBtn">
                        <div class="fWdgt">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Button CssClass="btn primaryAction" ID="Button1" runat="server" OnClick="Button1_Click" Text="Créer la liste des retours" meta:resourcekey="Button1Resource1" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div class="overlay">
                                        <div class="overlayContent">
                                            <div class="loading_msg">
                                                <img src="/images/loader.gif" alt="Loading" />
                                                <p>Création du rapport en cours....<br />
                                                ce processus peut prendre quelques minutes,<br />
                                                veuillez patienter svp...</p>
                                            </div>                                               
                                        </div>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                        </div>
                    </div>
                </div>
            </div>
            <!--END /Nouveau-->
            <%-- <div style="clear: both;">&nbsp;</div>
            <div class="container">
                <h2 class="title">À Compléter</h2>
                <p>Sélectionner les transactions à completer</p>
               <div class="frame">        	
                     <asp:Repeater ID="Repeat_1" runat="server">
                            <ItemTemplate>
                                     <div class="frmElmnt">
                                        <div class="fLbl"><label><%# DataBinder.Eval(Container.DataItem,"TransactDate") %></label></div>
                                        <div class="fWdgt"><asp:HyperLink ID="HyperLink1" CssClass="links" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Lien") %>' runat="server">
                                           <%# DataBinder.Eval(Container.DataItem,"monText") %></asp:HyperLink></div>
                                        <div class="clearer"></div>
                                    </div>
                            </ItemTemplate>
                        </asp:Repeater>
                </div>   
            </div>--%>
            <div style="clear: both;">&nbsp;</div>
            <div class="container">
                <h2 class="title">Réponse Fournisseur</h2>
                <p>Sélectionner les transactions à finaliser</p>
                <div class="frame">
                    <asp:Repeater ID="Repeat_2" runat="server">
                        <ItemTemplate>
                            <div class="frmElmnt">
                                <div class="fLbl">
                                    <label><%# DataBinder.Eval(Container.DataItem,"TransactDate") %></label></div>
                                <div class="fWdgt">
                                    <asp:HyperLink ID="HyperLink1" CssClass="links" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Lien") %>' runat="server">
                                           <%# DataBinder.Eval(Container.DataItem,"monText") %></asp:HyperLink>
                                </div>
                                <div class="clearer"></div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <!--END /À Compléter-->
            <div style="clear: both;">&nbsp;</div>
            <div class="container last">
                <h2 class="title">Réponse Magasin</h2>
                <p>Sélectionner une transaction terminé</p>
                <div class="frame">
                    <asp:Repeater ID="Repeat_3" runat="server">
                        <ItemTemplate>
                            <div class="frmElmnt">
                                <div class="fLbl">
                                    <label><%# DataBinder.Eval(Container.DataItem,"TransactDate") %></label>
                                </div>
                                <div class="fWdgt">
                                    <asp:HyperLink ID="Historique" CssClass="links" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Lien") %>' runat="server" meta:resourcekey="HistoriqueResource1">
                                    <label><%# DataBinder.Eval(Container.DataItem,"monText") %></label></asp:HyperLink>
                                </div>
                                <div class="clearer"></div>
                            </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <!--END /Historique-->
            <div style="clear: both;">&nbsp;</div>

            <!--END /.content-->
        </div>
        <!--END /.wrapper-->
    </div>


</asp:Content>
