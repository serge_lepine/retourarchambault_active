﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault.AchatAdmin
{
    public partial class HistoriqueStore : System.Web.UI.Page
    {

        private Usr usager = new Usr();
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;

            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {
                //if (Session["Cie"] == null) { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                //if (Session["IdUsr"] == null) { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
             //   { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                getParam();
                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    setMenu();
                    //Remplit Grille 1
                    string query = Tool.getQuery("gvHistoriqueStore.sql");
                    gvHistoriqueStore.DataSource = setGrid(query,Request.Params["Lien"].ToString());
                    gvHistoriqueStore.DataBind();

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void getParam()
        {
            if (Session["USR"] != null)
            {
                if (Session["lang"] != null) { this.Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { this.Lang = "fr"; }
                usager = (Usr)Session["USR"];
                usager.idCieParam = usager.idCie;
                usager.idParam = usager.id;
            }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            usager.index = 1;
        }
        private void setMenu()
        {
            getParam();
            //Menu 1
            this.Master.HyperLink1Text = "Tableau de bord";
            this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam;
            this.Master.HyperLink1Visible = true;
            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;
            this.Master.Lang = "fr";
            //Bonjour
           // this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name;

        }
        private IDataReader setGrid(string query,string Lien)
        {
            getParam();
            SqlDataReader record = null;
            SqlConnection con = Connexion.ConnectSQL();
            query = String.Format(query, Lien,this.Lang);
            SqlCommand cmd = new SqlCommand(query, con);
            //    record = cmd.ExecuteReader();

            int i = Convert.ToInt32(cmd.ExecuteScalar());
            if (i > 0)
            {
                Image3.Visible = false;
                gvHistoriqueStore.Visible = true;
                record = cmd.ExecuteReader();
            }
            return record;
        }
        protected void btnDetails_Click(object sender, EventArgs e)
        {
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lien = (Label)grv.FindControl("lblLien");
            Label NoMag = (Label)grv.FindControl("lblNoMag");
            string StoreLien = lien.Text;
            string noMag = NoMag.Text;
            Response.Redirect("HistoriqueStoreList.aspx?Temp=0&usr=" + usager.id + "&Lien=" + StoreLien + "&NoMag=" + noMag);
        }
    }
}