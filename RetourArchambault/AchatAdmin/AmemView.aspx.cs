﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Threading;
using ConvertToCSV;
using System.Net;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.xml;
using System.Xml;
using iTextSharp.text.html.simpleparser;

namespace RetourArchambault.Achat
{
    public partial class AmemView : System.Web.UI.Page
    {
        private int m_qty;
        private double m_amount;
        private string query;
        private SqlCommand cmd;
        private SqlConnection c_con;
        private DataSet ds1;
        private int dept;
        private int CieSearch;
        private int count;
        private double amount;
        private int qty_over;
        private int qty_minus;
        private string m_condition;
        private string TransactName;
        private int idUser;
        private int model;
        private Usr usager = new Usr();
        private int temp;
        private string m_lien;

        void initialise()
        {
            m_amount = 0.0;
            m_qty = 0;
            query = "";
            qty_over = 0;
            qty_minus = 0;
            m_condition = "";
            TransactName = "";
            idUser = 0;
            m_lien = "";
        }
        override protected void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Load += new System.EventHandler(this.Page_Load);
            //Force la culture francais canadien
           
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Thread.CurrentThread.CurrentCulture.Name != "fr-CA")
            {
                //Set culture info
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("fr-CA");
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("fr-CA");
            }
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
            //Autentification de l'utilisateur
            usager = (Usr)Session["USR"];
            usager.idCieParam = int.Parse(Request.Params["Cie"]);
            usager.idParam = int.Parse(Request.Params["Usr"]);
            usager.index = 1;

            if (!IsPostBack)
            {
                
                DroitAcces da = new DroitAcces();
               
                if ( da.VerifAcces(usager))
                {
                try
                {
                    BindData();
                }
                catch (Exception fd)
                {
                    log.logError(fd.Message);
                    Response.Redirect("Amem.aspx");

                }
                }
                else
                {
                    Response.Redirect("../Login.aspx");
                }

            }

        }
       
        private void BindData()
        {
            count = 0;
            amount = 0.0;
            //Initialise les paramètres fournit par l'url
            getParam();
            getCondition();
            //Construction de la requête
            query = "";
            query = File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/Req_AmemView.sql"));
            query = String.Format(query, "Temp_Trans_Provider_" + m_lien, this.CieSearch, m_condition);
            log.Log(query);
            
            setData(query);

        }
        private void setData(string query)
        {
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            try
            {
                //Ouvre connexion
                c_con = Connexion.ConnectSQL();
                SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
                //Replit dataset
                adapter.Fill(ds);
                myDataTable.GlobalDataTable = ds.Tables[0];
               // Session["DataSource"] = ds;
                //initialise Qty et montant
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    count += int.Parse(row[13].ToString());
                    amount += double.Parse(row[14].ToString());

                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }

            if (ds.Tables.Count > 0)
            {
                //Initialise le textbox 
                lbl_Distinct.Text = count.ToString();
                lblRetour_money.Text = String.Format("{0:C2}", amount);
                DataTable dt = myDataTable.GlobalDataTable;
                if (Session["sort"] != null)
                {
                     
                    if (Session["sort"] != null)
                    {

                        dt.DefaultView.Sort = Session["sort"].ToString();
                        setGrid();
                    }
        
                }
                GridView1.DataSource = dt;
                GridView1.DataBind();
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }


        }
        void getParam()
        {
            //Initilise variable selon parametre url
            if (Request.Params["Dept"] == "") { dept = 0; } else { dept = int.Parse(Request.Params["Dept"]); }
            if (Request.Params["CieSearch"] == "") { this.CieSearch = 0; } else { this.CieSearch = int.Parse(Request.Params["CieSearch"]); }
            if (Request.Params["Usr"] == "") { idUser = 0; } else { idUser = int.Parse(Request.Params["Usr"]); }
          //  if (Request.Params["Temp"] == "") { temp = 0; } else { temp = int.Parse(Request.Params["Temp"]); }
            if (dept != 0 && this.CieSearch != 0) { m_lien = Request.Params["Lien"]; } else { m_lien = null; }

        }
        void getCondition()
        {
            if (txt_O1.Text != "")
            {
                qty_minus = int.Parse(txt_O1.Text);
                m_condition = " AND Qty_a_Retourner > " + qty_minus;

            }
            if (txt_O2.Text != "")
            {
                qty_over = int.Parse(txt_O2.Text);
                m_condition += " AND Qty_a_Retourner < " + qty_over;
            }
            if (!String.IsNullOrEmpty(txtModel.Text))
            {
                model = int.Parse(txtModel.Text);
                m_condition += " AND Model_Calcule < " + model;
            }

        }

        protected void btnCalcul_Click(object sender, EventArgs e)
        {
            refreshData();
           

        }
        void refreshData()
        {
            //Initialise les paramètres fournit par l'url
            getParam();
            getCondition();
            //Lit la requete stockée si lien != null
            if (!String.IsNullOrEmpty(m_lien))
            {
                query = File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/Req_AmemView.sql"));
                query = String.Format(query, "Temp_Trans_Provider_" + m_lien, this.CieSearch, m_condition);
                log.Log(query);
                setData(query);
            }

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {
            //Cacher le bouton Save
            BtnSave.Visible = false;
            //Rendre visible label de sélection
            //    lblSelection.Visible = true;
            // Rendre visible dropdownlist
            //  ListBox1.Visible = true;
            // Rendre visible le bouton send
            BtnSend.Visible = true;
        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            bool succes;
            string email = "";
            int count = 0;
            //Reload parametre de url
            getParam();
            //Instancie object parametre Amem
            ParamAmem pa = new ParamAmem();
            pa.cie = this.CieSearch;
            pa.dept = dept;
          
            
            pa.dt = myDataTable.GlobalDataTable; 
            //Liste les courriels et recupere le premier ID
            //foreach (System.Web.UI.WebControls.ListItem v in ListBox1.Items)
            //{
            //    count++;
            //    email += v.Text + ";";
            //    if (count == 1)
            //    {
            //        pa.idUsrDestination = int.Parse(v.Value);
            //    }
            //}


            pa.idUsrEnvoi = idUser;
            pa.lien = m_lien; //Tools.setTransactName(this.CieSearch, dept, 1);
            pa.section = 1;
            //Enregistre le datagrid pour l'envoi vers le fournisseur
            AmemToProvider atp = new AmemToProvider();
            succes = atp.amemToProvider(pa);
            
            //Si succes envoi courriel et affiche page succes
            if (succes)
            {
                //Module envoi courriel
                Response.Redirect("../succes.aspx");
            }
            else
            {
                //Afficher page erreur et rediriger vers page initial

            }
        }
        private void setGrid()
        {
            GridView1.DataSource = myDataTable.GlobalDataTable;
            GridView2.DataSource = myDataTable.GlobalDataTable;
            GridView1.DataBind();
            GridView2.DataBind();
            
        }
        protected void ProviderEmail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            setGrid();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            DeleteCommand(id);
            refreshData();

           
         

        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
           // DataSet ds = (DataSet)Session["DataSource"];
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            TextBox txtQty = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtQtyReturn");
            UpdateCommand(id, int.Parse(txtQty.Text));
            GridView1.EditIndex = -1;
            refreshData();
           
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            setGrid();
        }
        private void UpdateCommand(int id, int Qty)
        {
            getParam();
            //Ouvre connexion SQL
            SqlConnection con = Connexion.ConnectSQL();
            //Creation de la requete
            string query = "UPDATE Temp_Trans_Provider_" + m_lien + " SET Qty_a_Retourner=" + Qty + " WHERE id=" + id;
            //Creation de la commande
            Connexion.execCommand(query);
           
        }
        private void DeleteCommand(int id)
        {
            getParam();
            //Ouvre connexion SQL
            SqlConnection con = Connexion.ConnectSQL();
            //Creation de la requete
            string query = "DELETE FROM Temp_Trans_Provider_" + m_lien + " WHERE id=" + id;
          
           Connexion.execCommand(query);
          
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            setGrid();
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            //DataSet ds = (DataSet)Session["DataSource"];
            DataTable dt = myDataTable.GlobalDataTable; //ds.Tables[0];
            //Crée un DataView
            DataView dv = new DataView(dt);
            if(dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                Session["sort"] = dt.DefaultView.Sort;
                setGrid();
            }
        
        }
        /// <summary>
        /// Order de trie des colonnes
        /// </summary>
        /// <param name="column">Nom de la colonne à trier</param>
        /// <returns>Ordre de la direction du trie</returns>
        private string GetSortDirection(string column)
        {
            // Ordre croissance par défaut.
            string sortDirection = "ASC";
            // Recherche le dernier ordre de croissance.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                //Vérifie si c"est la meme colonne qui doit être triée
                //Sinon la valeur par defaut peut être retourné
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }

            // Sauvegarde le dernier resultat dans le ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridView1.SelectedRow;
            string r = row.Cells[12].Text;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[15].Controls[0])).ToolTip = "Modifier";
                    ((System.Web.UI.LiteralControl)(e.Row.Cells[15].Controls[1])).Text = "&nbsp;";
                    ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[15].Controls[2])).ToolTip = "Supprimer";

                    if (e.Row.RowState == DataControlRowState.Edit || e.Row.RowState.ToString() == "Alternate, Edit")
                    {
                        int i = 0;
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            if (e.Row.Cells.GetCellIndex(cell) == 15)
                            {
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[15].Controls[0])).ToolTip = "Sauvegarder";
                                ((System.Web.UI.LiteralControl)(e.Row.Cells[15].Controls[1])).Text = "&nbsp;";
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[15].Controls[2])).ToolTip = "Fermer modification";
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.NewSelectedIndex];
            string r = row.Cells[12].Text;
        }

        protected void CreatePDF_Click(object sender, EventArgs e)
        {





        }

        protected void btnPDF_Click(object sender, EventArgs e)
        {
           
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            PdfPTable pdfTable = new PdfPTable(GridView2.HeaderRow.Cells.Count);

            foreach (TableCell headerCell in GridView2.HeaderRow.Cells)
            {
                Font font = new Font();
                font.Color = new BaseColor(GridView2.HeaderStyle.ForeColor);
                font.Size = 8;

                PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text, font));
                pdfCell.BackgroundColor = new BaseColor(GridView2.HeaderStyle.BackColor);
                pdfTable.AddCell(pdfCell);
            }

            foreach (GridViewRow gridViewRow in GridView2.Rows)
            {
                foreach (TableCell tableCell in gridViewRow.Cells)
                {
                    Font font = new Font();
                    font.Color = new BaseColor(GridView2.RowStyle.ForeColor);
                    font.Size = 8;
                    PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text, font));
                    pdfCell.BackgroundColor = new BaseColor(GridView2.RowStyle.BackColor);
                    pdfTable.AddCell(pdfCell);
                }
            }

            Document pdfDocument = new Document(PageSize.LEGAL.Rotate());
            PdfWriter.GetInstance(pdfDocument, Response.OutputStream);

            pdfDocument.Open();
            pdfDocument.Add(pdfTable);
            pdfDocument.Close();

            Response.ContentType = "application/pdf";
            Response.AppendHeader("content-disposition", "attachment;filename=Employees.pdf");
            Response.Write(pdfDocument);
            Response.Flush();
            Response.End();
        }

    }
}