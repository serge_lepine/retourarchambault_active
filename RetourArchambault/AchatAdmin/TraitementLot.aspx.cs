﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace RetourArchambault.AchatAdmin
{
    public partial class TraitementLot : System.Web.UI.Page
    {
        private Usr usager = new Usr();
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Contents.Remove("sort");
            Session.Contents.Remove("sort");
            Session.Contents.Remove("Date_Limite");
            Session.Contents.Remove("RefNumber");
            Session.Contents.Remove("RETOUR");
            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {

                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }

                getParam();

                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {

                    //Créer dropdownlist pour creation 1- nom des provider 2- departement 3- Provider usr selon provider
                    setMenu();

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = (int)Session["Cie"]; }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            if (Session["CieSearch"] != null) { usager.CieSearch = (int)Session["CieSearch"]; }

            usager.index = 1;
        }

        private void setMenu()
        {
            getParam();
            //Menu 1
            this.Master.HyperLink1Text = "Tableau de bord";
            this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam;
            this.Master.HyperLink1Visible = true;

            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;

            this.Master.Lang = "fr";

        }

        protected void BtnUpLoad_Click(object sender, EventArgs e)
        {
            getParam();
            NewTransaction nt = new NewTransaction();
            //Obtenir le chemin du fichier temporaire
            string FilePath = "/Temp/";
            string filename = string.Empty;
            string upc = string.Empty;
            string p = string.Empty;
          //  int NoCie = 0;
            int Dept = 999;
        //    DateTime date_limite = new DateTime();

            //Créer fonction pour lire le fichier xlsx
            //Vérifie si le Upload n'est pas vide
            if (FileUpload1.HasFile)
            {
                try
                {
                    string[] allowdFile = { ".xls", ".xlsx" };
                    //On verifie si Excel File
                    string FileExt = System.IO.Path.GetExtension(FileUpload1.PostedFile.FileName);
                    //Verifie extension
                    bool isValidFile = allowdFile.Contains(FileExt);
                    if (!isValidFile)
                    {
                        lblMsg.ForeColor = System.Drawing.Color.Red;
                        lblMsg.Text = "Seul les fichiers .xls sont accepté";
                    }
                    else
                    {
                        // Obtient la grosseur du fichier
                        int FileSize = FileUpload1.PostedFile.ContentLength;
                        //Limite le fichier à maximum 3MB
                        //if (FileSize <= 3145728)//1048576 byte = 1MB
                        //{
                        //Obtient le nom du fichier
                        filename = Path.GetFileName(Server.MapPath(FileUpload1.FileName));

                        //Sauvegarde le fichier sur le serveur
                        FileUpload1.SaveAs(Server.MapPath(FilePath) + filename);

                        //Obtient le nom du chemin
                        string filePath = Server.MapPath(FilePath) + filename;

                        //Instancie Classe pour lecture du fichier Uploader
                        XLS readXLS = new XLS(filePath, FileExt);

                        //Instancie la Classe pour la réponse de la lecture du fichier Uploader
                        RetourXLS response = new RetourXLS();

                        //Extrait les données du fichier Uploader
                        response = readXLS.getSku();

                        //Si UPC non vide alors Lance requete dans as400
                        if (!String.IsNullOrEmpty(response.UPC) || response.NoCie != 0)
                        {

                            //Créer la table
                            CreateNewProvider cp = new CreateNewProvider(response.Lien);
                            if(cp.createAddNewProvider())
                            {
                                //  Transferer les données dans table Temporaire
                                AjoutProduitManuel ajm = new AjoutProduitManuel(response);

                                ajm.addProduct();

                                p = "../Achat/AmemView.aspx?CieSearch=" + response.NoCie + "&Cie=" + usager.idCie + "&Dept=" + Dept.ToString() + "&Usr=" + usager.id + "&Lien=" + response.Lien;
                            }
                          
                        }
                        //  }
                        else
                        {
                            lblMsg.Text = "Le fichier ne doit pas excéder 3 MB!";
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblMsg.Text = "Erreur lors de l'ouverture du fichier : " + ex.Message;
                }
            }
            else
            {
                lblMsg.Text = "SVP Sélectionner un fichier à télécharger";
            }
            if (!String.IsNullOrEmpty(p))
            {
                Response.Redirect(p);
            }
        }
    }
}