﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gestion.aspx.cs" Inherits="RetourArchambault.AchatAdmin.Gestion" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <div id="header">
        <asp:Image ID="Image1" ImageUrl="~/images/lg_archambault01White.png" runat="server" meta:resourcekey="Image1Resource1" />
    </div>
    <form id="form1" runat="server">
        <div id="wrapper">
            <div id="content">
                 <div class="container">
                     <div class="rapport_container">
                          <div class="frame">
                            <div class="dept_table_cont">
                                <table class="dept_table">
                                    <tr class="event">
                                        <td>
                                            Rechercher par :
                                            <asp:DropDownList ID="DropDownList1" runat="server">
                                                <asp:ListItem Value="0">--Sélectionner--</asp:ListItem>
                                                <asp:ListItem Value="1">Compagnie</asp:ListItem>
                                                <asp:ListItem Value="2">Usager</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                         </div>
                     </div>
                 </div>
            <div style="align-content: center;" class="result_table_cont amem_tbl">

                <asp:GridView ID="gvFournisseur"
                    runat="server"
                    CellPadding="4"
                    GridLines="None" AutoGenerateColumns="False" AllowSorting="true" HorizontalAlign="Center" CssClass="dashboard_table" Width="730px">
                    <RowStyle CssClass="odd" />
                    <AlternatingRowStyle CssClass="even" />
                    <Columns>
                        <asp:TemplateField HeaderText="Id" SortExpression="Id" Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                            <ItemTemplate>
                                <asp:TextBox ID="txtNoCie" runat="server" Text='<%# Eval("idCie") %>'></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Cie Nom" SortExpression="CieName">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCieName" runat="server" Text='<%# Eval("CieName") %>'></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nom usager" SortExpression="email">
                            <ItemTemplate>
                                <asp:TextBox ID="txtUserName" runat="server" Text='<%# Eval("email") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Mot de passe" SortExpression="password">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPassword" runat="server" Text='<%# Eval("password") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actif" SortExpression="is_active">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActif" runat="server" AutoPostBack="True" Checked='<%# Eval("is_active") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fournisseur" SortExpression="is_public">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkPublic" runat="server" Checked='<%# Eval("is_public") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Magasin" SortExpression="is_store">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkStore" runat="server" Checked='<%# Eval("is_store") %>' />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle BackColor="#990000" ForeColor="White" Font-Bold="True" HorizontalAlign="Left" />
                    <%--   <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />--%>
                    <%--  <SortedAscendingCellStyle BackColor="#FDF5AC" />
                                    <SortedAscendingHeaderStyle BackColor="#4D0000" />
                                    <SortedDescendingCellStyle BackColor="#FCF6C0" />
                                    <SortedDescendingHeaderStyle BackColor="#820000" />--%>
                </asp:GridView>


            </div>
                </div>
        </div>
    </form>
</body>
</html>
