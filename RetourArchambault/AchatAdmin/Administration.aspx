﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Administration.aspx.cs" Inherits="RetourArchambault.AchatAdmin.Administration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            border-style: solid;
            border-width: 2px;
        }
        .auto-style10 {
            width: 50%;
        }
        .auto-style11 {
            width: 50%;
            height: 186px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <h1>Tableau de bord</h1>
    <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </ajaxToolkit:ToolkitScriptManager>
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="980px" Height="603px">
        <ajaxToolkit:TabPanel runat="server" HeaderText="Administration" ID="TabPanel1">
            <ContentTemplate>
                


                <table class="auto-style1" border="2" style="border-color:black">
                    <tr>
                        <td style="text-align:center;" aria-orientation="horizontal" class="auto-style10"><h2><b>Magasin</b></h2></td>
                        <td style="text-align:center;" class="auto-style10"><h2><b>Fournisseur</b></h2></td>
                    </tr>
                    <tr>
                        <td class="auto-style11">
                            &nbsp;</td>
                        <td class="auto-style11">
                             <asp:GridView ID="gvProvider" runat="server" BackColor="White" BorderColor="#2A0A0A" BorderStyle="None" BorderWidth="1px" CellPadding="3" HorizontalAlign="Center">
                                <AlternatingRowStyle BackColor="Gainsboro" />
                                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#000065" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:center;" class="auto-style11"><h2><b>Historique</b></h2></td>
                        <td style="text-align:center;" class="auto-style11"><h2><b>Magasin</b></h2></td>
                    </tr>
                    <tr>
                        <td class="auto-style10" style="text-align:center;">
                             <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlProvider1" Height="286px" Width="363px">
                                 <Series>
                                     <asp:Series ChartArea="ChartArea1" Name="Series1" XValueMember="NoCie" YValueMembers="Montant_demande" YValuesPerPoint="2">
                                     </asp:Series>
                                 </Series>
                                 <ChartAreas>
                                     <asp:ChartArea Name="ChartArea1">
                                     </asp:ChartArea>
                                 </ChartAreas>
                             </asp:Chart>
                        </td>
                        <td class="auto-style10">
                            <asp:SqlDataSource ID="SqlProvider1" runat="server" ConnectionString="<%$ ConnectionStrings:SWPPIXWEB02ConnectionString %>" SelectCommand="SELECT [NoCie], [Montant_demande] FROM [Transaction_Link_Provider]"></asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                


            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Fournisseur">
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="Magasin">
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>
    <br />
</asp:Content>
