﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault.AchatAdmin
{
    public partial class HistoriqueStoreList : System.Web.UI.Page
    {
        private Usr usager = new Usr();
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
            //Initialise le menu
            setMenu();

         
            if (!IsPostBack)
            {
                
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
     
                getParam();

                //Identification de l'utilisateur
                //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                   
                    //Remplit Grille 1
                    string query = Tool.getQuery("gvHistoriqueStoreList.sql");
                    string Lien = Request.Params["Lien"].ToString();
                    int NoMag = int.Parse(Request.Params["NoMag"]);
                    DataSet ds = new DataSet();

                    SqlConnection con = Connexion.ConnectSQL();
                    query = String.Format(query, Lien, NoMag,this.Lang);
                    SqlDataAdapter adapter = new SqlDataAdapter(query, con);
                    try
                    {
                        //Remplit dataset
                        adapter.Fill(ds);
                        Session["dthsl"] = ds.Tables[0];
                        

                    }
                    catch (Exception ex)
                    {
                        log.logError(ex.Message);
                    }
                    finally
                    {
                        //Ferme la connexion
                        Connexion.closeSQL(con);
                    }

                    int i = ds.Tables[0].Rows.Count;
                    if (i > 0)
                    {
                        setGrid();
                    }
                   
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void getParam()
        {

            if (Session["lang"] != null) { this.Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { this.Lang = "fr"; }
            if (Session["USR"] != null)
            {
                usager = (Usr)Session["USR"];
                usager.idCieParam = usager.idCie;
                usager.idParam = usager.id;
            }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            usager.index = 1;
           
        }
        private void setMenu()
        {
            getParam();
            hlPrecedent.NavigateUrl = "HistoriqueStore.aspx?Temp=0&usr=" + usager.id + "&Lien=" + Request.Params["Lien"].ToString();
            hlDashboard.NavigateUrl = "/AchatAdmin/dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam;
            hlHistorique.NavigateUrl = "Historique.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            
        }
        private void setGrid()
        {
                Image3.Visible = false;
                GridView1.Visible = true;
                GridView1.DataSource = (DataTable)Session["dthsl"];
                GridView1.DataBind();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            setGrid();
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
    }
}