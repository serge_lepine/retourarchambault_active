﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Globalization;

namespace RetourArchambault.AchatAdmin
{
    public partial class dashboard_v2 : System.Web.UI.Page
    {
        private Usr usager = new Usr();
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
            Tool.setThread_FR();
         
            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {
                
                 Session["Cie"] = int.Parse(Request.Params["Cie"]); 
                 Session["IdUsr"] = int.Parse(Request.Params["Usr"]);
                getParam();
               
                DroitAcces da = new DroitAcces(this.usager);
               
               
                if (da.VerifAcces())
                {
                    setHyperLink();
                    //Remplit grille Non compete
                    string query = Tool.getQuery("set_gvNonComplete.sql");
                    query = String.Format(query, this.Lang);
                    log.Log("set_gvNonComplete : " + query);
                    gvNonComplete.DataSource = setGrid(query, "gvNonComplete");
                    gvNonComplete.DataBind();
                    //Remplit Grille 1
                    query = Tool.getQuery("gvProvider.sql");
                    query = String.Format(query, this.Lang);
                    gvFournisseur1.DataSource = setGrid(query,"gvFournisseur1");
                    gvFournisseur1.DataBind();

                    //Remplit Grille 2
                    query = Tool.getQuery("gvProvider2.sql");
                    query = String.Format(query, this.Lang);
                    gvFournisseur2.DataSource = setGrid(query, "gvFournisseur2");
                    gvFournisseur2.DataBind();

                    query = "";
                    query = Tool.getQuery("gvStore.sql");
                    query = String.Format(query, this.Lang);
                    gvStore1.DataSource = setGrid(query,"gvStore1");
                    gvStore1.DataBind();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
            
        }
        void getParam()
        {
            try
            {
                if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
                if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
                if (usager.idCieParam == 0) { usager.idCieParam = (int)Session["Cie"]; }
                if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
                usager.index = 1;
            }
            catch
            {
                Response.Redirect("../Login.aspx");
            }
        }

        private void setHyperLink()
        {
            hlDemande.NavigateUrl = "../Achat/Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            hlHistorique.NavigateUrl = "Historique.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            hlDesuetude.NavigateUrl = "UploadDesu.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            hlDefectueux.NavigateUrl = "Defectueux.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            hlTraitementLot.NavigateUrl = "TraitementLot.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            //hlGestion.NavigateUrl = "Gestion.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie;
            hlQuit.NavigateUrl = "../Login.aspx";

        }
        private IDataReader setGrid(string query,string grid)
        {
            SqlDataReader record = null;
            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd = new SqlCommand(query, con);
        //    record = cmd.ExecuteReader();

            int i = Convert.ToInt32(cmd.ExecuteScalar());
            if(i>0)
            {
                switch (grid)
                {
                    case "gvNonComplete":
                        Image6.Visible = false;
                        gvNonComplete.Visible = true;
                        break;
                    case "gvFournisseur1":
                        Image3.Visible = false;
                        gvFournisseur1.Visible = true;
                        //Image4.Visible = false;
                        //Chart4.Visible = true;
                        break;
                    case "gvFournisseur2":
                        Image1.Visible = false;
                        gvFournisseur2.Visible = true;
                        Image4.Visible = false;
                    //    Chart4.Visible = true;
                        break;
                    case "gvStore1":
                        Image2.Visible = false;
                        gvStore1.Visible = true;
                        Image5.Visible = false;
                        //Chart5.Visible = true;
                        break;
                    default:
                        break;
                }
                record = cmd.ExecuteReader();
            }
            return record;
        }
        protected void btnStore1_Click(object sender, EventArgs e)
        {
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lien = (Label)grv.FindControl("lblStoreLien");

            string StoreLien = lien.Text;

            Response.Redirect("StoreDashboard.aspx?StoreLien=" + StoreLien);
          
        }
       

        protected void btnProvider_Click(object sender, EventArgs e)
        {
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lien = (Label)grv.FindControl("lblLien");

            string StoreLien = lien.Text;

            Response.Redirect("WebForm1.aspx?Temp=0&usr="+ usager.id + "&Lien=" + lien.Text);
        }

        protected void btnProvider_Click1(object sender, EventArgs e)
        {
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lien = (Label)grv.FindControl("lblLien");

            string StoreLien = lien.Text;

            Response.Redirect("WebForm2.aspx?Temp=0&usr=" + usager.id + "&Lien=" + lien.Text);
        }

        protected void btnNonComplete_Click(object sender, EventArgs e)
        {
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lien = (Label)grv.FindControl("lblLien1");

            string StoreLien = lien.Text;
            string[] item = StoreLien.Split('_');
            int CieSearch = int.Parse(item[0].ToString());
            int dept = int.Parse(item[1].ToString());
            string newPath = "/Achat/AmemView.aspx?CieSearch=" + CieSearch + "&Cie=1&Dept=" + dept + "&Usr=" + usager.id + "&Lien=" + lien.Text;
            Response.Redirect(newPath);
            //Server.Transfer(newPath);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            //Initialise les parametres
            getParam();
            //Recupere le No Cammande
              GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
              Label lien = (Label)grv.FindControl("lblLien1");
 
            //Supprime la table temporaire
              Connexion.DeleteTableTemp(lien.Text);
            //Met a jour la table TempAmem
              Response.Redirect(Request.RawUrl);

        }
    }
}