﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Threading;
using ConvertToCSV;

namespace RetourArchambault.AchatAdmin
{
    public partial class WebForm2 : System.Web.UI.Page
    {
      //  private int m_qty;
      //  private double m_amount;
        private string query;
     //   private int dept;
        private int cie;
     //   private DataSet ds1;
        private int count;
        private double amount;
   //     private int qty_over;
   //     private int qty_minus;
   //     private string m_condition;
   //     private string TransactName;
        private string Lien;
        private int idUser;
        private Usr usager = null;
   //     private string m_pathFiles;
        private int AutCount = 0;
        private double AutAmount = 0.0;
        public string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            initialise();

            Tool.setThread_FR();

            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;

    
            if (!IsPostBack)
            {
               
                 Session["Lien"] = Request.Params["Lien"]; 
                 Session["IdUsr"] = int.Parse(Request.Params["Usr"]); 

                getParam();

                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    BinData();

                }
                else
                {
                    if (usager.is_admin)
                    {
                        Response.Redirect("Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                    }
                    else
                    {
                        Response.Redirect("Achat/Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                    }

                }
            }
        }
        private void BinData()
        {
            //Initialise les paramètres fournit par l'url
            getParam();

            //Requete pour vérifier quantité en date du jour
            //   VerifQuantite vf = new VerifQuantite(this.Lien);

            //Lit la requete stockée
            query = Tool.getQuery("WebForm1_1.sql"); // File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/WebForm1_1.sql"));
            //Insère les paramètrews dans la requête
            query = String.Format(query, this.Lien,this.Lang);
            log.Log(query);

            setData(query);

        }
        private void setData(string query)
        {
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            //Ouvre connexion
            SqlConnection c_con = Connexion.ConnectSQL();
            SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
            try
            {
                //Remplit dataset
                adapter.Fill(ds);
                count = 0;
                amount = 0.0;
                Session["dtwf2"] = ds.Tables[0];
               // Globals.g.dt = (DataTable)Session["dtwf2"];
                setTopSection();
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }

        }
        void setTopSection()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtwf2"];
            //initialise Qty et montant
            foreach (DataRow row in dt.Rows)
            {
                int tempCount = 0;
                double tempAmount = 0.0;

                count += int.Parse(row[12].ToString());
                amount += double.Parse(row[13].ToString());
                if (String.IsNullOrEmpty(row[20].ToString())) { tempCount = 0; } else { tempCount = int.Parse(row[20].ToString()); }

                if (tempCount != 0)
                {

                    //Faut calculer le montant selon la quantité autorisé
                    tempAmount = (tempCount * double.Parse(row[11].ToString()));
                }
                else
                {
                    //Sinon on ajoute le montant prévu
                    tempAmount = double.Parse(row[13].ToString());
                    //On met à zéro la quantité et le montant de la ligne si annulé
                    if (!String.IsNullOrEmpty(row[19].ToString()))
                    {
                        tempAmount = 0;
                        tempCount = 0;
                    }
                    else
                    {
                        tempCount = int.Parse(row[12].ToString());
                    }
                }
                AutCount += tempCount;
                AutAmount += (tempAmount);
            }
            getParam();
            //Initialise le textbox 
            lbl_Distinct.Text = count.ToString();
            lblRetour_money.Text = String.Format("{0:C2}", amount);
            lblDistinctAutorise.Text = AutCount.ToString();
            lblRetourAutorise.Text = String.Format("{0:C2}", AutAmount);

            if (this.cie == 7)
            {
                GridView1.PageSize = 50;
            }
            else
            {
                GridView1.PageSize = 50;
            }

            if (Session["Sort"] != null)
            {
                DataView dv = dt.DefaultView;
                dv.Sort = Session["Sort"].ToString();
                GridView1.DataSource = dv;
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }


        }
        void initialise()
        {
            //m_amount = 0.0;
            //m_qty = 0;
            query = "";
            //ds1 = null;
            //qty_over = 0;
            //qty_minus = 0;
            //m_condition = "";
            //TransactName = "";
            idUser = 0;
        }
        void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            //Initilise variable selon parametre url
            if (Session["Lien"] != null) { this.Lien = (string)Session["Lien"]; }
            this.cie = int.Parse(this.Lien.Substring(0, 3));
            //Créer Request pour récupérer le userId
           
            //Identification de l'utilisateur
            if (Session["USR"] != null) { usager = (Usr)Session["USR"]; }
            idUser = usager.id;
            usager.idCieParam = usager.idCie;
            if (Session["IdUsr"] != null) { usager.idParam = (int)Session["IdUsr"]; }
            usager.index = 1;
        }

        private void setGrid()
        {
            //Met a jour la section du haut
            setTopSection();
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtwf2"];
            //Met a jour la grille
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            setGrid();
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            TextBox txtRefuse = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtRefuse");
            TextBox txtQty = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtQtyAutorise");
            TextBox txtCommentaires = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtCommentaires");

            if (String.IsNullOrEmpty(txtQty.Text)) { txtQty.Text = "0"; }
            if (txtRefuse.Text.Length != 0) { txtQty.Text = "0"; }
            if (String.IsNullOrEmpty(txtCommentaires.Text)) { txtCommentaires.Text = ""; }

            UpdateCommand(id, int.Parse(txtQty.Text), txtRefuse.Text, txtCommentaires.Text);
            GridView1.EditIndex = -1;
            BinData();

        }
        private void UpdateCommand(int id, int Qty, string refuse, string Commentaires)
        {
            getParam();

            //Creation de la requete
            string query = Tool.getQuery("Update_WebForm1.sql");  
            query = String.Format(query, Qty, refuse, Commentaires, id);
            //Creation de la commande
            Connexion.execCommand(query);
        }
        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            setGrid();
        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            setGrid();
        }
        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtwf2"];
            //Crée un DataView
            DataView dv = new DataView(dt);
            //Trie le Dataview
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression.ToString());
                Session["Sort"] = dt.DefaultView.Sort;
                setGrid();
            }
        }
        /// <summary>
        /// Order de trie des colonnes
        /// </summary>
        /// <param name="column">Nom de la colonne à trier</param>
        /// <returns>Ordre de la direction du trie</returns>
        private string GetSortDirection(string column)
        {
            // Ordre croissance par défaut.
            string sortDirection = "ASC";
            // Recherche le dernier ordre de croissance.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                //Vérifie si c"est la meme colonne qui doit être triée
                //Sinon la valeur par defaut peut être retourné
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }

            // Sauvegarde le dernier resultat dans le ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }
        protected void GridView1_DataBound(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Convert.ToInt32(drv["Qty_a_Retourner"]) != Convert.ToInt32(drv["Qty_autorise"]))
                    {
                        if (!String.IsNullOrEmpty(drv["Refuse"].ToString()))
                        {
                            e.Row.Font.Bold = true;
                            e.Row.ForeColor = System.Drawing.Color.White;
                            e.Row.BackColor = System.Drawing.Color.Blue;
                        }
                        else if (!String.IsNullOrEmpty(drv["Commentaires"].ToString()))
                        {
                            e.Row.Font.Bold = true;
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                        }

                        else
                        {
                            e.Row.Font.Bold = true;
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                        }

                    }
                    else if (!String.IsNullOrEmpty(drv["Commentaires"].ToString()))
                    {
                        e.Row.Font.Bold = true;
                        e.Row.BackColor = System.Drawing.Color.Yellow;
                    }

                    e.Row.Cells[14].ToolTip = "Modifier";
                    if (e.Row.RowState == DataControlRowState.Edit || e.Row.RowState.ToString() == "Alternate, Edit")
                    {
                        int i = 0;
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            if (e.Row.Cells.GetCellIndex(cell) == 14)
                            {
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[14].Controls[0])).ToolTip = "Modifier ou Refuser";
                                ((System.Web.UI.LiteralControl)(e.Row.Cells[14].Controls[1])).Text = "&nbsp;";
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[14].Controls[2])).ToolTip = "Fermer modification";
                            }
                            i++;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }

        }

        protected void BtnSend_Click(object sender, EventArgs e)
        {
            getParam();


            //Mettre à jour Amem_Link
            Update_TransLink ul = new Update_TransLink();
            ul.upDate_TransLink(this.Lien, 3);
            // Préparer l'envoi courriel


            //Créer un lien dans le Transaction Store
            CreateTransactionStore cts = new CreateTransactionStore();
            bool rep = cts.createTransactionStore(this.Lien);
            //Enregistrer dans la table transaction en cours pour la liste de Amem afin qu'il puisse suivre les transactions complété ou non

            if (rep)
            {
                //Enregistrer dans Transaction_Store_List
                CreateTransactionStoreList ctsl = new CreateTransactionStoreList();
                bool cs = ctsl.createTransactionStoreList(this.Lien);

                if (cs)
                {
                    Response.Redirect("../succes.aspx");
                }
            }
            else
            {
                if (usager.is_admin)
                {
                    Response.Redirect("Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                }
                else
                {
                    Response.Redirect("Achat/Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                }
            }


        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void ProviderEmail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            getParam();
            VerifQuantite vq = new VerifQuantite();
            //Rafraichir la grille
            BinData();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            getParam();
            Response.Redirect("dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam);
        }
    }
}