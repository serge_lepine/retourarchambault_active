﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="UploadDesu.aspx.cs" Inherits="RetourArchambault.AchatAdmin.UploadDesu" %>

<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">

            <div class="container">
                <div class="rapport_container">
                    <div class="button_cont">
                        <asp:FileUpload runat="server" Height="37px" Width="225px" BackColor="" ID="FileUploadToServer" />

                        <asp:Button ID="BtnUpLoad" CssClass="btn primaryAction" runat="server" Text="Charger le fichier" Visible="true" OnClick="BtnUpLoad_Click" />
                        <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
     <%--   </div>
    </div>--%>
    <asp:GridView ID="GridView1" runat="server"></asp:GridView>
</asp:Content>
