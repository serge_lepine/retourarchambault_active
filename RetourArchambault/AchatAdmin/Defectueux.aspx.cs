﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
namespace RetourArchambault.AchatAdmin
{
    public partial class Defectueux : System.Web.UI.Page
    {
        private Usr usager = new Usr();
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Contents.Remove("sort");
            Session.Contents.Remove("Date_Limite");
            Session.Contents.Remove("RefNumber");
            Session.Contents.Remove("RETOUR");
            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {

                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }

                getParam();

                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    //Affiche le menu
                    setMenu();
                   // Load datagrid
                    BinData();
                    

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }


        }
        private void setMenu()
        {
            getParam();
            getParam();
            //Menu 1
            this.Master.HyperLink1Text = "Tableau de bord";
            this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam;
            this.Master.HyperLink1Visible = true;

            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;

            this.Master.Lang = "fr";
        }
        private void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = (int)Session["Cie"]; }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            if (Session["CieSearch"] != null) { usager.CieSearch = (int)Session["CieSearch"]; }

            usager.index = 1;
        }

        protected void btnLoadData_Click(object sender, EventArgs e)
        {
            //LoadDefectueux ld = new LoadDefectueux();
            //try
            //{
            //    if (!ld.TransfertData()) { ScriptManager.RegisterStartupScript(this, GetType(), "btnLoadData", "alert('Aucun produit a traiter');", true); }
            //}
            //catch
            //{
               
            //}
            //BinData();
        }
        private void reloadData()
        {
            
                LoadDefectueux ld = new LoadDefectueux();
                try
                {
                    if (!ld.TransfertData()) { ScriptManager.RegisterStartupScript(this, GetType(), "btnLoadData", "alert('Aucun produit a traiter');", true); }
                }
                catch
                {
                    log.logError("Page Défectueux reloadData");
                }
            
          
        }
        private void BinData()
        {
            
            //Met à jour une nouvelle table
            reloadData();
            //get_defectueux
            string query = Query.GetDefectueux(); //Tool.getQuery("getDefectueux.sql");
            query = String.Format(query, Lang);
            log.LogQuery(query);
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            //Ouvre connexion
            SqlConnection c_con = Connexion.ConnectSQL();
            SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
            
            try
            {

                //Replit dataset
                adapter.Fill(ds);
                Session["defTab"] = ds.Tables[0];
                dt = (DataTable)Session["defTab"];

                GridView1.DataSource = dt;
                GridView1.DataBind();


            }
            catch 
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "btnLoadData", "alert('Aucun produit a traiter');", true);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }

        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
       
            DataTable dt = (DataTable)Session["defTab"];
            //Crée un DataView
            DataView dv = new DataView(dt);
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                Session["sort2"] = dt.DefaultView.Sort;
                setGrid();
            }
        }
        private void setGrid()
        {
            GridView1.DataSource = (DataTable)Session["defTab"];

            GridView1.DataBind();
           // saveCommande();



        }
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            getParam();

            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label Nocie = (Label)grv.FindControl("lblNoCie");
            Label NoRetour = (Label)grv.FindControl("lblRetour");

            int NoCie = int.Parse(Nocie.Text);

           //Créer lien pour la sélection
            string lien = Tool.creerLien(NoCie, 3, 999);

            //Transfert vers table temporaire
            NewTransaction nt = new NewTransaction();
            nt.NoCie = NoCie;
            nt.Lien = lien;
            nt.NoRetour = NoRetour.Text;

            CreateNewProvider cp = new CreateNewProvider(nt);

            if (cp.createDefectProvider()) { cp.deleteDefectProvider(); }

            string p = "../Achat/AmemView.aspx?CieSearch=" + NoCie + "&Cie=" + usager.idCie + "&Dept=999&Usr=" + usager.id + "&Lien=" + nt.Lien;

            Response.Redirect(p);
        }
        /// <summary>
        /// Order de trie des colonnes
        /// </summary>
        /// <param name="column">Nom de la colonne à trier</param>
        /// <returns>Ordre de la direction du trie</returns>
        private string GetSortDirection(string column)
        {
            // Ordre croissance par défaut.
            string sortDirection = "ASC";
            // Recherche le dernier ordre de croissance.
            string sortExpression = ViewState["SortExpressionDef"] as string;

            if (sortExpression != null)
            {
                //Vérifie si c"est la meme colonne qui doit être triée
                //Sinon la valeur par defaut peut être retourné
                string lastDirection = ViewState["SortDirectionDef"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }

            // Sauvegarde le dernier resultat dans le ViewState.
            ViewState["SortDirectionDef"] = sortDirection;
            ViewState["SortExpressionDef"] = column;

            return sortDirection;
        }

    }
}