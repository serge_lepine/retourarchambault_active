﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="HistoriqueStore.aspx.cs" Inherits="RetourArchambault.AchatAdmin.HistoriqueStore" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
     <asp:Image ID="Image3" ImageUrl="~/images/imageDefault.jpg" runat="server" />
    <asp:GridView ID="gvHistoriqueStore" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:TemplateField HeaderText="Id">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="CieName">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="NoMag">
                <ItemTemplate>
                    <asp:Label ID="lblNoMag" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date Envoi">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("Date_Envoi") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cueillette">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("Date_Cueillette") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dept">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AuthTemp">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("AuthTemporaire") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AuthProvider">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("AuthProvider") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Auth400">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Auth400") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty Retourner">
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Qty_Retourner") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Montant">
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("Montant_retourne","{0:C2}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lien" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Voir">
                <ItemTemplate>
                    <asp:Button ID="btnDetails" OnClick="btnDetails_Click" runat="server" Text="Details" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
     </asp:GridView>
</asp:Content>
