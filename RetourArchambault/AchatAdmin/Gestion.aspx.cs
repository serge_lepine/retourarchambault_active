﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace RetourArchambault.AchatAdmin
{
    public partial class Gestion : System.Web.UI.Page
    {

        private Usr usager = new Usr();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
           
            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {

                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                getParam();
                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    setMenu();
                    //Remplit Grille 1
                   
                 
                    gvFournisseur.DataSource = setGrid(Query.setUser());
                    gvFournisseur.DataBind();

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void getParam()
        {
            
            if (Session["USR"] != null) { usager = (Usr)Session["USR"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = (int)Session["Cie"]; }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            usager.index = 1;
        }
        private void setMenu()
        {
            getParam();
            //Menu 1"
            //this.Master.HyperLink1Text = "Tableau de bord";
            //this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr=" + usager.idParam + "&Cie=" + usager.idCieParam;
            //this.Master.HyperLink1Visible = true;
            ////Menu Quitter
            //this.Master.HyperLink2Text = "Quitter";
            //this.Master.HyperLink2URL = "/Login.aspx";
            //this.Master.HyperLink2Visible = true;

            //this.Master.Lang = "fr";
            //Bonjour
            //this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name;

        }
        private IDataReader setGrid(string query)
        {
            SqlDataReader record = null;
            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd = new SqlCommand(query, con);
            //    record = cmd.ExecuteReader();

            int i = Convert.ToInt32(cmd.ExecuteScalar());
            if (i > 0)
            {
                gvFournisseur.Visible = true;
                record = cmd.ExecuteReader();
            }
            return record;
        }
    }
}