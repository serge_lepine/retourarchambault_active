﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Succes.aspx.cs" Inherits="RetourArchambault.Succes1" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">                
                <div class="container">
                    <h2 class="title"><%=(Lang == "fr") ? "Merci" : "Thank you"%></h2>
                    <p><%=(Lang == "fr") ? "Votre transaction c'est déroulé avec succès, un courriel a été envoyé automatiquement à votre destinataire pour traitement.<br /> Merci d'utiliser Retour Archambault" : "Your transaction was succesful. An email was sent automatically to your recipient for treatment.<br /> Thank you for using Retour Archambault."%></p>
                   
                </div><!--END /.container-->
           
            
        
</asp:Content>
