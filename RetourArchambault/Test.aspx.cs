﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace RetourArchambault
{
   
    public partial class Test : System.Web.UI.Page
    {

        Crypte cryp;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            cryp = new Crypte();
            Label1.Text = cryp.crypte(TextBox1.Text);
            Label2.Text = cryp.decrypte(Label2.Text);
        }
        private string response(byte[] array)
        {
            StringBuilder hex = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
            {
                hex.AppendFormat("{0:X2}", b);
            }

            return hex.ToString();
           
        }
    }
}