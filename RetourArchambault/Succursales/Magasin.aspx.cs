﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.IO;
using System.Data.OleDb;

namespace RetourArchambault.Succursales
{
    public partial class Magasin : System.Web.UI.Page
    {
        //private DataTable m_dt;
        //private ZoneRepeater zr;
        private Usr usager = new Usr();
        private int id_Usr;
        private int NoMag;
     //   private string arrLien = "";
        private SqlConnection c_con = null;
        private string Lang;
        public string ErrorSku;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.MaintainScrollPositionOnPostBack = true;
            if (!IsPostBack)
            {

                { Session["cie"] = int.Parse(Request.Params["Cie"]); }
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                getParam();
                DroitAcces da = new DroitAcces(this.usager);

                if (!da.VerifAcces())
                {
                    Response.Redirect("Login.aspx");
                }
                initialise();
                SetInitialRow();
                setMenu();
            }
        }
        private void initialise()
        {
            //m_dt = null;
            //zr = null;
            id_Usr = int.Parse(Request.Params["usr"]);
            this.NoMag = int.Parse(Request.Params["cie"]);
        }
        private void setMenu()
        {
            getParam();
            initialise();
            //Menu historique
            this.Master.HyperLink1Text = "Historique";
            this.Master.HyperLink1URL = "/Succursales/HistoriqueMagasin.aspx?usr=" + id_Usr + "&cie=" + this.NoMag;
            this.Master.HyperLink1Visible = true;
            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;
            this.Master.Lang = "fr";
     
        }
        private void getParam()
        {
            if (Session["lang"] != null) { this.Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["USR"] != null) { usager = (Usr)Session["USR"]; }
            if (Session["cie"] != null) { this.NoMag = (int)Session["cie"]; }
            if (Session["IdUsr"] != null) { this.id_Usr = (int)Session["IdUsr"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = this.NoMag; }
            if (usager.idParam == 0) { usager.idParam = this.id_Usr; }
            usager.index = 3;

        }
        /// <summary>
        /// Remplit les sections
        /// </summary>
        private void SetInitialRow()
        {
            

            BindDataGrid1();



        }


        private void BindDataGrid1()
        {

            //Initialise les paramètres fournit par l'url
            getParam();

            string query = "";
            Tool.setThread_EN();
            query = Tool.getQuery("StoreGrid1.sql");
            query = String.Format(query, usager.idCie,this.Lang);
            log.Log(query);
            Tool.setThread_FR();
            if (Connexion.execScalar(query))
            {
                Image1.Visible = false;
                setData(query);
            }
            else
            {
                Image1.Visible = true;
            }

        }
        private void setData(string query)
        {
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            //Ouvre connexion
            c_con = Connexion.ConnectSQL();
            SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
            try
            {

                //Replit dataset
                adapter.Fill(ds);
                Session["dtm"] = ds.Tables[0];


            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }

            if (ds.Tables.Count > 0)
            {
                DataTable dt = new DataTable();
                dt = (DataTable)Session["dtm"];
                GridView1.DataSource = dt;
                GridView1.DataBind();

            }


        }

        //protected void btnPDF_Click(object sender, EventArgs e)
        //{

        //}
        private void setGrid()
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtm"];
            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }

        }
        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            setGrid();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            TextBox txtNoRetour = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtAuth400");
            Label NoMag = (Label)GridView1.Rows[e.RowIndex].FindControl("Label7");
            Label lblLien = (Label)GridView1.Rows[e.RowIndex].FindControl("lblLien");
            Label NoAutorisation = (Label)GridView1.Rows[e.RowIndex].FindControl("lblAuthProvider");
            char[] myChar = txtNoRetour.Text.ToCharArray();

            bool rep = false;

            //Vérifie si champ vide
            if (!String.IsNullOrEmpty(txtNoRetour.Text) || !String.IsNullOrWhiteSpace(txtNoRetour.Text))
            {
                //Verifie si nombre seulement

                foreach (char c in myChar)
                {
                    if (Char.GetNumericValue(c) >= 0)
                    {
                        rep = true;
                    }
                    else
                    {
                        rep = false;
                        break;
                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "confirmation", "confirmation()", true);
            }
            if (!rep)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "confirmation", "confirmation()", true);
            }
            else
            {

                if (UpdateCommand(id, txtNoRetour.Text, DateTime.Now, NoMag.Text, lblLien.Text,NoAutorisation.Text))
                {
                    DataTable dt = new DataTable();
                    dt = (DataTable)Session["dtm"];
                    dt.Rows[row.DataItemIndex]["Id"] = id.ToString();
                    dt.Rows[row.DataItemIndex]["Auth400"] = txtNoRetour.Text;
                    dt.Rows[row.DataItemIndex]["Date_Limite"] = DateTime.Now;
                    Session.Contents.Remove("dtm");
                    Session["dtm"] = dt;

                    GridView1.EditIndex = -1;
                    GridView1.Rows[e.RowIndex].Cells[3].Visible = true;
                }
                setGrid();

            }

        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            setGrid();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="AuthText"></param>
        /// <param name="dat"></param>
        /// <param name="idProvider"></param>
        /// <param name="lien"></param>
        /// <returns></returns>
        private bool UpdateCommand(int id, string AuthText, DateTime dat,string idProvider,string lien,string noAutorisation)
        {
            getParam();
            ValidationRetour_Reponse vr1_response ;
            ValidationRetour_1 vr1;
            ValidationRetour_Param vrp;
            ValidationRetour_2 vr2;
            //Étape 2 Validation des statuts


            // Étape 3 Validation Sku
           // string inumbr = string.Empty;
            //Vérification des sku de la commande avec ceux du No retour
            OleDbConnection con400 = Connexion.Connect();

            // 1 - Vérifier status par type = 1 Overstock ('F','C', 'Z', 'B'  selon le type de transaction 
            vrp = new ValidationRetour_Param();
            vrp.Conn400 = con400;
            vrp.NoFournisseur = int.Parse(idProvider);
            vrp.NoMag = this.NoMag;
            vrp.NoRetour = AuthText;
            vrp.TypeTransaction = Tool.getTransactionType(lien);

            vr1 = new ValidationRetour_1(vrp);
            vr1_response = new ValidationRetour_Reponse();
            vr1_response = vr1.validationRetour_1();
            
            if(vr1_response.succes == false)
            {
                //Fermer la connection
                Connexion.close(con400);
                //Afficher message d'erreur
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scripts", "<script>alert('" + vr1_response.errorMessage + "')</script>", false);
                return false;
            }

            // 2 - Valider les sku de la commandes
            vrp = new ValidationRetour_Param();
            vrp.Conn400 = con400;
            vrp.Lien = lien;
            vrp.NoRetour = AuthText;

            vr2 = new ValidationRetour_2(vrp);
            vr1_response = new ValidationRetour_Reponse();
            vr1_response = vr2.validationRetour_2();

            if (vr1_response.succes == false)
            {
                //Fermer la connection
                Connexion.close(con400);
                //Afficher message d'erreur
                Page.ClientScript.RegisterStartupScript(this.GetType(), "scripts", "<script>alert('" + vr1_response.errorMessage + "')</script>", false);
                return false;
            }

            //Recherche dans AS400 la quantite retourne et le montant
            string query400 = string.Empty;
            query400 = String.Format(Query.Return400(), AuthText, this.NoMag,idProvider);
            log.Log("Magasins.aspx Return400.sql :" + query400);
            OleDbCommand cmd = new OleDbCommand(query400, con400);
            OleDbDataReader reader;
            IDataRecord record;
            int qtyReturn = 0;
            double AmountReturn = 0.0;

            try
            {
                //Verifie si valeur de retour sinon afficher Popup
                int i = Convert.ToInt32(cmd.ExecuteScalar());

                if (i != 0)
                {
                    reader = cmd.ExecuteReader();
                    reader.Read();

                    record = (IDataRecord)reader;
                    qtyReturn = int.Parse(record[1].ToString());
                    AmountReturn = double.Parse(record[2].ToString());

                    Tool.setThread_EN();
                    //Creation de la requete pour montant retourné
                    string query = string.Empty;
                    query = String.Format(Query.UpdateTransactionStore(), AuthText, 1, id, dat, qtyReturn, AmountReturn);
                    log.Log(query);
                    Tool.setThread_FR();
                    Connexion.execCommand(query);

                    query = string.Empty;
                    query = String.Format(Query.UpdateRTVSaut(), AuthText,DateTime.Now.ToString("yyyy-MM-dd-HH.mm.ss.ffffff") ,noAutorisation);
                    log.Log(query);
                    cmd = new OleDbCommand(query, con400);
                    cmd.ExecuteNonQuery();

                   
                    return true;
                }
                else
                {
                 //   Page.ClientScript.RegisterStartupScript(this.GetType(), "scripts", "<script>alert('" + String.Format("Le SKU {0} ne fait pas partie de cette commande.<br/> SVP enlever cette article de la commande et recommencer la validation Merci!", inumbr) + "')</script>", false);
                    ScriptManager.RegisterStartupScript(this, GetType(), "confirmation2", "confirmation2()", true);
                    return false;
                }


            }
            catch (Exception ex)
            {
                qtyReturn = 0;
                AmountReturn = 0.0;
                log.logError("Magasin.aspx 196 :" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                Connexion.close(con400);
            }



        }
        private bool validationRetour_1(OleDbConnection Conn400, string NoRetour,int NoMag,int NoCie,int TranactionType)
        {
            string query = String.Format(Query.Validation_1(), NoRetour);
            OleDbCommand cmd400 = new OleDbCommand(query,Conn400);

            
            return true;
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Cells[6].ToolTip = "Modifier";
                    if (e.Row.RowState == DataControlRowState.Edit || e.Row.RowState.ToString() == "Alternate, Edit")
                    {
                        int i = 0;
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            if (e.Row.Cells.GetCellIndex(cell) == 7)
                            {
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[7].Controls[0])).ToolTip = "Modifier";
                                ((System.Web.UI.LiteralControl)(e.Row.Cells[7].Controls[1])).Text = "&nbsp;";
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[7].Controls[2])).ToolTip = "Confirmer modification";
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logError("GridView1_RowDataBound : " + ex.Message);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lbNoCie = (Label)grv.FindControl("Label7");
            Label lbNoMag = (Label)grv.FindControl("Label8");
            int NoCie = int.Parse(lbNoCie.Text);
            int NoMag = int.Parse(lbNoMag.Text);
            Label lien = (Label)grv.FindControl("lblLien");
            Label CieName = (Label)grv.FindControl("Label1");


            SqlConnection con = Connexion.ConnectSQL();
            string query = "";
            Tool.setThread_EN();

            if (Tool.getTransactionType(lien.Text) == 3)
            {
                query = Tool.getQuery("StoreListeType3.sql");
            }
            else
            {
                query = Tool.getQuery("StoreList.sql");
            }
            //========================= PDF =================================
             
            query = String.Format(query, NoMag, NoCie, lien.Text,this.Lang);
            log.Log(query);
            Tool.setThread_FR();
            DataSet ds = new DataSet();
            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                IDataReader idr = cmd.ExecuteReader();
                GridView2.DataSource = idr;
                GridView2.DataBind();
                idr.Close();
               
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {

                //Ferme la connexion
                Connexion.closeSQL(con);
            }

            //Appeler pdf
            PdfPTable pdfTable = new PdfPTable(GridView2.HeaderRow.Cells.Count);
            //Font
          
            iTextSharp.text.pdf.BaseFont bf = iTextSharp.text.pdf.BaseFont.CreateFont(HttpContext.Current.Server.MapPath("/fonts/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED,true);
           
            pdfTable.WidthPercentage = 100;
           
            if (pdfTable != null)
            {
                pdfTable.SetWidths(new int[] { 4, 4, 6, 2, 4, 2, 6, 10, 10, 6, 2,5, 2, 3, 3, 2 });
            }

            foreach (TableCell headerCell in GridView2.HeaderRow.Cells)
            {
                
                iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);

                font.Color = new BaseColor(GridView2.HeaderStyle.ForeColor);
                //  font.Size = 8;
                
                PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text, font));
                pdfCell.BackgroundColor = new BaseColor(GridView2.HeaderStyle.BackColor);
                pdfCell.Column.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                pdfTable.AddCell(pdfCell);
            }

            

            foreach (GridViewRow gridViewRow in GridView2.Rows)
            {
                int i = 0;
                int qty = 0;

                foreach (TableCell tableCell in gridViewRow.Cells)
                {
                    string p = GridView2.HeaderRow.Cells[i].Text;
 
                    iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);
                    font.Color = new BaseColor(GridView2.RowStyle.ForeColor);
                    
                    PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text, font));
                 
                    //Si Qty = 0 alors modifier couleur de la ligne
                     if(i == 14)
                    {
                        if(!String.IsNullOrEmpty(tableCell.Text)){qty = int.Parse(tableCell.Text);} else{qty = 0;}

                         if(qty == 0)
                         {
                             pdfCell.BackgroundColor = new BaseColor(System.Drawing.Color.Yellow);
                         }
                         else
                         {
                             pdfCell.BackgroundColor = new BaseColor(GridView2.RowStyle.BackColor);
                         }
                       
                    }
                     else
                     {
                         pdfCell.BackgroundColor = new BaseColor(GridView2.RowStyle.BackColor);
                     }

                    
                    pdfCell.Column.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                    pdfCell.HorizontalAlignment = iTextSharp.text.Image.ALIGN_CENTER;
                    pdfTable.AddCell(pdfCell);
                    i++;
                }
            }

            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("/images/lg_archambault01Red_sm.png"));
            logo.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

            pdfTable.HeaderRows = 1;

            Document pdfDocument = new Document(PageSize.LEGAL.Rotate(), 5f, 15f, 20f, 0f);

            PdfWriter.GetInstance(pdfDocument, Response.OutputStream);


           
            pdfDocument.Open();
            pdfDocument.Add(logo);
            pdfDocument.Add(pdfTable);

            pdfDocument.Close();
            Response.Charset = "utf-8";
            Response.ContentType = "application/pdf";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.AppendHeader("content-disposition", "attachment;filename=" + CieName.Text + ".pdf");
            Response.Write(pdfDocument);
            Response.Flush();
            Response.End();
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void btnQuitter_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Login.aspx");

        }

        public void ExportToExcel(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                getParam();
                string filename = "DownloadListNoExcel.xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();
                
                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = "";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            getParam();
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            Label lbNoCie = (Label)grv.FindControl("Label7");
            Label lbNoMag = (Label)grv.FindControl("Label8");
            int NoCie = int.Parse(lbNoCie.Text);
            int NoMag = int.Parse(lbNoMag.Text);
            Label lien = (Label)grv.FindControl("lblLien");
            Label CieName = (Label)grv.FindControl("Label1");

            //================== EXCEL===================================
            //Créer DataTable
            SqlConnection con = Connexion.ConnectSQL();
            Tool.setThread_EN();
            string query = String.Empty;
            if (Tool.getTransactionType(lien.Text) == 3)
            {
                query = Tool.getQuery("StoreListeType3.sql");
            }
            else
            {
                query = Tool.getQuery("StoreList.sql");
            }
            query = String.Format(query, NoMag, NoCie, lien.Text,this.Lang);
            log.Log(query);
            Tool.setThread_FR();
            DataSet ds = new DataSet();

            SqlDataAdapter adp = new SqlDataAdapter(query, con);
            DataTable dt = new DataTable();
            try
            {
                adp.Fill(ds);
                dt = ds.Tables[0];
                ExportToExcel(dt);
            }
            catch (Exception ex)
            {
                log.logError("Magasin btnExcel_Click : " + ex.Message + " StackTrace : " + ex.StackTrace);
            }
            finally
            {
                Connexion.closeSQL(con);
            }
        }

    }
}