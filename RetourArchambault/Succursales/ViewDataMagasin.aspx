﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewDataMagasin.aspx.cs" Inherits="RetourArchambault.Succursales.ViewDataMagasin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />
    <title>Retour Magasins</title>
</head>
<body>
    <form id="form1" runat="server">
        <div id="header">
            <asp:Image ID="entete" ImageUrl="~/images/lg_archambault01White.png" AlternateText="Archambault" runat="server" />
        </div>
        <div class="result_table_cont">
            <div class="result_table_cont">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:TemplateField HeaderText="Retour" SortExpression="Retour">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoRetour") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Catégorie" SortExpression="Category">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Categorie") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No Mag" SortExpression="NoMag">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SKU" SortExpression="SKU">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("SKU") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ISBN" SortExpression="ISBN">
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("ISBN") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AUTEUR" SortExpression="AUTEUR">
                            <ItemTemplate>
                                <asp:Label ID="Label7" runat="server" Text='<%# Eval("AUTEUR") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TITRE" SortExpression="TITRE">
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("TITRE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Compositeur" SortExpression="Compo">
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("Compos") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FORMAT" SortExpression="FORMAT">
                            <ItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("FORMAT") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Étiquette" SortExpression="ETIQUETTE">
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("Etiquette") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="À Garder" SortExpression="QtyAGarder">
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Eval("AGarder") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="À Retourner" SortExpression="QtyARetourner">
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("ARetourner") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="En Stock">
                            <ItemTemplate>
                                <asp:Label ID="Label14" runat="server" Text='<%# Eval("EnStock") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div id="footer">
        </div>
    </form>
</body>
</html>
