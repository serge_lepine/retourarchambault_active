﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;

namespace RetourArchambault.Succursales
{
    public partial class HistoriqueMagasin : System.Web.UI.Page
    {
        private Usr usager = new Usr();
        private int NoMag, id_Usr;//, Cie;
        private string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;

            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {

                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                getParam();
                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    setMenu();
                    //Remplit Grille 1
                    string query = Tool.getQuery("gvHistStore.sql");
                    query = String.Format(query, NoMag, this.Lang);
                    gvHistStore.DataSource = setGrid(query);
                    gvHistStore.DataBind();

                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        private void setMenu()
        {

            getParam();
            //Menu 1
            this.Master.HyperLink1Text = "Page précédente";
            this.Master.HyperLink1URL = "/Succursales/Magasin.aspx?usr=" + id_Usr + "&cie=" + NoMag;
            this.Master.HyperLink1Visible = true;
            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;
            this.Master.Lang = "fr";
            //Bonjour
            //     this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name;

        }
        private void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["Cie"] != null) { this.NoMag = (int)Session["Cie"]; }
            if (Session["IdUsr"] != null) { this.id_Usr = (int)Session["IdUsr"]; }
            if (Session["USR"] != null) { usager = (Usr)Session["USR"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = this.NoMag; }
            if (usager.idParam == 0) { usager.idParam = this.id_Usr; }
            usager.index = 3;
        }
        private IDataReader setGrid(string query)
        {
            SqlDataReader record = null;
            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd = new SqlCommand(query, con);
            DataTable dt = new DataTable();

            int i = Convert.ToInt32(cmd.ExecuteScalar());
            if (i > 0)
            {
                Image3.Visible = false;
                gvHistStore.Visible = true;
                record = cmd.ExecuteReader();
               
            }
            return record;
        }
        private string setDataTable(GridViewRow grv)
        {
            DataTable dt = new DataTable();
           
            getParam();
            Label lbNoCie = (Label)grv.FindControl("Label4");
            Label lbNoMag = (Label)grv.FindControl("Label2");
            int NoCie = int.Parse(lbNoCie.Text);
            int NoMag2 = int.Parse(lbNoMag.Text);
            Label lien = (Label)grv.FindControl("lblLien");
            Label CieName = (Label)grv.FindControl("Label5");

            SqlConnection con = Connexion.ConnectSQL();
            Tool.setThread_EN();
            string query = Tool.getQuery("StoreList.sql");
            query = String.Format(query, NoMag2, NoCie, lien.Text, this.Lang);
            log.Log(query);
            Tool.setThread_FR();
            DataSet ds = new DataSet();
            SqlDataReader idr;

            SqlCommand cmd = new SqlCommand(query, con);
            try
            {
                idr = cmd.ExecuteReader();
                dt.Load(idr);
                Session["dths"] = dt;
                GridView2.DataSource = dt;
                GridView2.DataBind();
                idr.Close();
                //Remplit dataset
                //adapter.Fill(ds);
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                cmd.Dispose();
                //Ferme la connexion
                Connexion.closeSQL(con);
            }
            return CieName.Text;

        }
        protected void btnAffiche_Click(object sender, EventArgs e)
        {
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            string CieName = setDataTable(grv);
            //getParam();
            //Label lbNoCie = (Label)grv.FindControl("Label4");
            //Label lbNoMag = (Label)grv.FindControl("Label2");
            //int NoCie = int.Parse(lbNoCie.Text);
            //int NoMag2 = int.Parse(lbNoMag.Text);
            //Label lien = (Label)grv.FindControl("lblLien");
            //Label CieName = (Label)grv.FindControl("Label5");

            //SqlConnection con = Connexion.ConnectSQL();
            //Tool.setThread_EN();
            //string query = Tool.getQuery("StoreList.sql");
            //query = String.Format(query, NoMag2, NoCie, lien.Text, this.Lang);
            //log.Log(query);
            //Tool.setThread_FR();
            //DataSet ds = new DataSet();
            //SqlCommand cmd = new SqlCommand(query, con);
            //try
            //{
            //    IDataReader idr = cmd.ExecuteReader();
            //    GridView2.DataSource = idr;
            //    GridView2.DataBind();
            //    idr.Close();
            //    //Remplit dataset
            //    //adapter.Fill(ds);
            //}
            //catch (Exception ex)
            //{
            //    log.logError(ex.Message);
            //}
            //finally
            //{

            //    //Ferme la connexion
            //    Connexion.closeSQL(con);
            //}

            try
            {

                //Appeler pdf
                PdfPTable pdfTable = new PdfPTable(GridView2.HeaderRow.Cells.Count);
                //Font
                iTextSharp.text.pdf.BaseFont bf = iTextSharp.text.pdf.BaseFont.CreateFont(HttpContext.Current.Server.MapPath("/fonts/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED, true);
                pdfTable.WidthPercentage = 100;
                if (pdfTable != null)
                {
                    pdfTable.SetWidths(new int[] { 4, 4, 6, 2, 4, 2, 6, 10, 10, 6, 2, 2, 3, 2 });
                }

                foreach (TableCell headerCell in GridView2.HeaderRow.Cells)
                {

                    iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);

                    font.Color = new BaseColor(GridView2.HeaderStyle.ForeColor);
                    //  font.Size = 8;

                    PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text, font));
                    pdfCell.BackgroundColor = new BaseColor(GridView2.HeaderStyle.BackColor);
                    pdfCell.Column.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                    pdfTable.AddCell(pdfCell);
                }

                foreach (GridViewRow gridViewRow in GridView2.Rows)
                {
                    foreach (TableCell tableCell in gridViewRow.Cells)
                    {
                        //  BaseFont bf = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("/fonts/arial.ttf"), "iso-8859-9", true);
                        //  BaseFont bf = BaseFont.CreateFont(HttpContext.Current.Server.MapPath("/fonts/arial.ttf"), BaseFont.IDENTITY_H, BaseFont.EMBEDDED,true);
                        iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 8f, iTextSharp.text.Font.NORMAL);
                        font.Color = new BaseColor(GridView2.RowStyle.ForeColor);
                        //   font.Size = 8;
                        PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text, font));
                        pdfCell.BackgroundColor = new BaseColor(GridView2.RowStyle.BackColor);
                        pdfCell.Column.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                        pdfCell.HorizontalAlignment = iTextSharp.text.Image.ALIGN_CENTER;
                        pdfTable.AddCell(pdfCell);
                    }
                }

                iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath("/images/lg_archambault01Red_sm.png"));
                logo.Alignment = iTextSharp.text.Image.ALIGN_LEFT;

                pdfTable.HeaderRows = 1;
                Document pdfDocument = new Document(PageSize.LEGAL.Rotate(), 5f, 15f, 20f, 0f);

                PdfWriter.GetInstance(pdfDocument, Response.OutputStream);



                pdfDocument.Open();
                pdfDocument.Add(logo);
                pdfDocument.Add(pdfTable);

                pdfDocument.Close();

                Response.ContentType = "application/pdf";
                Response.AppendHeader("content-disposition", "attachment;filename=" + CieName + ".pdf");
                Response.Write(pdfDocument);
                Response.Flush();
                Response.End();
            }
            catch
            {
                log.logError("Historique Magasins btnAffiche_Click : ");
                //  Response.Redirect(Request.RawUrl);
            }

        }

        private void ExportToExcel(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = "DownloadListNoExcel.xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = "";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            GridViewRow grv = ((Button)sender).Parent.Parent as GridViewRow;
            setDataTable(grv);
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dths"];
            ExportToExcel(dt);
        }
    }
}
