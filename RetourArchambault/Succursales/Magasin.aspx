﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Magasin.aspx.cs" Inherits="RetourArchambault.Succursales.Magasin" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function printError() {
            if (confirm('Bonjour <%= Literal1.Text%>')) {
                return true;
            }
            else {
                return false;
            }
        }
        function confirmation2() {
            if (confirm('Le numéro d\'autorisation est invalide ...')) {
                return true;
            }
            else {
                return false;
            }
        }
        function confirmation() {
            if (confirm('Le numéro d\'autorisation ne doit contenir que des nombres sans espace Merci')) {
                return true;
            }
            else {
                return false;
            }
        }
    
    </script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%-- <RowStyle CssClass="odd" />
                                <AlternatingRowStyle CssClass="even" />--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <%--<div class="welcomeMessage">
        <asp:Label ID="lblBonjour" runat="server" Font-Bold="true" Text="Bonjour"></asp:Label>
    </div>--%>
            <%--<div class="container">--%>
                <div class="store_result_table_cont">
                    
                    <%-- <RowStyle CssClass="odd" />
                                <AlternatingRowStyle CssClass="even" />--%>

                    <h2 class="title">Nouveau</h2>
                    <asp:Image ID="Image1" ImageUrl="~/images/imageDefault.jpg" runat="server" />
                    <asp:GridView ID="GridView1"
                         OnRowEditing="GridView1_RowEditing"
                         OnRowUpdating="GridView1_RowUpdating"
                         OnRowCancelingEdit="GridView1_RowCancelingEdit"
                         OnRowDataBound="GridView1_RowDataBound"
                         OnRowCommand="GridView1_RowCommand" runat="server" AutoGenerateColumns="False" Font-Size="8pt" DataKeyNames="Id">
                        <%-- <RowStyle CssClass="odd" />
                                <AlternatingRowStyle CssClass="even" />--%>
                        <Columns>
                            <asp:TemplateField HeaderText="Nom Cie">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoCommande" SortExpression="Lien">
                                <ItemTemplate>
                                    <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Dept">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="# Temporaire">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server"  Text='<%# Eval("AuthTemporaire") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PDF">
                                <ItemTemplate>
                                    <asp:Button ID="btnPdf" runat="server" OnClick="Button1_Click" Text="PDF" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EXCEL">
                                <ItemTemplate>
                                    <asp:Button ID="btnExcel" runat="server" OnClick="btnExcel_Click" Text="EXCEL" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Confirmation">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server"  Visible='<%# DataBinder.Eval(Container.DataItem, "Auth400").ToString() == "" ? false : true %>' Text='<%# DateTime.Now %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="#Autorisation">
                                <ItemTemplate>
                                    <asp:Label ID="lblAuthProvider" Visible='<%# DataBinder.Eval(Container.DataItem, "Auth400").ToString() == "" ? false : true %>' runat="server" Text='<%# Eval("AuthProvider") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="#Retour">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtAuth400" runat="server" Text='<%# Eval("Auth400") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Auth400") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField ControlStyle-Width="25" HeaderText="Confirmer" ButtonType="Image" ShowDeleteButton="false" ShowEditButton="true" ShowHeader="true" CancelImageUrl="~/images/icon-Cancel.png" DeleteImageUrl="~/images/Delete.png" EditImageUrl="~/images/icon-edit.png" UpdateImageUrl="~/images/icon-update.png" >


<ControlStyle Width="25px"></ControlStyle>


                            </asp:CommandField>

                            <asp:TemplateField HeaderText="NoMag" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("IdCie") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NoProvider" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                          <%--  <asp:TemplateField HeaderText="Lien" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="Date Limite" SortExpression="Date_Limite">
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Date_Limite") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Quantité">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Qty") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            <%--</div>--%><!--END /.container-->
            <div class="store pdf_btn">
                <%-- <asp:Button ID="btnPDF" CssClass="createPdf_btn" runat="server" Text="" OnClick="btnPDF_Click" />--%>
                <asp:Button ID="btnQuitter" CssClass="btn secondaryAction" Visible="false" Text="Quitter" runat="server" OnClick="btnQuitter_Click" />
            </div>
            
            
             <!--GridView non visible pour le PDF-->
            <asp:GridView ID="GridView2" Visible="False" runat="server" AutoGenerateColumns="False" CellPadding="4"  ForeColor="#333333" GridLines="None" PageSize="50" Font-Size="6px">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Date_Limite" HeaderText="Date Limite" />
                    <asp:BoundField DataField="Temporaire" HeaderText="#Temp Retour" SortExpression="Temporaire" />
                    <asp:BoundField DataField="Category" HeaderText="Categorie" SortExpression="Category" />
                    <asp:BoundField DataField="NoMag" HeaderText="NoMag" SortExpression="NoMag" />
                    <asp:BoundField DataField="Sku" HeaderText="Sku" SortExpression="Sku" />
                    <asp:BoundField DataField="NoCie" HeaderText="NoCie" SortExpression="NoCie" />
                    <asp:BoundField DataField="UPC" HeaderText="ISBN" ItemStyle-Width="100px" SortExpression="UPC" >
                    <ItemStyle Width="100px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Auteur" HeaderText="Auteur" SortExpression="Auteur" />
                    <asp:BoundField DataField="Titre" HeaderText="Titre" SortExpression="Titre" />
                    <asp:BoundField DataField="Compo" HeaderText="Compos" SortExpression="Compo" ConvertEmptyStringToNull="False" />
                    <asp:BoundField DataField="Format" HeaderText="Format" SortExpression="Format" />
                    <asp:BoundField DataField="Desc" HeaderText="Transaction" SortExpression="Desc" />
                    <asp:BoundField DataField="EnStock" HeaderText="EnStock" SortExpression="EnStock" />
                    <asp:BoundField DataField="Garder" HeaderText="A garder" SortExpression="Garder" />
                    <asp:BoundField DataField="Qty_autorise" HeaderText="Qte autorise" SortExpression="Qty_autorise" />
                    <asp:TemplateField HeaderText=" ">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Height="10px" Width="10px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" Width="100px" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView> 
            <!--END /.container-->
            <div style="clear: both;">&nbsp;</div>
    <asp:Literal ID="Literal1" Visible="false" runat="server"></asp:Literal>
</asp:Content>
