﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="HistoriqueMagasin.aspx.cs" Inherits="RetourArchambault.Succursales.HistoriqueMagasin" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:Image ID="Image3" ImageUrl="~/images/imageDefault.jpg" runat="server" />
    <asp:GridView ID="gvHistStore" runat="server" BackColor="White" Visible="False" BorderColor="#999999" BorderStyle="None" 
        BorderWidth="1px" CellPadding="3" GridLines="Vertical" AutoGenerateColumns="False">
        <AlternatingRowStyle BackColor="#DCDCDC" />
        <Columns>
            <asp:TemplateField HeaderText="Id" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="NoMag">
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Lien" SortExpression="Lien" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="lblLien" runat="server" Text='<%# Eval("Lien") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="IdCie" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("IdCie") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cie" SortExpression="CieName">
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("CieName") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Transaction" SortExpression="Desc">
                <ItemTemplate>
                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Temporaire">
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("AuthTemporaire") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Autorisation" SortExpression="AuthProvider">
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("AuthProvider") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dept" SortExpression="Dept">
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Dept") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="is_complete" Visible="False">
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Eval("is_complete") %>' Enabled="False" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date" SortExpression="Date_Envoi">
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Date_Envoi") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date limite" SortExpression="Date_Limite">
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Date_Limite") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="# Retour" SortExpression="Auth400">
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("Auth400") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cueillette" SortExpression="Date_Cueillette">
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("Date_Cueillette") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qty retourné" SortExpression="Qty_Retourner">
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("Qty_Retourner") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Montant" SortExpression="Montant_retourne">
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("Montant_retourne","{0:C2}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PDF">
                <ItemTemplate>
                    <asp:Button ID="Button1" runat="server" Text="PDF" OnClick="btnAffiche_Click" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EXCEL">
                <ItemTemplate>
                    <asp:Button ID="btnExcel" runat="server" Text="EXCEL" OnClick="btnExcel_Click" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
        <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
        <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#0000A9" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

        <asp:GridView ID="GridView2" Visible="False" runat="server" AutoGenerateColumns="False" CellPadding="4"  ForeColor="#333333" GridLines="None" PageSize="50" Font-Size="6px">
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:BoundField DataField="Date_Limite" HeaderText="Date Limite" />
                    <asp:BoundField DataField="Temporaire" HeaderText="#Temp Retour" SortExpression="Temporaire" />
                    <asp:BoundField DataField="Category" HeaderText="Categorie" SortExpression="Category" />
                    <asp:BoundField DataField="NoMag" HeaderText="NoMag" SortExpression="NoMag" />
                    <asp:BoundField DataField="Sku" HeaderText="Sku" SortExpression="Sku" />
                    <asp:BoundField DataField="NoCie" HeaderText="NoCie" SortExpression="NoCie" />
                    <asp:BoundField DataField="UPC" HeaderText="ISBN" ItemStyle-Width="100px" SortExpression="UPC" >
                    <ItemStyle Width="100px"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Auteur" HeaderText="Auteur" SortExpression="Auteur" />
                    <asp:BoundField DataField="Titre" HeaderText="Titre" SortExpression="Titre" />
                    <asp:BoundField DataField="Compo" HeaderText="Compos" SortExpression="Compo" />
                    <asp:BoundField DataField="Format" HeaderText="Format" SortExpression="Format" />
                    <asp:BoundField DataField="Garder" HeaderText="A garder" SortExpression="Garder" />
                    <asp:BoundField DataField="Qty_autorise" HeaderText="Qte autorise " SortExpression="Qty_autorise" />
                    <asp:TemplateField HeaderText=" ">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox2" runat="server" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Height="10px" Width="10px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" Width="100px" ForeColor="White" />
                <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <SortedAscendingCellStyle BackColor="#FDF5AC" />
                <SortedAscendingHeaderStyle BackColor="#4D0000" />
                <SortedDescendingCellStyle BackColor="#FCF6C0" />
                <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView> 
</asp:Content>
