﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RetourArchambault
{
    public partial class Succes1 : System.Web.UI.Page
    {
        private Usr usager;
        public string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["lang"] == null)
            {
                Lang = "fr";
            }
            else
            {
                Lang = Convert.ToString(Session["lang"]).Substring(0, 2);
            }
            usager = (Usr)Session["USR"];
            if (this.usager != null)
            {
                setMenu();
                //  lblBonjour.Text = "Merci " + this.usager.first_name + " " + this.usager.last_name;
            }
            else
            {
                Response.Redirect("../Login.aspx");
            }
        }
        private void setMenu()
        {

          
            //Menu Quitter
            if (Lang == "en") { this.Master.HyperLink2Text = "Close"; } else { this.Master.HyperLink2Text = "Quitter"; }
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;

            //Bonjour
           // this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name;

        }
        protected void btnRetour_Click(object sender, EventArgs e)
        {
            if (usager.is_amem)
            {
                Server.Transfer("Achat/Amem.aspx?usr=" + usager.id + "&cie=" + usager.idCie);

            }
            else
            {
                Server.Transfer("Fournisseur/Fournisseurs.aspx?usr=" + usager.id + "&cie=" + usager.idCie);

            }

        }

        protected void btnQuitter_Click(object sender, EventArgs e)
        {

            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            if (browser.Browser == "Firefox" || browser.Browser == "Chrome")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                Response.Redirect("Login.aspx");
               
            }





        }
    }
}