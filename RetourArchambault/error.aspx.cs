﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RetourArchambault
{
    public partial class error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            //Inscrit le message d'erreur à l'usagé
           lblError.Text = Session["CurrentError"].ToString();
            //Avise le support par courriel
           Tool.ErrorMail(Session["CurrentError"].ToString());
        }

        protected void btnRetour_Click(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void btnQuitter_Click(object sender, EventArgs e)
        {
            Response.Redirect("login.aspx");
        }
    }
}