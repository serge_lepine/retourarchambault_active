﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RetourArchambault
{
    

    public partial class Maitre : System.Web.UI.MasterPage
    {
        public string Lang;
        protected void Page_Load(object sender, EventArgs e)
        {
            //Recherche la langue utilisé
            Language l = new Language((string)Session["lang"]);
            Lang = l.getLanguage();
         
        }

        public string HyperLink1Text { set { HyperLink1.Text = value; }}
        public string HyperLink1URL { set { HyperLink1.NavigateUrl = value; }}
        public bool HyperLink1Visible { set { HyperLink1.Visible = value; }}

        public string HyperLink2Text { set { HyperLink2.Text = value; } }
        public string HyperLink2URL { set { HyperLink2.NavigateUrl = value; } }
        public bool HyperLink2Visible { set { HyperLink2.Visible = value; } }

        public bool LinkLnkLang { set { lnkLang.Visible = value; } }
        public string LinkLnkLangText { set { lnkLang.Text = value; } }
       
       // public string Bonjour { set { lblBonjour.Text = value; } }

        protected void lnkLang_Click(object sender, EventArgs e)
        {
             //Si premier log alors francais vers anglais
            if (Session["lang"] == null)
            {
                Session["lang"] = "en-CA";
                Response.Redirect(Request.RawUrl);

            }
            else
            {
                string lang = Convert.ToString(Session["lang"]).Substring(0, 2);

                switch (lang)
                {
                    case "en":
                        //Mettre les valeurs de anglais vers francais
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    case "fr":
                        Session["lang"] = "en-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    default:
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;

                }
            }
        }
    }
}