﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Succes2.aspx.cs" Inherits="RetourArchambault.Succes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Archambault</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="css/default.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
</head>
 
<body>
    <div id="header">
        <img src="images/lg_archambault01White.png" />
    </div>
    <form id="form1" runat="server">

        <div id="wrapper">
            <div id="content">
                <div class="welcomeMessage">
                    <asp:Label ID="lblBonjour" runat="server" Text="Bonjour"></asp:Label>
                </div>
                <div class="container">
                    <h2 class="title">Merci</h2>
                    <p>Votre transaction c'est déroulé avec succès, un courriel a été envoyé automatiquement à votre destinataire pour traitement.
                        <br />
                        Merci d'utiliser Retour Archambault</p>
                    <div class="button_cont">
                        <asp:Button CssClass="btn primaryAction" ID="btnRetour" runat="server" Text="Faire une autre transaction" OnClick="btnRetour_Click" />
                        <asp:Button CssClass="btn secondaryAction" ID="btnQuitter" runat="server" Text="Quitter" OnClick="btnQuitter_Click" />
                    </div>
                </div><!--END /.container-->
            </div><!--END /.content-->
            <div id="footer">
                <div id="brand">&copy; Tous droits réservés. Groupe Archambault inc.</div>
            </div>
        </div><!--END /.wrapper-->
    </form>
  
</body>
</html>
