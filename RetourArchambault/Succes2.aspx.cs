﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RetourArchambault
{
    public partial class Succes : System.Web.UI.Page
    {
        private Usr usager;

        protected void Page_Load(object sender, EventArgs e)
        {
            usager = (Usr)Session["USR"];
            if (this.usager != null)
            {
              //  lblBonjour.Text = "Merci " + this.usager.first_name + " " + this.usager.last_name;
            }
            else
            {
                Response.Redirect("../Login.aspx");
            }
        }

        protected void btnRetour_Click(object sender, EventArgs e)
        {
            if (usager.is_amem)
            {
                Server.Transfer("Achat/Amem.aspx?usr=" + usager.id + "&cie=" + usager.idCie);

            }
            else
            {
                Server.Transfer("Fournisseur/Fournisseurs.aspx?usr=" + usager.id + "&cie=" + usager.idCie);

            }
            
        }

        protected void btnQuitter_Click(object sender, EventArgs e)
        {

            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            if(browser.Browser == "Firefox" || browser.Browser == "Chrome")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                Response.Redirect("Login.aspx");
                //string rep = "window.open('','_self','');window.close();";

                //Response.Write("<body><script>" + rep + "</script></body>");
            }
            
           



        }
    }
}