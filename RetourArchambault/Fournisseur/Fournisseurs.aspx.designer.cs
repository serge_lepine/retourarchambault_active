﻿//------------------------------------------------------------------------------
// <généré automatiquement>
//     Ce code a été généré par un outil.
//
//     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
//     le code est régénéré.
// </généré automatiquement>
//------------------------------------------------------------------------------

namespace RetourArchambault.Fournisseur {
    
    
    public partial class Fournisseurs {
        
        /// <summary>
        /// Contrôle Repeat_1.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater Repeat_1;
        
        /// <summary>
        /// Contrôle Repeat_3.
        /// </summary>
        /// <remarks>
        /// Champ généré automatiquement.
        /// Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater Repeat_3;
        
        /// <summary>
        /// Propriété Master.
        /// </summary>
        /// <remarks>
        /// Propriété générée automatiquement.
        /// </remarks>
        public new RetourArchambault.Maitre Master {
            get {
                return ((RetourArchambault.Maitre)(base.Master));
            }
        }
    }
}
