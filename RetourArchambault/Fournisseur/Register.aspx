﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="RetourArchambault.Fournisseur.Register" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>REGISTER Archambault</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/main.css" rel="stylesheet" type="text/css" />
</head>
<body class="t02">
    <div id="wrapper">
        <div id="header">
            <img src="../images/lg_archambault01White.png" />
        </div>
        <div class="wrapMain">
            <div class="subWrap main">
                <div id="content">
                    <div class="refLine">
                        <h1>Inscription</h1>
                        <div class="intro">
                            <p></p>
                        </div>
                        <form action="register_error.html" method="post" runat="server">
                            <div class="frmInnerWrap">
                                <div class="frmElmnt fTxt">
                                    <div class="fLbl">
                                        <asp:Label ID="Label1" runat="server" Text="Prénom"></asp:Label>
                                    </div>
                                    <div class="fWdgt">
                                        <asp:TextBox ID="txtPrenom" CssClass="autocorrectJs" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                                <div class="frmElmnt fTxt">
                                    <div class="fLbl">
                                        <asp:Label ID="Label2" runat="server" Text="Nom"></asp:Label>
                                    </div>
                                    <div class="fWdgt">
                                        <asp:TextBox ID="txtNom" CssClass="autocorrectJs" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                                <div class="frmElmnt fTxt">
                                    <div class="fLbl">
                                        <asp:Label ID="Label3" runat="server" Text="Adresse courriel"></asp:Label>
                                    </div>
                                    <div class="fWdgt">
                                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                                <div class="frmElmnt fTxt">
                                    <div class="fLbl">
                                        <asp:Label ID="Label4" runat="server" Text="Mot de passe"></asp:Label>
                                    </div>
                                    <div class="fWdgt">
                                        <asp:TextBox ID="txtPassword" TextMode="Password" CssClass="passwordStrengthTrigger" runat="server"></asp:TextBox>
                                        <p class="passwordStrength psLevel01" style="display: none">Trop court</p>
                                        <p class="passwordStrength psLevel02" style="display: none">Faible</p>
                                        <p class="passwordStrength psLevel03" style="display: none">Bon</p>
                                        <p class="passwordStrength psLevel04" style="display: none">Excellent</p>
                                        <p class="hlpMsg">Doit être formé d'au moin 6 caractères.</p>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                                <div class="frmElmnt fTxt">
                                    <div class="fLbl">
                                        <asp:Label ID="Label5" runat="server" Text="Confirmer mot de passe"></asp:Label>
                                    </div>
                                    <div class="fWdgt">
                                        <asp:TextBox ID="txtPassword2" TextMode="Password" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                                <div class="frmElmnt fSlct">
                                    <div class="fLbl">
                                        <asp:Label ID="Label6" runat="server" Text="Nom de la compagnie"></asp:Label>
                                    </div>
                                    <div class="fWdgt">
                                        <select name="element07" id="element07">
                                            <option value="0">Choisir…</option>
                                            <option value="1">Compagnie X</option>
                                            <option value="1">Compagnie Y</option>
                                            <option value="1">Compagnie Z</option>
                                        </select>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                                <div class="frmElmnt fBtn">
                                    <div class="fWdgt">
                                        <asp:Button ID="btnSend" CssClass="btn primaryAction" runat="server" Text="Créer le compte" />
                                        <asp:HyperLink ID="HyperLink1" CssClass="btn secondaryAction cancel" NavigateUrl="~/Login.aspx" runat="server">Annuler</asp:HyperLink>
                                    </div>
                                    <div class="clearer"><!-- --></div>
                                </div>
                            </div><!--END /.frmInnerWrap -->
                        </form>
                    </div><!-- /.refLine-->
                </div><!--END /.content-->
            </div><!--END /.main-->
            <div class="extra01"><!-- --></div>
            <div class="extra02"><!-- --></div>
        </div>
        <div id="footer">
            <div id="brand">&copy; Tous droits réservés. Groupe Archambault inc., une société de Québecor Média, 1999-2014.</div>
        </div>
    </div><!--END /.wrapper-->

</body>
</html>
