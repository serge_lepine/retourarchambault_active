﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Data.Common;
using iTextSharp.text;
using iTextSharp.text.pdf;

using System.Globalization;
using System.Threading;

namespace RetourArchambault.Fournisseur
{
    public partial class Fournisseurs : BasePage
    {
        private DataTable m_dt;
        private ZoneRepeater zr;
        private Usr usager = new Usr();
        private int id_Usr;
        private int Cie;
        public string lien = "";
        public string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            Tool.setThread_FR();
         

            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;
          
          
            if(!IsPostBack)
            {
                //if (Session["Cie"] == null) { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                //if (Session["IdUsr"] == null) { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                getParam();
                DroitAcces da = new DroitAcces(this.usager);
              
                if(da.VerifAcces())
                {
                  
                    SetInitialRow();
                    setMenu();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }  
        }
        void getParam()
        {
            Language lang = new Language((string)Session["lang"]);
            Lang = lang.getLanguage();
            if (Session["IdUsr"] != null) { this.id_Usr = (int)Session["IdUsr"]; }
            if (Session["Cie"] != null) { this.Cie = (int)Session["Cie"]; }
            if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
            if (Session["USR"] != null)
            {
             
                this.usager.idCieParam = this.Cie;
                this.usager.idParam = this.id_Usr;
                this.usager.index = 2;
            }



        }
       
        private void setMenu()
        {
            getParam();
            //Menu Quitter
            if (Lang == "en") { this.Master.HyperLink2Text = "Close"; } else { this.Master.HyperLink2Text = "Quitter"; }
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            if (Lang == "en") { this.Master.LinkLnkLangText = "Français"; } else { this.Master.LinkLnkLangText = "English"; }

            //Bonjour
         //   if (Lang == "en") { this.Master.Bonjour = "Welcome" + usager.first_name + " " + usager.last_name; } else { this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name; }

        }
        /// <summary>
        /// Remplit les sections
        /// </summary>
        private void SetInitialRow()
        {
            getParam();
            //Inscrit le nom dans en-tête
            //lblBonjour.Text = "Bonjour " + usager.first_name + " " + usager.last_name;
            //Section nouveau
            zr = new ZoneRepeater();
            m_dt = new DataTable();
            m_dt.Clear();
            m_dt = zr.zoneRepeater(1, "Transaction_Link_Provider", this.id_Usr,this.Lang);

            ViewState["Nouveau"] = m_dt;
           
            Repeat_1.DataSource = m_dt;
            Repeat_1.DataBind();

         
            //Historique

            m_dt = new DataTable();
            m_dt.Clear();
            zr = new ZoneRepeater();
            m_dt = zr.zoneRepeater(11, "Transaction_Link_Provider", this.id_Usr,this.Lang);

            ViewState["Historique"] = m_dt;

            Repeat_3.DataSource = m_dt;
            Repeat_3.DataBind();
        }
    }
}