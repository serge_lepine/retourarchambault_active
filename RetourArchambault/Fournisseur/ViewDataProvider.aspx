﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewDataProvider.aspx.cs" EnableViewState="true" Inherits="RetourArchambault.Fournisseur.ViewDataProvider" meta:resourcekey="PageResource1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="fr-ca">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />

   

    <title>Retour Inventaire</title>
</head>
<body class="<%=Lang %>">
    <div id="header">
        <asp:Image ID="entete" ImageUrl="~/images/lg_archambault01White.png" AlternateText="Archambault" runat="server" meta:resourcekey="enteteResource1" />
        <div class="languageBtn"><%--<a href="#">English</a>--%>
                    <asp:LinkButton ID="lnkLang" OnClick="lnkLang_Click" meta:resourcekey="lnkLang" runat="server">English</asp:LinkButton>
            </div>
    </div>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="wrapper">
            <div id="content">
                <div class="container">
                    <div class="rapport_container">
                          <%-- <asp:Button ID="btnClose" runat="server" CssClass="btn backAction" Text="Retour au tableau de bord" OnClick="btnClose_Click" />--%>
                        <%--<asp:Button ID="btnClose" runat="server" CssClass="btn backAction" Text="Retour au tableau de bord" OnClick="btnClose_Click" />--%>
                        <div class="frame">
                            <div class="table_cont">
                                <h2> <%=(Lang == "fr") ? "Proposé" : "Proposed"%></h2>
                                <div class="stock_table_cont">
                                    <table class="stock_table">
                                        <tr class="even">
                                            <td> <%=(Lang == "fr") ? "À retourner" : "Return"%></td>
                                            <td colspan="2">
                                               
                                                <asp:Label ID="lbl_Distinct" runat="server" Text="0" meta:resourcekey="lbl_DistinctResource1"></asp:Label></td>
                                        </tr>
                                        <tr class="odd">
                                            <td> <%=(Lang == "fr") ? "À retourner ($)" : "Return ($)"%></td>
                                            <td colspan="2">
                                                <asp:Label ID="lblRetour_money" runat="server" Text="0.0" meta:resourcekey="lblRetour_moneyResource1"></asp:Label></td>
                                        </tr>
                                        <tr class="even last drop-lists">
                                            <td>
                                                 <%=(Lang == "fr") ? "Rechercher par" : "Search by"%>
                                                <asp:DropDownList ID="ddlHeader" runat="server" AutoPostBack="True"
                                                     OnSelectedIndexChanged="ddlHeader_SelectedIndexChanged" meta:resourcekey="ddlHeaderResource1">
                                                    <asp:ListItem Text="--Selectionner entête--" Value="0" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                                </asp:DropDownList>                                        
                                            </td>
                                            <td valign="bottom">
                                                <asp:DropDownList ID="ddlDescription" runat="server" AutoPostBack="True"
                                                     OnSelectedIndexChanged="ddlDescription_SelectedIndexChanged" meta:resourcekey="ddlDescriptionResource1">
                                                    <asp:ListItem Text="--Selectionner item--" Value="0" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                                </asp:DropDownList>
                                               <%-- <input type="text" class="inputSizeLong" />--%>
                                            </td>
                                           <td valign="bottom" class="last" >
                                            <asp:DropDownList ID="ddlStore" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlStore_SelectedIndexChanged" >
                                                <asp:ListItem Text="Tous les magasins" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="table_cont">
                                <h2> <%=(Lang == "fr") ? "Accepté" : "Accepted"%></h2>
                                <div class="stock_table_cont lastTbl">
                                    <table class="stock_table">
                                        <tr class="even">
                                            <td><%=(Lang == "fr") ? "À retourner" : "Return"%></td>
                                            <td>
                                                <asp:Label ID="lblDistinctAutorise" runat="server" Text="0" meta:resourcekey="lblDistinctAutoriseResource1"></asp:Label></td>
                                        </tr>
                                        <tr class="odd last">
                                            <td><%=(Lang == "fr") ? "À retourner ($)" : "Return ($)"%></td>
                                            <td>
                                                <asp:Label ID="lblRetourAutorise" runat="server" Text="0.0" meta:resourcekey="lblRetourAutoriseResource1"></asp:Label></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                                 
                        </div>
                        <div class="button_cont">
                            <asp:Button CssClass="export_excel_btn" ID="btnExportExcel" runat="server" OnClick="btnExportExcel_Click" meta:resourcekey="btnExportExcelResource1" />
                            <asp:Button ID="btnUpdate" CssClass="update_btn" runat="server" OnClick="btnUpdate_Click" meta:resourcekey="btnUpdateResource1" />
                         <%--   <button class="update_btn"></button>--%>
                            <asp:Button CssClass="sauvegarder_btn" ID="btnSave" runat="server" OnClick="Button1_Click" meta:resourcekey="btnSaveResource1" />
                            <asp:Button CssClass="autoriser_btn" ID="btnSend" runat="server" OnClick="btnSend_Click" meta:resourcekey="btnSendResource1" />
                          
                        </div>
                    
                    </div>
                </div><!-- END .container-->
                <div style="align-content: center;" class="result_table_cont provider_tbl">
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" 
                        OnSelectedIndexChanging="GridView1_SelectedIndexChanging" AutoGenerateColumns="False"
                         DataKeyNames="id" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCancelingEdit="GridView1_RowCancelingEdit" 
                        OnRowDataBound="GridView1_RowDataBound" OnRowEditing="GridView1_RowEditing" OnRowUpdating="GridView1_RowUpdating"
                         OnSorting="GridView1_Sorting" PageSize="50" meta:resourcekey="GridView1Resource1">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <FooterStyle HorizontalAlign="Left" />
                         <PagerSettings Mode="NumericFirstLast" FirstPageText="Début" LastPageText="Fin" NextPageText="Suivante" PreviousPageText="Précédente" Position="Bottom" />
                        <Columns>
                            <asp:TemplateField HeaderText="Référence" SortExpression="NoAvis">
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("NoAvis") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoMag" SortExpression="NoMag" meta:resourcekey="TemplateFieldResource1">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoMag") %>' meta:resourcekey="Label1Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Magasin" SortExpression="NomMagasin">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("NomMagasin") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SKU" SortExpression="SKU" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("Sku") %>' meta:resourcekey="Label2Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="NoCie" SortExpression="NoCie" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("NoCie") %>' meta:resourcekey="Label3Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ISBN" SortExpression="ISBN" meta:resourcekey="TemplateFieldResource4">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("ISBN") %>' meta:resourcekey="Label4Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UPC" SortExpression="UPC" meta:resourcekey="TemplateFieldResource5">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("UPC") %>' meta:resourcekey="Label5Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="AUTEUR" SortExpression="Auteur" meta:resourcekey="TemplateFieldResource6">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("Auteur") %>' meta:resourcekey="Label6Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TITRE" SortExpression="Titre" meta:resourcekey="TemplateFieldResource7">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("Titre") %>' meta:resourcekey="Label7Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FORMAT" SortExpression="Format" meta:resourcekey="TemplateFieldResource8">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("Format") %>' meta:resourcekey="Label8Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ÉTIQUETTE" SortExpression="Etiquette" meta:resourcekey="TemplateFieldResource9">
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("Etiquette") %>' meta:resourcekey="Label9Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="COST" SortExpression="Cost" meta:resourcekey="TemplateFieldResource10">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Cost","{0:C2}") %>' meta:resourcekey="Label10Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty à Retourner" SortExpression="Qty_a_Retourner" meta:resourcekey="TemplateFieldResource11">
                                <ItemTemplate>
                                    <asp:Label ID="lblReturn" runat="server" Text='<%# Eval("Qty_a_Retourner") %>' meta:resourcekey="lblReturnResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Valeur des Retours" SortExpression="ValeurRetour" meta:resourcekey="TemplateFieldResource12">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("ValeurRetour","{0:C2}") %>' meta:resourcekey="Label12Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction">
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Refusé" SortExpression="Refuse" meta:resourcekey="TemplateFieldResource13">
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRefuse" CssClass="inputSizeSml" runat="server" Text='<%# Eval("Refuse") %>' meta:resourcekey="txtRefuseResource1"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("Refuse") %>' meta:resourcekey="Label13Resource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Qty Autorisée" SortExpression="Qty_autorise" meta:resourcekey="TemplateFieldResource14">
                               <%-- <EditItemTemplate>
                                    <asp:TextBox ID="txtQtyAutorise" CssClass="inputSizeSml" runat="server" Text='<%# Eval("Qty_autorise") %>' meta:resourcekey="txtQtyAutoriseResource1"></asp:TextBox>
                                </EditItemTemplate>--%>
                                <ItemTemplate>
                                    <asp:TextBox ID="TxtQtyAutorise" runat="server" AutoPostBack="true" OnTextChanged="TxtQtyAutorise_TextChanged" Text='<%# Eval("Qty_autorise") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Commentaires" SortExpression="Commentaires" meta:resourcekey="TemplateFieldResource15">
                             <%--   <EditItemTemplate>
                                    <asp:TextBox ID="txtCommentaires" CssClass="inputSizeLong" runat="server" meta:resourcekey="txtCommentairesResource1"></asp:TextBox>
                                </EditItemTemplate>--%>
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" OnTextChanged="TextBox1_TextChanged" Text='<%# Eval("Commentaires") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Id" Visible="False" meta:resourcekey="TemplateFieldResource16">
                                <ItemTemplate>
                                    <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id") %>' meta:resourcekey="lblIdResource1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Limite" SortExpression="Date_Limite">
                                <ItemTemplate>
                                    <asp:Label ID="Label17" runat="server" Text='<%# Eval("Date_Limite") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annuler" meta:resourcekey="TemplateFieldResource17">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="ChkAll" runat="server" OnCheckedChanged="ChkAll_CheckedChanged"  AutoPostBack="true" Text="Select. tout" TextAlign="Left" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkRefuse" runat="server" OnCheckedChanged="chkRefuse_CheckedChanged"  AutoPostBack="True" Checked='<%# Eval("is_checked") %>' meta:resourcekey="chkRefuseResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                 </div>
            </div><!-- END .content-->
        </div><!-- END .wrapper-->
    </form>
</body>
</html>
