﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AutorizeProvider.aspx.cs" Inherits="RetourArchambault.Fournisseur.AutorizeProvider" meta:resourcekey="PageResource1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" /> 
    <script src="Scripts/jquery-1.8.2.js" type="text/javascript"></script>
 <script type="text/javascript">

    $(document).ready(function () {
        $("#gvAutorisation").keydown(function (e) {
            if (e.keyCode == 13) {
                return false;
            }
        });

    });

</script>
  <script type="text/javascript">
      function confirmation() {
          if (confirm('<%=(Lang == "fr") ? "Le numéro d autorisation ne doit pas être vide...Merci" : "Authorization number must be completed...Thank"%>')) {
              return true;
          }
          else {
              return false;
          }
      }
    </script>
    <title>Retour Inventaire</title>
</head>
<body class="<%=Lang %>">
      <form id="form1" runat="server" defaultbutton="btnDisableEnter">
    <div id="header">
        <asp:Image ID="entete" ImageUrl="~/images/lg_archambault01White.png" AlternateText="Archambault" runat="server" meta:resourcekey="enteteResource1" />
         <div class="languageBtn">
                    <asp:LinkButton ID="lnkLang" OnClick="lnkLang_Click" meta:resourcekey="lnkLang" runat="server">English</asp:LinkButton>
            </div>
    </div>
  
        
        <asp:Button ID="btnDisableEnter" runat="server" Visible="False" OnClientClick="return false" meta:resourcekey="btnDisableEnterResource1" />
         <div id="wrapper">
            <div id="content">
                <div class="container">
                    <div class="rapport_container">
                        <%--<asp:Button ID="btnClose" runat="server" CssClass="btn backAction" Text="Retour au tableau de bord" OnClick="btnClose_Click" />--%>
                        <asp:Button ID="btnClose" runat="server" CssClass="btn backAction" Text="Retour à la grille" OnClick="btnClose_Click" />
                        <div class="frame">
                              <h1><strong><%=(Lang == "fr") ? "# Autorisation" : "Authorization #"%></strong></h1>
                        </div>
                        <div class="button_cont">
                            <asp:Button CssClass="email_btn" UseSubmitBehavior="False" ID="btnEnvoi"  runat="server"  OnClick="btnEnvoi_Click" />
                        </div>
                    </div>
                </div><!-- END .container-->                
                <div  class="result_table_cont authProvider_tbl">
                     <%--Grille numero temporaire--%>
                     <asp:GridView ID="gvAutorisation"
                                 runat="server" 
                            OnRowEditing="gvAutorisation_RowEditing"
                          OnRowUpdating="gvAutorisation_RowUpdating"
                          OnRowCancelingEdit="gvAutorisation_RowCancelingEdit"
                          AutoGenerateColumns="False"
                           DataKeyNames="Id" OnRowDataBound="gvAutorisation_RowDataBound" meta:resourcekey="gvAutorisationResource1">

                          <RowStyle CssClass="odd" />
                                        <AlternatingRowStyle CssClass="even" />
                         <Columns>
                             <asp:TemplateField HeaderText="No Mag" meta:resourcekey="TemplateFieldResource1">
                                 <ItemTemplate>
                                     <asp:Label ID="Label15" runat="server" Text='<%# Eval("NoMag") %>' meta:resourcekey="Label15Resource1"></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Magasin" meta:resourcekey="TemplateFieldResource2">
                                 <ItemTemplate>
                                     <asp:Label ID="Label16" runat="server" Text='<%# Eval("Magasin") %>' meta:resourcekey="Label16Resource1"></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="#Retour temporaire" meta:resourcekey="TemplateFieldResource3">
                                 <ItemTemplate>
                                     <asp:Label ID="Label17" runat="server" Text='<%# Eval("Temporaire") %>' meta:resourcekey="Label17Resource1"></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Votre # Autorisation" meta:resourcekey="TemplateFieldResource4">
                                 <ItemTemplate>
                                     <asp:TextBox ID="txtAutorisation" runat="server" OnTextChanged="txtAutorisation_TextChanged" Text='<%# Eval("Autorisation") %>' meta:resourcekey="txtAutorisationResource1"></asp:TextBox>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:CommandField HeaderText="Opérations" ButtonType="Image" ShowEditButton="True" ShowHeader="True" EditImageUrl="~/images/icon-edit.png" CancelImageUrl="~/images/icon-Cancel.png" UpdateImageUrl="~/images/icon-update.png" Visible="False" meta:resourcekey="CommandFieldResource1" />
                             <asp:TemplateField HeaderText="Id" Visible="False" meta:resourcekey="TemplateFieldResource5">
                                 <ItemTemplate>
                                     <asp:Label ID="lblId" runat="server" Text='<%# Eval("Id") %>' meta:resourcekey="lblIdResource1"></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                         </Columns>
                     </asp:GridView>
                </div>
            </div>    
        </div>
    </form>
</body>
</html>
