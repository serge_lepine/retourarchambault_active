﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Fournisseurs.aspx.cs" Inherits="RetourArchambault.Fournisseur.Fournisseurs" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    </asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
       <%-- <div class="welcomeMessage">
            <asp:Label ID="lblBonjour" runat="server" Font-Bold="true" Text="Bonjour"></asp:Label>
        </div>--%>
        <%--<div class="container">--%>
            <%-- Créer une zone répétée pouor date lien vers la page View_data --%>
            <h2 class="title"> <%=(Lang == "fr") ? "Nouveau" : "New"%></h2>
            <p><%=(Lang == "fr") ? "Sélectionner les transactions à completer" : "Select transactions to complete"%></p>
            <div class="frame">        	
                <asp:Repeater ID="Repeat_1" runat="server">
                    <ItemTemplate>
                                <div class="frmElmnt">
                                <div class="fLbl"><label><%# DataBinder.Eval(Container.DataItem,"TransactDate") %></label></div>
                                <div class="fWdgt"><asp:HyperLink ID="HyperLink1"  CssClass="links" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Lien") %>' runat="server">
                                    <%# DataBinder.Eval(Container.DataItem,"monText") %> &nbsp;&nbsp;<label><%# DataBinder.Eval(Container.DataItem,"TranssactionType") %></label></asp:HyperLink>
                                </div> 
                               <%-- <div class="fLbl">--%><%--</div>--%>
                                <div class="clearer"></div>
                            </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div> 
        <%--</div>--%><!--END /.container-->
        <div style="clear: both;">&nbsp;</div>
        <div class="container last">
            <h2 class="title"><%=(Lang == "fr") ? "Non complété" : "Uncompleted"%></h2>
            <p><%=(Lang == "fr") ? "Transaction à terminer" : "End a transaction"%></p>
            <div class="frame">
                <asp:Repeater ID="Repeat_3" runat="server">
                    <ItemTemplate>
                        <div class="frmElmnt">
                            <div class="fLbl">
                                <label><%# DataBinder.Eval(Container.DataItem,"TransactDate") %></label>
                            </div>
                            <div class="fWdgt">
                                <asp:HyperLink ID="Historique" CssClass="links" NavigateUrl='<%# DataBinder.Eval(Container.DataItem,"Lien") %>' runat="server">
                                <%# DataBinder.Eval(Container.DataItem,"monText") %> &nbsp;&nbsp;<label><%# DataBinder.Eval(Container.DataItem,"TranssactionType") %></asp:HyperLink>
                            </div>
                            <div class="clearer"></div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div><!--END /.container-->
        <div style="clear: both;">&nbsp;</div>        
</asp:Content>
