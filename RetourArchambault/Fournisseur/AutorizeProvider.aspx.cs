﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RetourArchambault.Fournisseur
{
    public partial class AutorizeProvider : BasePage
    {
        private string temporaryNumber = "";
        private string Lien = "";
   //     private string m_pathFiles = "";
        private Usr usager = null;
        private int IdUsr;
        private string[] item = null;
        private int cie;
        private int dept;
        private int section;
        public string Lang;

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.DefaultButton = null;
            Page.MaintainScrollPositionOnPostBack = true;
            //Vérifie si usr est autorisé
            if (Session["lang"] == null)
            {
                Lang = "fr";
                lnkLang.Text = "English";

            }
            else
            {
                Lang = Convert.ToString(Session["lang"]).Substring(0, 2);
                if (Lang == "en") { lnkLang.Text = "Français"; } else { lnkLang.Text = "English"; }
               
            }
            if (Lang == "en") { btnClose.Text = "Return to grid"; } else { btnClose.Text = "Retour à la grille"; }
            //Vide la table d'autorisation
    
            IDataRecord record;
            //Création de la table 
            if(!IsPostBack)
            {
               
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Lien"] = Request.Params["lien"]; }

                getParam();
                Autorisation auth = new Autorisation(this.Lien);

                //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
                if (String.IsNullOrEmpty(Request.Params["Usr"]) || Session["USR"] == null)
                {
                    //Retourne au login si utilisateur non authentifier ou sans login
                    Response.Redirect("../Login.aspx");
                }
                else
                {

                    //2e vérification pour s'assurer que c'est le même usager du login qui entre
                    if (usager.id != int.Parse(Request.Params["Usr"]) || usager.idCie != this.cie)
                    {
                        log.logError("Il y a une erreur entre l'usagé du login et l'url... Est ce que l'url a été modifié ?");
                        Response.Redirect("../Login.aspx");
                    }
                    else if (usager.is_public == false)
                    {
                        //Refuse l'usagé si non enregistré dans le groupe de AMEM
                        log.logError("Usagé n'est pas enregistré dans le groupe AMEM");
                        Response.Redirect("../Login.aspx");
                    }
                }

                //Verifier si deja existant sinon procéder
                if (auth.getAutorisationList())
                {
                    //On update la nouvelle table
                    SqlConnection con = Connexion.ConnectSQL();
                    SqlConnection con2 = Connexion.ConnectSQL();
                    string query = Tool.getQuery("Autorisation.sql");
                    query = String.Format(query, this.Lien);
                    SqlCommand cmd = new SqlCommand(query, con);
                    SqlDataReader reader = cmd.ExecuteReader();
                    try
                    {
                        //Lit le resultat via IDataRecord
                        while (reader.Read())
                        {
                            record = (IDataRecord)reader;
                            string temp = getTempNumber(this.cie, record[1].ToString(), int.Parse(record[6].ToString()));

                            string queryUpdate = Tool.getQuery("UpdateAutorisation2.sql");
                            queryUpdate = String.Format(queryUpdate, "@TemporaryNumber", "@NoMag", "@Id", this.Lien);
                            log.Log(queryUpdate);
                            SqlCommand cmdUpd = new SqlCommand(queryUpdate, con2);

                            cmdUpd.Parameters.Add(new SqlParameter("@TemporaryNumber", SqlDbType.VarChar, 50));
                            cmdUpd.Parameters.Add(new SqlParameter("@NoMag", SqlDbType.VarChar, 4));
                            cmdUpd.Parameters.Add(new SqlParameter("@Id", SqlDbType.Int, 4));

                            cmdUpd.Parameters["@TemporaryNumber"].Value = temp;
                            cmdUpd.Parameters["@Id"].Value = record[0];
                            cmdUpd.Parameters["@NoMag"].Value = record[1];
                            cmdUpd.ExecuteNonQuery();
                            cmdUpd.Dispose();
                        }
                    }
                    catch (Exception up)
                    {
                        log.logError("Page AutorizeProvider UpdateAutorisation2 " + up.Message);

                    }
                    finally
                    {
                        cmd.Dispose();

                        Connexion.closeSQL(con);
                        Connexion.closeSQL(con2);
                    }
                }
               // }
                //else
                //{

                //    //Afficher page erreur
                //}
                BindData();
            }

        }
        private void BindData()
        {
            getParam();
            //Creation de la reuqete
            string query = Tool.getQuery("Autorisation.sql"); 
            query = String.Format(query, this.Lien);
            log.Log(query);
            DataSet dt = new DataSet();
            //DataTable dt = new DataTable();
          
            SqlConnection con = Connexion.ConnectSQL();
            SqlDataAdapter adp = new SqlDataAdapter(query, con);
            try
            {
                adp.Fill(dt);
                Session["DataAutorisation"] = dt;
            }
            catch (Exception SQLEx)
            {
                log.Log(SQLEx.Message);
            }
            finally
            {
                Connexion.closeSQL(con);
            }
            gvAutorisation.DataSource = dt;
            gvAutorisation.DataBind();

        }
        void getParam()
        {

            if (Session["Lien"] != null) { this.Lien = (string)Session["Lien"]; }
            if (Session["IdUsr"] != null) { this.IdUsr = (int)Session["IdUsr"]; }
            if ((Usr)Session["USR"] != null) { usager = (Usr)Session["USR"]; }
            //Récupere le noCie et Dept selon Lien
            this.item = this.Lien.Split('_');
            
            this.cie = int.Parse(item[0]);
            this.dept = int.Parse(item[1]);
           // this.Lien = this.m_pathFiles;
            this.section = Connexion.getSection(this.Lien); 
         

        }
        protected void gvAutorisation_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvAutorisation.EditIndex = -1;
            BindData();

        }

        protected void gvAutorisation_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int id = int.Parse(gvAutorisation.DataKeys[e.RowIndex].Value.ToString());
            TextBox txtAutorisation = (TextBox)gvAutorisation.Rows[e.RowIndex].FindControl("txtAutorisation");
            UpdateCommand(id, txtAutorisation.Text);
            gvAutorisation.EditIndex = -1;
            BindData();
        }

        protected void gvAutorisation_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvAutorisation.EditIndex = e.NewEditIndex;
            BindData();
        }
        private void UpdateCommand(int id,string Autorisation)
        {
            getParam();

            //Ouvre connexion SQL
            SqlConnection con = Connexion.ConnectSQL();
            //Creation de la requete
            string query = Tool.getQuery("UpdateAutorisation.sql"); 
            query = String.Format(query, Autorisation, id, this.Lien);
            //Creation de la commande
            Connexion.execCommand(query);
            
        }
        /// <summary>
        /// Creation du numero temporaire
        /// </summary>
        /// <param name="cie"></param>
        /// <param name="magasin"></param>
        /// <returns># Temporaire</returns>
        private string getTempNumber(int cie,string magasin, int dept)
        {
            string dep = "";
            switch (dept)
            {
                case 400:
                    dep = "M";
                    break;
                case 420:
                    dep = "V";
                    break;
            }
           this.temporaryNumber = cie.ToString().PadLeft(3,'0')+dep+DateTime.Now.ToString("yy")+"-"+DateTime.Now.ToString("ss")+"-"+magasin.PadLeft(3,'0');
           return this.temporaryNumber;
        }
        //private bool setNewTable(string lien)
        //{
        //    string query = Tool.getQuery("CreateAutorizeProvider.sql"); 
        //    query = String.Format(query, lien);
        //    log.Log(query);
        //    return Connexion.execCommand(query);
        //}

        protected void btnEnvoi_Click(object sender, EventArgs e)
        {
            getParam();
            //Copie la datagrid dans une table
            //Met à jour Transaction Amem
            //Met à jour Transaction Provider
            ProviderToAmem pta = new ProviderToAmem();
            int QtyAutorise = 0;
            double AmountAutorise = 0.0;

            if (Session["Qty_Autorise"] != null)
            {
                QtyAutorise = ((int)Session["Qty_Autorise"]);
            }
            if (Session["Montant_Autorise"] != null)
            {
                AmountAutorise = ((double)Session["Montant_Autorise"]);
            }




            //Vérifie si tout les autorisations sont complétés
            bool rep = Connexion.getAutorise(this.Lien);
            if (rep)
            {

                bool response = pta.providerToAmem(this.Lien, QtyAutorise, AmountAutorise);

                EnvoiEmail em = new EnvoiEmail(this.usager.idCie, 1, 2, true, "", Tool.getTransactionType(this.Lien));
                //Supprime la table temporaire
             
                    //On drop la table temporaire
                    string query = "";
                    query = Tool.getQuery("DropTransactionProviderTemp.sql");
                    query = String.Format(query, Lien);
                    if( Connexion.execCommand(query))
                    {
                        Response.Redirect("../Succes.aspx"); 
                    }
               
                  
            }
            else
            {
                ScriptManager.RegisterStartupScript(this,GetType(), "confirmation", "confirmation()", true);
            }
        }
      
        protected void gvAutorisation_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){return false;}");
                    e.Row.Cells[4].ToolTip = "Modifier";
                    if (e.Row.RowState == DataControlRowState.Edit || e.Row.RowState.ToString() == "Alternate, Edit")
                    {
                        
                        int i = 0;
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            if (e.Row.Cells.GetCellIndex(cell) == 4)
                            {

                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[4].Controls[0])).ToolTip = "Modifier ou Refuser";
                                ((System.Web.UI.LiteralControl)(e.Row.Cells[14].Controls[1])).Text = "&nbsp;";
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[4].Controls[2])).ToolTip = "Fermer modification";
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
        }

        protected void txtAutorisation_TextChanged(object sender, EventArgs e)
        {
            this.Page.Form.DefaultFocus = null;
            GridViewRow grv = ((TextBox)sender).Parent.Parent as GridViewRow;

            Label lbNoCie = (Label)grv.FindControl("lblId");
            TextBox txtAuto = (TextBox)grv.FindControl("txtAutorisation");
            UpdateCommand(int.Parse(lbNoCie.Text), txtAuto.Text);
        }

        protected void lnkLang_Click(object sender, EventArgs e)
        {
            //Si premier log alors francais vers anglais
            if (Session["lang"] == null)
            {
                Session["lang"] = "en-CA";
                Response.Redirect(Request.RawUrl);

            }
            else
            {
                string lang = Convert.ToString(Session["lang"]).Substring(0, 2);

                switch (lang)
                {
                    case "en":
                        //Mettre les valeurs de anglais vers francais
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    case "fr":
                        Session["lang"] = "en-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    default:
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;

                }
            }
        }

      
        protected void btnClose_Click(object sender, EventArgs e)
        {
            getParam();
            Response.Redirect("ViewDataProvider.aspx?Temp=0&usr=" + this.IdUsr + "&lien=" + this.Lien);
        }

    }
}