﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Drawing;
using System.Configuration;

namespace RetourArchambault.Fournisseur
{
    public partial class ViewDataProvider : BasePage
    {

        //private string FileName;
        //private string m_query = "";
        private string Lien = "";
        private int cie;
        private int count = 0;
        private double amount = 0.0;
        private int AutCount = 0;
        private double AutAmount = 0.0;
        private bool rep;
        private Usr usager;
        private int IdUser;
        private string m_condition = "";
        private string[] item;
        private int Section;
        private int Dept;
        public string Lang;
        public bool chk = false;
        //    protected System.Web.UI.HtmlControls.HtmlForm Form1;

        protected void Page_Load(object sender, EventArgs e)
        {
            //  form1.FindControl("GridView1");
            Page.MaintainScrollPositionOnPostBack = true;
        
            if (!IsPostBack)
            {

                Tool.setThread_FR();

                //Active la mémoire de la position sur la page
                Page.MaintainScrollPositionOnPostBack = true;

                if (!IsPostBack)
                {

                    { Session["IdUsr"] = int.Parse(Request.Params["usr"]); }
                    { Session["Lien"] = Request.Params["Lien"]; }

                    getParam();
                    DroitAcces da = new DroitAcces(this.usager);

                    //Si accès autorisé continu sinon on redirige vers la page fournisseur
                    if (da.VerifAcces())
                    {
                        //Instancie un objet pour parametre FillProviderData_1
                        SetProviderData paramData = new SetProviderData();
                        paramData.Lien = this.Lien;
                        paramData.Dept = this.Dept;
                        paramData.Cie = this.cie;
                        paramData.Lang = this.Lang;

                        //Obtient le numéro de section
                        this.Section = Connexion.getSection(this.Lien); 

                        //Instancie un objet pour transfert
                        FillProviderData_1 fdt_1 = new FillProviderData_1(paramData);

                        //Transfert les données selon la section
                        if (this.Section == 1)
                        {
                            
                            rep = fdt_1.fillProviderData_1();
                        }
                        else
                        {
                            
                            rep = fdt_1.fillProviderData_2();
                        }

                        //Si transfert reussi load la page sinon retourne message d'erreur
                        if (rep)
                        {
                            //Initialise la gridview
                            BindData();

                            //Remplit les dropdownlist pour filtre
                            setDropDown();
                        }
                        else
                        {
                            log.logError("Erreur lors de la création de la table Provider");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Fournisseurs.aspx");
                    }
                }

            }
          

        }
      /// <summary>
      /// Initi9alise les paramètres local
      /// </summary>
        void getParam()
        {
           
            //Initilise variable selon parametre url
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (!String.IsNullOrEmpty((string)Session["Lien"])) { this.Lien = (string)Session["Lien"]; }
            if (Session["IdUsr"] != null) { this.IdUser = (int)Session["IdUsr"]; }
            if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
            if (!String.IsNullOrEmpty(this.Lien))
            {
                this.item = this.Lien.Split('_');
                if (item[3].ToString() == "02") { Session["RETOUR"] = "Desuet"; } else { Session["RETOUR"] = null; }
                this.cie = int.Parse(item[0]);
                if(int.Parse(item[1].ToString()) != 500) {this.Dept = int.Parse(item[1]);} else {this.Dept = 0;}
                this.usager.idCieParam = this.cie;
                this.usager.idParam = this.IdUser;
                this.usager.index = 2;
            }



        }
        private void BindData()
        {
            getParam();
            //Creation de la requete
            string query = Tool.getQuery("TransactionProviderViewTemp.sql");
            query = String.Format(query, this.Lien, this.cie,this.Lang);
            log.Log(query);
            setData(query);
           

        }
        private void setData(string query)
        {
            DataSet ds = new DataSet();

            SqlConnection con = Connexion.ConnectSQL();
            SqlDataAdapter adp = new SqlDataAdapter(query, con);
            try
            {
                adp.Fill(ds);
                count = 0;
                amount = 0.0;
                AutAmount = 0.0;
                AutCount = 0;
                Session["dtvdp"] = ds.Tables[0];

                setTopSection();
            }

            catch (Exception e)
            {
                log.logError(e.Message);
            }
            finally
            {
                Connexion.closeSQL(con);
            }



        }
        void setTopSection()
        {
            getParam();
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtvdp"];
            //initialise Qty et montant
            foreach (DataRow row in dt.Rows)
            {
                int tempCount = 0;
                double tempAmount = 0.0;

                count += int.Parse(row[12].ToString());
                amount += double.Parse(row[13].ToString());
                if (String.IsNullOrEmpty(row[20].ToString())) { tempCount = 0; } else { tempCount = int.Parse(row[20].ToString()); }

                if (tempCount != 0)
                {

                    //Faut calculer le montant selon la quantité autorisé
                    tempAmount = (tempCount * double.Parse(row[11].ToString()));
                }
                else
                {
                    //Sinon on ajoute le montant prévu
                    tempAmount = double.Parse(row[13].ToString());
                    //On met à zéro la quantité et le montant de la ligne si annulé
                    if (!String.IsNullOrEmpty(row[19].ToString()))
                    {
                        tempAmount = 0;
                        tempCount = 0;
                    }
                    else
                    {
                        tempCount = int.Parse(row[12].ToString());
                    }
                }
                this.AutCount += tempCount;
                this.AutAmount += (tempAmount);
            }

            //Initialise le textbox 
            lbl_Distinct.Text = count.ToString();
            lblRetour_money.Text = String.Format("{0:C2}", this.amount);
            lblDistinctAutorise.Text = AutCount.ToString();
            lblRetourAutorise.Text = String.Format("{0:C2}", this.AutAmount);
            Session["Qty_Autorise"] = this.AutCount;
            Session["Montant_Autorise"] = this.AutAmount;

            GridView1.PageSize = 50;

            
            if (Session["Sort"] != null)
            {
                DataView dv = dt.DefaultView;
                dv.Sort = Session["Sort"].ToString();
                
                GridView1.DataSource = dv;
                GridView1.DataBind();
            }
            else
            {
              
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }


        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            //Mettre selectionner tout à false;
            Session["ALL"] = false;
            //Mettre section a 11
            getParam();
            string query = Tool.getQuery("setSection.sql");
            query = string.Format(query, this.Lien);
            Connexion.execCommand(query);
            Response.Redirect("Fournisseurs.aspx?usr=" + this.IdUser + "&cie=" + this.cie);
        }
        private void setGrid()
        {
            //Met a jour la section du haut
            setTopSection();
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtvdp"];
            //Met a jour la grille
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            setGrid();
        }
        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            Int16 is_check = 0;
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            TextBox txtRefuse = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtRefuse");
            TextBox txtQty = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtQtyAutorise");
            TextBox txtCommentaires = (TextBox)GridView1.Rows[e.RowIndex].FindControl("txtCommentaires");
            if (String.IsNullOrEmpty(txtQty.Text)) { txtQty.Text = "0"; }
            if (txtRefuse.Text.Length != 0) { txtQty.Text = "0"; is_check = 1; }

            UpdateCommand(id, int.Parse(txtQty.Text), txtRefuse.Text, txtCommentaires.Text, is_check);
            GridView1.EditIndex = -1;
            //BindData();
            refreshData();

        }
        private void UpdateCommand(int id, int Qty, string refuse, string commentaires)
        {
            getParam();
            //Creation de la requete
            string query = Tool.getQuery("UpdateTransactionProviderTemp.sql");
            query = String.Format(query, this.Lien, Qty, refuse, commentaires, id);
            Connexion.execCommand(query);

        }
        private void UpdateCommand(int id, int Qty, string refuse, string commentaires, Int16 is_checked)
        {
            getParam();
            //Creation de la requete
            string query = Tool.getQuery("UpdateTransactionProviderTemp.sql");
            query = String.Format(query, this.Lien, Qty, refuse, commentaires.Replace("'", "''"), id, is_checked);

            Connexion.execCommand(query);

        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            setGrid();
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            setGrid();
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {

            DataTable dt = (DataTable)Session["dtvdp"];
            //Crée un DataView
            DataView dv = new DataView(dt);
            //Trie le Dataview
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression.ToString());
                Session["Sort"] = dt.DefaultView.Sort;

                setGrid();
            }
        }

        /// <summary>
        /// Order de trie des colonnes
        /// </summary>
        /// <param name="column">Nom de la colonne à trier</param>
        /// <returns>Ordre de la direction du trie</returns>
        private string GetSortDirection(string column)
        {
            // Ordre croissance par défaut.
            string sortDirection = "ASC";
            // Recherche le dernier ordre de croissance.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                //Vérifie si c"est la meme colonne qui doit être triée
                //Sinon la valeur par defaut peut être retourné
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }

            // Sauvegarde le dernier resultat dans le ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void gvAutorisation_RowEditing(object sender, GridViewEditEventArgs e)
        {
            // gvAutorisation.EditIndex = e.NewEditIndex;
            //BindData();
        }

        protected void gvAutorisation_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void gvAutorisation_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //  gvAutorisation.EditIndex = -1;
            //BindData();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            if (Session["ALL"] == null) { Session["ALL"] = false; }
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    ((CheckBox)e.Row.FindControl("ChkAll")).Checked = (bool)Session["ALL"];

                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Convert.ToInt32(drv["Qty_a_Retourner"]) != Convert.ToInt32(drv["Qty_autorise"]))
                    {
                        if (!String.IsNullOrEmpty(drv["Refuse"].ToString()))
                        {
                            e.Row.Font.Bold = true;
                            e.Row.ForeColor = System.Drawing.Color.White;
                            e.Row.BackColor = System.Drawing.Color.FromName("#62a8ff");
                        }

                        else
                        {
                            e.Row.Font.Bold = true;
                            e.Row.BackColor = System.Drawing.Color.Yellow;
                        }
                    }
                    else if (!String.IsNullOrEmpty(drv["Commentaires"].ToString()))
                    {
                        e.Row.Font.Bold = true;
                        e.Row.BackColor = System.Drawing.Color.Yellow;
                    }

                    e.Row.Cells[14].ToolTip = "Modifier";
                    if (e.Row.RowState == DataControlRowState.Edit || e.Row.RowState.ToString() == "Alternate, Edit")
                    {
                        int i = 0;
                        foreach (TableCell cell in e.Row.Cells)
                        {
                            if (e.Row.Cells.GetCellIndex(cell) == 14)
                            {
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[14].Controls[0])).ToolTip = "Modifier ou Refuser";
                                ((System.Web.UI.LiteralControl)(e.Row.Cells[14].Controls[1])).Text = "&nbsp;";
                                ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[14].Controls[2])).ToolTip = "Fermer modification";
                            }
                            i++;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void chkRefuse_CheckedChanged(object sender, EventArgs e)
        {

            foreach (GridViewRow row in GridView1.Rows)
            {

                CheckBox cb = (CheckBox)row.FindControl("chkRefuse");
                TextBox lblCommentaires = (TextBox)row.FindControl("TextBox1");
                //Label lblQtyAutorise = (Label)row.FindControl("LlQtyAutorise");
                TextBox lblQtyAutorise = (TextBox)row.FindControl("TxtQtyAutorise");

                Label lblReturn = (Label)row.FindControl("lblReturn");
                Label lblId = (Label)row.FindControl("lblId");
                int id = int.Parse(lblId.Text);
                if (cb.Checked == true)
                {
                    //On modifie la grille

                    if (int.Parse(lblQtyAutorise.Text) != 0)
                    {
                        UpdateCommand(id, 0, "x", lblCommentaires.Text, 1);
                        cb.Checked = true;
                    }
                    else
                    {
                        //   UpdateCommand(id, int.Parse(lblReturn.Text), "", lblCommentaires.Text,0);
                        //  cb.Checked = false;
                    }
                    if (String.IsNullOrEmpty(lblCommentaires.Text)) { lblCommentaires.Text = ""; }

                    GridView1.EditIndex = -1;

                    // BindData();
                    refreshData();
                    cb.Checked = true;
                }
                else
                {
                    if (int.Parse(lblQtyAutorise.Text) == 0)
                    {
                        UpdateCommand(id, int.Parse(lblReturn.Text), "", lblCommentaires.Text, 0);
                        cb.Checked = false;
                        GridView1.EditIndex = -1;

                        refreshData();
                        cb.Checked = true;
                    }

                }
            }

        }
        public override void VerifyRenderingInServerForm(Control control)
        {

        }
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtvdp"];
            ExportToExcel(dt);
        }
        public void ExportToExcel(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = "DownloadListNoExcel.xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = "";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            getParam();
            Autorisation aut = new Autorisation(this.Lien,this.IdUser);
            //Verifier si deja existant sinon procéder
            if (!aut.getAutorisationList())
            {
                try
                {
                    //Création de la grille Autorisaton
                    aut.setAutorisation();
                    //Mise à jour de la grille selon magasin impacté
                    aut.setAutorisationStore();
                   
                }
                catch
                {
                    //Si erreur alors on réaffiche la page actuel
                    Response.Redirect(Request.RawUrl);
                }
     
            }
            else
            {
                
                try
                {
                    //Si déjà créé alors on met à jour selon la grille actuel
                    aut.setAutorisationStore();
                    
                }
                catch (Exception ex)
                {
                    string rep = ex.Message;
                    //Si erreur alors on réaffiche la page actuel
                    Response.Redirect(Request.RawUrl);
                }
            
            }
            //Si pas erreur on affiche page Autorisation
            Response.Redirect(aut.urlReturn());

        }
   
        private void setDropDown()
        {

            //Supprime données existantes
            ddlHeader.AppendDataBoundItems = true;
            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT id,Name_" + this.Lang + " FROM SearchList ORDER BY Name_" + this.Lang;
            cmd.Connection = con;
            try
            {
                ddlHeader.DataSource = cmd.ExecuteReader();
                ddlHeader.DataTextField = "Name_" + this.Lang;
                ddlHeader.DataValueField = "Id";
                ddlHeader.DataBind();

            }
            catch (Exception sdd)
            {
                throw sdd;
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(con);
            }

            //Set DropDown Store
            ddlStore.AppendDataBoundItems = true;
            SqlConnection conStore = Connexion.ConnectSQL();
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT Id,CONVERT(varchar,NoMag) + ' - ' + NomMagasin as 'Magasin' FROM Magasin ORDER BY NoMag";
            cmd.Connection = conStore;
            try
            {
                ddlStore.DataSource = cmd.ExecuteReader();
                ddlStore.DataTextField = "Magasin";
                ddlStore.DataValueField = "Id";
                ddlStore.DataBind();

            }
            catch (Exception dds)
            {
                log.logError("AmemView ddlStore :" + dds.Message + " StackTrace : " + dds.StackTrace);
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(conStore);
            }

        }

        protected void ddlHeader_SelectedIndexChanged(object sender, EventArgs e)
        {

            ddlDescription.Items.Clear();
            ddlDescription.Items.Add(new System.Web.UI.WebControls.ListItem("--Sélectionner item--","0"));
            ddlDescription.AppendDataBoundItems = true;
            getParam();
            //Creation de la condition par magasin
            int index;
            if (this.ddlStore.SelectedValue == "0") { index = 0; } else { index = int.Parse(this.ddlStore.SelectedValue.ToString()); }

            string response = Tool.searchMag(index);
            string textField = "";
            string query = "";
            if (ddlHeader.SelectedValue != "0")
            {
                int ID = int.Parse(ddlHeader.SelectedItem.Value);

                switch (ID)
                {
                    case 1:
                        textField = "Sku";
                        query = "SELECT DISTINCT tp.Sku FROM Transaction_Provider_" + this.Lien + " tp WHERE tp.Sku <> '' " + response + " ORDER BY tp.Sku";
                        break;
                    case 2:
                        //if (this.Lang == "fr") { textField = "Auteur"; } else { textField = "Author"; }
                        textField = "Auteur";
                        query = "SELECT DISTINCT tp.Auteur FROM Transaction_Provider_" + this.Lien + " tp WHERE tp.Auteur <> '' " + response + " ORDER BY tp.Auteur";
                        break;
                    case 3:
                        // if (this.Lang == "fr") { textField = "Titre"; } else { textField = "Title"; };
                        textField = "Titre";
                        query = "SELECT DISTINCT tp.Titre FROM Transaction_Provider_" + this.Lien + " tp WHERE tp.Titre <> '' " + response + " ORDER BY tp.Titre";
                        break;
                    case 4:
                        //if (this.Lang == "fr") { textField = "Etiquette"; } else { textField = "Title"; }; textField = "Etiquette";
                        textField = "Etiquette";
                        query = "SELECT DISTINCT tp.Etiquette FROM Transaction_Provider_" + this.Lien + " tp WHERE tp.Etiquette <> '' " + response + " ORDER BY tp.Etiquette";
                        break;
                    default:
                        break;

                }
            }
            if (!String.IsNullOrEmpty(query))
            {
                SqlConnection con = Connexion.ConnectSQL();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                cmd.Connection = con;
                try
                {
                    ddlDescription.DataSource = cmd.ExecuteReader();
                    ddlDescription.DataTextField = textField;
                    //   ddlDescription.DataValueField = "Id";
                    ddlDescription.DataBind();
                }
                catch (Exception ddd)
                {
                    throw ddd;
                }
                finally
                {
                    cmd.Dispose();
                    Connexion.closeSQL(con);
                }
            }

        }

        protected void ddlDescription_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshData();
        }
        void refreshData()
        {
            getParam();
            getCondition(true);
            string querySection = Tool.getQuery("setSection.sql");
            querySection = string.Format(querySection, this.Lien);
            Connexion.execCommand(querySection);
            string query = Tool.getQuery("TransactProviderViewRefresh.sql"); 
            query = String.Format(query, this.Lien, this.m_condition,this.Lang);
            setData(query);
        }
        private void getCondition(bool tp)
        {
            this.m_condition = "";
            string response = "";
            //Creation de la condition par magasin
            int index;
            if (this.ddlStore.SelectedValue == "0") { index = 0; } else { index = int.Parse(this.ddlStore.SelectedValue.ToString()); }

            
            if (tp)
            {
                
                 response = Tool.searchMagTP(index);
                 response = String.Format(response, "tp.");
            }
            else
            {
                 response = Tool.searchMag(index);
            }

            if (ddlHeader.SelectedValue != "0")
            {
                if (ddlDescription.SelectedValue != "0")
                {
                    int ID = int.Parse(ddlHeader.SelectedItem.Value);
                 
                    switch (ID)
                    {
                        case 1:
                            this.m_condition += "  WHERE sku = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 2:
                            this.m_condition += "  WHERE Auteur = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 3:
                            this.m_condition += "  WHERE Titre = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 4:
                            this.m_condition += "  WHERE Etiquette = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        default:
                            break;
                    }
                }
                else 
                {
                    //Si m_condition = vide alors ajouter Where
                    if (String.IsNullOrEmpty(this.m_condition))
                    {
                        if (tp)
                        {
                            response = String.Format(response, "tp.");
                            this.m_condition += " WHERE tp.sku <> '' " + response;
                        }
                        else
                        {
                            this.m_condition += " WHERE sku <> '' " + response;
                        }

                    }
                    else
                    {
                        //Retourner seulement le and
                        this.m_condition += response;
                    }

                }
            }
            else
            {
                //Si m_condition = vide alors ajouter Where
                if (String.IsNullOrEmpty(this.m_condition))
                {
                    if(tp)
                    {
                        response = String.Format(response, "tp.");
                        this.m_condition += " WHERE tp.sku <> '' " + response;
                    }
                   else
                    {
                        this.m_condition += " WHERE sku <> '' " + response;
                    }
                    
                }
                else
                {
                    //Retourner seulement le and
                    this.m_condition += response;
                }


            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {

            getParam();
            //Mettre Selectionner tout a false
            Session["ALL"] = false;
            string query = Tool.getQuery("setSection.sql");
            query = string.Format(query, this.Lien);
            log.Log("setSection :" + query);
            try
            {
                Connexion.execCommand(query);
            }
            catch (TimeoutException te)
            {
                log.logError("setSection TimeOut : " + te.Message);
            }
            catch (Exception ex)
            {
                log.logError("setSection Exception : " + ex.Message);
            }
            finally
            {
                Response.Redirect("ViewDataProvider.aspx?usr=" + this.IdUser + "&Lien=" + this.Lien);
            }



        }

        protected void lnkLang_Click(object sender, EventArgs e)
        {
            //Si premier log alors francais vers anglais
            if (Session["lang"] == null)
            {
                Session["lang"] = "en-CA";
                Response.Redirect(Request.RawUrl);

            }
            else
            {
                string lang = Convert.ToString(Session["lang"]).Substring(0, 2);

                switch (lang)
                {
                    case "en":
                        //Mettre les valeurs de anglais vers francais
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    case "fr":
                        Session["lang"] = "en-CA";
                        Response.Redirect(Request.RawUrl);
                        break;
                    default:
                        Session["lang"] = "fr-CA";
                        Response.Redirect(Request.RawUrl);
                        break;

                }
            }
        }

        protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshData();
        }

        protected void ChkAll_CheckedChanged(object sender, EventArgs e)
        {
            getParam();
            getCondition(false);

            CheckBox cb = (CheckBox)GridView1.HeaderRow.FindControl("ChkAll");

            if (cb.Checked == true)
            {
                Session["ALL"] = true;
                //UpdateSelectAll.sql
                string query = Tool.getQuery("UpdateSelectAll.sql");
                query = String.Format(query, this.Lien, this.m_condition);
                log.Log("ViewDataProvider ChkAll_CheckedChanged : " + query);
                Connexion.execCommand(query);
                refreshData();


            }
            else
            {
                Session["ALL"] = false;
                //UpdateUnSelect.sql
                string query = Tool.getQuery("UpdateUnSelect.sql");
                query = String.Format(query, this.Lien, this.m_condition);
                log.Log("ViewDataProvider ChkAll_CheckedChanged : " + query);
                Connexion.execCommand(query);
                refreshData();

            }


        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

            getParam();
            string comment = "";
            GridViewRow grv = ((TextBox)sender).Parent.Parent as GridViewRow;

            Label lblId = (Label)grv.FindControl("lblID");
            TextBox txtComment = (TextBox)grv.FindControl("TextBox1");
            if (String.IsNullOrEmpty(txtComment.Text)) { comment = ""; } else { comment = txtComment.Text; }
            string query = Tool.getQuery("UpdateComment.sql");
            query = String.Format(query, this.Lien, comment.Replace("'", "''"), int.Parse(lblId.Text));
            log.Log("ViewDataProvider TextBox1_TextChanged : " + query);
            Connexion.execCommand(query);
            refreshData();
        }

        protected void TxtQtyAutorise_TextChanged(object sender, EventArgs e)
        {
            getParam();
            int comment = 0;
            GridViewRow grv = ((TextBox)sender).Parent.Parent as GridViewRow;

            Label lblId = (Label)grv.FindControl("lblID");
            TextBox txtQty = (TextBox)grv.FindControl("TxtQtyAutorise");
            if (String.IsNullOrEmpty(txtQty.Text)) { comment = 0; } else { comment = int.Parse(txtQty.Text); }
            string query = Tool.getQuery("UpdateQty.sql");
            query = String.Format(query, this.Lien, comment, int.Parse(lblId.Text));
            log.Log("UpdateQty TxtQtyAutorise_TextChanged : " + query);
            Connexion.execCommand(query);
            refreshData();
        }

        private void clearAutorisation()
        {
            getParam();
            //Supprimer les autorisations
            string query = "DELETE FROM Autorisation WHERE Lien = '" + this.Lien + "'";
            log.Log("Page Autorisation btnClose_Click " + query);
            Connexion.execCommand(query);
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {

        }


    }
}
