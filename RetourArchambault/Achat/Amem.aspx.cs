﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Common;
using ConvertToCSV;
using System.IO;
using System.Globalization;
using System.Threading;
using System.Data.SqlClient;

namespace RetourArchambault.Achat
{
    public partial class Amem : System.Web.UI.Page
    {
        //private DataTable m_dt;
        //private ZoneRepeater zr;
        private int id_Usr;
        private Usr usager = new Usr();
        private string m_lien;
        private string Lang;
        private DateTime m_Date_Limite;

        protected void Page_Load(object sender, EventArgs e)
        {
            Tool.setThread_FR();
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;

            Session.Contents.Remove("sort");
            Session.Contents.Remove("Date_Limite");
            Session.Contents.Remove("RefNumber");
            Session.Contents.Remove("RETOUR");
            //Identification de l'utilisateur
            //Vérifie si il y a un utilisateur authentifié sinon on essai accéder à la page sans login.
            if (!IsPostBack)
            {
               
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                getParam();
                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    //Initialise les variables local
                    initialise();
                    //Créer dropdownlist pour creation 1- nom des provider 2- departement 3- Provider usr selon provider
                 
                    setMenu();
                    setDataGrid();
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["USR"] != null) { this.usager = (Usr)Session["USR"]; }
            if (usager.idCieParam == 0) { usager.idCieParam = (int)Session["Cie"]; }
            if (usager.idParam == 0) { usager.idParam = (int)Session["IdUsr"]; }
            if (Session["CieSearch"] != null) { usager.CieSearch = (int)Session["CieSearch"]; }
            this.m_Date_Limite = DateTime.Now.AddDays(15);
            Session["Date_Limite"] = this.m_Date_Limite;
            usager.index = 1;
        }
        private void setMenu()
        {
            getParam();
            //Menu 1
            this.Master.HyperLink1Text = "Tableau de bord";
            this.Master.HyperLink1URL = "/AchatAdmin/dashboard_v2.aspx?Usr="+usager.idParam+"&Cie="+usager.idCieParam;
            this.Master.HyperLink1Visible = true;

            //Menu Quitter
            this.Master.HyperLink2Text = "Quitter";
            this.Master.HyperLink2URL = "Login.aspx";
            this.Master.HyperLink2Visible = true;
            this.Master.LinkLnkLang = false;

            this.Master.Lang = "fr";
            //Bonjour
          //  this.Master.Bonjour = "Bonjour " + usager.first_name + " " + usager.last_name;

        }
        private void initialise()
        {
            //m_dt = null;
            //zr = null;
            id_Usr = usager.id;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           
            getParam();
            bool fdata = false;
            //Création de la nouvelle table temporaire
            CreateNewProvider cnp = new CreateNewProvider(Session.SessionID);
            fdata = cnp.createNewProvider();
            //On extrait les données de TransferLoad vers table temporaire
            string param1 = DropDownCie.SelectedItem.Value;
            Session["CieSearch"] = int.Parse(param1);
            string param2 = DropDownDept.SelectedItem.Value;
            Session["Dept"] = param2;
            int excDays;
            if (txtExclu.Text != "") { excDays = int.Parse(txtExclu.Text); } else { excDays = 0; }
            string datePeriode = Dateperiod(excDays);// Selon textbox
            this.m_lien = Tools.setTransactName(int.Parse(param1), int.Parse(param2), 1);
            System.Threading.Thread.Sleep(3000);
            //Requete pour trouver item dans table TransferLoad
            string query = File.ReadAllText(HttpContext.Current.Server.MapPath("/Requetes/Over_MV_01.sql"));
            query = String.Format(query, param1, param2, datePeriode, this.m_Date_Limite);
            log.Log(query);

            if (fdata != false)
            {
                try
                {
                    fdata = FillAmemData.copyTable(query, "Temp_Trans_Provider_",Session.SessionID);
                    fdata = FillAmemData.rename(this.m_lien, Session.SessionID);
                  
                }
                catch (Exception ex)
                {
                    log.logError("Page Amem FillAmemData : " + ex.Message);

                }
            }

            if (fdata)
            {
                //Suppression de l'ancienne table
                
                //Appel nouvelle page ave parametres dans URL
                string pathURL = setURL(); Response.Redirect(pathURL);
            }
            else { Response.Redirect("Amem.aspx?error=1"); }

        }
        /// <summary>
        /// Création de l'url de la page AmemView.aspx
        /// </summary>
        /// <returns>L'URL complet pour l'appel de la page AmemView.aspx</returns>
        private string setURL()
        {
            getParam();
            usager.CieSearch = int.Parse(DropDownCie.SelectedItem.Value);
            string p = "AmemView.aspx?CieSearch=" + usager.CieSearch + "&Cie=" + usager.idCie + "&Dept=" + DropDownDept.SelectedItem.Value + "&Usr=" + usager.id + "&Lien=" + m_lien;
            return p;
        }
       

        /// <summary>
        /// Calcul la dernière période
        /// </summary>
        /// <returns>Date du jour moins 3 mois</returns>
        private string Dateperiod(int nbrJour)
        {
            DateTime d = DateTime.Now;
            if (nbrJour == 0)
            {
                //Si vide on instancie a 90 jours
                d = d.AddDays(-90);
            }
            else
            {
                d = d.AddDays(-nbrJour);
            }

            return d.ToString("yyMMdd");
        }

        protected void DropDownCie_SelectedIndexChanged(object sender, EventArgs e)
        {
            setDataGrid();

        }
        private void setDataGrid()
        {
            int cie = 0;
            //Création de la requete pour alimenter le datagrid des 3 dernières transactions.
            string query = Tool.getQuery("AmemDatagrid.sql");
           if(String.IsNullOrEmpty(DropDownCie.SelectedValue)){cie = 38;}else{cie = int.Parse(DropDownCie.SelectedValue);}
            query = String.Format(query,cie);
            setData(query);
        }
        private void setData(string query)
        {
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            SqlConnection c_con =  Connexion.ConnectSQL();

            try
            {

                SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);

                //Replit dataset
                adapter.Fill(ds);

                Session["amemgrid"] = ds.Tables[0];

                dt = (DataTable)Session["amemgrid"];
                GridView1.DataSource = dt;
                GridView1.DataBind();

              
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }

        }
    }
}