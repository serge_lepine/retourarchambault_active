﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Threading;
using ConvertToCSV;
using System.Net;
using System.Text.RegularExpressions;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.xml;
using System.Xml;
using iTextSharp.text.html.simpleparser;


namespace RetourArchambault.Achat
{
    public partial class AmemView : System.Web.UI.Page
    {
      //  private int m_qty;
      //  private double m_amount;
        private string query;
     //   private SqlCommand cmd;
        private SqlConnection c_con;
      //  private DataSet ds1;
        private int dept;
        private int CieSearch;
        private int cie;
        private int count;
        private double amount;
        private int qty_over;
        private int qty_minus;
        private string m_condition;
      //  private string TransactName;
        private int idUser;
        private int model;
        private Usr usager = new Usr();
     //   private int temp;
        private string m_lien;
        private int qty_demande, qty_minus2, qty_over2, model2;
        private double montant_demande;
        private int m_index;
        private int m_Id_DllHeader;
        private int m_Id_DllDescription;
        public string Lang;
        private DateTime m_Date_Limite;

        public ParamDemande pd;

        void initialise()
        {
          //  m_amount = 0.0;
         //   m_qty = 0;
            query = "";
            qty_over = 0;
            qty_minus = 0;
            m_condition = "";
         //   TransactName = "";
            idUser = 0;
            m_lien = "";
        }
        override protected void OnInit(EventArgs e)
        {

        }
        protected void Page_Load(object sender, EventArgs e)
        {
           

            log.Log("Ma session :" + Session.SessionID);

            Tool.setThread_FR();

            Page.Form.DefaultButton = null;
            //Active la mémoire de la position sur la page
            Page.MaintainScrollPositionOnPostBack = true;

            if (!IsPostBack)
            {
                //Recupere URL

                { Session["CieSearch"] = int.Parse(Request.Params["CieSearch"]); }
                { Session["Cie"] = int.Parse(Request.Params["Cie"]); }
                { Session["Dept"] = int.Parse(Request.Params["Dept"]); }
                { Session["IdUsr"] = int.Parse(Request.Params["Usr"]); }
                { Session["Lien"] = Request.Params["Lien"]; }

                //Initialise les variables
                getParam();


                //Vérification des droits d'accès
                DroitAcces da = new DroitAcces(this.usager);

                if (da.VerifAcces())
                {
                    try
                    {
                        BindData();
                        setDropDown();
                    }
                    catch (Exception fd)
                    {
                        log.logError(fd.Message);
                        Response.Redirect("Amem.aspx");

                    }
                }
                else
                {
                    Response.Redirect("../Login.aspx");
                }

            }

        }

        private void BindData()
        {
            count = 0;
            amount = 0.0;
            //Initialise les paramètres fournit par l'url
            getParam();
            string condition = "";
            ConditionReponse conditionReponse = null;
            //Recherche
            conditionReponse = verifCondition();
            if (!String.IsNullOrEmpty(conditionReponse.condition)) { condition = conditionReponse.condition; }
            //Construction de la requête
            query = "";
            query = Tool.getQuery("Page_AmemView.sql");

            query = String.Format(query, "Temp_Trans_Provider_" + this.m_lien, this.CieSearch, condition, Lang);

            log.Log(query);
            if (conditionReponse.qty_minus > 0) { txt_O1.Text = conditionReponse.qty_minus.ToString(); } else { txt_O1.Text = ""; }
            if (conditionReponse.qty_over > 0) { txt_O2.Text = conditionReponse.qty_over.ToString(); } else { txt_O2.Text = ""; }
            if (conditionReponse.model > 0) { txtModel.Text = conditionReponse.model.ToString(); } else { txtModel.Text = ""; }
            if (String.IsNullOrEmpty((string)Session["RefNumber"])) { lblNoReference.Text = ""; } else { lblNoReference.Text = (string)Session["RefNumber"]; }
           // if (!String.IsNullOrEmpty((string)Session["Date_Limite"].ToString())) { lbldate_limite.Text = Tool.formatDate((DateTime)Session["Date_Limite"]); } else { lbldate_limite.Text = ""; }
            lbldate_limite.Text = Tool.formatDate(m_Date_Limite);
            if(Connexion.execScalar(query))
            {
                setData(query);
            }
            else
            {
                query = "";
                query = Tool.getQuery("Page_AmemView2.sql");

                query = String.Format(query, "Temp_Trans_Provider_" + this.m_lien, this.CieSearch, condition, Lang);
                setData(query);
            }

        }
        private ConditionReponse verifCondition()
        {
            getParam();
            bool check = false;
            ConditionReponse rep = new ConditionReponse();
            //Verifie si une condition existe déja
            string query = Tool.getQuery("AmemConditionVerif.sql");
            query = String.Format(query, this.m_lien);
            log.Log("AmemConditionVerif " + query);
            check = Connexion.execScalar(query);
            if (check)
            {
                rep = Connexion.getCondition(this.m_lien);
            }

            return rep;

        }
        private void setDropDown()
        {

            //Supprime données existantes
            ddlHeader.AppendDataBoundItems = true;
            SqlConnection con = Connexion.ConnectSQL();
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT id,Name_fr FROM SearchList ORDER BY Name_fr";
            cmd.Connection = con;
            try
            {
                ddlHeader.DataSource = cmd.ExecuteReader();
                ddlHeader.DataTextField = "Name_fr";
                ddlHeader.DataValueField = "Id";
                ddlHeader.DataBind();

            }
            catch (Exception sdd)
            {
                throw sdd;
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(con);
            }

            ddlStore.AppendDataBoundItems = true;
            SqlConnection conStore = Connexion.ConnectSQL();
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT Id,CONVERT(varchar,NoMag) + ' - ' + NomMagasin as 'Magasin' FROM Magasin ORDER BY NoMag";
            cmd.Connection = conStore;
            try
            {
                ddlStore.DataSource = cmd.ExecuteReader();
                ddlStore.DataTextField = "Magasin";
                ddlStore.DataValueField = "Id";
                ddlStore.DataBind();

            }
            catch (Exception dds)
            {
                log.logError("AmemView ddlStore :" + dds.Message + " StackTrace : " + dds.StackTrace);
            }
            finally
            {
                cmd.Dispose();
                Connexion.closeSQL(conStore);
            }

        }
        private void setData(string query)
        {
            //Lit la requête depuis le fichier 
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            
            try
            {
                //Ouvre connexion
                c_con = Connexion.ConnectSQL();
                SqlDataAdapter adapter = new SqlDataAdapter(query, c_con);
                //Replit dataset
                adapter.Fill(ds);

                Session["dtav"] = ds.Tables[0];
                lblLine.Text = ds.Tables[0].Rows.Count.ToString();
                dt = (DataTable)Session["dtav"];

                //initialise Qty et montant
                foreach (DataRow row in dt.Rows)
                {
                    this.count += int.Parse(row[13].ToString());
                    this.amount += double.Parse(row[14].ToString());

                }
            }
            catch (Exception ex)
            {
                log.logError(ex.Message);
            }
            finally
            {
                //Ferme la connexion
                Connexion.closeSQL(c_con);
            }



            if (ds.Tables.Count > 0)
            {
                //Initialise le textbox 
                lbl_Distinct.Text = this.count.ToString();
                lblRetour_money.Text = String.Format("{0:C2}", this.amount);

                //Vide les variables session
                Session.Contents.Remove("count");
                Session.Contents.Remove("amount");
                //Initialise les variables session
                Session["count"] = this.count;
                Session["amount"] = this.amount;



                if (Session["sort"] != null)
                {

                    if (Session["sort"] != null)
                    {

                        dt.DefaultView.Sort = Session["sort"].ToString();
                        setGrid();
                    }

                }
                if (ds.Tables[0].Rows.Count == 0)
                {
                    BtnDelSelect.Visible = false;
                    Literal1.Visible = true;
                    // GridView1.Visible = false;
                }
                else
                {
                    Literal1.Visible = false;

                }
                setGrid();
            }
            else
            {
                //
                BtnDelSelect.Visible = false;
            }

           
        }
        void getParam()
        {
            if (Session["lang"] != null) { Lang = Convert.ToString(Session["lang"]).Substring(0, 2); } else { Lang = "fr"; }
            if (Session["CieSearch"] != null) { this.CieSearch = (int)Session["CieSearch"]; }
            if (Session["Cie"] != null) { this.cie = (int)Session["Cie"]; }
            if (Session["Dept"] != null) { this.dept = (int)Session["Dept"]; }
            if (Session["IdUsr"] != null) { this.idUser = (int)Session["IdUsr"]; }
            if (Session["Lien"] != null) { this.m_lien = (string)Session["Lien"]; }
            if (Session["count"] != null) { this.qty_demande = (int)Session["count"]; }
            if (Session["amount"] != null) { this.montant_demande = (double)Session["amount"]; }
            if (Session["qty_minus"] != null) { this.qty_minus2 = (int)Session["qty_minus"]; }
            if (Session["qty_over"] != null) { this.qty_over2 = (int)Session["qty_over"]; }
            if (Session["model"] != null) { this.model2 = (int)Session["model"]; }
            if (Session["USR"] != null) { usager = (Usr)Session["USR"]; }
            if (usager.CieSearch == 0) { usager.CieSearch = this.CieSearch; }
            usager.idCieParam = this.cie;
            usager.idParam = this.idUser;
            usager.index = 1;
            //Vérifie si item selectionner 
            if (this.ddlStore.SelectedValue == "0") { this.m_index = 0; } else { this.m_index = int.Parse(this.ddlStore.SelectedValue.ToString()); }
            if (this.ddlHeader.SelectedValue == "0") { this.m_Id_DllHeader = 0; } else { this.m_Id_DllHeader = int.Parse(ddlHeader.SelectedItem.Value); }
            if (this.ddlDescription.SelectedValue == "0") { this.m_Id_DllDescription = 0; } else { this.m_Id_DllDescription = int.Parse(ddlDescription.SelectedValue); }
            //if (String.IsNullOrEmpty(this.ddlDescription.SelectedValue)) { this.m_Id_DllDescription = 0; } else if (ddlDescription.SelectedValue == "--Sélectionner item--") { this.m_Id_DllDescription = 0; } else { this.m_Id_DllDescription = int.Parse(ddlDescription.SelectedValue); }
            //Si tous les index ddl = 0 alors cacher bouton Tout Supprimer
            if (this.m_Id_DllDescription == 0 && this.m_index == 0) { BtnDelSelect.Visible = false; } else { BtnDelSelect.Visible = true; }
            if ((string)Session["RETOUR"] == "Desuet") { m_Date_Limite = (DateTime)Session["Date_Limite"]; } else { m_Date_Limite = DateTime.Now.AddDays(15); }
        }

        void getCondition()
        {
            getParam();
            //Vide les variables session
            Session.Contents.Remove("qty_minus");
            Session.Contents.Remove("qty_over");
            Session.Contents.Remove("model");

            //Récupère les valeurs entrer par l'usager
            if (txt_O1.Text != "")
            {
                qty_minus = int.Parse(txt_O1.Text);
                Session["qty_minus"] = qty_minus;
                this.m_condition = " AND Qty_a_Retourner > " + qty_minus;

            }
            if (txt_O2.Text != "")
            {
                qty_over = int.Parse(txt_O2.Text);
                Session["qty_over"] = qty_over2;
                this.m_condition += " AND Qty_a_Retourner < " + qty_over;
            }
            if (!String.IsNullOrEmpty(txtModel.Text))
            {
                model = int.Parse(txtModel.Text);
                Session["model"] = model;
                this.m_condition += " AND Model_Calcule < " + model;
            }

            string response = Tool.searchMag(this.m_index);

            if (ddlHeader.SelectedValue != "0")
            {
                if (ddlDescription.SelectedValue != "0")
                {
                    //   int ID = int.Parse(ddlHeader.SelectedItem.Value);

                    switch (this.m_Id_DllHeader)
                    {
                        case 1:
                            this.m_condition += " AND sku = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 2:
                            this.m_condition += " AND Auteur = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 3:
                            this.m_condition += " AND Titre = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 4:
                            this.m_condition += " AND Etiquette = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    this.m_condition +=  response;
                }
            }
            else
            {

                this.m_condition += response;

            }
          
        }

        protected void btnCalcul_Click(object sender, EventArgs e)
        {
            refreshData();
        }
        void refreshData()
        {
            getParam();
            getCondition();
            //Lit la requete stockée si lien != null
            if (!String.IsNullOrEmpty(this.m_lien))
            {

                query = Tool.getQuery("Page_AmemView.sql");

                query = String.Format(query, "Temp_Trans_Provider_" + this.m_lien, this.CieSearch, this.m_condition, Lang, m_Date_Limite);

                log.Log("Page_AmemView :" + query);

                if (Connexion.execScalar(query))
                {
                    setData(query);
                }
                else
                {
                    query = Tool.getQuery("Page_AmemView2.sql");

                    query = String.Format(query, "Temp_Trans_Provider_" + this.m_lien, this.CieSearch, this.m_condition, Lang, m_Date_Limite);

                    log.Log("Page_AmemView :" + query);

                    setData(query);
                }
            }

        }
        protected void BtnSave_Click(object sender, EventArgs e)
        {

            saveCommande();

        }
        private void saveCommande()
        {
            //Cacher le bouton Save
          //  BtnSave.Visible = false;
            //Verifie si deja enregistrer si oui rien faire
            bool rep = saveGrid();
            //Sinon on enregistre
            setSaveCondition();
            if (!rep)
            {
                getParam();
                //Instancie parametres entrer
                NewTransaction nt = new NewTransaction();
                nt.NoCie = this.CieSearch;
                nt.Lien = this.m_lien;

                //Instancie nouvelle classe avec parametre entrer
                AmemSave ams = new AmemSave(nt);
                //Sauvegarde les données de la grille
                ams.setAmemSave();

            }
            //Réafficher le bouton envoyé
            BtnSend.Visible = true;
        }
        private void setSaveCondition()
        {
            try
            {
                bool rep = false;

                getCondition();
                //string t = this.m_condition;
                //if (!String.IsNullOrEmpty(t))
                //{
                    getParam();
                    //Verifie si une condition existe déja
                    string query = Tool.getQuery("AmemConditionVerif.sql");
                    query = String.Format(query, this.m_lien);
                    log.Log("Requete AmemConditionVerif : " + query);
                    rep = Connexion.execScalar(query);
                    if (rep)
                    {
                        //Update la condition
                        query = Tool.getQuery("AmemConditionUpdate.sql");
                        query = String.Format(query, this.m_condition, this.m_lien, this.qty_minus2, this.qty_over2, this.model2);
                        log.Log("Requete AmemConditionUpdate : " + query);
                        Connexion.execCommand(query);
                    }
                    else
                    {
                        //On enregistre la condition
                        query = Tool.getQuery("AmemConditionInsert.sql");
                        query = String.Format(query, this.m_lien, this.m_condition, this.qty_minus2, this.qty_over2, this.model2);
                        log.Log("Requete AmemConditionInsert : " + query);
                        Connexion.execCommand(query);
                    }
               // }
            }
            catch (Exception condition)
            {
                log.logError("AmemView.aspx setSaveCondition :" + condition.Message);
            }

        }
        private bool saveGrid()
        {
            //Recherche si Lien existe
            getParam();
            string query = Tool.getQuery("checkLien.sql");
            query = String.Format(query, this.m_lien);
            log.Log("checkLien : " + query);
            return Connexion.execScalar(query);

        }
        protected void BtnSend_Click(object sender, EventArgs e)
        {

            bool succes = false;
            // string email = "";
            //   int count = 0;
            getCondition();
            getParam();
            //Instancie object parametre Amem
            ParamAmem pa = new ParamAmem();
            pa.cie = this.CieSearch;// this.CieSearch;
            pa.dept = this.dept; // dept;

            pa.amount.montant_demande = this.montant_demande;
            pa.amount.qty_demande = this.qty_demande;
            pa.condition = this.m_condition;
            pa.dt = (DataTable)Session["dtav"];
            pa.idUsrEnvoi = this.idUser;
            pa.lien = this.m_lien;
            pa.section = 1;
            pa.lang = Lang;
            pa.idTransactionType = Connexion.getTransactionType("Temp_Trans_Provider_" + this.m_lien);
            pa.DateLimite = this.m_Date_Limite;

            if (Session["RefNumber"] != null) { pa.noAvis = (string)Session["RefNumber"]; } else { pa.noAvis = ""; }


            if (pa.idTransactionType == 2)//((string)Session["RETOUR"] == "Desuet")
            {
                //Modifier les status des produits dans MMS pour discontinuée
                setDesuetudes400 s = new setDesuetudes400(this.m_lien);
                //Si erreur réaffichage de la grille
                if (!s.setStatusDesuetudes())
                {
                    //Afficher erreur lors du transfert au as400
                    ScriptManager.RegisterStartupScript(this, GetType(), "confirmationDesuetude", "confirmationDesuetude()", true);
                    //Reload page
                    Response.Redirect(Request.RawUrl);
                }
                else
                {
                    //Supprime les sku de la table Overstock
                    NewTransaction nt = new NewTransaction();
                    nt.Lien = pa.lien;
                    nt.NoCie = pa.cie;
                    CreateNewProvider cnp = new CreateNewProvider(nt);
                    cnp.delDesTransferLoad();
                }

            }

            //Si défectueux inscrire num retour dans AS400
            if (pa.idTransactionType == 3)
            {
                InsertDefectNumber idn = new InsertDefectNumber("Temp_Trans_Provider_" + this.m_lien);
                //Si inscription dans rtvsaut = true alors envoi courriel pour traitement en cours au magasin.
                if(idn.setDefectNumber())
                {
                    //0- NoRetour 1- idProvider 2 - NoMag
                    string NoRetour = idn.getNoRetour();
                    int NoMag = idn.getNoMag();

                    EnvoiEmail em = new EnvoiEmail(NoRetour,pa.cie,NoMag,"");
                }

            }

            //Enregistre le datagrid pour l'envoi vers le fournisseur
            try
            {
                succes = amemToProvider2.transfertByCopy(pa);
            }
            catch (Exception ex)
            {
                Session["CurrentError"] = "Page AmemView.aspx : 370" + ex.Message;
                log.logError(Session["CurrentError"].ToString());
            }

          

            //Si succes envoi courriel et affiche page succes
            if (succes)
            {
                //Update TempAmem a is_complete
                string query = Tool.getQuery("TempAmemUpdate.sql");
                query = String.Format(query, this.m_lien, 1);
                log.Log("Requete TempAmemUpdate : " + query);
                try
                {
                    Connexion.execCommand(query);
                }
                catch (Exception bsc)
                {
                    log.logError("AmemView.aspx BtnSend_Click : " + bsc.Message);
                }
                //Module envoi courriel
                EnvoiEmail em = new EnvoiEmail(this.CieSearch, this.CieSearch, 1, true, "", pa.idTransactionType);

                if (pa.idTransactionType != 2)//((string)Session["RETOUR"] != "Desuet")
                {
                    //Retour à la page Amem
                    Response.Redirect("Amem.aspx?Temp=0&usr=" + usager.id + "&cie=" + usager.idCie);
                }
                else
                {
                    Response.Redirect("../AchatAdmin/dashboard_v2.aspx?Temp=0&usr=" + this.idUser + "&cie=" + this.CieSearch);
                }
            }
            else
            {
                //Afficher page erreur et rediriger vers page initial
                Response.Redirect("../error.aspx");

            }
        }
        private void setGrid()
        {
            GridView1.DataSource = (DataTable)Session["dtav"];

            GridView1.DataBind();
            saveCommande();
           


        }
        protected void ProviderEmail_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            setGrid();
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            int id = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
            DeleteCommand(id);
            refreshData();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {


        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            setGrid();
        }
        private void UpdateCommand(int id, int Qty)
        {
            getParam();
            //Creation de la requete
            string query = Tool.getQuery("Update_AmemView.sql");
            query = String.Format(query, this.m_lien, Qty, id);
            log.Log("Requete Update_AmemView : " + query);
            //Creation de la commande
            Connexion.execCommand(query);

        }
        private void DeleteCommand(int id)
        {
            getParam();
            //Creation de la requete
            string query = Tool.getQuery("Delete_TempTransProvider.sql");
            query = String.Format(query, this.m_lien, id);
            log.Log("Requete Delete_TempTransProvider : " + query);
            Connexion.execCommand(query);

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            setGrid();
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            //DataSet ds = (DataSet)Session["DataSource"];
            DataTable dt = (DataTable)Session["dtav"];
            //Crée un DataView
            DataView dv = new DataView(dt);
            if (dt != null)
            {
                dt.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
                Session["sort"] = dt.DefaultView.Sort;
                setGrid();
            }

        }
        /// <summary>
        /// Order de trie des colonnes
        /// </summary>
        /// <param name="column">Nom de la colonne à trier</param>
        /// <returns>Ordre de la direction du trie</returns>
        private string GetSortDirection(string column)
        {
            // Ordre croissance par défaut.
            string sortDirection = "ASC";
            // Recherche le dernier ordre de croissance.
            string sortExpression = ViewState["SortExpression"] as string;

            if (sortExpression != null)
            {
                //Vérifie si c"est la meme colonne qui doit être triée
                //Sinon la valeur par defaut peut être retourné
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    sortDirection = "DESC";
                }
            }

            // Sauvegarde le dernier resultat dans le ViewState.
            ViewState["SortDirection"] = sortDirection;
            ViewState["SortExpression"] = column;

            return sortDirection;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = GridView1.SelectedRow;
            string r = row.Cells[12].Text;
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataRowView drv = (DataRowView)e.Row.DataItem;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if ((bool)drv["Add_Sku"] == true) 
                    { 
                        e.Row.Font.Bold = true; 
                         e.Row.ForeColor = System.Drawing.Color.Black;
                         e.Row.BackColor = System.Drawing.Color.LawnGreen;
                    }
                    //Désactive le enter sur les boutons
                    e.Row.Attributes.Add("onkeypress", "javascript:if (event.keyCode == 13){return false;}");

                    ((System.Web.UI.WebControls.ImageButton)(e.Row.Cells[15].Controls[0])).ToolTip = "Supprimer";


                }
            }
            catch (Exception ex)
            {
                  log.logError(ex.Message);
            }
        }

        protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.NewSelectedIndex];
            string r = row.Cells[12].Text;
        }

        protected void CreatePDF_Click(object sender, EventArgs e)
        {

        }

        protected void btnPDF_Click(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            getParam();

            PdfPTable pdfTable = new PdfPTable(GridView2.HeaderRow.Cells.Count);

            foreach (TableCell headerCell in GridView2.HeaderRow.Cells)
            {
                Font font = new Font();
                font.Color = new BaseColor(GridView2.HeaderStyle.ForeColor);
                font.Size = 8;

                PdfPCell pdfCell = new PdfPCell(new Phrase(headerCell.Text, font));
                pdfCell.BackgroundColor = new BaseColor(GridView2.HeaderStyle.BackColor);
                pdfTable.AddCell(pdfCell);
            }

            foreach (GridViewRow gridViewRow in GridView2.Rows)
            {
                foreach (TableCell tableCell in gridViewRow.Cells)
                {
                    Font font = new Font();
                    font.Color = new BaseColor(GridView2.RowStyle.ForeColor);
                    font.Size = 8;
                    PdfPCell pdfCell = new PdfPCell(new Phrase(tableCell.Text, font));
                    pdfCell.BackgroundColor = new BaseColor(GridView2.RowStyle.BackColor);
                    pdfTable.AddCell(pdfCell);
                }
            }

            pdfTable.HeaderRows = 1;
            Document pdfDocument = new Document(PageSize.LEGAL.Rotate());
            PdfWriter.GetInstance(pdfDocument, Response.OutputStream);

            pdfDocument.Open();
            pdfDocument.Add(pdfTable);
            pdfDocument.Close();

            Response.ContentType = "application/pdf";
            Response.AppendHeader("content-disposition", "attachment;filename=" + this.m_lien + ".pdf");
            Response.Write(pdfDocument);
            Response.Flush();
            Response.End();
        }

        protected void ddlHeader_SelectedIndexChanged(object sender, EventArgs e)
        {
            getParam();
            ddlDescription.Items.Clear();
            ddlDescription.Items.Add(new System.Web.UI.WebControls.ListItem("--Sélectionner item--","0"));
            ddlDescription.AppendDataBoundItems = true;


            string response = Tool.searchMag(this.m_index);
            string textField = "";
            query = "";
            if (ddlHeader.SelectedValue != "0")
            {
                

                switch (this.m_Id_DllHeader)
                {
                    case 1:
                        textField = "Sku";
                        query = "SELECT DISTINCT MIN(Id) as 'Id',Sku FROM Temp_Trans_Provider_" + this.m_lien + " WHERE Sku <> '' " + response + " GROUP BY Sku ORDER BY Sku";
                        break;
                    case 2:
                        textField = "Auteur";
                        query = "SELECT DISTINCT MIN(Id) as 'Id',Auteur FROM Temp_Trans_Provider_" + this.m_lien + " WHERE Auteur <> '' " + response + " GROUP BY Auteur ORDER BY Auteur";
                        break;
                    case 3:
                        textField = "Titre";
                        query = "SELECT DISTINCT MIN(Id) as 'Id',Titre FROM Temp_Trans_Provider_" + this.m_lien + " WHERE Titre <> '' " + response + " GROUP BY Titre ORDER BY Titre";
                        break;
                    case 4:
                        textField = "Etiquette";
                        query = "SELECT DISTINCT MIN(Id) as 'Id',Etiquette FROM Temp_Trans_Provider_" + this.m_lien + " WHERE Etiquette <> '' " + response + " GROUP BY Etiquette ORDER BY Etiquette";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                //   ddlDescription.SelectedValue = "";
                refreshData();
            }
            if (!String.IsNullOrEmpty(textField))
            {
                SqlConnection con = Connexion.ConnectSQL();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = query;
                log.Log("SELECT DISTINCT :" + query);
                cmd.Connection = con;
                try
                {
                    ddlDescription.DataSource = cmd.ExecuteReader();
                    ddlDescription.DataTextField = textField;
                    ddlDescription.DataValueField = "Id";
                    ddlDescription.DataBind();

                }
                catch (Exception ddd)
                {
                    log.logError(ddd.Message);
                }
                finally
                {
                    cmd.Dispose();
                    Connexion.closeSQL(con);
                }
            }
        }


        protected void ddlDescription_SelectedIndexChanged(object sender, EventArgs e)
        {
            refreshData();
            //Rendre le bouton supprimer tout visible
            //  BtnDelSelect.Visible = true;
        }

        protected void BtnDelSelect_Click(object sender, EventArgs e)
        {

            //Execute requete de suppression
            if (deleteAll())
            {
                ddlHeader.SelectedValue = "0";
                //  ddlDescription.SelectedValue = "";
                ddlDescription.Items.Clear();
                ddlDescription.Items.Add(new System.Web.UI.WebControls.ListItem("--Sélectionner item--","0"));
                ddlDescription.AppendDataBoundItems = true;
                ddlStore.SelectedValue = "0";

                //Rafraichit la grille
                refreshData();
                //  BtnDelSelect.Visible = false;
            }


        }
        private bool deleteAll()
        {
            getParam();

            string query = "DELETE FROM Temp_Trans_Provider_" + this.m_lien;

            string response = Tool.searchMag(this.m_index);

            if (ddlHeader.SelectedValue != "0")
            {
                if (ddlDescription.SelectedValue !="0")
                {

                    switch (this.m_Id_DllHeader)
                    {
                        case 1:
                            query += " WHERE sku = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 2:
                            query += " WHERE Auteur = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 3:
                            query += " WHERE Titre = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        case 4:
                            query += " WHERE Etiquette = '" + Tool.formatCondition(ddlDescription.SelectedItem.Text) + "' " + response;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    query += " WHERE sku <> '' " + response;
                }

                log.Log("DELETE : " + query);
                return Connexion.execCommand(query);
            }
            else
            {
                if (!String.IsNullOrEmpty(response))
                {
                    query += " WHERE sku <> '' " + response;
                    log.Log("DELETE : " + query);
                    return Connexion.execCommand(query);
                }
                else
                {
                    return false;
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            getParam();
            saveCommande();
            Response.Redirect("../AchatAdmin/dashboard_v2.aspx?Temp=0&usr=" + this.idUser + "&cie=" + this.CieSearch);
        }

        protected void txtQty_TextChanged(object sender, EventArgs e)
        {
            this.Page.Form.DefaultFocus = null;
            GridViewRow grv = ((TextBox)sender).Parent.Parent as GridViewRow;

            Label lblId = (Label)grv.FindControl("lblID");
            TextBox txtQty = (TextBox)grv.FindControl("TextBox1");
            //Si valeur inscrite = 0 alors supprimer la ligne
            if (int.Parse(txtQty.Text) == 0)
            {
                DeleteCommand(int.Parse(lblId.Text));
            }
            else
            {
                UpdateCommand(int.Parse(lblId.Text), int.Parse(txtQty.Text));
            }

            //  GridView1.EditIndex = -1;
            refreshData();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            this.Page.Form.DefaultFocus = null;
            GridViewRow grv = ((TextBox)sender).Parent.Parent as GridViewRow;

            Label lblId = (Label)grv.FindControl("lblID");
            TextBox txtQty = (TextBox)grv.FindControl("TextBox1");

            //Si qty égal 0 alors supprime ligne
            if (int.Parse(txtQty.Text) == 0)
            {
                DeleteCommand(int.Parse(lblId.Text));
            }
            else
            {
                //Sinon mettre à jour
                UpdateCommand(int.Parse(lblId.Text), int.Parse(txtQty.Text));
            }
            refreshData();
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

            refreshData();
            //Rendre le bouton supprimer tout visible
            //  BtnDelSelect.Visible = true;
        }

        protected void txtCost_TextChanged(object sender, EventArgs e)
        {
            this.Page.Form.DefaultFocus = null;
            GridViewRow grv = ((TextBox)sender).Parent.Parent as GridViewRow;

            Label lblId = (Label)grv.FindControl("lblID");
            TextBox txtQty = (TextBox)grv.FindControl("txtCost");

            getParam();
            //Creation de la requete
            string query = Tool.getQuery("UpdateAmemViewCost.sql");
            Tool.setThread_EN();
            query = String.Format(query, this.m_lien, double.Parse(txtQty.Text), int.Parse(lblId.Text));
            log.Log("Requete Update_AmemViewCost : " + query);
            //Creation de la commande
            Connexion.execCommand(query);
            Tool.setThread_FR();
            refreshData();
        }

        protected void btnAjout_Click(object sender, EventArgs e)
        {
            getParam();
            RetourXLS rt = new RetourXLS();
            rt.UPC = txtSku.Text;
            rt.NoCie = this.CieSearch;
            rt.Lien = this.m_lien;
            rt.Date_Limite = this.m_Date_Limite;
            AjoutProduitManuel ajm = new AjoutProduitManuel(rt);

            ajm.addProduct();

            //string newSku = txtSku.Text;
            ////Code pour ajouter produit
            //string query = Tool.getQuery("AddSku.sql");
            //query = String.Format(query, newSku, this.CieSearch, this.m_Date_Limite);
            //log.Log(query);
            //FillAmemData.copyAddProduct(query, "Temp_Trans_Provider_" + this.m_lien);
            refreshData();
        }

        private void ExportToExcel(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                string filename = "DownloadListNoExcel.xls";
                System.IO.StringWriter tw = new System.IO.StringWriter();
                System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
                DataGrid dgGrid = new DataGrid();
                dgGrid.DataSource = dt;
                dgGrid.DataBind();

                //Get the HTML for the control.
                dgGrid.RenderControl(hw);
                //Write the HTML back to the browser.
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.Charset = "";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
                this.EnableViewState = false;
                Response.Write(tw.ToString());
                Response.End();
            }
        }

        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = (DataTable)Session["dtav"];
            ExportToExcel(dt);
        
        }

    }
}