﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AmemView2.aspx.cs" Inherits="RetourArchambault.Achat.AmemView2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
 <title>Rapports</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' />
    <link href="../css/default.css" rel="stylesheet" type="text/css" />
    <link href="../css/rapports.css" rel="stylesheet" type="text/css" />
    <link href="../css/datagrid_style.css" rel="stylesheet" type="text/css" />
    <link href="../css/ajaxTool.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">

         $(document).ready(function () {
             $("#GridView1").keydown(function (e) {
                 if (e.keyCode == 13) {
                     return false;
                 }
             });

         });

</script>
    <script type="text/javascript">
        function confirmation() {
            if (confirm('Vous êtes certain de vouloir supprimer la sélection ?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</head>
<body class="fr">
    <div id="header">
        <asp:Image ID="Image1" ImageUrl="~/images/lg_archambault01White.png" runat="server" meta:resourcekey="Image1Resource1" />
    </div>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></ajaxToolkit:ToolkitScriptManager>
   <div id="wrapper">
            <div id="content">
                <div class="container">
                    <div class="rapport_container">
                           <asp:Button ID="btnClose" runat="server" CssClass="btn backAction" Text="Retour au tableau de bord" OnClick="btnClose_Click" />
                        <div class="frame">
                            <div class="dept_table_cont">
                                <table class="dept_table">
                                    <tr class="even">
                                        <td>Exclure les retours dont la qté à retourner est de</td>
                                        <td class="centered">
                                            <asp:TextBox ID="txt_O1" runat="server"></asp:TextBox>
                                        </td>
                                        <td>unités ou moins</td>
                                        <td class="last"></td>
                                    </tr>
                                    <tr class="odd">
                                        <td>Exclure les retours dont la qté à retourner est de</td>
                                        <td class="centered">
                                            <asp:TextBox ID="txt_O2" MaxLength="3" runat="server"></asp:TextBox></td>
                                        <td>unités ou plus</td>
                                        <td class="last"></td>
                                    </tr>
                                    <tr class="even">
                                        <td>Exclure les retours dont le Modèle calculé est de</td>
                                        <td class="centered">
                                            <asp:TextBox ID="txtModel" runat="server"></asp:TextBox>
                                        </td>
                                        <td>unités ou plus</td>
                                        <td class="last"></td>
                                    </tr>
                                    <tr class="odd last">
                                        <td>
                                            Rechercher par 
                                            <asp:DropDownList ID="ddlHeader" runat="server" AutoPostBack="true"
                                                 OnSelectedIndexChanged="ddlHeader_SelectedIndexChanged">
                                                <asp:ListItem Text="--Selectionner entête--" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td valign="bottom" class="centered">
                                            <asp:DropDownList ID="ddlDescription" runat="server" AutoPostBack="true"
                                                 OnSelectedIndexChanged="ddlDescription_SelectedIndexChanged">
                                                <asp:ListItem Text="--Selectionner item--" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <%-- <input type="text" class="inputSizeLong" />--%>
                                        </td>
                                        <td colspan="2" valign="bottom" align="right" class="last" style="padding-right:0px;padding-left:19px;">
                                            <asp:DropDownList ID="ddlStore" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" >
                                                <asp:ListItem Text="Tous les magasins" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>

                                </table>
                            </div>                            
                            <div class="nbrLine_cont">
                                <table class="stock_table">
                                    <tr>
                                        <td>
                                            <asp:Button ID="Button1" runat="server" Text="Ajouter produit" /> 

                                        </td>
                                    </tr>
                                    <tr class="even">
                                        <td>Nombre de lignes :</td>
                                        <td>
                                            <asp:Label ID="lblLine" CssClass="nbrLine" runat="server" Text="Nombre de lignes"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="stock_table_cont lastTbl">
                                
                                <table class="stock_table">
                                    <tr class="even">
                                        <td>À retourner</td>
                                        <td>
                                            <asp:Label ID="lbl_Distinct" runat="server" Text="0" meta:resourcekey="lbl_DistinctResource1"></asp:Label></td>
                                    </tr>
                                    <tr class="odd last">
                                        <td>À retourner ($)</td>
                                        <td>
                                            <asp:Label ID="lblRetour_money" runat="server" Text="0.0" meta:resourcekey="lblRetour_moneyResource1"></asp:Label></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="button_cont">
                            <asp:Button ID="btnCalcul" CssClass="refresh_btn" runat="server" Text="" OnClick="btnCalcul_Click" />
                            <asp:Button ID="BtnSave" runat="server" CssClass="sauvegarder_btn" Visible="true" Text="" OnClick="BtnSave_Click" />
                            
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="BtnSend" runat="server" CssClass="email_btn" Text="" OnClick="BtnSend_Click" />
                                    
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                <ProgressTemplate>
                                    <div class="overlay">
                                        <div class="overlayContent">
                                            <div class="loading_msg">
                                                <img src="/images/loader.gif" alt="Loading" />
                                                <p>
                                                    Création du rapport en cours....<br />
                                                    ce processus peut prendre quelques minutes,<br />
                                                    veuillez patienter svp...
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>  
                           
                              <asp:Button ID="BtnDelSelect" runat="server" CssClass="deleteAll_btn" Text="" OnClientClick="return confirmation()" OnClick="BtnDelSelect_Click" Visible="false" /> 

                            
                            <asp:SqlDataSource ID="ProviderEmail" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT DISTINCT [IdCie], [Email] FROM [Provider_Usr] WHERE ([IdCie] = @IdCie)" OnSelecting="ProviderEmail_Selecting">
                                <SelectParameters>
                                    <asp:QueryStringParameter DefaultValue="0" Name="IdCie" QueryStringField="Cie" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </div>
                    </div>
                </div><!--END /.container-->
                <%--  <div class="result_table_cont">--%>
                <div style="align-content: center;" class="result_table_cont amem_tbl">
                    <asp:Literal ID="Literal1" Visible="false" runat="server"><h2 class="noData_msg">Aucune données ne répond à ces critères.</h2></asp:Literal>
                    <asp:GridView ID="GridView1"
                        runat="server"
                        AutoGenerateColumns="False"
                        OnPageIndexChanging="GridView1_PageIndexChanging"
                        OnSorting="GridView1_Sorting"
                        OnRowCancelingEdit="GridView1_RowCancelingEdit"
                        OnRowDeleting="GridView1_RowDeleting"
                        OnRowUpdating="GridView1_RowUpdating"
                        OnRowEditing="GridView1_RowEditing"
                        AllowPaging="True" AllowSorting="True" DataKeyNames="id" GridLines="None" OnRowDataBound="GridView1_RowDataBound" PageSize="50">
                        <RowStyle CssClass="odd" />
                        <AlternatingRowStyle CssClass="even" />
                        <FooterStyle HorizontalAlign="Left" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="Début" LastPageText="Fin" NextPageText="Suivante" PreviousPageText="Précédente" Position="Bottom" />
                        <Columns>
                            <asp:TemplateField HeaderText="No Magasin" SortExpression="NoMag">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("NoMag") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SKU" SortExpression="SKU">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("SKU") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No Cie" SortExpression="NoCie">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("NoCie") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ISBN" SortExpression="ISBN">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("ISBN") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UPC" SortExpression="UPC">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# Eval("UPC") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Auteur" SortExpression="AUTEUR">
                                <ItemTemplate>
                                    <asp:Label ID="Label6" runat="server" Text='<%# Eval("AUTEUR") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Titre" SortExpression="TITRE">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("TITRE") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Compos." SortExpression="COMPOS">
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("COMPOS") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Format" SortExpression="FORMAT">
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("FORMAT") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Étiquette" SortExpression="Etiquette">
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("Etiquette") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Transaction">
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("Desc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Modèle Calculé" SortExpression="Model_Calcule">
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("Model_Calcule") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cost" SortExpression="COST">
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("COST","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="En Stock" SortExpression="EnStock">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("EnStock") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                            <asp:TemplateField HeaderText="Qty à retourner" SortExpression="Qty_a_Retourner">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="true" OnTextChanged="TextBox1_TextChanged" Text='<%# Eval("Qty_a_Retourner") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                         
                            <asp:TemplateField HeaderText="Valeur de retour" SortExpression="Valeur_Retour">
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("Valeur_Retour","{0:C2}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CommandField HeaderText="Opération" ButtonType="Image" ControlStyle-Width="25" ShowDeleteButton="True"  ShowHeader="True"  DeleteImageUrl="~/images/Delete.png"> <%-- EditImageUrl="~/images/icon-edit.png" UpdateImageUrl="~/images/icon-update.png">--%>
                                <ControlStyle Width="25px"></ControlStyle>
                            </asp:CommandField>
                            <asp:TemplateField HeaderText="Id" SortExpression="id" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
            </div><!--END /.content-->
        </div><!--END /.wrapper-->
    </form>
</body>
</html>
