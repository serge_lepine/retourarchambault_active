﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Maitre.Master" AutoEventWireup="true" CodeBehind="Amem.aspx.cs" Inherits="RetourArchambault.Achat.Amem" Culture="auto"  UICulture="auto" %>
<%@ MasterType VirtualPath="~/Maitre.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<ul>
                        <li>
                            <asp:HyperLink ID="HyperLink1" Visible="false" runat="server">Nouvelle demande</asp:HyperLink></li>
                        <li class="odd">
                            <asp:HyperLink ID="HyperLink2" Visible="false" runat="server">Retour des fournisseurs</asp:HyperLink></li>
                       <%-- <li>
                            <asp:HyperLink ID="HyperLink3" Visible="false" runat="server">Historique</asp:HyperLink></li>
                        <li class="odd">
                            <asp:HyperLink ID="HyperLink4" Visible="false" runat="server">Mise à jour Inventaire</asp:HyperLink></li>--%>
                   <%-- </ul>--%>
    <script type="text/javascript"> 
function noBack(){window.history.forward()} 
noBack(); 
window.onload=noBack; 
window.onpageshow=function(evt){if(evt.persisted)noBack()} 
window.onunload=function(){void(0)} 
</script>
    <%-- <ul>
        <li><a href="../Admin/Default.aspx" accesskey="1">Administration</a></li>
        <li class="active"><a href="../Achat/Amem.aspx" accesskey="2">Achat & Mise en marché</a></li>
        <li><a href="../Fournisseur/Fournisseurs.aspx" accesskey="3">Fournisseur</a></li>
        <li><a href="../Succursales/Magasin.aspx" accesskey="4">Succursale</a></li>
    </ul>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>    
            <%--<div class="welcomeMessage">
                <asp:Label ID="lblBonjour" runat="server" Font-Bold="true" Text="Bonjour"></asp:Label>
            </div>--%>
            
                <div class="container"> 
                    <h2 class="title">Nouveau</h2>
                    <p>Sélectionner le fournisseur ainsi que le département</p>

                    <div class="frame">
                        <div class="new_cie_select">
                            <div class="frmElmnt fSlct">
                                <div class="fLbl">
                                    <label>No cie</label>
                                </div>
                                <div class="fWdgt">
                                    <asp:DropDownList ID="DropDownCie" runat="server" DataSourceID="ProviderList" AutoPostBack="true" OnSelectedIndexChanged="DropDownCie_SelectedIndexChanged" DataTextField="CieName" DataValueField="NoCie"></asp:DropDownList>
                                </div>
                                <asp:SqlDataSource ID="ProviderList" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT [NoCie], [CieName] FROM [Provider] ORDER BY CieName"></asp:SqlDataSource>
                                <div class="clearer"></div>
                            </div>
                            <div class="frmElmnt fSlct">
                                <div class="fLbl">
                                    <label>Dept</label>
                                </div>
                                <div class="fWdgt">
                                    <asp:DropDownList ID="DropDownDept" runat="server" DataSourceID="ProviderDept" DataTextField="Descriptions" DataValueField="Dept"></asp:DropDownList>
                                </div>
                                <asp:SqlDataSource ID="ProviderDept" runat="server" ConnectionString="<%$ ConnectionStrings:StagingConnectionString %>" SelectCommand="SELECT * FROM [Provider_Dept]"></asp:SqlDataSource>
                                <div class="clearer"></div>
                            </div>
                            <div class="frmElmnt fTxt">
                                <div class="fLbl">
                                    <label>Nombre de jour  exclu</label>
                                </div>
                                <div class="fWdgt inputSize1">
                                    <asp:TextBox ID="txtExclu" ToolTip="Entrer le nombre de jours à exclure" runat="server" Text="90"></asp:TextBox>
                                </div>
                                <div class="clearer"><!-- --></div>
                            </div>
                            <div class="frmElmnt fBtn">
                                <div class="fWdgt">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:Button CssClass="btn primaryAction" ID="Button1" runat="server" OnClick="Button1_Click" Text="Créer la liste des retours" meta:resourcekey="Button1Resource1" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:UpdateProgress ID="UpdateProgress1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
                                        <ProgressTemplate>
                                            <div class="overlay">
                                                <div class="overlayContent">
                                                    <div class="loading_msg">
                                                        <img src="/images/loader.gif" alt="Loading" />
                                                        <p>
                                                            Création du rapport en cours....<br />
                                                            ce processus peut prendre quelques minutes,<br />
                                                            veuillez patienter svp...
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </div>
                            </div>
                        </div>
                        <div class="returnDate_table">

                            <h3>Derniers retours</h3>
                            <div class="returnDate_table_container">
                            <asp:GridView ID="GridView1" runat="server" CssClass="dept_table" AutoGenerateColumns="False"   BorderStyle="None" CellPadding="3" GridLines="None" >
                                <RowStyle CssClass="odd" />
                                <AlternatingRowStyle CssClass="even" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Id" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Text=''></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date" SortExpression="Date_Cration">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_Date" runat="server" Text='<%# Eval("Date_Creation") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qté" SortExpression="Qty_demande">
                                        <ItemTemplate>
                                            <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Qty_demande") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Montant" SortExpression="Montant_demande">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Montant_demande","{0:C2}") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type" SortExpression="TransType">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("TransType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle />
                                <HeaderStyle />
                                <PagerStyle HorizontalAlign="Center" />
                                
                                <SelectedRowStyle />
                                <SortedAscendingCellStyle />
                                <SortedAscendingHeaderStyle />
                                <SortedDescendingCellStyle />
                                <SortedDescendingHeaderStyle />
                            </asp:GridView>
                                </div>
                        </div>
                    </div>
                </div>
                <!--END /Nouveau-->
                <div style="clear: both;">&nbsp;</div>
            <!--END /À Compléter-->
</asp:Content>
