﻿SELECT tp.Id
      ,tp.NoMag 
      ,tp.Sku
      ,tp.NoCie
      ,tp.ISBN
      ,tp.UPC
      ,tp.Auteur
      ,tp.Titre
      ,tp.Compo
      ,tp.Format
      ,tp.Etiquette
      ,tp.Cost
      ,tp.Qty_a_Retourner
      ,tp.ValeurRetour
      ,tp.Dept
      ,tp.Lien
      ,tp.IdUser
      ,tp.Date_Creation
      ,tp.Model_Calcule
      ,tp.Refuse
      ,tp.Qty_autorise
	  ,tp.Commentaires
	  ,tp.is_checked
	  ,tt.Description_{2} as 'Desc'
	  ,m.NomMagasin
	  ,tp.Date_Limite
	  ,tp.NoAvis 
  FROM Transaction_Provider_{0} tp 
  JOIN Magasin m ON m.NoMag = tp.NoMag
  JOIN TransactionType tt ON tt.Id=tp.idTransactionType
  {1}