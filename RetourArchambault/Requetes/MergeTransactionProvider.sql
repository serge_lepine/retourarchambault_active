﻿  
  MERGE INTO [Transaction_Provider_{0}] T
   USING (
          SELECT [Id], [Qty_a_Retourner] 
            FROM [Transaction_Provider_{0}]
         ) S
      ON T.id = S.id
WHEN MATCHED THEN
   UPDATE 
      SET Qty_autorise = S.[Qty_a_Retourner];