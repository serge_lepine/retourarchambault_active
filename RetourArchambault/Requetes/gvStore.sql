﻿  
 SELECT
  tlp.Id,
  CONVERT(varchar,tlp.Date_Creation,105) as 'Date_Envoi', 
  p.CieName,
  tlp.Amount_Dispo as 'Montant',
  CONVERT(varchar,tlp.Date_Limite,105)as 'Date_Limite',
  tlp.Lien,
  tt.Description_FR as 'Desc',
 CONVERT(varchar,(SELECT COUNT(*) FROM Transaction_Store ts WHERE is_complete = 1 AND ts.Lien = tlp.lien))+ '/' + CONVERT(varchar,(SELECT COUNT(*) FROM Transaction_Store ts WHERE ts.Lien = tlp.lien)) as Tot
  FROM Transaction_Link_Provider tlp, Provider p, TransactionType tt
  WHERE tlp.NoCie=p.NoCie AND tlp.Section = 3  AND tt.Id = tlp.idTransactionType
  AND tlp.Lien IN(
   SELECT Lien FROM Transaction_Store WHERE is_complete = 0
  GROUP BY Lien)

  -- SELECT
  --tlp.Id,
  --CONVERT(varchar,tlp.Date_Creation,105) as 'Date_Envoi', 
  --p.CieName,
  --tlp.Amount_Dispo as 'Montant',
  --CONVERT(varchar,tlp.Date_Limite,105)as 'Date_Limite',
  --tlp.Lien,
  --tt.Description_{0} as 'Desc'
  --FROM Transaction_Link_Provider tlp, Provider p, TransactionType tt
  --WHERE tlp.NoCie=p.NoCie AND tlp.Section = 3  AND tt.Id = tlp.idTransactionType
  -- AND tlp.Lien IN(
  --  SELECT Lien FROM Transaction_Store WHERE is_complete = 0
  --GROUP BY Lien)