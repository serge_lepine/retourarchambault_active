﻿SELECT tlp.Id
	  ,tlp.Date_Creation	
      ,p.CieName
      ,tlp.Dept
      ,tlp.Lien
      ,tlp.Qty_demande
      ,tlp.Montant_demande
      ,tlp.Qty_autorise
      ,tlp.Montant_autorise
	  ,tt.Description_{0} as 'Desc'
  FROM [Transaction_Link_Provider] tlp
  JOIN Provider p ON p.NoCie=tlp.NoCie
  JOIN TransactionType tt ON tt.Id=tlp.idTransactionType
  WHERE Section = 3
  ORDER BY tlp.Date_Creation DESC