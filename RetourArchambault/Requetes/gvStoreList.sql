﻿ SELECT ts.Id,ts.NoMag,
  CONVERT(varchar,ts.Date_Envoi,105) as 'Date_Envoi',
  ts.IdCie,
  ts.CieName,
  (SELECT SUM(tls.Qty_Dispo * tls.Cost) FROM Transaction_Store_List tls WHERE tls.Lien=ts.Lien AND tls.NoMag=ts.NoMag AND tls.Refuse is null) as 'Montant',
  CONVERT(varchar,ts.Date_Limite,105) as 'Date_Limite',
  ts.is_complete,
  CONVERT(varchar,ts.Date_Cueillette,105) as 'Cueillette',
  ts.Qty_Retourner,
  ts.Montant_retourne,
  tt.Description_{1} as 'Desc'
  FROM Transaction_Store ts
  JOIN TransactionType tt ON tt.Id = ts.idTransactionType
  WHERE ts.Lien='{0}'