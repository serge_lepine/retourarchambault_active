﻿SELECT  bal.ISTORE as NoMag
,u.INUMBR as SKU
,iv.ASNUM as NoCie
,iv.IVNDP# as ISBN
,u.IUPC as UPC
,iv.ARTIST as Auteur
,COALESCE((SELECT IDESCR160 FROM mmarclib.INVDSC2  WHERE inumbr=u.inumbr),iv.IDESCR) as TITRE,
iv.COMPSR as Compos
,iv.IATRB1 as Format
,iv.LABEL as Etiquette
,COALESCE((SELECT rfmax FROM mmarclib.rplprf WHERE ISTORE=bal.ISTORE AND INUMBR=u.INUMBR),0) as Model_Calcule
,iv.ICUCST as COST
,bal.IBHAND as Qty_a_Retourner
,(SELECt dpt.DPTNAM FROM mmarclib.invdpt dpt WHERE dpt.IDEPT=iv.IDEPT AND dpt.ISDEPT=iv.ISDEPT AND dpt.ICLAS=iv.ICLAS AND dpt.ISCLAS=iv.ISCLAS ) as Category
,bal.IBHAND as EnStock
,1 as idTransactionType
,'{2}' as Date_Limite
,1 as Add_Sku
FROM 
mmarclib.invupc u 
JOIN mmarclib.invbal bal ON bal.INUMBR=u.INUMBR
JOIN mmarclib.invmst iv ON iv.INUMBR=bal.INUMBR
WHERE u.INUMBR IN({0}) AND iv.ASNUM={1} AND bal.IBHAND > 0