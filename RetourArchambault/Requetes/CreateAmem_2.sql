﻿
  MERGE INTO [Transaction_Provider] T
   USING (
          SELECT [Id]
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compo]
      ,[Format]
      ,[Etiquette]
      ,[Cost]
      ,[Qty_a_Retourner]
      ,[ValeurRetour]
      ,[Dept]
      ,[Lien]
      ,[IdUser]
      ,[Date_Creation]
      ,[Model_Calcule]
      ,[Refuse]
      ,[Qty_autorise]
	  ,[Commentaires]
	  ,[idTransactionType]  FROM Transaction_Provider_{0}
         ) S
      ON T.id = S.id
WHEN MATCHED THEN
   UPDATE 
      SET Qty_autorise = S.[Qty_autorise], Refuse = S.[Refuse], Commentaires = S.[Commentaires];

  UPDATE Transaction_Link_Amem SET Section=2, Qty_autorise={1}  ,Montant_autorise={2},is_complete=1 WHERE Lien='{0}';
  UPDATE Transaction_Link_Provider SET Section=2, Qty_autorise={1}  ,Montant_autorise={2},is_complete=1 WHERE Lien='{0}';

