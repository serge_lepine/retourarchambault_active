﻿SELECT a.Id
      ,a.NoMag
      ,a.Magasin
      ,a.Temporaire
      ,a.Autorisation
      ,a.Lien
      ,tp.Dept
  FROM [Autorisation] a
  JOIN Transaction_Link_Provider tp ON tp.Lien=a.Lien
  WHERE a.Lien ='{0}' AND is_active = 1