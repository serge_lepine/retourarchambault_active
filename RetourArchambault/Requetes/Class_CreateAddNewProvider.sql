﻿BEGIN TRY

CREATE TABLE [dbo].[Temp_Trans_Provider_{0}](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[NoMag] [int] NULL,
	[Sku] [varchar](50) NULL,
	[NoCie] [int] NULL,
	[ISBN] [varchar](50) NULL,
	[UPC] [varchar](50) NULL,
	[Auteur] [varchar](250) NULL,
	[Titre] [varchar](250) NULL,
	[Compos] [varchar](250) NULL,
	[Format] [varchar](50) NULL,
	[Etiquette] [varchar](250) NULL,
	[Model_Calcule] [int] NULL,
	[COST] [money] NULL,
	[Qty_a_Retourner] [int] NULL,
	[Category] [varchar](200) NULL,
	[EnStock] [int] DEFAULT 0,
	[idTransactionType] [int],
	[Date_Limite] [varchar](50),
	[Add_Sku] bit,
	[NoRetour] varchar(200))

  END TRY

  BEGIN CATCH
  DROP TABLE [Temp_Trans_Provider_{0}]

CREATE TABLE [dbo].[Temp_Trans_Provider_{0}](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[NoMag] [int] NULL,
	[Sku] [varchar](50) NULL,
	[NoCie] [int] NULL,
	[ISBN] [varchar](50) NULL,
	[UPC] [varchar](50) NULL,
	[Auteur] [varchar](250) NULL,
	[Titre] [varchar](250) NULL,
	[Compos] [varchar](250) NULL,
	[Format] [varchar](50) NULL,
	[Etiquette] [varchar](250) NULL,
	[Model_Calcule] [int] NULL,
	[COST] [money] NULL,
	[Qty_a_Retourner] [int] NULL,
	[Category] [varchar](200) NULL,
	[EnStock] [int] DEFAULT 0,
	[idTransactionType] [int],
	[Date_Limite] [varchar](50),
	[Add_Sku] bit,
	[NoRetour] varchar(200))

  END CATCH;