﻿SELECT ts.Id
      ,ts.Lien
      ,ts.IdCie
      ,ts.CieName
      ,ts.AuthTemporaire
      ,ts.AuthProvider
      ,ts.Date_Envoi
      ,CONVERT(varchar,ts.Date_Limite,105) as 'Date_Limite'
      ,ts.Auth400
      ,ts.NoMag
      ,ts.is_complete
      ,ts.Dept
	  ,(SELECT SUM(tsl.Qty_Dispo) FROM [Transaction_Store_List] tsl WHERE tsl.Lien = ts.Lien AND tsl.NoMag=ts.NoMag) as 'Qty'
	  ,tt.Description_{1} as 'Desc'
  FROM [Transaction_Store] ts
  JOIN TransactionType tt ON tt.Id=ts.idTransactionType
  WHERE ts.NoMag={0} AND ts.is_complete < 1
  