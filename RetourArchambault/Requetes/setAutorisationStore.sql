﻿ -- Remettre à zéro les is_active
 UPDATE Autorisation SET is_active = 0 WHERE Lien = '{0}';
 -- Met à jour le is_active selon la dernière grille
   MERGE INTO [Autorisation] T
   USING (
         SELECT DISTINCT tp.NoMag,tp.Lien 
		 FROM Transaction_Provider_{0} tp
         JOIN Magasin m ON m.NoMag=tp.NoMag 
		 WHERE  Refuse is null OR Refuse=''
         ) S
      ON T.Lien = S.Lien AND T.NoMag=S.NoMag
   WHEN MATCHED THEN
   UPDATE 
      SET T.is_active = 1;