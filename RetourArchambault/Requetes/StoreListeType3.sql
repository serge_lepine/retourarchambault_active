﻿SELECT (SELECT a.Temporaire FROM Autorisation a 
WHERE a.NoMag=tp.NoMag AND a.Lien=tp.Lien) as 'Temporaire',tp.Category,tp.NoMag,tp.Sku,tp.NoCie,
tp.UPC,tp.Auteur,tp.Titre,tp.Compo,tp.Format,tp.Model_Calcule,tp.EnStock,tp.Qty_Dispo as 'Qty_autorise',
tp.Model_Calcule as 'Garder',Convert(varchar,s.Date_Limite,105) as 'Date_Limite',
tt.Description_{3} as 'Desc'
FROM Transaction_Store_List tp 
JOIN Transaction_Store s ON s.Lien=tp.Lien AND s.NoMag=tp.NoMag
JOIN TransactionType tt ON tt.id= tp.idTransactionType
WHERE tp.Lien ='{2}'
AND tp.NoMag={0}
AND tp.NoCie={1}
ORDER BY tp.NoMag,tp.category,tp.auteur,tp.Titre