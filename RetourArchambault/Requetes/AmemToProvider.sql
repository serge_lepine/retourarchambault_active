﻿INSERT INTO [dbo].[Transaction_Provider]
           ([NoMag]
           ,[Sku]
           ,[NoCie]
           ,[ISBN]
           ,[UPC]
           ,[Auteur]
           ,[Titre]
           ,[Compo]
           ,[Format]
           ,[Etiquette]
           ,[Model_Calcule]
           ,[Cost]
           ,[Qty_a_Retourner]
           ,[ValeurRetour]
           ,[EnStock]
           ,[Category]
           ,[Dept]
           ,[Lien]
           ,[IdUser]
           ,[Date_Creation]
		   ,[idTransactionType]
		   ,[Date_Limite]
		   ,[NoAvis]
		   ,[NoRetour])
          SELECT [NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compos]
      ,[Format]
      ,[Etiquette]
      ,[Model_Calcule]
      ,[COST]
      ,[Qty_a_Retourner]
      ,([COST] * [Qty_a_retourner])
      ,[EnStock]
      ,[Category]
      ,{0}
      ,'{1}'
      ,{2}
      ,GETDATE()
	  ,{5},'{6}','{7}',NoRetour
  FROM [dbo].[Temp_Trans_Provider_{1}]
  WHERE Qty_a_Retourner > 0 AND NoCie = {3}{4}
  UNION
   SELECT [NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compos]
      ,[Format]
      ,[Etiquette]
      ,[Model_Calcule]
      ,[COST]
      ,[Qty_a_Retourner]
      ,([COST] * [Qty_a_retourner])
      ,[EnStock]
      ,[Category]
      ,{0}
      ,'{1}'
      ,{2}
      ,GETDATE()
	  ,{5},'{6}','{7}',NoRetour
  FROM [dbo].[Temp_Trans_Provider_{1}]
  WHERE Qty_a_Retourner > 0 AND NoCie = {3} AND Add_Sku = 1
