﻿SELECT tls.Id
      ,tls.NoMag
      ,tls.Sku
      ,tls.NoCie
      ,tls.ISBN
      ,tls.UPC
      ,tls.Auteur
      ,tls.Titre
      ,tls.Compo
      ,tls.Format
      ,tls.Etiquette
      ,tls.Cost
      ,tls.Qty_a_Retourner
      ,tls.ValeurRetour
      ,tls.Dept
      ,tls.Lien
      ,tls.IdUser
      ,tls.Date_Creation
      ,tls.Model_Calcule
      ,tls.Refuse
      ,tls.Qty_autorise
      ,tls.EnStock
      ,tls.Category
	  ,tt.Description_{2} as 'Desc'
  FROM Transaction_Store_List tls
   JOIN TransactionType tt ON tt.Id=tls.idTransactionType
  WHERE Lien = '{0}'
  AND NoMag = {1}