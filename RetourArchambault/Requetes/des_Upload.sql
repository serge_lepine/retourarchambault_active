﻿SELECT  bal.ISTORE as NoMag,
u.INUMBR as SKU,u.IUPC as UPC,
iv.ASNUM as NoCie,iv.IVNDP# as ISBN,iv.ARTIST as Auteur,
COALESCE((SELECT IDESCR160 FROM mmarclib.INVDSC2  WHERE inumbr=u.inumbr),iv.IDESCR) as TITRE,
iv.COMPSR as Compos,iv.IATRB1 as Format,iv.LABEL as Etiquette,0 as Model_Calcule,
iv.ICUCST as COST,
bal.IBHAND as Qty_a_Retourner,
(bal.IBHAND * iv.ICUCST) as Montant_Retour,
(SELECt dpt.DPTNAM FROM mmarclib.invdpt dpt WHERE dpt.IDEPT=iv.IDEPT AND dpt.ISDEPT=iv.ISDEPT AND dpt.ICLAS=iv.ICLAS AND dpt.ISCLAS=iv.ISCLAS ) as Category,
bal.IBHAND as EnStock,
iv.IDEPT as DEPT,
bal.IBALD2,
2 as idTransactionType,
'{2}' as Lien,
'{3}' as Date_Limite,
'{4}' as NoAvis,
 0 as Add_Sku
FROM 
mmarclib.invupc u 
JOIN mmarclib.invbal bal ON bal.INUMBR=u.INUMBR
JOIN mmarclib.invmst iv ON iv.INUMBR=bal.INUMBR
WHERE u.IUPC IN({0}) AND iv.ASNUM={1} -- AND bal.IBHAND > 0