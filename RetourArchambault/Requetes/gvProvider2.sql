﻿ SELECT
  tlp.Id, 
  tlp.NoCie,
  p.CieName,
  tlp.Dept,
  tlp.Lien,
  CONVERT(varchar,tlp.Date_Creation,105) as 'DateCreation',
  tlp.Qty_demande,
  tlp.Montant_demande,
  CONVERT(varchar,tlp.Date_Limite,105)as 'DateLimite',
  tlp.is_complete,
  tlp.Qty_autorise,
  tlp.Montant_autorise,
  tt.Description_{0} as 'Desc'
  FROM Transaction_Link_Provider tlp, Provider p,TransactionType tt
  WHERE tlp.NoCie=p.NoCie AND tlp.Section < 3
  AND tt.Id = tlp.idTransactionType
  AND tlp.is_complete = 1