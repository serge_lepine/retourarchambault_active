﻿BEGIN TRY

CREATE TABLE [dbo].[Temp_Trans_Provider_{0}](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[NoMag] [int] NULL,
	[SKU] [varchar](50) NULL,
	[NoCie] [int] NULL,
	[ISBN] [varchar](50) NULL,
	[UPC] [varchar](50) NULL,
	[AUTEUR] [varchar](250) NULL,
	[TITRE] [varchar](250) NULL,
	[COMPOS] [varchar](250) NULL,
	[FORMAT] [varchar](50) NULL,
	[Etiquette] [varchar](250) NULL,
	[Model_Calcule] [int] NULL,
	[COST] [money] NULL,
	[Qty_a_Retourner] [int] NULL,
	[Category] [varchar](200) NULL,
	[EnStock] [int] DEFAULT 0,
	[IDEPT] [int],
	[idTransactionType] [int],
	[Lien] [varchar](50),
	[Date_Limite] [DATETIME],
	[Add_Sku] bit,
	[NoRetour] varchar(200))

  END TRY

  BEGIN CATCH
  DROP TABLE [Temp_Trans_Provider_{0}]

CREATE TABLE [dbo].[Temp_Trans_Provider_{0}](
	[Id] [int] IDENTITY(1,1) PRIMARY KEY NOT NULL,
	[NoMag] [int] NULL,
	[SKU] [varchar](50) NULL,
	[NoCie] [int] NULL,
	[ISBN] [varchar](50) NULL,
	[UPC] [varchar](50) NULL,
	[AUTEUR] [varchar](250) NULL,
	[TITRE] [varchar](250) NULL,
	[COMPOS] [varchar](250) NULL,
	[FORMAT] [varchar](50) NULL,
	[Etiquette] [varchar](250) NULL,
	[Model_Calcule] [int] NULL,
	[COST] [money] NULL,
	[Qty_a_Retourner] [int] NULL,
	[Category] [varchar](200) NULL,
	[EnStock] [int] DEFAULT 0,
	[IDEPT] [int],
	[idTransactionType] [int],
	[Lien] [varchar](50),
	[Date_Limite] [DATETIME],
	[Add_Sku] bit,
	[NoRetour] varchar(200))

  END CATCH;