﻿  INSERT INTO Transaction_Store (NoMag,Lien,IdCie,CieName,AuthTemporaire,AuthProvider,Dept,Date_Envoi,Date_Limite,idTransactionType)
SELECT DISTINCT ta.NoMag,ta.Lien,ta.NoCie,p.CieName,a.Temporaire,a.Autorisation,ta.Dept,GETDATE(),'{1}',idTransactionType FROM [Transaction_Provider]  ta
LEFT JOIN [Autorisation] a ON a.NoMag=ta.NoMag AND a.Lien=ta.Lien
LEFT JOIN [Provider] p ON p.NoCie=ta.NoCie
WHERE ta.Lien = '{0}'
AND Refuse is null
AND Qty_Dispo > 0