﻿SELECT  
rt.RTVNUM as NoRetour, 
rt.RTVFLC as NoMag,
rdtl.INUMBR as Sku,
u.IUPC as UPC,
rdtl.RTVVND as NoCie,
rdtl.IVNDP# as ISBN,
iv.ARTIST as Auteur,
COALESCE((SELECT IDESCR160 FROM mmarclib.INVDSC2  WHERE inumbr=rdtl.INUMBR),iv.IDESCR) as Titre,
iv.COMPSR as Compos,
iv.IATRB1 as Format,
iv.LABEL as Etiquette,
0 as Model_Calcule,
iv.ICUCST as COST,
rdtl.RTVQTY as Qty_a_Retourner,
(rdtl.RTVQTY * iv.ICUCST) as Montant_Retour,
(SELECt dpt.DPTNAM FROM mmarclib.invdpt dpt WHERE dpt.IDEPT=rdtl.IDEPT AND dpt.ISDEPT=rdtl.ISDEPT AND dpt.ICLAS=rdtl.ICLAS AND dpt.ISCLAS=rdtl.ISCLAS ) as Category,
rdtl.IDEPT as Dept,
rt.RTVIDT as Date_Creation,
rt.RTVBDT as Date_Modification
  FROM mmarclib.RTVHDR  rt
JOIN mmarclib.RTVDTL rdtl ON rdtl.RTVNUM=rt.RTVNUM
JOIN mmarclib.invmst iv ON iv.INUMBR=rdtl.INUMBR
JOIN mmarclib.invupc u ON u.INUMBR=rdtl.INUMBR AND u.IUPPRM=1
WHERE  rt.RTVPTY in ('B')
AND rt.RTVSTS = 'W'
AND rt.RTVVND ={0}
ORDER BY rt.RTVVND,rt.RTVIDT,rt.RTVBDT