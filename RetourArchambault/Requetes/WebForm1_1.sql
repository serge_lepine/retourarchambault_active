﻿SELECT tp.Id
      ,tp.NoMag
      ,tp.Sku
      ,tp.NoCie
      ,tp.ISBN
      ,tp.UPC
      ,tp.Auteur
      ,tp.Titre
      ,tp.Compo
      ,tp.Format
      ,tp.Etiquette
      ,tp.Cost
      ,tp.Qty_a_Retourner
      ,tp.ValeurRetour
      ,tp.Dept
      ,tp.Lien
      ,tp.IdUser
      ,tp.Date_Creation
      ,tp.Model_Calcule
      ,tp.Refuse
      ,tp.Qty_autorise
	  ,tp.EnStock
	  ,tp.Commentaires
	  ,tp.Qty_Dispo
	  ,tp.Amount_Dispo
	  ,tt.Description_{1} as 'Desc'
  FROM [Transaction_Provider] tp
  JOIN TransactionType tt ON tt.Id = tp.idTransactionType
  WHERE Lien = '{0}' ;