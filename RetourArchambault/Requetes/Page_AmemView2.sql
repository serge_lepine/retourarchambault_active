﻿SELECT a.id,
a.NoMag,
a.SKU,
a.NoCie,
a.ISBN,
a.UPC,
a.AUTEUR,
a.TITRE,
a.COMPOS,
a.FORMAT,
a.Etiquette ,
a.Model_Calcule,
a.COST ,
a.Qty_a_Retourner,
(a.COST * a.Qty_a_Retourner) as 'Valeur_Retour',
a.EnStock,
a.Category,
a.Date_Limite,
tt.Description_{3} as 'Desc'
FROM {0} a
JOIN TransactionType tt ON tt.Id=a.idTransactionType
WHERE NoCie = {1}{2}