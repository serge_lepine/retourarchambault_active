﻿--SELECT TOP 3 Id,CONVERT(varchar,Date_Creation,105) as 'Date_Creation',
--Qty_demande,
--Montant_demande,
--CASE WHEN  idTransactionType = 1 THEN 'R' ELSE 'D' END as 'TransType'
--FROM Transaction_Link_Provider 
--WHERE NoCie = {0} AND idTransactionType = 1
--ORDER BY Date_Creation DESC

SELECT TOP 3 idcie
,CONVERT(varchar,Date_Envoi,105) as 'Date_Creation'
,SUM(Qty_Retourner)as 'Qty_demande'
,SUM(Montant_Retourne)as 'Montant_demande'
,'R' as 'TransType'
  FROM Transaction_Store
  WHERE IdCie={0} AND idTransactionType =1 
  AND Montant_retourne > 0
  GROUP BY IdCie,Date_Envoi
  ORDER BY Date_Envoi DESC