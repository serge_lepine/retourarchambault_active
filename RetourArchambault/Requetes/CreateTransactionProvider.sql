﻿BEGIN TRY

 SELECT [Id] 
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compo]--
      ,[Format]
      ,[Etiquette]
      ,[Cost]
      ,[Qty_a_Retourner]
      ,[ValeurRetour]
      ,[Dept]
      ,[Lien]
      ,[IdUser]
      ,[Date_Creation]
      ,[Model_Calcule]
      ,[Refuse]
      ,[Qty_autorise]
	  ,[Commentaires]
	  ,[is_checked]
	  ,[idTransactionType]
	  ,[Date_Limite]
	  ,[NoAvis]
  INTO [Transaction_Provider_{0}]
  FROM Transaction_Provider
  WHERE Lien='{0}' AND Dept={1} AND NoCie= {2}

  END TRY

  BEGIN CATCH
  DROP TABLE [Transaction_Provider_{0}]

   SELECT [Id] 
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compo]--
      ,[Format]
      ,[Etiquette]
      ,[Cost]
      ,[Qty_a_Retourner]
      ,[ValeurRetour]
      ,[Dept]
      ,[Lien]
      ,[IdUser]
      ,[Date_Creation]
      ,[Model_Calcule]
      ,[Refuse]
      ,[Qty_autorise]
	  ,[Commentaires]
	  ,[is_checked]
	  ,[idTransactionType]
	  ,[Date_Limite]
	  ,[NoAvis]
  INTO [Transaction_Provider_{0}]
  FROM Transaction_Provider
  WHERE Lien='{0}' AND Dept={1} AND NoCie= {2}

  END CATCH;