﻿SELECT ts.Id
      ,ts.CieName
      ,ts.NoMag
      ,COALESCE(CONVERT(varchar,ts.Date_Envoi,105),'NULL') as 'Date_Envoi'
      ,COALESCE(CONVERT (varchar,ts.Date_Cueillette,105),'- -') as 'Date_Cueillette'
      ,ts.Dept
      ,ts.AuthTemporaire
      ,ts.AuthProvider
      ,ts.Auth400
      ,ts.Qty_Retourner
      ,ts.Montant_retourne
      ,ts.Lien
	  ,tt.Description_{1} as 'Desc'
  FROM Transaction_Store ts
   JOIN TransactionType tt ON tt.Id=ts.idTransactionType
  WHERE Lien = '{0}'
  ORDER BY NoMag