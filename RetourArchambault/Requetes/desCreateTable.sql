﻿BEGIN TRY

SELECT [Id]
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compos]
      ,[Format]
      ,[Etiquette]
      ,[Model_Calcule]
      ,[COST]
      ,[Qty_a_Retourner]
      ,[Category]
	  ,[EnStock]
	  ,[idTransactionType] 
	  ,[Lien]
	  ,[Date_Limite]
  INTO [Temp_Trans_Provider_{0}]
  FROM [DesuetList] WHERE Lien='{0}';

END TRY

BEGIN CATCH
  DROP TABLE [Temp_Trans_Provider_{0}];

SELECT [Id]
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compos]
      ,[Format]
      ,[Etiquette]
      ,[Model_Calcule]
      ,[COST]
      ,[Qty_a_Retourner]
      ,[Category]
	  ,[EnStock]
	  ,[idTransactionType] 
	  ,[Lien]
	  ,[Date_Limite]
  INTO [Temp_Trans_Provider_{0}]
  FROM [DesuetList] WHERE Lien='{0}';

END CATCH;