﻿ --Requete pour trouver les transactions à compléter et historique
 SELECT tla.Id,p.CieName,tla.Date_Creation,tla.Lien,tt.Description_{2}
 FROM [Transaction_Link_Provider] tla, [Provider] p,[usr] u,TransactionType tt
  WHERE tla.NoCie=p.NoCie
  AND tt.Id = tla.idTransactionType
  AND u.Id = {0}
  AND tla.Section IN ({1}) 
  AND u.idCie=tla.NoCie