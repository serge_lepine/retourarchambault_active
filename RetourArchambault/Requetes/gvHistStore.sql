﻿--SELECT Id
--      ,NoMag
--      ,Lien
--      ,IdCie
--      ,CieName
--      ,AuthTemporaire
--      ,AuthProvider
--      ,Dept
--      ,is_complete
--      ,CONVERT(varchar,Date_Envoi,105) as 'Date_Envoi'
--      ,CONVERT(varchar,Date_Limite,105) as 'Date_Limite'
--      ,Auth400
--      ,Date_Cueillette
--      ,Qty_Retourner
--      ,Montant_retourne
--	  ,tt.Description_{1}
--  FROM Transaction_Store
--  JOIN TransactionType tt ON tt.Id=Transaction_Store.idTransactionType
--  WHERE NoMag={0} 
--  AND is_complete = 1

SELECT ts.Id
      ,ts.NoMag
      ,ts.Lien
      ,ts.IdCie
      ,ts.CieName
      ,ts.AuthTemporaire
      ,ts.AuthProvider
      ,ts.Dept
      ,ts.is_complete
      ,CONVERT(varchar,ts.Date_Envoi,105) as 'Date_Envoi'
      ,CONVERT(varchar,ts.Date_Limite,105) as 'Date_Limite'
      ,ts.Auth400
      ,ts.Date_Cueillette
      ,ts.Qty_Retourner
      ,ts.Montant_retourne
	  ,tt.Description_{1} as 'Desc'
  FROM Transaction_Store ts
  JOIN TransactionType tt ON tt.Id=ts.idTransactionType
  WHERE ts.NoMag={0}
  AND ts.is_complete = 1