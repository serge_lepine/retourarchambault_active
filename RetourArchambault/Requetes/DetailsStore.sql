﻿  SELECT ts.NoMag,
  CONVERT(varchar,ts.Date_Envoi,105) as 'Date Envoi',
  ts.IdCie,
  ts.CieName,
  '$' + CONVERT(varchar(12),(SELECT SUM(tls.ValeurRetour) FROM Transaction_Store_List tls WHERE tls.Lien=ts.Lien AND tls.NoMag=ts.NoMag),1) as 'Montant',
  CONVERT(varchar,ts.Date_Limite,105) as 'Date Limite',
  ts.is_complete,
  CONVERT(varchar,ts.Date_Cueillette,105) as 'Cueillette',
  ts.Qty_Retourner,
  '$' + CONVERT(varchar(12),ts.Montant_retourne,1) as 'Montant Retour'
  tt.Description_{1}
  FROM Transaction_Store ts
  JOIN TransactionType tt ON tt.Id = ts.idTransactionType
  WHERE ts.Lien='{0}'