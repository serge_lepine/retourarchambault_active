﻿BEGIN TRY

SELECT [Id]
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compos]
      ,[Format]
      ,[Etiquette]
      ,[Model_Calcule]
      ,[COST]
      ,[Qty_a_Retourner]
      ,[Category]
	  ,[EnStock]
	  ,[idTransactionType]
	  ,[Date_Limite] 
	  ,[Add_Sku]
	  ,[NoRetour]
  INTO [Temp_Trans_Provider_{0}]
  FROM [Temp_Trans_Provider_{1}];

DROP TABLE [Temp_Trans_Provider_{1}];

END TRY

BEGIN CATCH
  DROP TABLE [Temp_Trans_Provider_{0}];

SELECT [Id]
      ,[NoMag]
      ,[Sku]
      ,[NoCie]
      ,[ISBN]
      ,[UPC]
      ,[Auteur]
      ,[Titre]
      ,[Compos]
      ,[Format]
      ,[Etiquette]
      ,[Model_Calcule]
      ,[COST]
      ,[Qty_a_Retourner]
      ,[Category]
	  ,[EnStock]
	  ,[idTransactionType]
	  ,[Date_Limite] 
	  ,[Add_Sku] 
	  ,[NoRetour]
  INTO [Temp_Trans_Provider_{0}]
  FROM [Temp_Trans_Provider_{1}];

DROP TABLE [Temp_Trans_Provider_{1}];

END CATCH;